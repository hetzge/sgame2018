#version 330

#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP 
#endif

varying vec2 v_texCoords;
varying vec4 v_color;

uniform sampler2D u_texture;
uniform vec4 u_color;

void main()
{
   vec4 color = texture2D(u_texture, v_texCoords);
   if(v_color.a == 0.0)
   {
    if(color.r == 1.0 && color.g == 0.0 && color.b == 1.0 && color.a > 0.0f)
    {
     gl_FragColor = u_color * vec4(v_color.rgb, 1.0);
    } else {
     gl_FragColor = u_color * color;
    }
   }
   else
   {
    gl_FragColor = u_color * v_color * color;
   }
}