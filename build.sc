import mill._
import scalalib._
import ammonite.ops._
import scalafmt._

import $ivy.`com.lihaoyi::mill-contrib-scalapblib:0.3.6`
import contrib.scalapblib._
import coursier.maven.MavenRepository

trait base extends ScalaPBModule with ScalafmtModule {
  def scalaVersion = "2.12.9"
  def scalaPBVersion = "0.9.0"
  def scalaPBFlatPackage = true
  def ivyDeps = Agg(
      ivy"com.thesamet.scalapb:scalapb-runtime_2.12:0.9.0",
      ivy"com.thesamet.scalapb:scalapb-json4s_2.12:0.9.0",
      ivy"beyondthelines:pbdirect_2.12:0.2.1",
      ivy"com.badlogicgames.gdx:gdx:1.9.8",
      ivy"com.badlogicgames.gdx:gdx-tools:1.9.8",
      ivy"com.badlogicgames.gdx:gdx-backend-lwjgl:1.9.8",
      ivy"com.badlogicgames.gdx:gdx-platform:1.9.8",
      ivy"com.typesafe.play:play-json_2.12:2.6.9",
      ivy"de.ruedigermoeller:fst:2.57",
      ivy"org.lz4:lz4-java:1.4.0",
      ivy"commons-io:commons-io:2.6",
      ivy"org.apache.commons:commons-lang3:3.9"
  )
  def unmanagedClasspath = T {
    Agg.from(ammonite.ops.ls(super.millSourcePath / up / "natives").map(PathRef(_)))
  }

  def repositories = super.repositories ++ Seq(
    MavenRepository("https://dl.bintray.com/beyondthelines/maven/")
  )
}

object game extends base {
  def mainClass = Some("de.hetzge.sgame.Application")
  object test extends Tests{
    def ivyDeps = Agg(
      ivy"org.scalatest:scalatest_2.12:3.0.5",
      ivy"org.pegdown:pegdown:1.6.0"
    )
    def testFrameworks = Seq("org.scalatest.tools.Framework")
    def test(args: String*) = super.test("-h", "testout")
  }
}

/* object server extends base {
  def millSourcePath = ammonite.ops.pwd / "game"
  def mainClass = Some("de.hetzge.sgame.server.ServerApp")
}

object build extends base {
  def millSourcePath = ammonite.ops.pwd / "game" 
  def mainClass = Some("de.hetzge.sgame.build.Build")
}

object network extends base {
  def millSourcePath = ammonite.ops.pwd / "game"
  def mainClass = Some("de.hetzge.sgame.network.NetworkExampleApp")
}
 */