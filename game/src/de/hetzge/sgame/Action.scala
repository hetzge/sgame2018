package de.hetzge.sgame.action

import de.hetzge.sgame.building._
import de.hetzge.sgame.network._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.way._
import de.hetzge.sgame.waypoint._
import de.hetzge.sgame.export._
import de.hetzge.sgame.format

sealed trait Action extends Message {
  def nanoTime: Long
}
sealed trait AreaAction extends Action

object Action {
  final case class Noop(override val nanoTime: Long) extends Action
  final case class BuildWay(override val nanoTime: Long, x: TileInt, y: TileInt, wayTypeString: String, playerIdValue: Byte) extends AreaAction
  final case class BuildWaypoint(override val nanoTime: Long, x: TileInt, y: TileInt, waypointTypeString: String) extends AreaAction
  final case class DestroyWay(override val nanoTime: Long, x: TileInt, y: TileInt) extends AreaAction
  final case class BuildBuilding(override val nanoTime: Long, x: TileInt, y: TileInt, buildingTypeString: String, playerIdValue: Byte) extends AreaAction 
  final case class Goto(override val nanoTime: Long, personId: Int, x: TileInt, y: TileInt) extends AreaAction 
  final case class BuildStartBuilding(override val nanoTime: Long, personId: Int) extends AreaAction
}
