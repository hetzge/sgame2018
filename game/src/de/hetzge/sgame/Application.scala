package de.hetzge.sgame

import java.util.UUID

import de.hetzge.sgame.game._
import de.hetzge.sgame.render._
import de.hetzge.sgame.resources._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.base._
import de.hetzge.sgame.ui._
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.{ApplicationAdapter, Gdx, InputMultiplexer}
import com.badlogic.gdx.graphics.GL20
import de.hetzge.sgame.network.{Client, Network}
import de.hetzge.sgame.server.{Server, StartGameMessage, TriggerFrameMessage}
import de.hetzge.sgame.arealoader.AreaLoader
import de.hetzge.sgame.export._
import java.io.File

object ApplicationConfiguration extends LwjglApplicationConfiguration {
  title = "SGame"
  width = 900
  height = 400
  useGL30 = true
  fullscreen = false
}

object Application extends App {
  println("start")
  new LwjglApplication(GameApplicationAdapter, ApplicationConfiguration)

  val cpuFpsCounter        = new FpsCounter
  var isTestMode: Boolean  = false
  val isDebugMode: Boolean = true
}

final class Application {
  private[this] val mapPath = "assets/map/out.map"
  val resources             = new ApplicationResources

  private var _network: Option[Network] = None

  private[this] var _game: Option[Game] = None
  def isGameRunning: Boolean            = _game.isDefined

  def startNetworkGame(host: String = "127.0.0.1", port: Int = Server.defaultPort): Unit = {
    val network = new Network(Client(UUID.randomUUID().toString), port = (1234 + Math.random() * 1000).toInt)
    network.start
    network.connect(host, port)

    var waitForServer = true
    while (waitForServer) {
      println("wait for server")
      network.handle((client, message) => {
        message match {
          case startGameMessage @ StartGameMessage(gameConfiguration, frameMessages) => {
            waitForServer = false
            lazy val area = AreaLoader.load(resources, Gdx.files.internal(mapPath))
            val game      = Game(EntityKey("GAME"), MultiplayerGameType, Some(network), gameConfiguration, area)

            _game = Some(game)
            Gdx.input.setInputProcessor(game.inputMultiplexer)

            println(s"Replay from ${startGameMessage.fromFrameId} to ${startGameMessage.toFrameId}")
            for (frameTriggerMessage <- frameMessages) {
              game.handleMessage(client, frameTriggerMessage)
            }
          }
          case _ => {
            _game match {
              case Some(game) => {
                game.handleMessage(client, message)
              }
              case None => {
                message match {
                  case TriggerFrameMessage(frameId, actions) => println(s"Ignore trigger frame message: $frameId")
                  case _                                     => throw new IllegalStateException(s"Message ($message) not expected while game start")
                }
              }
            }
          }
        }
      })
      Utils.sleep(100)
    }
    _game match {
      case Some(game) => game.start
      case None       => throw new IllegalStateException("Failed to start game")
    }
  }

  def startGame(gameConfiguration: GameConfiguration): Unit = {
    lazy val area = AreaLoader.load(resources, Gdx.files.internal(mapPath))
    val game      = Export.load("last", resources)
    // val game      = Game(EntityKey("GAME"), SingleplayerGameType, None, gameConfiguration, area)

    _game = Some(game)
    Gdx.input.setInputProcessor(game.inputMultiplexer)
    game.start()
  }

  def create(): Unit = {
    setupCursor
  }

  def render(): Unit = {
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)

    for (game <- _game) {
      game.render()
    }
  }

  def resize(width: Int, height: Int): Unit = {
    println(s"resize $width, $height")

    for (game <- _game) {
      game.resize(width, height)
    }
  }

  def dispose(): Unit = {
    for (game <- _game) {
      game.dispose()
    }
  }

  private def setupCursor: Unit = {
    resources.region.cursor.flip(false, true)
    val pixmap = Utils.pixmap(resources.region.cursor)
    Gdx.graphics.setCursor(Gdx.graphics.newCursor(pixmap, 0, 0))
    pixmap.dispose()
  }
}

object GameApplicationAdapter extends ApplicationAdapter {

  private[this] var application: Application = null
  @inline def resources: ApplicationResources = {
    require(application != null, "Can't access application resources without started application")
    application.resources
  }

  override def create() = {
    application = new Application
    application.create()

    // application.startNetworkGame()
    application.startGame(GameConfiguration(playerCount = 10))
  }

  override def render(): Unit                        = application.render()
  override def resize(width: Int, height: Int): Unit = application.resize(width, height)
  override def dispose(): Unit                       = application.dispose()
}
