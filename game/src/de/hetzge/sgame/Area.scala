package de.hetzge.sgame.area

import java.util
import java.util.concurrent.atomic.AtomicInteger

import de.hetzge.sgame.action._
import de.hetzge.sgame.base._
import de.hetzge.sgame.building._
import de.hetzge.sgame.job._
import de.hetzge.sgame.path._
import de.hetzge.sgame.person._
import de.hetzge.sgame.render._
import de.hetzge.sgame.tile._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.way._
import de.hetzge.sgame.waypoint._
import de.hetzge.sgame.base.helper._
import de.hetzge.sgame.item._
import de.hetzge.sgame.fogofwar._
import de.hetzge.sgame.environment._
import de.hetzge.sgame.stock._
import de.hetzge.sgame.game._
import de.hetzge.sgame.player._
import de.hetzge.sgame.settings._
import de.hetzge.sgame.export._
import de.hetzge.sgame.format

import scala.collection.mutable
import scala.concurrent.duration._
import com.google.protobuf.ByteString

final case class AreaProto(
  key: String,
  position: PositionProto,
  tileMap: ReferenceProto,
  buildings: List[ReferenceProto],
  persons: List[ReferenceProto],
  environment: EnvironmentProto,
  economy: ReferenceProto,
  wayGroupCount: Int,
  waypointGroupCount: Int, 
  ways: List[ReferenceProto],
  ownership: Array[Byte],
  jobs: List[AreaJobProto]
)

final case class AreaJobProto (
  position: PositionProto,
  job: ReferenceProto
)

sealed trait AreaTrigger

trait AreaEntity extends Tiled {
  private[this] var _area: Area = null
  final def assignArea(area: Area): Unit = {
    require(area != null, "'area' is null")
    require(!hasArea || _area == area)
    setArea(area)
  }
  private[this] def setArea(area: Area): Unit = _area = area
  final def hasArea: Boolean                  = _area != null
  final def unsetArea(): Unit                 = _area = null
  final def area: Area = {
    require(hasArea, s"Area of '$this' is not set")
    _area
  }

  def isStatic: Boolean
  def viewRadius: TileInt
  def ownershipRadius: TileInt
  def render(renderContext: GameRenderContext, x: PixelFloat, y: PixelFloat, z: PixelFloat): Unit
  def execute(trigger: AreaTrigger): Unit = {}
}

trait AreaJob[T <: JobEntity[T]] extends Job[T] {
  private[this] var _jobEntity: Option[JobEntity[T]] = None
  final def entity: Option[JobEntity[T]]             = _jobEntity
  final def isOpen: Boolean                          = _jobEntity.isEmpty
  final def isAssigned: Boolean                      = _jobEntity.isDefined
  def area: Area
  def position: Position

  override def onSet(t: T) = {
    require(isOpen)
    _jobEntity = Some(t)
  }

  override def onUnset(t: T) = {
    require(isAssigned)
    _jobEntity = None
  }
}

object EntityRegistration {
  case object None                               extends EntityRegistration
  final case class Primary(entity: AreaEntity)   extends SomeEntityRegistration
  final case class Secondary(entity: AreaEntity) extends SomeEntityRegistration
}
sealed trait EntityRegistration
object SomeEntityRegistration {
  def unapply(registration: SomeEntityRegistration): Option[AreaEntity] = Some(registration.entity)
}
sealed trait SomeEntityRegistration extends EntityRegistration {
  def entity: AreaEntity
}

sealed trait RenderArea { area: Area =>
  def render(renderContext: GameRenderContext, x: PixelFloat, y: PixelFloat, z: PixelFloat, showFogOfWar: Boolean, showDebug: Boolean, showBorders: Boolean, showOwnership: Boolean): Unit = {
    val resources = renderContext.resources

    // tilemap
    tileMap.render(renderContext, x, y)

    val Tiled(tileX, tileY, tileWidth, tileHeight) = renderContext.visibleTileBounds(area, x, y, offset = 6)
    iterate(tileX, tileY, tileWidth, tileHeight) { position =>
      // tile coordinates (in tiles)
      val tx = position.x
      val ty = position.y

      // render coordinates (in pixels)
      val rx = x + tx * TILE_SIZE_FLOAT
      val ry = y + ty * TILE_SIZE_FLOAT

      val isFogOfWar = showFogOfWar && !fogOfWar.isVisible(position)

      if (!isFogOfWar) {

        // ways
        for (way <- way(position)) {
          val rx = x + tx * TILE_SIZE_FLOAT
          val ry = y + ty * TILE_SIZE_FLOAT
          way.render(
            renderContext,
            x = rx,
            y = ry,
            z = z + 1f
          )
        }

        if (showDebug && isJob(position, classOf[CarrierJob])) {
          resources.drawRectangle(renderContext, resources.shape.red, rx, ry, TILE_SIZE_FLOAT, TILE_SIZE_FLOAT, 2f)
        }

        // environment
        environment.render(renderContext, x, y, z, position)

        // entities
        entities(position).foreach {
          case EntityRegistration.Primary(entity) =>
            entity.render(
              renderContext,
              x = rx,
              y = ry,
              z = z + 2f
            )
          case _ => {}
        }

        val ownerPlayerId = owner(position)
        if ((showBorders && isOtherOwnerAround(position, ownerPlayerId)) || showOwnership) {
          ownerPlayerId.transparentRenderable.render(
            renderContext,
            userData = null,
            x = rx,
            y = ry,
            z = z + 5f,
            width = TILE_SIZE_FLOAT,
            height = TILE_SIZE_FLOAT
          )
        }
      }
    }

    if (showFogOfWar) {
      fogOfWar.render(renderContext, x, y)
    }

    fogOfWar.process()
  }
}

sealed trait OwnershipArea { area: Area =>
  private[area] val _ownership = new Array[Byte](size)
  def owner(position: Position): PlayerId = {
    PlayerId.get(_ownership(position))
  }
  def freeOwnership(position: Position, radius: TileInt): Unit = {
    forceOwnership(GaiaPlayer.id, position, radius)
  }
  def forceOwnership(playerId: PlayerId, position: Position, radius: TileInt): Unit = {
    takeOwnership(playerId, position, radius, forced = true)
  }
  def takeOwnership(playerId: PlayerId, position: Position, radius: TileInt, forced: Boolean = false): Unit = {
    traverse(position, radius * radius) { areaPosition =>
      if (forced || _ownership(areaPosition) == GaiaPlayer.id.value) {
        _ownership(areaPosition) = playerId.value
      }
      if (tileMap.isCollision(areaPosition)) DontGoDeeper
      else Continue
    }
  }
  def isOtherOwnerAround(position: Position): Boolean = {
    isOtherOwnerAround(position, owner(position))
  }
  def isOtherOwnerAround(position: Position, playerId: PlayerId): Boolean = {
    Orientation.SIMPLE_AROUND_VALUES.flatMap(position.around(_)).exists(owner(_) != playerId)
  }
}

sealed trait FogOfWarArea { area: Area =>
  val fogOfWar = new FogOfWar(this)

  def resetFogOfWar(): Unit = {
    fogOfWar.reset()

    for (building <- buildings) {
      show(building)
    }
    for (person <- persons) {
      show(person)
    }
  }

  private[area] def show(entity: AreaEntity): Unit = {
    if (entity.isInstanceOf[PlayerEntity] && entity.asInstanceOf[PlayerEntity].isOwnedByCurrentPlayer) {
      fogOfWar.show(entity.centerPosition, entity.viewRadius)
    }
  }

  private[area] def hide(entity: AreaEntity): Unit = {
    if (entity.isInstanceOf[PlayerEntity] && entity.asInstanceOf[PlayerEntity].isOwnedByCurrentPlayer) {
      fogOfWar.hide(entity.centerPosition, entity.viewRadius)
    }
  }
}

sealed trait WayArea { area: Area =>
  private[area] val _wayGroupCount: AtomicInteger = new AtomicInteger(1)
  def nextWayGroup                                = _wayGroupCount.getAndIncrement()

  private[area] val _waypointGroupCount: AtomicInteger = new AtomicInteger(1)
  def nextWaypointGroup                                = _waypointGroupCount.getAndIncrement()

  private[area] val _wayTileMapLayer: TileMapLayer = tileMap.getOrCreateLayer("WAY")
  private def setupWayInTileMap(way: Way): Unit = {
    val aroundWaysOrientationSet = OrientationSet(Orientation.SIMPLE_AROUND_VALUES.filter(way.position.around(_).exists(p => isWay(p) || (isDoor(way.position) && isBuilding(p)))))
    val wayTile = {
      if (way.isBuild) {
        way.wayType.renderableSet.defaultTile(aroundWaysOrientationSet)
      } else {
        way.wayType.renderableSet.disabledTile(aroundWaysOrientationSet)
      }
    }
    _wayTileMapLayer.set(way.position, wayTile)
    tileMap.forceRender()
  }
  private def unsetWayInTileMap(position: Position): Unit = {
    _wayTileMapLayer.unset(position)
    tileMap.forceRender()
  }

  private[area] val _ways: Array[Way]                         = new Array(size)
  def way(position: Position): Option[Way]                    = if (position.valid) Option(_ways(position)) else None
  def way(x: TileInt, y: TileInt): Option[Way]                = way(position(x, y))
  def isWay(position: Position): Boolean                      = position.valid && _ways(position) != null
  def isWayWithGroup(position: Position, group: Int): Boolean = way(position).map(_.getGroup == group).getOrElse(false)
  def isWay(x: TileInt, y: TileInt): Boolean                  = isWay(position(x, y))
  def canBuildWay(position: Position, wayType: WayType): Boolean = {
    def is(indexOption: Option[Position]): Boolean = indexOption match {
      case Some(i) => isWay(i)
      case None    => false
    }

    def isVertical   = Orientation.VERTICAL_AROUND_VALUES.exists(o => is(position.around(o)))
    def isHorizontal = Orientation.HORIZONTAL_AROUND_VALUES.exists(o => is(position.around(o)))
    def isDiagonal   = Orientation.DIAGONAL_AROUND_VALUES.exists(o => is(position.around(o)))

    def isAlreadyBuild: Boolean = way(position).exists(_.wayType == wayType)

    !isAlreadyBuild && !(isDiagonal && isHorizontal && isVertical) && !isBlocking(position)
  }
  def buildWay(startPosition: Position, endPosition: Position, wayType: WayType, playerId: PlayerId): Unit = {
    val horizontal         = (Math.min(startPosition.x.value, endPosition.x.value) to Math.max(startPosition.x.value, endPosition.x.value)).toList
    val vertical           = (Math.min(startPosition.y.value, endPosition.y.value) to Math.max(startPosition.y.value, endPosition.y.value)).toList
    val isHorizontal       = horizontal.size > vertical.size
    val (aValues, bValues) = if (isHorizontal) (horizontal, vertical) else (vertical, horizontal)

    for (v1 <- aValues) {
      val percent = (v1 - aValues.head).toFloat / aValues.size
      val v2      = bValues(Math.floor(percent).toInt)
      buildWay(
        this.position(
          x = if (isHorizontal) v1 else v2,
          y = if (isHorizontal) v2 else v1
        ),
        wayType,
        playerId
      )
    }
  }
  def buildWay(position: Position, wayType: WayType, playerId: PlayerId): Option[Way] = {
    if (canBuildWay(position, wayType)) {
      environment.removeWhileBuild(position)

      val way = new Way(this, position, wayType)
      way.setGroup(wayGroupAround(position).getOrElse(area.nextWayGroup))
      setWay(position, way)
      Some(way)
    } else {
      None
    }
  }
  private def wayGroupAround(position: Position): Option[Int] = {
    Orientation.SIMPLE_AROUND_VALUES
      .flatMap(position.around(_))
      .flatMap(way(_))
      .map(_.getGroup)
      .headOption
  }
  private def setWay(position: Position, way: Way): Unit = {
    for (waypoint <- waypoint(position)) {
      way.setWaypoint(waypoint)
    }

    _ways(position) = way
    setupWay(position)

    val simpleAroundPositions = Orientation.SIMPLE_AROUND_VALUES.flatMap(position.around(_))
    for (simpleAroundPosition <- simpleAroundPositions) {
      setupWay(simpleAroundPosition)
    }

    val waypointsAround = getWaypointsAround(position)
    for (waypointAround <- waypointsAround) {
      updateWaypointConnections(waypointAround)
      updateWaypointJobs(waypointAround.position)
    }

    val hasDifferentWaypointGroup = waypointsAround.map(_.getGroup).distinct.size > 1
    if (hasDifferentWaypointGroup) {
      val group = nextWaypointGroup
      for (waypointAround <- waypointsAround) {
        waypointAround.floodGroup(group)
      }
    }
    floodWayGroup(position, way.getGroup)
  }
  def canDestroyWay(position: Position): Boolean = isWay(position) && !isDoor(position)
  def destroyWay(position: Position): Unit = {
    if (canDestroyWay(position)) {
      removeWay(position)
    }
  }
  private def removeWay(position: Position): Unit = {
    require(isWay(position))
    require(!isDoor(position), "can't remove way at door position")

    val waypointsAround = getWaypointsAround(position)

    if (isWaypoint(position)) {
      removeWaypoint(position)
    }

    val group = _ways(position).getGroup
    _ways(position) = null

    val simpleAroundPositions = Orientation.SIMPLE_AROUND_VALUES.flatMap(position.around(_))
    for (simpleAroundPosition <- simpleAroundPositions) {
      if (isWay(simpleAroundPosition)) {
        setupWay(simpleAroundPosition)
        floodWayGroup(simpleAroundPosition, nextWayGroup)
      }
    }

    for (waypoint <- waypointsAround) {
      updateWaypointConnections(waypoint)
      waypoint.floodGroup(nextWaypointGroup)
    }

    removeWaypointJobsBetween(waypointsAround)

    // WARNING: if way is removed then job is also removed
    removeJobs(position, classOf[CarrierJob])

    unsetWayInTileMap(position)
  }
  def setupWay(position: Position): Unit = {
    if (position.valid && isWay(position)) {
      val way = _ways(position)

      way.setupRenderable(
        OrientationSet(
          List(
            Some(Center),
            position.around(North).filter(isWayConnectable(_)).map(_ => North),
            position.around(South).filter(isWayConnectable(_)).map(_ => South),
            position.around(West).filter(isWayConnectable(_)).map(_ => West),
            position.around(East).filter(isWayConnectable(_)).map(_ => East)
          ).flatten
        )
      )

      if (!isWaypoint(position) && isWaypointNecessary(position) && canBuildWaypoint(position)) {
        buildWaypoint(position)
      }

      setupWayInTileMap(way)
    }
  }

  def floodWayGroup(startPosition: Position, floodGroup: Int): Unit = {
    traverse(startPosition, Integer.MAX_VALUE) { index =>
      if (index == startPosition) {
        way(index) match {
          case Some(way) => {
            way.setGroup(floodGroup)
            Continue
          }
          case None => Continue
        }
      } else {
        way(index) match {
          case Some(way) => {
            if (way.getGroup < floodGroup) {
              way.setGroup(floodGroup)
              Continue
            } else if (way.getGroup > floodGroup) {
              floodWayGroup(index, way.getGroup)
              Stop
            } else {
              DontGoDeeper
            }
          }
          case None => DontGoDeeper
        }
      }
    }
  }

  def pathOnWay(startX: TileInt, startY: TileInt, targetX: TileInt, targetY: TileInt): Option[Path] = pathOnWay(position(startX, startY), position(targetX, targetY))
  def pathOnWay(startPosition: Position, targetPosition: Position): Option[Path] = {
    require(isWay(targetPosition), s"to (${targetPosition.toDebugString}) is not a way")
    require(isSameWayGroup(startPosition, targetPosition), s"from '${startPosition.toDebugString}' is not same way group as '${targetPosition.toDebugString}'")

    if (startPosition == targetPosition) {
      Some(Path())
    } else {
      pathOnWay(startPosition, (p: Position) => p == targetPosition)
    }
  }
  def pathOnWay(startPosition: Position, predicate: Position => Boolean): Option[Path] = {
    require(isWay(startPosition), s"from (${startPosition.toDebugString}) is not a way")

    def next(p: Position) = Orientation.SIMPLE_AROUND_VALUES.flatMap(p.around(_)).filter(isWay(_))
    PathFinder
      .search(
        start = startPosition,
        next = next,
        predicate = predicate
      )
      .map(Path(_))
  }
  private[area] def isSameWayGroup(positionA: Position, positionB: Position): Boolean = {
    (for {
      wayA      <- way(positionA)
      wayB      <- way(positionB)
      waygroupA = wayA.getGroup
      waygroupB = wayB.getGroup
    } yield waygroupA == waygroupB).getOrElse(false);
  }

  // ############ WAYPOINT ################

  def waypoint(position: Position): Option[Waypoint]               = way(position).flatMap(_.getWaypoint)
  private[area] def isWayConnectable(index: Position): Boolean     = isWay(index) || isBuilding(index)
  def isWaypoint(position: Position): Boolean                      = area.isWay(position) && _ways(position).hasWaypoint
  def isWaypointWithGroup(position: Position, group: Int): Boolean = waypoint(position).map(_.getGroup == group).getOrElse(false)
  def isSpaceForWaypoint(position: Position): Boolean              = !Orientation.ADVANCED_AROUND_VALUES.exists(orientation => position.around(orientation).exists(isWaypoint(_)))
  def isWaypointNecessary(position: Position): Boolean = {
    val x     = position.x
    val y     = position.y
    val west  = if (x - 1 >= 0) isWay(x - 1, y) else false
    val east  = if (x + 1 < width) isWay(x + 1, y) else false
    val north = if (y - 1 >= 0) isWay(x, y - 1) else false
    val south = if (y + 1 < height) isWay(x, y + 1) else false

    isWay(position) && (west || east) && (north || south)
  }
  def canBuildWaypoint(position: Position): Boolean = isWay(position) && !isWaypoint(position) && isSpaceForWaypoint(position)
  def buildWaypoint(position: Position, waypointType: WaypointType = DefaultWaypointType): Option[Waypoint] = {
    if (canBuildWaypoint(position)) {
      val waypoint = Waypoint(waypointType, position)
      waypoint.assignArea(area)
      setWaypoint(position, waypoint)
      Some(waypoint)
    } else {
      None
    }
  }
  def destroyWaypoint(position: Position): Unit = {
    if (isWaypoint(position)) {
      removeWaypoint(position)
    }
  }
  private def setWaypoint(position: Position, waypoint: Waypoint): Unit = {
    require(canBuildWaypoint(position), s"Try to set waypoint where not possible at ${position.x}, ${position.y}")

    val way = _ways(position)
    updateWaypointConnections(waypoint)
    way.setWaypoint(waypoint)
    waypoint.floodGroup(waypoint.groupAround.getOrElse(nextWaypointGroup))
    updateWaypointJobs(waypoint.position)
  }
  private[area] def updateWaypointConnections(waypoint: Waypoint): Unit = {
    val waypointsAround = getWaypointsAround(waypoint.position)
    for (waypointAround <- waypointsAround) {
      waypointAround.removeConnections(waypointsAround)
    }
    for (waypointAround <- waypointsAround) {
      waypointAround.addConnection(waypoint)
    }
    waypoint.setConnections(waypointsAround)
    waypoint.updateItemTargets()
  }
  private[area] def updateWaypointJobs(position: Position): Unit = {
    val waypointsAround = getWaypointsAround(position)
    updateWaypointJobs(position, waypointsAround)
  }
  private[area] def updateWaypointJobs(position: Position, waypointsAround: Seq[Waypoint]): Unit = {
    waypoint(position) match {
      case Some(currentWaypoint) => {
        // unset existing jobs between waypoints around
        removeWaypointJobsBetween(waypointsAround)
        // set new jobs (if necessary) between waypoints around and current waypoint
        for (wp2 <- waypointsAround) {
          if (currentWaypoint.position.isOnSameLine(wp2.position)) {
            val jobPosition = currentWaypoint.position.between(wp2.position)
            if (!isJob(jobPosition, classOf[CarrierJob])) {
              addJob(new CarrierJob(currentWaypoint, wp2))
            }
          }
        }
      }
      case None => {
        // remove job between current index and waypoints around
        for (wp2 <- waypointsAround) {
          if (position.isOnSameLine(wp2.position)) {
            val jobPosition = position.between(wp2.position)
            removeJobs(jobPosition, classOf[CarrierJob])
          }
        }
        // set new job (if necessary) between waypoints around
        for (wp1 <- waypointsAround; wp2 <- waypointsAround) {
          if (wp1.position.isOnSameLine(wp2.position)) {
            val jobPosition = wp1.position.between(wp2.position)
            if (!isJob(jobPosition, classOf[CarrierJob])) {
              addJob(new CarrierJob(wp1, wp2))
            }
          }
        }
      }
    }
  }
  private[area] def removeWaypointJobsBetween(waypointsAround: Seq[Waypoint]): Unit = {
    for (wp1 <- waypointsAround; wp2 <- waypointsAround) {
      if (wp1.position.isOnSameLine(wp2.position)) {
        val jobPosition = wp1.position.between(wp2.position)
        removeJobs(jobPosition, classOf[CarrierJob])
      }
    }
  }
  private[area] def getWaypointsAround(position: Position): Seq[Waypoint] = {
    val waypointsAround: mutable.Buffer[Waypoint] = new mutable.ArrayBuffer(4)
    traverse(position, Integer.MAX_VALUE) { p =>
      if (p == position) {
        Continue
      } else {
        val currentWay      = _ways(p)
        val currentWaypoint = if (currentWay != null) currentWay.getWaypoint.orNull else null
        val isAtSameLine    = p.x == position.x || p.y == position.y

        if (isAtSameLine && currentWaypoint != null) {
          waypointsAround.append(currentWaypoint)
        }

        if (isAtSameLine && currentWay != null && currentWaypoint == null) Continue
        else DontGoDeeper
      }
    }
    waypointsAround
  }
  private def removeWaypoint(position: Position): Unit = {
    require(isWay(position), s"Try to remove waypoint on non way at ${position.x}, ${position.y}")

    _ways(position).removeWaypoint()
    updateWaypointJobs(position)
  }
}

sealed trait JobArea { area: Area =>

  private[area] val _jobs = mutable.LinkedHashMap[Position, Array[AreaJob[_]]]()

  def addJob(job: AreaJob[_]): Unit = _jobs.get(job.position) match {
    case Some(jobs) => _jobs.put(job.position, jobs :+ job)
    case None       => _jobs.put(job.position, Array(job))
  }
  def removeJobs[T <: AreaJob[_]](position: Position, jobClass: Class[T]): Unit = {
    _jobs.get(position) match {
      case Some(jobs) =>
        for (job <- jobs) {
          if (job.getClass == jobClass) {
            removeJob(job)
          }
        }
      case None => {}
    }
  }
  def removeJob(job: AreaJob[_]): Unit = _jobs.get(job.position) match {
    case Some(jobs) => {
      println("remove job " + job)

      import collection.JavaConverters._
      val arrayList = new util.ArrayList[AreaJob[_]]()
      arrayList.addAll(jobs.toList.asJava)
      arrayList.remove(job)

      val newJobs: Array[AreaJob[_]] = arrayList.asScala.toArray
      if (!newJobs.isEmpty) {
        _jobs.put(job.position, newJobs)
      } else {
        _jobs.remove(job.position)
      }

      for (entity <- job.entity) {
        entity.resetAndExecuteJob(Trigger.Default)
      }
    }
    case None => throw new IllegalStateException("Can't remove job assignment that not exists.")
  }
  def isJob[T <: AreaJob[_]](position: Position, jobClass: Class[T]): Boolean = {
    _jobs.get(position).exists(_.exists(j => j.getClass == jobClass))
  }
  def isJob[T <: AreaJob[_]](position: Position, job: T): Boolean = {
    _jobs.get(position).exists(_.exists(j => j == job))
  }
  def findJobs(predicate: AreaJob[_] => Boolean): Seq[AreaJob[_]] = {
    val result = new mutable.ListBuffer[AreaJob[_]]()

    for (assignments <- _jobs.values) {
      for (assignment <- assignments) {
        if (predicate(assignment)) {
          result.append(assignment)
        }
      }
    }

    result
  }
}

sealed trait BuildingArea { area: Area =>
  private[this] val _buildingsById                       = new mutable.LinkedHashMap[Int, Building]()
  def buildings: Iterable[Building]                      = _buildingsById.values
  def building(x: TileInt, y: TileInt): Option[Building] = building(position(x, y));
  def building(position: Position): Option[Building] =
    entities(position).flatMap {
      case SomeEntityRegistration(building: Building) => Some(building)
      case _                                          => None
    }.headOption
  def isBuilding(x: TileInt, y: TileInt): Boolean = isBuilding(position(x, y))
  def isBuilding(position: Position): Boolean = entities(position).exists {
    case SomeEntityRegistration(building: Building) => true
    case _                                          => false
  }
  def canBuildBuilding(position: Position, buildingType: BuildingType, playerId: PlayerId): Boolean = {
    (for {
      doorPosition <- position.relative(buildingType.doorX, buildingType.doorY)
    } yield {
      lazy val areaPositions = this.areaPositions(position, buildingType.width, buildingType.height)
      def isInArea           = isValid(doorPosition) && area.areaPositions(position, buildingType.width, buildingType.height).size == buildingType.width * buildingType.height
      def isOwnArea          = areaPositions.forall(areaPosition => owner(areaPosition) == playerId || (buildingType.canBuildInGaia && owner(areaPosition) == GaiaPlayer.id))
      def isSpaceForDoor     = (!isBlocking(doorPosition) && isEmpty(doorPosition)) || isWay(doorPosition)
      def isSpaceForWaypoint = this.isSpaceForWaypoint(doorPosition)
      def isSpaceForBuilding = areaPositions.forall(p => !isBlocking(p) && isEmpty(p))

      isInArea && isSpaceForBuilding && isOwnArea && (!buildingType.hasDoor || (isSpaceForDoor && isSpaceForWaypoint))
    }).getOrElse(false)
  }
  def buildBuilding(position: Position, buildingType: BuildingType, playerId: PlayerId): Option[Building] = {
    if (canBuildBuilding(position, buildingType, playerId)) {
      // remove environment
      iterate(position.x, position.y, buildingType.width, buildingType.height) { position =>
        environment.removeWhileBuild(position)
      }

      val building = Building(area, position, buildingType)

      takeOwnership(playerId, building.centerPosition, buildingType.ownershipRadius)
      registerBuilding(building)

      // door
      if (building.buildingType.hasDoor) {
        val doorX        = position.x + buildingType.doorX
        val doorY        = position.y + buildingType.doorY
        val doorPosition = this.position(doorX, doorY)
        if (!isWay(doorPosition)) {
          buildWay(doorPosition, DefaultWayType, playerId)
        }
        assert(isWay(doorPosition))

        // create waypoint if not already available
        if (!isWaypoint(doorPosition)) {
          buildWaypoint(doorPosition)
        }
        assert(isWaypoint(doorPosition))

        // connect waypoint stock to building
        waypoint(doorPosition).get.setBuilding(building)
      }

      // connect around
      if (building.buildingType.isInstanceOf[BuildingType.Stock]) {
        val selectedBuildingOption = for {
          game             <- GameContext.optionalGet
          currentPlayer    = game.localSettings.player
          selectedBuilding <- game.localSettings.selectedBuilding
          if (selectedBuilding.playerId == currentPlayer.id)
        } yield selectedBuilding

        if (selectedBuildingOption.isEmpty) {
          throw new IllegalStateException("Can't build stock building without selected building.")
        }

        for {
          selectedBuilding <- selectedBuildingOption
          masterStock <- selectedBuilding.stock match {
                          case m: MasterStock => Some(m)
                          case _              => None
                        }
          subStock <- building.stock match {
                       case s: SubStock => Some(s)
                       case _           => None
                     }
        } subStock.setMaster(masterStock)
      } else {
        def stocksAround = building.buildingsAround().map(_.stock)
        building.stock match {
          case masterStock: MasterStock => stocksAround.collect { case subStock: SubStock if !subStock.hasMaster => subStock }.foreach(subStock => subStock.setMaster(masterStock))
          case subStock: SubStock       => stocksAround.collect { case masterStock: MasterStock => masterStock }.headOption.foreach(masterStock => subStock.setMaster(masterStock))
          case _                        => { /* ignore */ }
        }
      }

      building.initialize()
      building.startKi()
      Some(building)
    } else {
      None
    }
  }
  def destroyBuilding(building: Building): Unit = {
    building.stock.onDestroy(area.economy)
    unregisterBuilding(building)
  }
  def upgradeBuilding(building: Building, newBuildingType: BuildingType): Unit = {
    unregisterBuilding(building)
    buildBuilding(building.position, newBuildingType, building.playerId)
  }
  private[area] def registerBuilding(building: Building): Unit = {
    require(!_buildingsById.contains(building.id), "building already registered")

    _buildingsById.put(building.id, building)
    registerEntity(building)
  }
  private[area] def unregisterBuilding(building: Building): Unit = {
    _buildingsById.remove(building.id)
    unregisterEntity(building)
  }

  def searchBuilding(predicate: Building => Boolean): Iterable[Building] = {
    buildings.filter(predicate)
  }

  def isAroundBuilding(position: Position, building: Building): Boolean = {
    Orientation.ADVANCED_AROUND_VALUES.toStream.flatMap(position.around(_)).flatMap(area.building(_)).exists(_ == building)
  }
}

sealed trait PersonArea { area: Area =>

  private[this] val _personsById      = new mutable.LinkedHashMap[Int, Person]()
  def persons: Iterable[Person]       = _personsById.values
  def person(id: Int): Option[Person] = _personsById.get(id)
  def persons(position: Position): Seq[Person] = entities(position).collect {
    case SomeEntityRegistration(entity) if entity.isInstanceOf[Person] => entity.asInstanceOf[Person]
  }
  def buildPerson(position: Position, personType: PersonType, playerId: PlayerId): Option[Person] = {
    // TODO check if can create
    val person = Person(position, personType, playerId)
    person.assignArea(area)
    registerPerson(person)
    person.startKi()
    Some(person)
  }
  def registerPerson(person: Person): Unit = {
    require(!_personsById.contains(person.id), s"'$person' already registered'")

    _personsById.put(person.id, person)
    registerEntity(person)
  }
  def unregisterPerson(person: Person): Unit = {
    _personsById.remove(person.id)
    unregisterEntity(person)
  }
  def swapPersons(person: Person, otherPerson: Person): Unit = {
    val personPosition: Position      = person.position
    val otherPersonPosition: Position = otherPerson.position

    unregisterEntity(person)
    unregisterEntity(otherPerson)

    person.setPosition(
      x = otherPersonPosition.x,
      y = otherPersonPosition.y
    )
    otherPerson.setPosition(
      x = personPosition.x,
      y = personPosition.y
    )

    registerEntity(person)
    registerEntity(otherPerson)
  }
  def reregisterPerson(person: Person)(callback: => Unit): Unit = {
    unregisterPerson(person)
    callback
    registerPerson(person)
  }
}

object EntityArea {
  val EMPTY_REGISTRATIONS_ARRAY: Array[EntityRegistration] = Array()
}
sealed trait EntityArea { area: Area =>

  private[this] val _entityRegistrations: Array[Array[EntityRegistration]] = new Array(size)

  private[area] def entities(x: TileInt, y: TileInt): Array[EntityRegistration] = entities(position(x, y))
  private[area] def entities(position: Position): Array[EntityRegistration] = {
    val registrations = _entityRegistrations(position)
    if (registrations != null) registrations else EntityArea.EMPTY_REGISTRATIONS_ARRAY
  }

  private[area] def registerEntity(entity: AreaEntity): Unit = {

    entity.assignArea(area)

    show(entity)

    iterate(entity.position.x, entity.position.y, entity.width, entity.height) { position =>
      val registration: EntityRegistration = if (position == entity.position) new EntityRegistration.Primary(entity) else new EntityRegistration.Secondary(entity)
      if (_entityRegistrations(position) == null) {
        _entityRegistrations(position) = Array(registration)
      } else {
        _entityRegistrations(position) = _entityRegistrations(position) :+ registration
      }
    }
  }
  private[area] def unregisterEntity(entity: AreaEntity): Unit = {

    hide(entity)

    iterate(entity.position.x, entity.position.y, entity.width, entity.height) { position =>
      val array = _entityRegistrations(position)
      if (array != null) {
        _entityRegistrations(position) = array.filterNot {
          case SomeEntityRegistration(e) if e == entity => true
          case _                                        => false
        }
      }
    }
  }

  def isDoor(position: Position): Boolean = {
    Orientation.SIMPLE_AROUND_VALUES.exists { orientation =>
      position.around(orientation) match {
        case Some(aroundIndex) =>
          entities(aroundIndex).exists {
            case SomeEntityRegistration(entity: Building) if entity.doorPosition == position => true
            case _                                                                           => false
          }
        case _ => false
      }
    }
  }

  def isEmpty(position: Position): Boolean = !isStaticEntity(position) && !isDynamicEntity(position) && !isWay(position) && !environment.isBlocking(position)
  def isStaticEntity(position: Position): Boolean = entities(position).exists {
    case null                                  => false
    case EntityRegistration.None               => false
    case SomeEntityRegistration(e: AreaEntity) => e.isStatic
    case value                                 => throw new UnsupportedOperationException(s"for $value")
  }
  def isDynamicEntity(position: Position): Boolean = entities(position).exists {
    case null                    => false
    case EntityRegistration.None => false
    case _                       => !isStaticEntity(position)
  }
  def isBlocking(position: Position): Boolean = isStaticEntity(position) || environment.isBlocking(position) || tileMap.isCollision(position)
}

sealed trait ExportArea extends ExportObject[format.Area] with ExportIndexedObject { area: Area =>

  override def export()(implicit index: ExportedIndex) = format.Area(
    position = Some(
      format.Position(position.v)
    ),
    tileMap = Some(index(area.tileMap)),
    buildings = buildings.map(index(_)).toSeq,
    persons = persons.map(index(_)).toSeq,
    environment = Some(environment.export()),
    economy = Some(index(area.economy)),
    wayGroupCount = _wayGroupCount.get(),
    waypointGroupCount = _waypointGroupCount.get(),
    ways = _ways.filter(_ != null).map(index(_)),
    ownership = ByteString.copyFrom(_ownership),
    jobs = _jobs.flatMap(entry => entry._2.map(job => format.AreaJob(Some(entry._1.export), Some(index(job))))).toSeq
  )

  override def initLoad(f: format.Area)(implicit index: ImportedIndex) = {
    // Whoop whoop
    for (entry <- f.jobs.groupBy(_.position.get).map(entry => (Position(entry._1), entry._2.map(_.job.get).map(index[AreaJob[_]](_))))) {
      _jobs.put(entry._1, entry._2.toArray)
    }

    f.ownership.toByteArray().copyToArray(_ownership)
    _wayGroupCount.set(f.wayGroupCount)
    _waypointGroupCount.set(f.waypointGroupCount)
    new Array[Way](_ways.size).copyToArray(_ways)
    for (way <- f.ways.map(index[Way](_))) {
      _ways(way.position.index) = way
    }
    for (person <- f.persons.map(index[Person](_))) {
      registerPerson(person)
    }
    for (building <- f.buildings.map(index[Building](_))) {
      registerBuilding(building)
    }

    resetFogOfWar()
  }

  override def index() = ExportIndex(
    objects = (persons ++ buildings ++ Set(tileMap) ++ Set(economy) ++ _ways.filter(_ != null)).toSet ++ _jobs.values.flatten
  )
}

object ImportArea {

  def load(f: format.Area)(implicit index: ImportedIndex): Area = {
    new Area(
      position = Position(f.position.get.value),
      tileMap = index[TileMap](f.tileMap.get),
      economy = index[ItemEconomy](f.economy.get),
      environment = ImportEnvironment.load(f.environment.get)
    )
  }
}

object Area {
  def createEmpty(width: TileInt, height: TileInt): Area = {
    apply(
      position = Position(),
      tileMap = new TileMap(width, height, new SimpleTileSet(Seq()))
    )
  }
  def apply(
      position: Position,
      tileMap: TileMap
  ): Area = {
    new Area(
      position = position,
      tileMap = tileMap,
      economy = new ItemEconomy(),
      environment = new Environment(tileMap.width, tileMap.height)
    )
  }
}
final class Area(
    override val position: Position,
    val tileMap: TileMap,
    val economy: ItemEconomy,
    val environment: Environment
) extends {
  override val width: TileInt  = tileMap.width
  override val height: TileInt = tileMap.height
} with Tiled with WayArea with EntityArea with BuildingArea with PersonArea with JobArea with RenderArea with FogOfWarArea with OwnershipArea with ExportArea {

  def update(): Unit = {
    if (GameContext.frames.isEveryDurationFrame(3.seconds)) {
      environment.growEnvironment()
    }

    if (GameContext.frames.isEveryDurationFrame(1.second)) {
      economy.update()
    }
  }

  def apply(areaAction: AreaAction): Unit = {
    areaAction match {
      case BuildWayAction(x, y, wayType, playerIdValue)           => buildWay(position(x, y), wayType, PlayerId.get(playerIdValue))
      case DestroyWayAction(x, y)                                 => destroyWay(position(x, y))
      case BuildWaypointAction(x, y, waypointType)                => buildWaypoint(position(x, y), waypointType)
      case BuildBuildingAction(x, y, buildingType, playerIdValue) => buildBuilding(position(x, y), buildingType, PlayerId.get(playerIdValue))
      case GotoAction(personId, x, y) => {
        for (person <- person(personId)) {
          person.goto(position(x, y))
        }
      }
      case BuildStartBuilding(personId) => {
        for (person <- person(personId)) {
          buildStartBuilding(person)
        }
      }
    }
  }

  // TODO write TEST (correct player id of initialized persons in building)
  private def buildStartBuilding(person: Person): Unit = {
    require(person.personType == PersonType.Start)

    val playerId = person.playerId
    val position = person.position

    // unregister before checks to exclude selfcollision in checks
    unregisterPerson(person)

    val aroundPositions = Orientation.ADVANCED_AROUND_VALUES.flatMap(position.around(_))
    def canBuild        = canBuildBuilding(position, BuildingType.Start, playerId)
    def canBuildStocks  = aroundPositions.forall(canBuildBuilding(_, BuildingType.Stock(DummyItemType), GaiaPlayer.id))
    if (canBuild && canBuildStocks) {
      GameContext.ifSet(game => game.localSettings.deselectAll())
      val startBuilding = buildBuilding(position, BuildingType.Start, playerId).getOrElse(throw new IllegalStateException("failed to build start building"))
      GameContext.localSettings.setSelection(BuildingSelection(startBuilding))
      for (aroundPosition <- aroundPositions) {
        for (stockBuilding <- buildBuilding(aroundPosition, BuildingType.Stock(WoodItemType), playerId)) {
          for (noItem <- stockBuilding.stock.noItems(WoodItemType)) {
            noItem.supply(economy, noItem.itemType)
          }
        }
      }

      // TODO move to more general place
      if (GameContext.frames.isFuture(person.targetFrameId)) {
        GameContext.frames.get(person.targetFrameId).remove(person)
        println("removed callback")
      }
    } else {
      println("failed to build start building, reregister start person")
      registerPerson(person)
    }
  }

  def trigger(position: Position, radius: TileInt, trigger: AreaTrigger): Unit = {
    for (entity <- entitiesInRadius(position, radius)) {
      entity.execute(trigger)
    }
  }

  private def entitiesInRadius(position: Position, radius: TileInt): Iterable[AreaEntity] = {
    val entities = new mutable.LinkedHashSet[AreaEntity]()

    iterate(
      position,
      radius
    ) { position =>
      for (building <- building(position)) {
        entities.add(building)
      }
      for (way <- way(position)) {
        for (waypoint <- way.getWaypoint) {
          entities.add(waypoint)
        }
      }
      for (person <- persons(position)) {
        entities.add(person)
      }
    }

    entities
  }

  def pathTo(startPosition: Position, targetPosition: Position, useWay: Boolean = false): Option[Path] = {
    if (startPosition == targetPosition) {
      Some(Path())
    } else {
      val isStartWay  = isWay(startPosition)
      val isTargetWay = isWay(targetPosition)

      if (useWay && isSameWayGroup(startPosition, targetPosition)) {
        // complete path on way
        pathOnWay(startPosition, targetPosition)
      } else if (useWay && !isStartWay && isTargetWay) {
        // partial path on way
        val pathToWayOption = PathFinder
          .search(
            start = startPosition,
            next = (p: Position) => p.simpleAround.filter(!isBlocking(_)),
            predicate = (p: Position) => isSameWayGroup(p, targetPosition)
          )
          .map(Path(_))

        for {
          pathToWay    <- pathToWayOption
          lastPosition <- pathToWay.last
        } yield {
          if (lastPosition == targetPosition) {
            pathToWay
          } else {
            pathToWay ++ pathOnWay(lastPosition, targetPosition).get
          }
        }
      } else {
        // path without way
        PathFinder
          .search(
            start = startPosition,
            next = (p: Position) => p.simpleAround.filter(!isBlocking(_)),
            predicate = (p: Position) => p == targetPosition
          )
          .map(Path(_))
      }
    }
  }

  def speedFactor(position: Position): Float = {
    tileMap.speedFactor(position)
  }
}
