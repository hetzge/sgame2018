package de.hetzge.sgame.arealoader

import de.hetzge.sgame.unit._
import de.hetzge.sgame.area._
import de.hetzge.sgame.tile._
import de.hetzge.sgame.tilemaploader._
import de.hetzge.sgame.person._
import de.hetzge.sgame.resources._
import de.hetzge.sgame.player._
import de.hetzge.sgame.environment._
import com.badlogic.gdx.files.FileHandle
import play.api.libs.json._
import java.{util => ju}

object AreaLoader {
  def load(resources: ApplicationResources, handle: FileHandle): Area = {
    val tileMap: TileMap = TileMapLoader.load(resources, handle)
    val area             = Area(Position(), tileMap)
    loadEnvironment(handle, area)
    loadEntities(handle, area)
    area
  }

  private def loadEntities(handle: FileHandle, area: Area): Unit = {
    val in = handle.read()
    try {
      val jsValue: JsValue = Json.parse(in)

      (jsValue \ "persons") match {
        case JsDefined(JsArray(personJsValues)) => {
          for (personJsValue <- personJsValues) {
            val personType = (personJsValue \ "type").get.as[String] match {
              case "Start" => PersonType.Start
            }
            val position            = area.position((personJsValue \ "x").get.as[Int], (personJsValue \ "y").get.as[Int])
            val playerId            = PlayerId.get((personJsValue \ "playerId").toOption.map(_.as[Byte]).getOrElse(0))
            val buildedPersonOption = area.buildPerson(position, personType, playerId)

            assert(buildedPersonOption.isDefined)
          }
        }
        case JsUndefined() => // ignore
      }
    } finally {
      in.close()
    }
  }

  private def loadEnvironment(handle: FileHandle, area: Area): Unit = {
    val in = handle.read()
    try {
      val jsValue: JsValue = Json.parse(in)
      val width            = (jsValue \ "width").get.as[Int]
      val height           = (jsValue \ "height").get.as[Int]

      (jsValue \ "layers").get match {
        case JsArray(layerJsValues) => {
          for (layerJsValue <- layerJsValues) {
            val name = (layerJsValue \ "name").get.as[String]

            (name match {
              case "FOREST" => Some(EnvironmentType.Forest)
              case "STONE"  => Some(EnvironmentType.Stone)
              case _        => None
            }) match {
              case Some(environmentType) => {
                val data   = (layerJsValue \ "data").get.as[String]
                val bitSet = ju.BitSet.valueOf(ju.Base64.getDecoder().decode(data))

                for (x <- 0 until width; y <- 0 until height) {
                  val position = Position(x, y, width, height)
                  if (bitSet.get(position) && !area.tileMap.isCollision(position)) {
                    area.environment.set(position, environmentType, count = 9, level = 1f)
                  }
                }
              }
              case None => {
                println(s"ignore '$name' for environment")
              }
            }
          }
        }
        case _ => throw new IllegalStateException
      }
    } finally {
      in.close()
    }
  }
}
