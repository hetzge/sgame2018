package de.hetzge.sgame.base

import de.hetzge.sgame.unit._
import de.hetzge.sgame.base.helper._
import de.hetzge.sgame.render._
import de.hetzge.sgame.game.Game
import de.hetzge.sgame.game.GameContext
import de.hetzge.sgame.item.Item
import de.hetzge.sgame.stock.Stock
import de.hetzge.sgame.building.Building
import de.hetzge.sgame.person.Person
import de.hetzge.sgame.format

import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Pixmap.Format
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.utils.TimeUtils
import com.badlogic.gdx.math.MathUtils

import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.collection.immutable.HashMap
import scala.collection.mutable.{ArrayBuffer, HashSet, ListBuffer}
import de.hetzge.sgame.format.Orientation.NORTH_EAST

object Utils {
  def hack[A, B](a: A): B = a.asInstanceOf[B]
  def read(handle: FileHandle): String = {
    val source = scala.io.Source.fromFile(handle.file())
    try source.mkString
    finally source.close()
  }
  def sleep(ms: Long) {
    try {
      Thread.sleep(ms)
    } catch {
      case e: InterruptedException => throw new InterruptedException
    }
  }
  def pixmap(textureRegion: TextureRegion): Pixmap = {
    val texture = textureRegion.getTexture
    if (!texture.getTextureData().isPrepared()) {
      texture.getTextureData().prepare()
    }

    val texturePixmap = texture.getTextureData().consumePixmap()
    val pixmap        = new Pixmap(textureRegion.getRegionWidth, textureRegion.getRegionHeight, Format.RGBA8888)
    for (x <- 0 until textureRegion.getRegionWidth; y <- 0 until textureRegion.getRegionHeight) {
      val pixel = texturePixmap.getPixel(textureRegion.getRegionX + x, textureRegion.getRegionY - textureRegion.getRegionHeight + y)
      pixmap.drawPixel(x, y, pixel)
    }
    texturePixmap.dispose()
    pixmap
  }
}

object OrientationSet {
  private val MAX                                                = OrientationSet(Orientation.ALL_VALUES).value
  def apply(): OrientationSet                                    = OrientationSet(0)
  def apply(orientations: Orientation*): OrientationSet          = apply(orientations)
  def apply(orientations: Iterable[Orientation]): OrientationSet = OrientationSet(orientations.foldLeft(0)(_ | _.binary))
  val ALL                                                        = OrientationSet(MAX)
}
final case class OrientationSet(value: Int) extends AnyVal {
  def contains(orientation: Orientation): Boolean      = (value & orientation.binary) == orientation.binary
  def containsAll(orientations: Orientation*): Boolean = orientations.forall(contains(_))
  def values: List[Orientation]                        = Orientation.ALL_VALUES.filter(contains(_))
  def +(orientation: Orientation)                      = OrientationSet(value | orientation.binary)
  def -(orientation: Orientation)                      = OrientationSet(value ^ orientation.binary)
  def isEmpty: Boolean                                 = value == 0
  def isFull: Boolean                                  = value == OrientationSet.MAX
  def rotate: OrientationSet                           = OrientationSet(values.map(_.rotate))
  def flipVertical: OrientationSet                     = OrientationSet(values.map(_.flipVertical))
  def flipHorizontal: OrientationSet                   = OrientationSet(values.map(_.flipHorizontal))

  override def toString() = s"OrientationSet($value = ${values.mkString(", ")})"
}

object Orientation {
  val ALL_VALUES               = List(NorthWest, North, NorthEast, West, Center, East, SouthWest, South, SouthEast)
  val SIMPLE_AROUND_VALUES     = List(North, East, South, West)
  val ADVANCED_AROUND_VALUES   = List(North, NorthEast, East, SouthEast, South, SouthWest, West, NorthWest)
  val DIAGONAL_AROUND_VALUES   = List(NorthEast, SouthEast, SouthWest, NorthWest)
  val HORIZONTAL_VALUES        = List(West, Center, East)
  val HORIZONTAL_AROUND_VALUES = List(West, East)
  val VERTICAL_VALUES          = List(North, Center, South)
  val VERTICAL_AROUND_VALUES   = List(North, South)
  @inline def each(run: Orientation => Unit): Unit = {
    run(North)
    run(East)
    run(South)
    run(West)
  }

  val DEFAULT = Center

  def byOffset(offsetX: Float, offsetY: Float): Orientation = {
    if (offsetX == 0f && offsetY == 0f) Orientation.DEFAULT
    else if (Math.abs(offsetX) > Math.abs(offsetY)) {
      if (offsetX > 0f) East
      else West
    } else {
      if (offsetY > 0f) South
      else North
    }
  }

  import scala.language.implicitConversions
  implicit def orientationOrdering[T <: Orientation](orientation: T) = new Ordering[T] {
    def compare(a: T, b: T) = {
      val xResult = a.xInt compare b.xInt
      if (xResult == 0) xResult else a.yInt compare b.yInt
    }
  }

  def apply(f: format.Orientation): Orientation = {
    f match {
      case format.Orientation.NORTH      => North
      case format.Orientation.NORTH_EAST => NorthEast
      case format.Orientation.EAST       => East
      case format.Orientation.SOUTH_EAST => SouthEast
      case format.Orientation.SOUTH      => South
      case format.Orientation.SOUTH_WEST => SouthWest
      case format.Orientation.WEST       => West
      case format.Orientation.NORTH_WEST => NorthWest
      case format.Orientation.CENTER     => Center
    }
  }

  def apply(key: String): Orientation = {
    key match {
      case "North"     => North
      case "NorthEast" => NorthEast
      case "East"      => East
      case "SouthEast" => SouthEast
      case "South"     => South
      case "SouthWest" => SouthWest
      case "West"      => West
      case "NorthWest" => NorthWest
      case "Center"    => Center
      case _ => throw new IllegalStateException(s"Unsupported orientation key: '$key'")
    }
  }
}
sealed trait Orientation {
  val x: Float
  val y: Float

  // !!! don't use val here
  def xInt: Int = x.toInt
  def yInt: Int = y.toInt

  val opposit: Orientation
  val rotate: Orientation
  val flipVertical: Orientation
  val flipHorizontal: Orientation
  val binary: Int
  val export: format.Orientation
  val key: String

  def isDiagonal   = x != 0f && y != 0f;
  def isHorizontal = x != 0f && y == 0f;
  def isVertical   = x == 0f && y != 0f;
}
sealed trait SimpleOrientation extends Orientation {
  val opposit: SimpleOrientation
}

case object NorthWest extends Orientation {
  override val x = West.x
  override val y = North.y

  override val opposit        = SouthEast
  override val rotate         = NorthEast
  override val flipVertical   = NorthEast
  override val flipHorizontal = SouthWest
  override val binary         = 1
  override val export         = format.Orientation.NORTH_WEST
  override val key           = "NorthWest"
}
case object North extends SimpleOrientation {
  override val x = 0f
  override val y = -1f

  override val opposit        = South
  override val rotate         = East
  override val flipVertical   = North
  override val flipHorizontal = South
  override val binary         = 2
  override val export         = format.Orientation.NORTH
  override val key           = "North"
}
case object NorthEast extends Orientation {
  override val x = East.x
  override val y = North.y

  override val opposit        = SouthWest
  override val rotate         = SouthEast
  override val flipVertical   = NorthWest
  override val flipHorizontal = SouthEast
  override val binary         = 4
  override val export         = format.Orientation.NORTH_EAST
  override val key           = "NorthEast"
}

case object West extends SimpleOrientation {
  override val x = -1f
  override val y = 0f

  override val opposit        = East
  override val rotate         = North
  override val flipVertical   = East
  override val flipHorizontal = West
  override val binary         = 8
  override val export         = format.Orientation.WEST
  override val key           = "West"
}
case object Center extends SimpleOrientation {
  override val x = 0f
  override val y = 0f

  override val opposit        = Center
  override val rotate         = Center
  override val flipVertical   = Center
  override val flipHorizontal = Center
  override val binary         = 16
  override val export         = format.Orientation.CENTER
  override val key           = "Center"
}
case object East extends SimpleOrientation {
  override val x = 1f
  override val y = 0f

  override val opposit        = West
  override val rotate         = South
  override val flipVertical   = West
  override val flipHorizontal = East
  override val binary         = 32
  override val export         = format.Orientation.EAST
  override val key           = "East"
}

case object SouthWest extends Orientation {
  override val x = West.x
  override val y = South.y

  override val opposit        = NorthEast
  override val rotate         = NorthWest
  override val flipVertical   = SouthEast
  override val flipHorizontal = NorthWest
  override val binary         = 64
  override val export         = format.Orientation.SOUTH_WEST
  override val key           = "SouthWest"
}
case object South extends SimpleOrientation {
  override val x = 0f
  override val y = 1f

  override val opposit        = North
  override val rotate         = West
  override val flipVertical   = South
  override val flipHorizontal = North
  override val binary         = 128
  override val export         = format.Orientation.SOUTH
  override val key           = "South"
}
case object SouthEast extends Orientation {
  override val x = East.x
  override val y = South.y

  override val opposit        = NorthWest
  override val rotate         = SouthWest
  override val flipVertical   = SouthWest
  override val flipHorizontal = NorthEast
  override val binary         = 256
  override val export         = format.Orientation.SOUTH_EAST
  override val key           = "SouthEast"
}

trait Tiled {
  def position: Position             = Position()
  private[this] final def x: TileInt = position.x
  private[this] final def y: TileInt = position.y
  def width: TileInt
  def height: TileInt

  final def size: Int = width * height
  final def clampPosition(x: TileInt, y: TileInt): Position = position(
    x = MathUtils.clamp(x, this.x, this.x + this.width - 1),
    y = MathUtils.clamp(y, this.y, this.y + this.height - 1)
  )
  final def position(x: TileInt, y: TileInt): Position = Position(x, y, width, height)

  final def isValid(position: Position): Boolean     = isValid(position.x, position.y)
  final def isValid(x: TileInt, y: TileInt): Boolean = x >= this.x && y >= this.y && x < this.x + width && y < this.y + height

  final def isEndOfTiled(position: Position): Boolean     = isEndOfTiled(position.x, position.y)
  final def isEndOfTiled(x: TileInt, y: TileInt): Boolean = (x == this.x || y == this.y || x.value == this.x + this.width - 1 || y.value == this.y + this.height - 1) && isValid(x, y)

  /** Return true if position is end of given tiled. */
  final def isEndOfTiled(position: Position, tiled: Tiled): Boolean = {
    isEndOfTiled(position.withPosition(position.x - tiled.position.x, position.y - tiled.position.y))
  }

  final def centerX: TileInt         = x + width / 2
  final def centerY: TileInt         = y + height / 2
  final def centerPosition: Position = position.withPosition(centerX, centerY)

  /** Returns a list with the positions around this tiled on given tiled */
  final def around(tiled: Tiled): Seq[Position] = {
    List(
      for (x <- (x - 1) to (x + width) if y - 1 >= 0 && x >= 0 && x < tiled.width) yield tiled.position(x, y - 1),
      for (y <- y.value until (y + height) if x + width < tiled.width && y >= 0 && y < tiled.height) yield tiled.position(x + width, y),
      for (x <- (x + width) to (x - 1) by -1 if y + height < tiled.height && x >= 0 && x < tiled.width) yield tiled.position(x, y + height),
      for (y <- (y + height - 1) to y.value by -1 if x - 1 >= 0 && y >= 0 && y < tiled.height) yield tiled.position(x - 1, y)
    ).flatten
  }

  final def outline(tiled: Tiled): Seq[Position] = {
    tiled.areaPositions(position, width, height).filter(isEndOfTiled(_, tiled))
  }

  def traverse(startPosition: Position, maxStepCount: Int)(visit: Position => Traverse): Unit = {
    helper.traverse[Position](startPosition, maxStepCount)(current => {
      Orientation.SIMPLE_AROUND_VALUES.flatMap(current.around(_))
    })(visit)
  }

  def traverseFind(startPosition: Position, maxStepCount: Int)(visit: Position => Traverse): Option[Position] = {
    helper.traverseFind[Position](startPosition, maxStepCount)(current => {
      Orientation.SIMPLE_AROUND_VALUES.flatMap(current.around(_))
    })(visit)
  }

  @inline def iterate(position: Position, radius: TileInt)(callback: Position => Unit): Unit = {
    if (radius > 0) {
      iterate(
        tileX = TileInt(position.x - radius),
        tileY = TileInt(position.y - radius),
        tileWidth = radius * 2 + 1,
        tileHeight = radius * 2 + 1
      )(callback)
    }
  }

  @inline def iterate(tileX: TileInt = 0, tileY: TileInt = 0, tileWidth: TileInt = width, tileHeight: TileInt = height)(callback: Position => Unit): Unit = {
    val xRange = Math.max(tileX.value, 0) until Math.min(tileX.value + tileWidth.value, width.value)
    val yRange = Math.max(tileY.value, 0) until Math.min(tileY.value + tileHeight.value, height.value)

    for (iy <- yRange; ix <- xRange) {
      callback(position(ix, iy)): @inline
    }
  }

  def areaPositions(position: Position, radius: TileInt): Seq[Position] = {
    if (radius > 0) {
      areaPositions(
        tileX = TileInt(position.x - radius),
        tileY = TileInt(position.y - radius),
        tileWidth = TileInt(radius * 2 + 1),
        tileHeight = TileInt(radius * 2 + 1)
      )
    } else {
      Seq()
    }
  }

  def areaPositions(position: Position, tileWidth: TileInt, tileHeight: TileInt): Seq[Position] = {
    areaPositions(position.x, position.y, tileWidth, tileHeight)
  }

  def areaPositions(tileX: TileInt = 0, tileY: TileInt = 0, tileWidth: TileInt = width, tileHeight: TileInt = height): Seq[Position] = {
    val xRange = Math.max(tileX.value, 0) until Math.min(tileX.value + tileWidth.value, width.value)
    val yRange = Math.max(tileY.value, 0) until Math.min(tileY.value + tileHeight.value, height.value)

    for {
      iy <- yRange
      ix <- xRange
    } yield position(ix, iy)
  }
}
object Tiled {
  def unapply(tiled: Tiled): Option[(TileInt, TileInt, TileInt, TileInt)] = Some((tiled.position.x, tiled.position.y, tiled.width, tiled.height))
}

final case class SimpleTiled(
    override val position: Position = Position(),
    override val width: TileInt = 0,
    override val height: TileInt = 0
) extends Tiled

object helper {
  val BOOLEANS = List(true, false)

  sealed trait Traverse
  case object Continue     extends Traverse
  case object DontGoDeeper extends Traverse
  case object Stop         extends Traverse
  case object Found        extends Traverse

  def traverseFind[T](start: T, maxStepCount: Int)(getNexts: T => Seq[T])(visit: T => Traverse): Option[T] = {
    var result: Option[T] = None
    traverse[T](start, maxStepCount)(getNexts) { t =>
      visit(t) match {
        case Found => {
          result = Some(t)
          Stop
        }
        case other => {
          other
        }
      }
    }
    result
  }

  def traverse[T](start: T, maxStepCount: Int)(getNexts: T => Seq[T])(visit: T => Traverse): Unit = {
    val blacklist: scala.collection.mutable.Set[T] = new HashSet
    var stopped                                    = false

    var stepCount: Int            = 0
    var nexts: ArrayBuffer[T]     = null
    var nextNexts: ArrayBuffer[T] = ArrayBuffer(start)
    do {
      nexts = nextNexts
      nextNexts = new ArrayBuffer

      for (current <- nexts if !stopped && stepCount < maxStepCount) {
        if (!blacklist.contains(current)) {
          blacklist.add(current)
          visit(current) match {
            case Continue     => nextNexts.appendAll(getNexts(current))
            case DontGoDeeper => { /* do nothing */ }
            case Stop         => stopped = true
            case _            => throw new UnsupportedOperationException
          }
          stepCount += 1
        }
      }

    } while (!nextNexts.isEmpty && !stopped)
  }

  def time[R](block: => R): R = {
    val before = System.nanoTime()
    val result = block
    println("Elapsed time: " + (System.nanoTime() - before) / 1000000f + "ms")
    result
  }
}

object PathFinder {

  def search[T](start: T, next: T => Seq[T], predicate: T => Boolean, reverse: Boolean = false): Option[List[T]] = {
    searchWithWeight(start, (t: T) => next(t).map((1, _)), predicate, reverse)
  }

  def searchWithWeight[T](start: T, next: T => Seq[(Int, T)], predicate: T => Boolean, reverse: Boolean = false): Option[List[T]] = {
    val ratingByNode = scala.collection.mutable.HashMap[T, Int]()
    val parentByNode = scala.collection.mutable.HashMap[T, T]()
    ratingByNode.put(start, 0)

    val nexts     = ListBuffer[T](start)
    val nextNexts = ListBuffer[T]()

    var target: Option[T] = None
    var max: Int          = Integer.MAX_VALUE

    if (predicate(start)) {
      return Some(List.empty)
    }

    while (!nexts.isEmpty) {
      for (n <- nexts) {
        val nRating = ratingByNode.getOrElse(n, 0)

        if (nRating < max) {
          for (nn <- next(n)) {
            val weight = nn._1
            val node   = nn._2

            if (!ratingByNode.contains(node) || predicate(node)) {
              parentByNode.put(node, n)

              val rating = nRating + weight

              if (rating < max) {
                if (predicate(node)) {
                  target = Some(node)
                  max = rating
                }

                ratingByNode.put(node, rating)

                nextNexts.append(node)
              }
            }
          }
        }
      }
      nexts.clear()
      nexts.appendAll(nextNexts)
      nextNexts.clear()
    }

    target.map { node =>
      val result                  = ArrayBuffer[T]()
      var n                       = node
      var parentOption: Option[T] = parentByNode.get(n)
      while (parentOption.isDefined) {
        result.append(n)
        n = parentOption.get
        parentOption = parentByNode.get(n)
      }

      result.append(start)

      if (reverse) result.toList
      else result.reverse.toList
    }
  }

}

object Entity {
  private[this] val _nextId: AtomicInteger = new AtomicInteger(0)
  def nextId: Int                          = _nextId.getAndIncrement()

  /** Use with care !!! */
  def setNextId(id: Int): Unit = _nextId.set(id)
}
abstract class Entity(val id: Int) {
  override def equals(obj: Any) = {
    obj match {
      case other: Entity => other.id == this.id
      case _             => false
    }
  }

  override def hashCode() = id
}

final class FpsCounter {
  private[this] val ONE_SECOND_IN_NANO_SECONDS = Duration(1, TimeUnit.SECONDS).toNanos
  private[this] var _lastCountTime: Long       = TimeUtils.nanoTime()
  private[this] var _fps: Int                  = 0
  private[this] var _count: Int                = 0

  def fps: Int = _fps
  def count(): Unit = {
    val nanoTime = TimeUtils.nanoTime()
    if (nanoTime - _lastCountTime >= ONE_SECOND_IN_NANO_SECONDS) {
      _fps = _count
      _count = 0
      _lastCountTime = nanoTime
    }
    _count = _count + 1
  }
}

trait Watcher[E] {
  def get(game: Game): Option[E]
  def onSet(entity: E): Unit
  def onUnset(entity: E): Unit
  def onUpdate(entity: E): Unit

  private[this] var _selectedEntity: Option[E] = None
  def selectedEntity: Option[E]                = _selectedEntity
  def update(game: Game): Unit = {
    get(game) match {
      case Some(entity) => {
        if (_selectedEntity != Some(entity)) {
          onSet(entity)
          _selectedEntity = Some(entity)
        } else {
          onUpdate(entity)
        }
      }
      case None => {
        _selectedEntity match {
          case Some(selectedEntity) => {
            onUnset(selectedEntity)
            _selectedEntity = None
          }
          case None => {
            // ignore
          }
        }
      }
    }
  }
}
