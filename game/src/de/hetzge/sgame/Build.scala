package de.hetzge.sgame.build

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.tools.texturepacker.TexturePacker
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings
import com.badlogic.gdx.Gdx

object Build extends App {
  new LwjglApplication(BuildApplicationAdapter, new LwjglApplicationConfiguration)
}

object BuildApplicationAdapter extends ApplicationAdapter {
  override def create() = {
    val settings = new Settings()
    settings.square = true
    settings.combineSubdirectories = true

    TexturePacker.process( settings, "assets", "assets/generated", "pack")

    Gdx.app.exit()
  }
}