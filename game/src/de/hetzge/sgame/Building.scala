package de.hetzge.sgame.building

import de.hetzge.sgame._
import de.hetzge.sgame.stock._
import de.hetzge.sgame.base._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.render._
import de.hetzge.sgame.area._
import de.hetzge.sgame.game._
import de.hetzge.sgame.way._
import de.hetzge.sgame.waypoint._
import de.hetzge.sgame.person._
import de.hetzge.sgame.item._
import de.hetzge.sgame.job._
import de.hetzge.sgame.player._
import de.hetzge.sgame.export._

import scala.concurrent.duration._
import scala.collection.mutable
import de.hetzge.sgame.format.Reference

final case class BuildingProto(
    buildingType: BuildingTypeProto,
    id: Long,
    area: Reference,
    position: PositionProto,
    stock: Reference,
    playerId: Long,
    persons: List[Reference]
)

final case class BuildingTypeProto(
    buildingType: String,
    itemType: Option[String],
    targetBuildingType: Option[BuildingTypeProto]
)

sealed trait BuildingType extends Serializable {
  @transient lazy val renderable: Renderable = GameApplicationAdapter.resources.emptyRenderable

  val needItemsToBuild: Map[ItemType, Int] = Map()
  final def needItemsToBuildCount: Int     = needItemsToBuild.map(_._2).sum

  val needItemTypes: Set[ItemType]     = Set()
  val producesItemTypes: Set[ItemType] = Set()

  val allowedPersonCountByType: Map[PersonType, Int]      = Map()
  val initialPersonTypes: List[PersonType]                = List()
  def getAllowedPersonsCount(personType: PersonType): Int = allowedPersonCountByType.get(personType).getOrElse(0)

  val width: TileInt  = 1
  val height: TileInt = 1

  def renderWidth: TileInt  = width
  def renderHeight: TileInt = height

  final def renderOffsetX: PixelFloat = width.pixelFloat - renderWidth.pixelFloat
  final def renderOffsetY: PixelFloat = height.pixelFloat - renderHeight.pixelFloat

  val viewRadius: TileInt      = 10
  val ownershipRadius: TileInt = 0
  val canBuildInGaia: Boolean  = false

  // door relative to position
  def doorX: TileInt   = Math.floor(width / 2f).toInt
  def doorY: TileInt   = height
  def hasDoor: Boolean = doorX != TileInt(0) || doorY != TileInt(0)

  def createStock(economy: ItemEconomy): Stock = new MasterStock()
  def createJob(): Job[Building]               = DummyJob.asInstanceOf[Job[Building]]

  def export: BuildingTypeProto
  def typeString: String
}
object BuildingType {
  def apply(from: BuildingTypeProto): BuildingType = {
    from.buildingType match {
      case "Stock"           => BuildingType.Stock(ItemType(from.itemType.get))
      case "Start"           => BuildingType.Start
      case "SmallResidence"  => BuildingType.SmallResidence
      case "SawMill"         => BuildingType.SawMill
      case "Quarry"          => BuildingType.Quarry
      case "NormalResidence" => BuildingType.NormalResidence
      case "Main"            => BuildingType.Main
      case "Lumberjack"      => BuildingType.Lumberjack
      case "LandMark"        => BuildingType.LandMark
      case "CropFarm"        => BuildingType.CropFarm
      case "BuildingLot"     => BuildingType.BuildingLot(apply(from.targetBuildingType.get))
      case "BigResidence"    => BuildingType.BigResidence
    }
  }

  case object Start extends BuildingType {
    @transient override lazy val renderable = GameApplicationAdapter.resources.building.stock

    override val allowedPersonCountByType = Map(
      PersonType.Carrier      -> 99999,
      PersonType.StreetWorker -> 99999
    )
    override val initialPersonTypes = (0 until 25).map(_ => PersonType.Carrier).toList

    override val width  = 1
    override val height = 1

    override val viewRadius      = 25
    override val ownershipRadius = 100

    override val canBuildInGaia = true

    override def exportType = format.Building.Type(
      `type` = format.BuildingType.START_BUILDING
    )
    override def typeString = "Start"
  }

  case object Main extends BuildingType {
    @transient override lazy val renderable = GameApplicationAdapter.resources.building.main

    override val allowedPersonCountByType = Map(
      PersonType.Carrier      -> 99999,
      PersonType.StreetWorker -> 99999
    )
    override val initialPersonTypes = (0 until 100).map(_ => PersonType.Carrier).toList

    override val width  = 6
    override val height = 4

    override val renderWidth  = 6
    override val renderHeight = 8

    override def createJob() = ItemBuildingJob

    override val producesItemTypes = Set(StoneItemType, WoodItemType)
    override val needItemTypes     = Set(StoneItemType, WoodItemType)

    override def export = BuildingTypeProto(
      buildingType = typeString
    )
    override def typeString = "Main"
  }

  case object Warehouse extends BuildingType {
    @transient override lazy val renderable = GameApplicationAdapter.resources.building.stock

    override val width  = 1
    override val height = 1

    override def createStock(economy: ItemEconomy) = new MasterStock()

    override def export = BuildingTypeProto(
      buildingType = typeString
    )
    override def typeString = "Warehouse"
  }

  final case class Stock(itemType: ItemType) extends BuildingType {
    @transient override lazy val renderable = GameApplicationAdapter.resources.building.stock

    override val width  = 1
    override val height = 1

    override val doorX = 0
    override val doorY = 0

    override def createStock(economy: ItemEconomy) = new SubStock(ItemTypeStockType(itemType))

    override def export = BuildingTypeProto(
      buildingType = typeString,
      itemType = Some(itemType.export)
    )
    override def typeString = "Stock"
  }

  final case object StreetWorker extends BuildingType {
    @transient override lazy val renderable = GameApplicationAdapter.resources.building.streetWorker

    override val allowedPersonCountByType = Map(
      PersonType.StreetWorker -> 1
    )
    override val initialPersonTypes = List(PersonType.StreetWorker)

    override val needItemsToBuild = Map(
      WoodItemType -> 1
    )

    override val width  = 5
    override val height = 4

    override val renderWidth  = 5
    override val renderHeight = 5

    override val doorX = 1
    override val doorY = height

    override val needItemTypes = Set(StoneItemType)

    override def export = BuildingTypeProto(
      buildingType = typeString
    )
    override def typeString = "StreetWorker"
  }

  final case class BuildingLot(val targetBuildingType: BuildingType) extends BuildingType {
    @transient override lazy val renderable = GameApplicationAdapter.resources.buildingLotRenderable

    override val width  = targetBuildingType.width
    override val height = targetBuildingType.height

    override val doorX = targetBuildingType.doorX
    override val doorY = targetBuildingType.doorY

    override def createStock(economy: ItemEconomy) = {
      val stock = new SingleStock(new MixedStockType(targetBuildingType.needItemsToBuildCount))
      for ((itemType, count) <- targetBuildingType.needItemsToBuild) {
        for (i <- 0 until count) {
          stock.noItem(itemType).get.demand(economy, itemType)
        }
      }
      stock
    }

    override def createJob = BuildingLotJob

    override def export = BuildingTypeProto(
      buildingType = typeString,
      targetBuildingType = Some(targetBuildingType.export)
    )
    override def typeString = "BuildingLot"
  }

  case object SmallResidence extends BuildingType {
    @transient override lazy val renderable = GameApplicationAdapter.resources.building.lumberjack

    override val allowedPersonCountByType = Map(
      PersonType.Carrier -> 10
    )
    override val initialPersonTypes = (0 until allowedPersonCountByType(PersonType.Carrier)).map(_ => PersonType.Carrier).toList

    override val width  = 7
    override val height = 5

    override val doorX = 5
    override val doorY = 5

    override val needItemsToBuild = Map(
      WoodItemType  -> 3,
      StoneItemType -> 3
    )

    override def export = BuildingTypeProto(
      buildingType = typeString
    )
    override def typeString = "SmallResidence"
  }

  case object NormalResidence extends BuildingType {
    @transient override lazy val renderable = GameApplicationAdapter.resources.building.lumberjack

    override val allowedPersonCountByType = Map(
      PersonType.Carrier -> 25
    )
    override val initialPersonTypes = (0 until allowedPersonCountByType(PersonType.Carrier)).map(_ => PersonType.Carrier).toList

    override val width  = 7
    override val height = 5

    override val doorX = 5
    override val doorY = 5

    override val needItemsToBuild = Map(
      WoodItemType  -> 6,
      StoneItemType -> 6
    )

    override def export = BuildingTypeProto(
      buildingType = typeString
    )
    override def typeString = "NormalResidence"
  }

  case object BigResidence extends BuildingType {
    @transient override lazy val renderable = GameApplicationAdapter.resources.building.main

    override val allowedPersonCountByType = Map(
      PersonType.Carrier -> 50
    )
    override val initialPersonTypes = (0 until allowedPersonCountByType(PersonType.Carrier)).map(_ => PersonType.Carrier).toList

    override val width  = 6
    override val height = 5

    override val renderWidth  = 6
    override val renderHeight = 8

    override val doorX = 3
    override val doorY = 5

    override val needItemsToBuild = Map(
      WoodItemType  -> 9,
      StoneItemType -> 9
    )

    override def export = BuildingTypeProto(
      buildingType = typeString
    )
    override def typeString = "BigResidence"
  }

  case object Lumberjack extends BuildingType {
    @transient override lazy val renderable = GameApplicationAdapter.resources.building.building3

    override val allowedPersonCountByType = Map(
      PersonType.Lumberjack -> 1
    )
    override val initialPersonTypes = List(PersonType.Lumberjack)

    override val width  = 3
    override val height = 3

    override val doorX = 1
    override val doorY = 3

    override val producesItemTypes = Set(WoodItemType)

    override def export = BuildingTypeProto(
      buildingType = typeString
    )
    override def typeString = "Lumberjack"
  }

  case object Quarry extends BuildingType {
    @transient override lazy val renderable = GameApplicationAdapter.resources.building.building2

    override val allowedPersonCountByType = Map(
      PersonType.Mason -> 1
    )
    override val initialPersonTypes = List(PersonType.Mason)

    override val width  = 3
    override val height = 3

    override val doorX = 1
    override val doorY = 3

    override val producesItemTypes = Set(StoneItemType)

    override def export = BuildingTypeProto(
      buildingType = typeString
    )
    override def typeString = "Quarry"
  }

  case object CropFarm extends BuildingType {
    @transient override lazy val renderable = GameApplicationAdapter.resources.building.building1

    override val allowedPersonCountByType = Map(
      PersonType.Farmer -> 1
    )
    override val initialPersonTypes = List(PersonType.Farmer)

    override val width  = 3
    override val height = 3

    override val doorX = 1
    override val doorY = 3

    override val producesItemTypes = Set(CropItemType)

    override def export = BuildingTypeProto(
      buildingType = typeString
    )
    override def typeString = "CropFarm"
  }

  case object SawMill extends BuildingType {
    override val needItemTypes     = Set(WoodItemType)
    override val producesItemTypes = Set(WoodBlankItemType)

    override def export = BuildingTypeProto(
      buildingType = typeString
    )
    override def typeString = "SawMill"
  }

  case object LandMark extends BuildingType {
    @transient override lazy val renderable = GameApplicationAdapter.resources.building.landmark

    override val width  = 1
    override val height = 1

    override val renderHeight = 2

    override val ownershipRadius = 20

    override val needItemsToBuild = Map(
      StoneItemType -> 2
    )

    override def export = BuildingTypeProto(
      buildingType = typeString
    )
    override def typeString = "LandMark"
  }

  final case class Test(
      override val width: TileInt = 1,
      override val height: TileInt = 1,
      override val doorX: TileInt = 0,
      override val doorY: TileInt = 0
  ) extends BuildingType {
    override def export = BuildingTypeProto(
      buildingType = typeString
    )
    override def typeString = "Test"
  }
}

sealed trait RenderBuilding { building: Building =>
  def render(renderContext: GameRenderContext, x: PixelFloat, y: PixelFloat, z: PixelFloat): Unit = {
    val renderOffsetX = buildingType.renderOffsetX
    val renderOffsetY = buildingType.renderOffsetY

    buildingType.renderable.render(
      renderContext,
      ZIndexOffset(renderOffsetY),
      x = x + renderOffsetX,
      y = y + renderOffsetY,
      z,
      width = buildingType.renderWidth.pixelFloat,
      height = buildingType.renderHeight.pixelFloat
    )
    buildingType match {
      case _: BuildingType.Stock => stock.render(renderContext, x, y, z)
      case _                     =>
    }
  }
}

sealed trait PersonBuilding { building: Building =>
  private[building] val _persons = new mutable.ListBuffer[Person]
  def persons: Seq[Person]       = _persons

  def canReserve(person: Person): Boolean         = !_persons.contains(person) && canReserve(person.personType)
  def canReserve(personType: PersonType): Boolean = _persons.count(_.personType == personType) < buildingType.getAllowedPersonsCount(personType)
  def canEnter(person: Person): Boolean           = _persons.contains(person) && person.isReservedInBuilding
  def canLeave(person: Person): Boolean           = _persons.contains(person) && person.isInBuilding && doorWay.isDefined

  /** canReserve should be called before **/
  def reserve(person: Person): Unit = {
    require(canReserve(person), s"'$person' can't reserve '$this' stay")

    person.setBuildingStay(ReservedBuildingStay(this))
    _persons += person
  }

  /** canEnter should be called before **/
  def enter(person: Person): Unit = {
    area.unregisterPerson(person)
    setEntered(person)
  }

  private def setEntered(person: Person): Unit = {
    require(canEnter(person), s"'$person' can't enter '$this'")

    person.setPosition(0, 0)
    person.setBuildingStay(EnteredBuildingStay(this))
  }

  /** canLeave should be called before **/
  def leave(person: Person, keepReservation: Boolean = false): Unit = {
    require(canLeave(person), s"'$person' can't leave '$this'")

    if (keepReservation) {
      person.setBuildingStay(ReservedBuildingStay(this))
    } else {
      person.setBuildingStay(NoneBuildingStay)
      _persons -= person
    }

    person.setPosition(
      x = doorX,
      y = doorY
    )

    area.registerPerson(person)
  }

  def cancelReservation(person: Person): Unit = {
    require(person.isReservedInBuilding(this))

    _persons -= person
    person.setBuildingStay(NoneBuildingStay)
  }

  /** Call canReserve before */
  def buildPerson(personType: PersonType): Option[Person] = {
    if (canReserve(personType)) {
      val person = Person(area.position(0, 0), personType, playerId)
      person.assignArea(area)
      reserve(person)
      setEntered(person)
      person.startKi()
      Some(person)
    } else {
      None
    }
  }
}

sealed trait DoorBuilding { building: Building =>
  def doorX: TileInt         = position.x.value + buildingType.doorX
  def doorY: TileInt         = position.y.value + buildingType.doorY
  def doorPosition: Position = area.position(doorX, doorY)
  def doorWay: Option[Way]   = area.way(doorPosition)
  def doorWaypoint: Option[Waypoint] = {
    buildingType match {
      case _: BuildingType.Stock => {
        stock.master.entity match {
          case it if it == this => {
            Orientation.ADVANCED_AROUND_VALUES.toStream
              .flatMap(building.position.around(_))
              .flatMap(area.waypoint(_))
              .headOption
          }
          case building: Building => {
            building.doorWaypoint
          }
          case _ => {
            throw new IllegalStateException
          }
        }
      }
      case _ => {
        area.waypoint(doorPosition).orElse {
          for {
            _        <- area.way(doorPosition)
            path     <- area.pathOnWay(doorPosition, area.isWaypoint(_))
            last     <- path.last
            waypoint <- area.waypoint(last)
          } yield waypoint
        }
      }
    }
  }
}

sealed trait ExportBuilding extends ExportObject[format.Building] with ExportIndexedObject { building: Building =>

  override def export()(implicit index: ExportedIndex) = BuildingProto(
    buildingType = ???, 
    id = ???, 
    area = ???, 
    position = ???, 
    stock = ???, 
    playerId = ???, 
    persons = ???
    )


    id = id,
    area = Some(index(area)),
    `type` = Some(buildingType.exportType),
    position = Some(format.Position(_position.v)),
    stock = Some(index(stock)),
    persons = _persons.map(index(_))
  )

  override def initLoad(f: format.Building)(implicit index: ImportedIndex) = {
    assignArea(index[Area](f.area.get))
    _persons.clear()
    _persons ++= f.persons.map(index[Person](_))
  }

  override def index() = ExportIndex(
    objects = Set(building.stock) ++ persons
  )

}

object ImportBuilding {

  def load(f: format.Building)(implicit index: ImportedIndex) = {
    new Building(
      id = f.id,
      _position = Position(f.position.get.value),
      buildingType = BuildingType(f.`type`.get),
      stock = index[Stock](f.stock.get)
    )
  }

}

object Building {
  def apply(
      area: Area,
      position: Position,
      buildingType: BuildingType
  ): Building = {
    val stock    = buildingType.createStock(area.economy)
    val building = new Building(Entity.nextId, position, buildingType, stock)
    building.assignArea(area)
    stock.assignEntity(building)
    building
  }
}
final class Building private[building] (
    id: Int,
    private[building] var _position: Position,
    val buildingType: BuildingType,
    override val stock: Stock
) extends Entity(id)
    with KiEntity[format.Building]
    with PlayerEntity
    with DoorBuilding
    with PersonBuilding
    with RenderBuilding
    with AreaEntity
    with StockEntity
    with JobEntity[Building]
    with ExportBuilding {
  override def width  = buildingType.width
  override def height = buildingType.height

  override def position                         = _position
  def setPosition(x: TileInt, y: TileInt): Unit = _position = _position.withPosition(x, y)
  def upgrade(targetType: BuildingType): Unit   = area.upgradeBuilding(this, targetType)
  override def playerId                         = area.owner(_position)

  override def stockWay      = doorWay
  override def stockWaypoint = doorWaypoint

  override def viewRadius       = buildingType.viewRadius
  override def ownershipRadius  = buildingType.ownershipRadius
  override def isStatic         = true
  override def createDefaultJob = buildingType.createJob

  override def ki() = {
    execute(Trigger.Default)
    GameContext.frames.get(5.seconds).add(this)
  }

  def initialize(): Unit = {
    for (initialPersonType <- buildingType.initialPersonTypes) {
      buildPerson(initialPersonType)
    }
  }

  def buildingsAround(): Seq[Building] = {
    around(area).flatMap(area.building(_)).distinct
  }

  override def toString() = s"Building(${position}, $buildingType)"
}
