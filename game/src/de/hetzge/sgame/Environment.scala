package de.hetzge.sgame.environment

import de.hetzge.sgame.GameApplicationAdapter
import de.hetzge.sgame.base._
import de.hetzge.sgame.item._
import de.hetzge.sgame.render._
import de.hetzge.sgame.stock._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.format
import de.hetzge.sgame.export._
import com.google.protobuf.ByteString

final case class EnvironmentProto(
    width: Int,
    height: Int,
    environment: Array[Byte],
    level: Array[Byte]
)

sealed trait EnvironmentType {
  def renderableSet: EnvironmentRenderableSet

  val i: Int
  val itemType: ItemType
  val mergeWithNeighbor: Boolean
  val isBlocking: Boolean
  val removeWhenBuildingOnIt: Boolean
  val growPerMinute: BytePercent

  lazy val isGrowing: Boolean = !growPerMinute.isEmpty

  val exportType: format.EnvironmentType
}
object EnvironmentType {
  val STEP                                                = 10
  val MAX_COUNT                                           = STEP - 1
  private[environment] val _types: Array[EnvironmentType] = Array(Forest, Stone, Crop)
  def apply(i: Int): EnvironmentType                      = _types(i)
  def apply(f: format.EnvironmentType): EnvironmentType   = _types(f.index)

  case object Forest extends EnvironmentType {
    @transient override lazy val renderableSet = GameApplicationAdapter.resources.environment.forest
    override val i                             = 0
    override val itemType                      = WoodItemType
    override val mergeWithNeighbor             = true
    override val isBlocking                    = true
    override val removeWhenBuildingOnIt        = false
    override val growPerMinute                 = BytePercent(0.1f)
    override val exportType                    = format.EnvironmentType.FOREST_ENVIRONMENT_TYPE
  }
  case object Stone extends EnvironmentType {
    @transient override lazy val renderableSet = GameApplicationAdapter.resources.environment.stone
    override val i                             = 1
    override val itemType                      = StoneItemType
    override val mergeWithNeighbor             = true
    override val isBlocking                    = true
    override val removeWhenBuildingOnIt        = false
    override val growPerMinute                 = BytePercent(0f)
    override val exportType                    = format.EnvironmentType.STONE_ENVIRONMENT_TYPE
  }
  case object Crop extends EnvironmentType {
    @transient override lazy val renderableSet = GameApplicationAdapter.resources.environment.crop
    override val i                             = 2
    override val itemType                      = CropItemType
    override val mergeWithNeighbor             = false
    override val isBlocking                    = false
    override val removeWhenBuildingOnIt        = true
    override val growPerMinute                 = BytePercent(0.1f)
    override val exportType                    = format.EnvironmentType.CROP_ENVIRONMENT_TYPE
  }
}

final case class EnvironmentRenderableSet(variations: Array[Renderable]) {
  def variationCount: Int                        = variations.length
  def variation(variationIndex: Int): Renderable = variations(variationIndex)
}

object EnvironmentByte {
  def apply(environmentType: EnvironmentType, count: Int): EnvironmentByte = {
    require(count < EnvironmentType.STEP, s"max count of ${EnvironmentType.MAX_COUNT} is allowed but is $count")
    EnvironmentByte((environmentType.i * EnvironmentType.STEP + count).toByte)
  }
}
final case class EnvironmentByte(value: Byte) extends AnyVal {
  def environmentType: EnvironmentType = EnvironmentType((value - value % EnvironmentType.STEP) / EnvironmentType.STEP)
  private[environment] def increment(): EnvironmentByte = {
    if (!isFull) EnvironmentByte((value + 1).toByte)
    else this
  }
  private[environment] def decrement(): EnvironmentByte = {
    if (!isEmpty) EnvironmentByte((value - 1).toByte)
    else this
  }
  def isFull: Boolean  = itemCount == EnvironmentType.MAX_COUNT
  def isEmpty: Boolean = itemCount == 0
  def itemCount: Int   = value % EnvironmentType.STEP
}

sealed trait RenderEnvironment { environment: Environment =>

  def render(renderContext: GameRenderContext, x: Float, y: Float, z: Float, position: Position): Unit = {

    // tile coordinates (in tiles)
    val tx = position.x
    val ty = position.y

    // render coordinates (in pixels)
    val rx = x + tx * TILE_SIZE_FLOAT
    val ry = y + ty * TILE_SIZE_FLOAT

    val e                = this.environmentByte(position)
    val environmentType  = e.environmentType
    val environmentLevel = if (e.environmentType.isGrowing) this.environmentLevel(position).percentFloat else 1f
    if (!e.isEmpty && environmentLevel > 0f) {
      val renderableSet       = environmentType.renderableSet
      val variationRenderable = renderableSet.variations(Math.abs(-tx - ty * tx) % renderableSet.variationCount)

      if (environmentType.mergeWithNeighbor) {
        if (tx.isOdd == ty.isOdd) {
          val isDoubleWidth = position.around(East) match {
            case Some(eastPosition) => {
              val eastEnvironment     = this.environmentByte(eastPosition)
              val eastEnvironmentType = eastEnvironment.environmentType
              !eastEnvironment.isEmpty && eastEnvironmentType == environmentType
            }
            case None => {
              false
            }
          }

          if (isDoubleWidth) {
            variationRenderable.render(
              renderContext,
              userData = null,
              x = rx,
              y = ry - TILE_SIZE_FLOAT,
              z = z + 2f,
              width = TILE_SIZE_FLOAT * 2 * environmentLevel,
              height = TILE_SIZE_FLOAT * 2 * environmentLevel
            )
          } else {
            variationRenderable.render(
              renderContext,
              userData = null,
              x = rx - HALF_TILE_SIZE_FLOAT / 2,
              y = ry - HALF_TILE_SIZE_FLOAT / 2,
              z = z + 2f,
              width = TILE_SIZE_FLOAT + HALF_TILE_SIZE_FLOAT * environmentLevel,
              height = TILE_SIZE_FLOAT + HALF_TILE_SIZE_FLOAT * environmentLevel
            )
          }
        } else {
          val shouldRender = position.around(West) match {
            case Some(westPosition) => {
              val westEnvironment     = this.environmentByte(westPosition)
              val westEnvironmentType = westEnvironment.environmentType
              westEnvironment.isEmpty || westEnvironmentType != environmentType
            }
            case None => {
              false
            }
          }

          if (shouldRender) {
            variationRenderable.render(
              renderContext,
              userData = null,
              x = rx,
              y = ry - TILE_SIZE_FLOAT / 2f,
              z = z + 2f,
              width = TILE_SIZE_FLOAT * 1.5f * environmentLevel,
              height = TILE_SIZE_FLOAT * 1.5f * environmentLevel
            )
          }
        }
      } else {
        val size   = TILE_SIZE_FLOAT * environmentLevel
        val offset = (TILE_SIZE_FLOAT - size) / 2f
        variationRenderable.render(
          renderContext,
          userData = null,
          x = rx + offset,
          y = ry + offset,
          z = z + 2f,
          width = size,
          height = size
        )
      }
    }
  }

}

sealed trait ExportEnvironment extends ExportObject[EnvironmentProto] { environment: Environment =>

  override def export()(implicit index: ExportedIndex) = EnvironmentProto(
    width = width,
    height = height,
    environment = _environment,
    level = _environmentLevel
  )
}

object ImportEnvironment {

  def load(from: EnvironmentProto)(implicit index: ImportedIndex): Environment = {
    val environment = new Environment(
      width = from.width,
      height = from.height
    )
    Array.copy(from.environment, 0, environment._environment, 0, environment._environment.length)
    Array.copy(from.level, 0, environment._environmentLevel, 0, environment._environmentLevel.length)
    environment
  }
}

final class Environment(
    override val width: TileInt,
    override val height: TileInt
) extends Tiled
    with ExportEnvironment
    with RenderEnvironment {

  private[environment] val _environment      = Array.fill[Byte](size)(0.toByte)
  private[environment] val _environmentLevel = Array.fill[Byte](size)(0.toByte)

  @inline def environmentByte(position: Position): EnvironmentByte = new EnvironmentByte(_environment(position))
  @inline def environmentLevel(position: Position): BytePercent    = BytePercent(_environmentLevel(position))

  def set(position: Position, environmentType: EnvironmentType, count: Int, level: Float): Unit = {
    _environment(position) = EnvironmentByte(environmentType, count).value
    _environmentLevel(position) = BytePercent(level).value
  }
  def unset(position: Position): Unit = {
    _environment(position) = 0
    _environmentLevel(position) = 0
  }
  def has(position: Position, environmentType: EnvironmentType, minLevel: Float): Boolean = {
    has(position, environmentType) && environmentLevel(position).percentFloat >= minLevel
  }
  def has(position: Position, environmentType: EnvironmentType): Boolean = {
    val environmentByte = this.environmentByte(position)
    environmentByte.environmentType == environmentType && !environmentByte.isEmpty
  }
  def takeItem(position: Position, stock: Stock): Boolean = {
    val e = environmentByte(position)
    if (!e.isEmpty) {
      val itemType = e.environmentType.itemType
      for (noItem <- stock.noItem(itemType)) {
        _environment(position) = e.decrement().value
        noItem.block(itemType)
      }
      true
    } else {
      false
    }
  }
  def itemCount(position: Position, itemType: ItemType): Int = {
    val environmentByte = this.environmentByte(position)
    if (environmentByte.environmentType.itemType == itemType) environmentByte.itemCount else 0
  }
  def isEnvironment(position: Position): Boolean = !environmentByte(position).isEmpty
  def isBlocking(position: Position): Boolean = {
    val e = environmentByte(position)
    !e.isEmpty && e.environmentType.isBlocking
  }

  def growEnvironment(): Unit = {
    iterate()(growEnvironment(_))
  }

  def growEnvironment(position: Position): Unit = {
    val environment = this.environmentByte(position)
    if (environment.environmentType.isGrowing) {
      val environmentLevel = this.environmentLevel(position)
      if (!environmentLevel.isFull) {
        val newValue  = environmentLevel.value + environment.environmentType.growPerMinute.value
        val byteValue = Math.min(Byte.MaxValue, newValue).toByte

        _environmentLevel(position) = byteValue
      }
    }
  }

  def removeWhileBuild(position: Position): Unit = {
    if (environmentByte(position).environmentType.removeWhenBuildingOnIt) {
      unset(position)
    }
  }
}
