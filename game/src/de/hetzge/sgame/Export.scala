package de.hetzge.sgame.export

import de.hetzge.sgame.item._
import de.hetzge.sgame.stock._
import de.hetzge.sgame.building._
import de.hetzge.sgame.person._
import de.hetzge.sgame.game._
import de.hetzge.sgame.tile._
import de.hetzge.sgame.format
import de.hetzge.sgame.resources._
import de.hetzge.sgame.way._
import de.hetzge.sgame.waypoint._
import de.hetzge.sgame.area._
import de.hetzge.sgame.base.Entity
import de.hetzge.sgame.job._

import scala.collection.immutable.HashSet
import scala.reflect.ClassTag
import scala.collection.mutable

import java.util.zip.GZIPOutputStream
import java.io.FileOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.{util => ju}
import scalapb.GeneratedMessage
import java.util.zip.GZIPInputStream
import java.io.FileInputStream
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.SerializationUtils

final case class ReferenceProto(id: Long)

object DataProto {
  def apply(value: Serializable): DataProto = DataProto(SerializationUtils.serialize(value))
}
final case class DataProto(bytes: Array[Byte]) {
  def as[T: ClassTag]: T = SerializationUtils.deserialize[T](bytes)
}

/** A object that can be exported to type F */
trait ExportObject[F] extends ImportObject[F] {
  def export()(implicit index: ExportedIndex): F
}

/** A object that can be imported from type F */
trait ImportObject[F] {
  def initLoad(f: F)(implicit index: ImportedIndex): Unit = { /** override if needed */ }
}

/** A entity that can index other entities in preperation of an export */
trait ExportIndexedObject {
  def index(): ExportIndex
}

/** Collection of indexed objects */
object ExportIndex {
  def apply(objects: Set[Any]): ExportIndex = {
    new ExportIndex(objects = objects.flatMap {
      case e: ExportIndexedObject => e.index().objects
      case _                      => Nil
    } ++ objects)
  }
}
final class ExportIndex private[export] (
    val objects: Set[Any]
) {
  def export(): ExportedIndex = {
    new ExportedIndex(objects = objects.zipWithIndex.map {
      case (key, value) => (key -> value.toLong)
    }.toMap)
  }
}

/** Collection of indexed objects by id */
final class ExportedIndex(
    val objects: Map[Any, Long]
) {
  private[this] implicit val self     = this
  def apply(o: Any): ReferenceProto = ReferenceProto(id = objects(o))
  def all[T: ClassTag]                = objects.keys.filter(_.isInstanceOf[T])
  def export(game: format.Game) = format.Index(
    game = Some(game),
    nextEntityId = Entity.nextId,
    entries = objects.map {
      case (key, value) =>
        format.Index.Entry(
          id = value,
          value = key match {
            case it: Person      => format.Index.Entry.Value.Person(it.export())
            case it: Building    => format.Index.Entry.Value.Building(it.export())
            case it: TileMap     => format.Index.Entry.Value.TileMap(it.export())
            case it: Area        => format.Index.Entry.Value.Area(it.export())
            case it: Item        => format.Index.Entry.Value.Item(it.export())
            case it: Stock       => format.Index.Entry.Value.Stock(it.export())
            case it: ItemEconomy => format.Index.Entry.Value.ItemEconomy(it.export())
            case it: Way         => format.Index.Entry.Value.Way(it.export())
            case it: Waypoint    => format.Index.Entry.Value.Waypoint(it.export())
            case it: Job[_]      => format.Index.Entry.Value.Job(it.export())
          }
        )
    }.toSeq
  )
}

final class ImportedIndex(
    valuesById: Map[Long, format.Index.Entry.Value],
    resources: ApplicationResources
) {
  private[this] var _loaded = new mutable.HashMap[Long, Loaded[_, _]]()

  def apply[T: ClassTag](reference: ReferenceProto)(implicit index: ImportedIndex): T = {
    apply[T](reference.id)
  }
  def apply[T: ClassTag](id: Long)(implicit index: ImportedIndex): T = {
    val value = valuesById(id)
    _loaded.getOrElseUpdate(id, Export.load(value, resources)).get.asInstanceOf[T]
  }
  def load(): Unit = {
    implicit val self = this
    valuesById.keys.foreach(apply[ExportObject[_]](_))
    _loaded.values.foreach(_.init())
  }
}

object Export {
  def export(
      game: Game,
      name: String = new SimpleDateFormat("yyyy-DD-mm_HH-mm-ss_SSS").format(new ju.Date())
  ): Unit = {
    val before        = System.currentTimeMillis()
    val index         = game.index()
    val exportedIndex = index.export
    val gameFormat    = game.export()(index.export)
    val indexFormat   = exportedIndex.export(gameFormat)

    val indexFile = new File(s"save/$name.savegame")
    indexFile.getParentFile().mkdirs()
    val indexOutputStream = new GZIPOutputStream(new FileOutputStream(indexFile))
    try {
      indexOutputStream.write(indexFormat.toByteArray)
    } finally {
      indexOutputStream.close()
    }
    println(s"saved game '$name' in ${System.currentTimeMillis() - before}ms")
  }
  def load(
      name: String,
      resources: ApplicationResources
  ): Game = {
    val before      = System.currentTimeMillis()
    val inputStream = new GZIPInputStream(new FileInputStream(new File(s"save/$name.savegame")))
    try {
      val bytes = IOUtils.toByteArray(inputStream)
      val index = format.Index.parseFrom(bytes)
      val game  = load(index, resources)
      println(s"loaded game '$name' in ${System.currentTimeMillis() - before}ms")
      game
    } finally {
      inputStream.close()
    }
  }
  private[export] def load(
      index: format.Index,
      resources: ApplicationResources
  ): Game = {
    Entity.setNextId(index.nextEntityId)
    val valuesById             = index.entries.map(entry => (entry.id -> entry.value)).toMap
    val gameFormat             = index.game.get
    implicit val importedIndex = new ImportedIndex(valuesById, resources)
    val game                   = ImportGame.load(gameFormat)
    importedIndex.load()
    game.initLoad(gameFormat)
    game
  }

  private[export] def load(
      value: format.Index.Entry.Value,
      resources: ApplicationResources
  )(implicit index: ImportedIndex): Loaded[_, _] = {
    value match {
      case format.Index.Entry.Value.TileMap(tileMap)         => Loaded(ImportTileMap.load(tileMap, resources.tileSet), tileMap)
      case format.Index.Entry.Value.Area(area)               => Loaded(ImportArea.load(area), area)
      case format.Index.Entry.Value.Building(building)       => Loaded(ImportBuilding.load(building), building)
      case format.Index.Entry.Value.Person(person)           => Loaded(ImportPerson.load(person), person)
      case format.Index.Entry.Value.Item(item)               => Loaded(ImportItem.load(item), item)
      case format.Index.Entry.Value.Stock(stock)             => Loaded(ImportStock.load(stock), stock)
      case format.Index.Entry.Value.ItemEconomy(itemEconomy) => Loaded(ImportItemEconomy.load(itemEconomy), itemEconomy)
      case format.Index.Entry.Value.Way(way)                 => Loaded(ImportWay.load(way), way)
      case format.Index.Entry.Value.Waypoint(waypoint)       => Loaded(ImportWaypoint.load(waypoint), waypoint)
      case format.Index.Entry.Value.Job(job)                 => Loaded(ImportJob.load(job), job)
      case _                                                 => throw new IllegalStateException(s"Can't load '$value'")
    }
  }
}

final case class Loaded[F <: GeneratedMessage, T <: ImportObject[F]](
    t: T,
    f: F
) {
  def get: T                                      = t
  def init()(implicit index: ImportedIndex): Unit = t.initLoad(f)
}
// TODO create, init, initContext
// TODO reset random
