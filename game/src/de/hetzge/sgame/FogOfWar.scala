package de.hetzge.sgame.fogofwar

import java.util.concurrent.{Executors, Future}
import java.util.concurrent.atomic.{AtomicIntegerArray, AtomicReference}

import de.hetzge.sgame._
import de.hetzge.sgame.base._
import de.hetzge.sgame.game._
import de.hetzge.sgame.render._
import de.hetzge.sgame.tile._
import de.hetzge.sgame.unit._

import scala.collection.immutable.Queue
import scala.collection.mutable.ArrayBuffer

private sealed trait Change
private final case object Reset                                                                     extends Change
private final case class Update(fromPosition: Position, toPosition: Position, viewRadius: TileInt)  extends Change
private final case class Show(position: Position, viewRadius: TileInt)                              extends Change
private final case class Hide(position: Position, viewRadius: TileInt)                              extends Change
private final case class Expand(position: Position, oldViewRadius: TileInt, newViewRadius: TileInt) extends Change

sealed trait RenderFogOfWar { fogOfWar: FogOfWar =>
  private[this] def _tileMapping: OrientationTileSetMapping = {
    if (!GameContext.isTestMode) GameApplicationAdapter.resources.orientationTileSetMapping else OrientationTileSetMapping.FAKE
  }
  private[this] def _tileSet: TileSet = {
    if (!GameContext.isTestMode) GameApplicationAdapter.resources.tileSet else TileSet.FAKE
  }

  private[this] val _tileMap: TileMap                = new TileMap(width, height, _tileSet)
  private[this] val _cloudTileMapLayer: TileMapLayer = _tileMap.getOrCreateLayer("CLOUD")
  _cloudTileMapLayer.fill(_tileMapping.get(TileType.Cloud, OrientationSet.ALL))

  def render(renderContext: GameRenderContext, x: PixelFloat, y: PixelFloat): Unit = {
    _tileMap.render(renderContext, x, y)
  }

  private[fogofwar] def process(changedPositionValues: Seq[Long]): Unit = {
    if (!changedPositionValues.isEmpty) {
      _tileMap.forceRender()
    }
    for (changedPositionValue <- changedPositionValues) {
      val changedPosition = Position(changedPositionValue)
      for (aroundPosition <- Orientation.ALL_VALUES.flatMap(changedPosition.around(_))) {
        val aroundOrientationSet = if (!isVisible(aroundPosition)) OrientationSet(Orientation.ALL_VALUES.filter(aroundPosition.around(_).exists(!isVisible(_)))) else OrientationSet()
        if (!aroundOrientationSet.isEmpty) {
          _cloudTileMapLayer.set(aroundPosition, _tileMapping.get(TileType.Cloud, aroundOrientationSet))
        } else {
          _cloudTileMapLayer.unset(aroundPosition)
        }
      }
    }
  }
}

object FogOfWar {
  private val executor = Executors.newSingleThreadExecutor()
}
final class FogOfWar(tiled: Tiled) extends RenderFogOfWar with Tiled {

  override def width  = tiled.width
  override def height = tiled.height

  private[this] val _visibilityCounts           = new AtomicIntegerArray(tiled.size)
  private[this] val _atomicChangeQueueReference = new AtomicReference(Queue[Change]())
  private[this] val _changedPositionValues      = ArrayBuffer[Long]()

  def process(): Unit = {
    val changes = _atomicChangeQueueReference.getAndUpdate(_ => Queue[Change]())
    if (!changes.isEmpty) {
      FogOfWar.executor.execute(new Runnable {
        override def run() = {
          try {
            changes.foreach {
              case Update(fromPosition, toPosition, viewRadius) => {
                decrement(fromPosition, viewRadius)
                increment(toPosition, viewRadius)
              }
              case Show(position, viewRadius) => {
                increment(position, viewRadius)
              }
              case Hide(position, viewRadius) => {
                decrement(position, viewRadius)
              }
              case Expand(position, oldViewRadius, newViewRadius) => {
                decrement(position, oldViewRadius)
                increment(position, newViewRadius)
              }
              case Reset => {
                clear()
              }
            }

            process(_changedPositionValues)
            _changedPositionValues.clear()

          } catch {
            case throwable: Throwable => {
              throwable.printStackTrace()
              System.exit(0)
            }
          }
        }
      })
    }
  }

  private def clear(): Unit = {
    for (i <- 0 until size) {
      _visibilityCounts.set(i, 0)
    }
    _changedPositionValues.clear()
    _atomicChangeQueueReference.set(Queue[Change]())
  }

  private def increment(position: Position, radius: TileInt): Unit = change(position, radius, _visibilityCounts.incrementAndGet(_))
  private def decrement(position: Position, radius: TileInt): Unit = change(position, radius, _visibilityCounts.decrementAndGet(_))

  private def change(position: Position, radius: TileInt, op: Int => Int): Unit = {
    iterate(position, radius) { relativePosition =>
      val beforeValue = _visibilityCounts.get(relativePosition)
      var newValue    = op(relativePosition)
      if (newValue < 0) throw new IllegalStateException(s"Invalid fog of war at $relativePosition")

      if ((beforeValue == 0 && newValue > 0) || (beforeValue > 0 && newValue == 0)) {
        _changedPositionValues.append(relativePosition.v)
      }
    }
  }

  def update(from: Position, to: Position, viewRadius: TileInt): Unit = {
    enqueue(Update(from, to, viewRadius))
  }

  def show(position: Position, viewRadius: TileInt): Unit = {
    enqueue(Show(position, viewRadius))
  }

  def hide(position: Position, viewRadius: TileInt): Unit = {
    enqueue(Hide(position, viewRadius))
  }

  def expand(position: Position, oldViewRadius: TileInt, newViewRadius: TileInt): Unit = {
    enqueue(Expand(position, oldViewRadius, newViewRadius))
  }

  def reset(): Unit = {
    enqueue(Reset)
  }

  private def enqueue(change: Change): Unit = {
    _atomicChangeQueueReference.updateAndGet(buffer => buffer.enqueue(change))
  }

  def isVisible(position: Position): Boolean = _visibilityCounts.get(position) > 0
  def isHidden(position: Position): Boolean  = !isVisible(position)
}
