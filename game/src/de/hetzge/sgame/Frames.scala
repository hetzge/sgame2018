package de.hetzge.sgame.frames

import de.hetzge.sgame.action._
import de.hetzge.sgame.format
import de.hetzge.sgame.export._
import de.hetzge.sgame.Application

import scala.language.implicitConversions
import scala.annotation.tailrec
import scala.concurrent.duration._
import scala.collection.mutable

import java.time.Instant
import java.io.FileWriter
import java.io.File

final case class FramesProto(
  current: FrameProto
)

final case class FrameProto(
  id: Long,
  next: Option[FrameProto],
  processed: Boolean,
  actions: Seq[DataProto],
  callbacks: Seq[ReferenceProto]
)

trait FrameCallback[F] extends ImportObject[F] {
  def apply(frameId: FrameId): Unit
}

object FrameDuration {
  def apply(duration: Duration): FrameDuration = FrameDuration(Math.ceil(duration.toMillis.toFloat / Frames.FRAME_TIME.toMillis).toLong)
}
final case class FrameDuration(frames: Long) extends AnyVal {
  def milliseconds = frames * Frames.FRAME_TIME.toMillis
}

object FrameId {
  @inline implicit def frameIdToLong(frameId: FrameId): Long = frameId.value
  @inline implicit def longToFrameId(id: Long): Long         = FrameId(id)
}
final case class FrameId(value: Long) extends AnyVal {
  @inline def next: FrameId                   = FrameId(value + 1L)
  @inline def +(frameDuration: FrameDuration) = FrameId(value + frameDuration.frames)
}

sealed trait FramesMode
case object ByTime   extends FramesMode
case object ByAction extends FramesMode
case object ByUpdate extends FramesMode

sealed trait ExportFrames extends ExportObject[FramesProto] { frames: Frames =>

  override def export()(implicit index: ExportedIndex) = FramesProto(
    current = current.export()
  )
}

object ImportFrames {

  def load(from: FramesProto)(implicit index: ImportedIndex): Frames = {
    val frames = new Frames()
    frames._currentFrame = ImportFrame.load(from.current)
    frames
  }
}

object Frames {
  val FRAMES_PER_SECOND: Int        = 30
  val FRAME_TIME: Duration          = (1000f / FRAMES_PER_SECOND.toFloat).milliseconds
  val EVERY_X_FRAMES_MAJOR_FRAME    = FRAMES_PER_SECOND
  val EMPTY_CALLBACK: Frame => Unit = f => ()
}
final class Frames(
    val mode: FramesMode = ByTime
) extends ExportFrames {
  private[this] var _callback: Frame => Unit = Frames.EMPTY_CALLBACK
  def setCallback(callback: Frame => Unit): Unit = {
    require(_callback == Frames.EMPTY_CALLBACK, "Callback already set")
    _callback = callback
  }

  private[frames] var _currentFrame                       = Frame(FrameId(0))
  private[frames] var _relativeFrameId: FrameId           = FrameId(0)
  private[frames] var _relativeFrameTime: Instant         = Instant.ofEpochMilli(0)
  private[frames] var _asyncCallbacks: List[Unit => Unit] = List()
  private[frames] val _logger                             = new FramesLogger("default")

  def isTimeForNextFrame: Boolean = mode match {
    case ByAction => next.hasActions
    case ByTime   => Instant.now.toEpochMilli > lastFrameExecution + Frames.FRAME_TIME.toMillis
    case ByUpdate => true
  }
  def lastFrameExecution = _relativeFrameTime.toEpochMilli + (_currentFrame.id.value - _relativeFrameId.value) * Frames.FRAME_TIME.toMillis

  def current: Frame                 = _currentFrame
  def next: Frame                    = _currentFrame.next
  def get(duration: Duration): Frame = get(current.id + FrameDuration(duration))
  def get(id: FrameId): Frame = {
    require(id != current.id)
    current.get(id)
  }
  def executeAsync(callback: Unit => Unit): Unit = _asyncCallbacks = _asyncCallbacks :+ callback
  def isCurrent(id: FrameId): Boolean            = id.value == current.id.value
  def isPast(id: FrameId): Boolean               = id.value < current.id.value
  def isFuture(id: FrameId): Boolean             = id.value > current.id.value
  def isCurrentOrFuture(id: FrameId): Boolean    = id.value >= current.id.value
  def isCurrentOrPast(id: FrameId): Boolean      = id.value <= current.id.value

  def isEveryDurationFrame(duration: Duration): Boolean = {
    return current.id.value % FrameDuration(duration).frames == 0
  }

  def isStarted: Boolean = {
    _relativeFrameTime.toEpochMilli != 0
  }

  def start(): Unit = {
    println("start frames")
    _relativeFrameId = _currentFrame.id
    _relativeFrameTime = Instant.now
  }

  def stop(): Unit = {
    _relativeFrameId = FrameId(0)
    _relativeFrameTime = Instant.ofEpochMilli(0)
  }

  /**
    * Executes the current frame if last frame time has passed and move forward to the next frame.
    */
  def processFrame(): Unit = {
    if (!isStarted) {
      start()

      // execute callbacks at start frame
      executeAsyncCallbacks()
    }

    mode match {
      case ByUpdate => callFrame()
      case _        => while (isTimeForNextFrame) callFrame()
    }
  }

  private[this] def callFrame(): Unit = {
    _currentFrame = next
    _currentFrame.markProcessed
    _callback(_currentFrame)
    _currentFrame.executeCallbacks
    executeAsyncCallbacks()
    _logger.log(_currentFrame)
  }

  private def executeAsyncCallbacks(): Unit = {
    for (asyncCallback <- _asyncCallbacks) {
      asyncCallback()
    }
    _asyncCallbacks = List()
  }
}

sealed trait ExportFrame extends ExportObject[FrameProto] { frame: Frame =>
  import pbdirect._

  def export()(implicit index: ExportedIndex) = FrameProto(
    id = id.value,
    next = if (hasNext) Some(next.export()) else None,
    processed = isProcessed,
    actions = _actions.map(DataProto(_)),
    callbacks = _callbacks.map(index(_))
  )
}

object ImportFrame {
  import pbdirect._
  def load(from: FrameProto)(implicit index: ImportedIndex): Frame = {
    val frame = Frame(FrameId(from.id))
    from.next match {
      case Some(next) => frame._next = ImportFrame.load(next)
      case None       => // ignore
    }

    frame._processed = from.processed
    frame._actions.clear()
    frame._actions ++= from.actions.map(_.as[Action])
    frame._callbacks.clear()
    frame._callbacks ++= from.callbacks.map(index[FrameCallback[_]](_))
    frame
  }
}

final case class Frame(val id: FrameId) extends ExportFrame {
  private[frames] var _next: Frame = null
  def hasNext: Boolean             = _next != null
  def next: Frame = {
    if (!hasNext) {
      _next = Frame(id.next)
    }
    _next
  }

  private[frames] var _processed: Boolean = false
  def markProcessed: Unit                 = _processed = true
  def isProcessed: Boolean                = _processed

  private[frames] val _actions: mutable.ListBuffer[Action]             = mutable.ListBuffer()
  private[frames] val _callbacks: mutable.ListBuffer[FrameCallback[_]] = mutable.ListBuffer()
  @tailrec
  final def get(id: FrameId): Frame = {
    if (this.id > id) throw new IllegalStateException(s"Missed frame: current: ${this.id}, requested: $id")
    else if (this.id == id) this
    else next.get(id)
  }

  def add(action: Action): Unit                = if (!_processed) _actions.append(action) else throw new IllegalStateException(s"already processed frame: ${this.id}")
  def addAll(actions: Seq[Action]): Unit       = if (!_processed) _actions.append(actions: _*) else throw new IllegalStateException(s"already processed frame: ${this.id}")
  def add(callback: FrameCallback[_]): Unit    = if (!_processed) _callbacks.append(callback) else throw new IllegalStateException(s"already processed frame: ${this.id}")
  def remove(callback: FrameCallback[_]): Unit = if (!_processed) (_callbacks -= callback).ensuring(true) else throw new IllegalStateException(s"already processed frame: ${this.id}")

  def getActions: Seq[Action] =
    _actions.sortWith((a, b) => {
      val result = a.nanoTime.compareTo(b.nanoTime)
      if (result != 0) result < 0
      else throw new IllegalStateException(s"Conflicting actions: '$a' vs '$b'")
    })
  def hasActions: Boolean = !_actions.isEmpty
  def callbackCount: Int  = _callbacks.size
  private[frames] def executeCallbacks: Unit = {
    for (callback <- _callbacks) {
      callback(id)
    }
  }

  def iterator(to: FrameId) = new FramesIterator(this, to)

  def isMajorFrame: Boolean = id.value % Frames.EVERY_X_FRAMES_MAJOR_FRAME == 0
}

/** Iterate inclusive 'to'. */
final class FramesIterator(val from: Frame, val to: FrameId) extends Iterator[Frame] {
  private var frame    = from
  override def hasNext = frame.id.value <= to.value
  override def next() = {
    val current = frame
    frame = frame.next
    current
  }
}

final class FramesLogger(name: String) {
  private[this] val _fileName = s"actions_$name.log"
  private[this] val _logFile  = new File(_fileName)

  deleteLog()

  def log(frame: Frame): Unit = {
    if (Application.isDebugMode) {
      val _writer       = new FileWriter(_fileName, true)
      val actions       = frame.getActions
      val callbackCount = frame.callbackCount
      if (callbackCount > 0) {
        _writer.write(s"${frame.id.value}: ${callbackCount}${System.lineSeparator()}")
      }
      for (action <- actions) {
        _writer.write(s"${frame.id.value}: ${action}${System.lineSeparator()}")
      }
      _writer.close()
    }
  }

  private[this] def deleteLog(): Unit = {
    if (_logFile.exists()) {
      _logFile.delete()
    }
  }
}
