package de.hetzge.sgame.game

import java.util.{Random, UUID}

import de.hetzge.sgame.render._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.frames._
import de.hetzge.sgame.action._
import de.hetzge.sgame.network._
import de.hetzge.sgame.server._
import de.hetzge.sgame.input._
import de.hetzge.sgame.settings._
import de.hetzge.sgame.area._
import de.hetzge.sgame.base._
import de.hetzge.sgame.player._
import de.hetzge.sgame.ui._
import de.hetzge.sgame.person._
import de.hetzge.sgame.minimap._
import de.hetzge.sgame.Application
import de.hetzge.sgame.export._
import de.hetzge.sgame.format
import de.hetzge.sgame.building._

import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.math.RandomXS128


object GameContext {
  private[this] var instance: Game = null
  private[this] def get: Game = {
    require(isSet, "No game is set in context. Set the current game via GameContext.set(game) first.")
    instance
  }
  def optionalGet: Option[Game]                   = Option(instance)
  def set(game: Game): Unit                       = instance = game
  def isSet: Boolean                              = instance != null
  @inline def ifSet(callback: Game => Unit): Unit = if (isSet) callback(game)

  def isTestMode: Boolean = !isSet || get.gameType.isTestMode

  def game          = get
  def frames        = game.frames
  def area          = game.area
  def localSettings = game.localSettings
}

sealed trait GameType extends Serializable {
  val useNetwork: Boolean
  val framesMode: FramesMode
  val isTestMode: Boolean
}

case object SingleplayerGameType extends GameType {
  override val useNetwork = false
  override val framesMode = ByTime
  override val isTestMode = false
}

case object MultiplayerGameType extends GameType {
  override val useNetwork = true
  override val framesMode = ByAction
  override val isTestMode = false
}

/** just for test */
case object TestGameType extends GameType {
  override val useNetwork = false
  override val framesMode = ByUpdate
  override val isTestMode = true
}

final case class GameConfiguration(
    playerCount: Int
)

sealed trait ExportGame extends ExportObject[format.Game] with ExportIndexedObject { game: Game =>

  override def export()(implicit index: ExportedIndex) = format.Game(
    area = Some(index(area)),
    frames = Some(frames.export())
  )

  override def index() = ExportIndex(
    objects = Set(area)
  )
}

object ImportGame {

  def load(f: format.Game)(implicit index: ImportedIndex): Game = {
    val gameType = SingleplayerGameType
    new Game(
      key = EntityKey("Game"),
      gameType = gameType,
      network = None,
      gameConfiguration = GameConfiguration(
        playerCount = 1
      ),
      loadArea = index[Area](f.area.get),
      frames = ImportFrames.load(f.frames.get)
    )
  }

}

object Game {
  def apply(
      key: EntityKey,
      gameType: GameType,
      network: Option[Network],
      gameConfiguration: GameConfiguration,
      loadArea: => Area = Area.createEmpty(100, 100)
  ): Game = {
    new Game(
      key,
      gameType,
      network,
      gameConfiguration,
      frames = new Frames(gameType.framesMode),
      loadArea
    )
  }
}

final class Game(
    val key: EntityKey,
    val gameType: GameType,
    val network: Option[Network],
    val gameConfiguration: GameConfiguration,
    val frames: Frames,
    loadArea: => Area = Area.createEmpty(100, 100)
) extends GameInput
    with ExportGame {
  GameContext.set(this)
  frames.setCallback(onFrame(_))

  /** true if the application should render */
  private[this] var _render = false

  lazy val players: Players                   = new Players(gameConfiguration.playerCount)
  lazy val renderContext: GameRenderContext   = new GameRenderContext()
  lazy val localSettings                      = new LocalSettings(game = this)
  lazy val random: Random                     = new RandomXS128(1234567)
  lazy val area: Area                         = loadArea
  lazy val gameUi: GameUi                     = new GameUi(game = this)
  lazy val inputMultiplexer: InputMultiplexer = new InputMultiplexer(gameUi.input, this)

  gameType match {
    case SingleplayerGameType =>
      players.nextEmptyPlayer match {
        case Some(emptyPlayer) => {
          val player = new NormalPlayer(emptyPlayer.id)
          players.setPlayer(player)
          localSettings.setPlayer(player)
        }
        case None => {
          throw new IllegalStateException("No empty slot available")
        }
      }
    case _ => // ignore
  }

  def area(x: TileInt, y: TileInt): Option[Area] = {
    if (area.isValid(x, y)) Some(area) else None
  }

  def start(): Unit = {
    require(!gameType.isTestMode, "Game can't be started in test mode")

    setupCameraAtStartPerson()
    selectStartPerson()

    gameLoopThread.start()
    _render = true
  }

  def update(): Unit = {
    network match {
      case Some(network) => network.handle(handleMessage(_, _))
      case None          => frames.processFrame()
    }
    if (gameType != TestGameType) {
      // prevent cpu from running 100%
      Utils.sleep(1)
    }
  }

  def render(): Unit = {
    if (_render) {
      handleInput(renderContext)
      renderContext.render(this)
      gameUi.render()
    }
  }

  def renderContent(): Unit = {
    area.render(
      renderContext,
      x = 0f,
      y = 0f,
      z = 0f,
      localSettings.showFogOfWar,
      localSettings.showDebug,
      localSettings.showBorders,
      localSettings.showOwnership
    )
    renderInput(renderContext, 0f, 0f)
  }

  def resize(width: Int, height: Int): Unit = {
    renderContext.resize(width: Int, height: Int)
    gameUi.resize(width, height)
  }

  def dispose(): Unit = {
    renderContext.dispose()
    gameUi.dispose()
  }

  def handleMessage(client: Client, message: Object): Unit = {
    message match {
      case TriggerFrameMessage(frameId, actions) => onTriggerFrame(frameId, actions)
      case playerConnectedMessage: PlayerConnectedMessage => {
        val networkPlayer = playerConnectedMessage.networkPlayer
        players.setPlayer(networkPlayer)
        if (network.get.client == playerConnectedMessage.client) {
          localSettings.setPlayer(networkPlayer)
          println(s"Local player set: $networkPlayer")
        } else {
          println(s"New player connected: $networkPlayer")
        }
      }
      case _ => {}
    }
  }

  private def onTriggerFrame(frameId: FrameId, actions: Seq[Action]): Unit = {
    require(frames.next.id == frameId, s"next frame id '${frames.next.id}' do not match '$frameId'")

    if (!actions.isEmpty) {
      println(s"received action '${actions.mkString(",")}'")
      frames.next.addAll(actions)
    } else {
      frames.next.add(NoopAction)
    }

    frames.processFrame()
  }

  private def onFrame(frame: Frame): Unit = {
    area.update()

    for (action <- frame.getActions) {
      action match {
        case areaAction: AreaAction => area(areaAction)
        case NoopAction             => // ignore
      }
    }
  }

  def submit(action: Action): Unit = {
    network match {
      case Some(network) => network.sendAll(ActionMessage(action))
      case None          => frames.next.add(action)
    }
  }

  private def setupCameraAtStartPerson(): Unit = {
    findStartPerson() match {
      case Some(startPerson) => renderContext.setCamera(startPerson.position.x, startPerson.position.y)
      case None =>
        findStartBuilding() match {
          case Some(startBuilding) => renderContext.setCamera(startBuilding.position.x, startBuilding.position.y)
          case None                => // ignore
        }
    }
  }

  private def selectStartPerson(): Unit = {
    for (startPerson <- findStartPerson()) {
      localSettings.setSelection(PersonSelection(startPerson))
    }
  }

  private def findStartPerson(): Option[Person] = {
    area.persons.collectFirst { case startPerson if (startPerson.personType == PersonType.Start) && startPerson.playerId == localSettings.player.id => startPerson }
  }

  private def findStartBuilding(): Option[Building] = {
    area.buildings.collectFirst { case startBuilding if (startBuilding.buildingType == BuildingType.Start) && startBuilding.playerId == localSettings.player.id => startBuilding }
  }

  object gameLoopThread extends Thread("game loop") {
    setUncaughtExceptionHandler((thread, exception) => {
      exception.printStackTrace()
      System.exit(0)
    })
    setDaemon(true)
    override def run() {
      while (true) {
        update()
        Application.cpuFpsCounter.count()
      }
    }
  }
}
