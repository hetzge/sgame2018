package de.hetzge.sgame.input

import de.hetzge.sgame.action._
import de.hetzge.sgame.area._
import de.hetzge.sgame.building._
import de.hetzge.sgame.game._
import de.hetzge.sgame.render._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.way._
import de.hetzge.sgame.waypoint._
import de.hetzge.sgame.item._
import de.hetzge.sgame.settings._
import de.hetzge.sgame.person._
import de.hetzge.sgame.export._

import com.badlogic.gdx.Input.{Keys => GdxKeys}
import com.badlogic.gdx.{Gdx, InputAdapter}
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType
import com.badlogic.gdx.math.MathUtils
import org.lwjgl.util.vector.Vector2f

object RenderInput {
  val LIGHT_TRANSPARENT_WHITE = new Color(1f, 1f, 1f, 0.2f)
  val TRANSPARENT_WHITE       = new Color(1f, 1f, 1f, 0.8f)
  val LIGHT_TRANSPARENT_RED   = new Color(1f, 0f, 0f, 0.2f)
  val TRANSPARENT_RED         = new Color(1f, 0f, 0f, 0.8f)
}
sealed trait GameRenderInput { game: Game =>
  @transient private[input] var lastRenderContext: GameRenderContext = null

  protected def renderInput(renderContext: GameRenderContext, x: PixelFloat, y: PixelFloat): Unit = {
    lastRenderContext = renderContext

    game.localSettings.areaInput match {
      case SelectionAreaInput                             => renderSelectionInput(renderContext)
      case buildWayAreaInput: BuildWayAreaInput           => renderBuildWayInput(renderContext)
      case buildBuildingAreaInput: BuildBuildingAreaInput => renderBuildBuildingInput(renderContext, buildBuildingAreaInput)
      case buildStockAreaInput: BuildStockAreaInput       => renderStockAreaInput(renderContext)
      case _                                              => renderTileCursor(renderContext)
    }

    game.localSettings.selection match {
      case Some(selection) =>
        selection match {
          case PersonSelection(person)     => renderPersonSelection(renderContext, person)
          case BuildingSelection(building) => renderBuildingSelection(renderContext, building)
          case _                           => {}
        }
      case _ => {}
    }

  }

  private def renderSelectionInput(renderContext: GameRenderContext): Unit = {
    renderTileCursor(renderContext)
  }

  private def renderBuildWayInput(renderContext: GameRenderContext): Unit = {
    renderDrag(renderContext, oneDimension = true)
    renderTileCursor(renderContext)
  }

  private def renderBuildBuildingInput(renderContext: GameRenderContext, buildBuildingAreaInput: BuildBuildingAreaInput): Unit = {
    renderContext.shapeRenderer.begin(ShapeType.Filled)

    val currentPlayerId = game.localSettings.player.id
    val buildingType    = buildBuildingAreaInput.buildingType
    val canBuild        = game.area.canBuildBuilding(game.area.position(renderContext.mouseTileX, renderContext.mouseTileY), buildingType, currentPlayerId)
    renderContext.shapeRenderer.setColor(if (canBuild) RenderInput.TRANSPARENT_WHITE else RenderInput.TRANSPARENT_RED)

    // building
    if (canBuild) {
      renderContext.resources.buildingLotRenderable.render(
        renderContext,
        userData = null,
        x = renderContext.mouseTileX.pixelFloat,
        y = renderContext.mouseTileY.pixelFloat,
        width = buildingType.width.pixelFloat,
        height = buildingType.height.pixelFloat
      )
    } else {
      renderContext.shapeRenderer.rect(
        renderContext.mouseTileX.pixelFloat,
        renderContext.mouseTileY.pixelFloat,
        buildingType.width.pixelFloat,
        buildingType.height.pixelFloat
      )
    }

    // door
    renderContext.shapeRenderer.rect(
      renderContext.mouseTileX.pixelFloat + buildingType.doorX.pixelFloat,
      renderContext.mouseTileY.pixelFloat + buildingType.doorY.pixelFloat,
      TILE_SIZE_FLOAT,
      TILE_SIZE_FLOAT
    )

    renderContext.shapeRenderer.end()
  }

  private def renderStockAreaInput(renderContext: GameRenderContext): Unit = {
    renderContext.shapeRenderer.begin(ShapeType.Filled)

    game.localSettings.selection match {
      case Some(BuildingSelection(selectedBuilding)) => {
        for (aroundPosition <- selectedBuilding.around(game.area)) {
          val canBuild = BuildStockAreaInput.canBuild(game, aroundPosition)
          val color    = if (canBuild) RenderInput.LIGHT_TRANSPARENT_WHITE else RenderInput.LIGHT_TRANSPARENT_RED
          renderContext.shapeRenderer.setColor(color)
          renderContext.shapeRenderer.rect(
            aroundPosition.x * TILE_SIZE_FLOAT,
            aroundPosition.y * TILE_SIZE_FLOAT,
            TILE_SIZE_FLOAT,
            TILE_SIZE_FLOAT
          )
        }
      }
      case _ => {
        // ignore
      }
    }

    val canBuild = BuildStockAreaInput.canBuild(game, game.area.position(renderContext.mouseTileX, renderContext.mouseTileY))
    val color    = if (canBuild) RenderInput.TRANSPARENT_WHITE else RenderInput.TRANSPARENT_RED
    renderContext.shapeRenderer.setColor(color)

    renderContext.shapeRenderer.rect(
      renderContext.mouseTileX.pixelFloat,
      renderContext.mouseTileY.pixelFloat,
      TILE_SIZE_FLOAT,
      TILE_SIZE_FLOAT
    )

    renderContext.shapeRenderer.end()
  }

  private def renderDrag(renderContext: GameRenderContext, oneDimension: Boolean): Unit = {
    if (renderContext.downX != PixelFloat(0) || renderContext.downY != PixelFloat(0)) {
      for {
        downArea    <- game.area(renderContext.downX.tileInt, renderContext.downY.tileInt)
        currentArea <- game.area(renderContext.mouseTileX, renderContext.mouseTileY)
        if (downArea == currentArea)
        if (renderContext.downButton != -1 && renderContext.downButton != com.badlogic.gdx.Input.Buttons.RIGHT)
      } yield {
        renderDrag(renderContext, renderContext.downX.tileInt, renderContext.downY.tileInt, renderContext.mouseTileX, renderContext.mouseTileY, oneDimension)
      }
    }
  }

  private def renderDrag(renderContext: GameRenderContext, downX: TileInt, downY: TileInt, currentX: TileInt, currentY: TileInt, oneDimension: Boolean): Unit = {
    val x      = Math.min(downX.pixelFloat, currentX.pixelFloat)
    val y      = Math.min(downY.pixelFloat, currentY.pixelFloat)
    val width  = Math.abs(downX.pixelFloat - currentX.pixelFloat) + TILE_SIZE_FLOAT
    val height = Math.abs(downY.pixelFloat - currentY.pixelFloat) + TILE_SIZE_FLOAT
    val color  = if (!oneDimension || PixelFloat(width) == TILE_SIZE_FLOAT || PixelFloat(height) == TILE_SIZE_FLOAT) Color.BLUE else Color.RED

    renderContext.shapeRenderer.begin()
    renderContext.shapeRenderer.setColor(color)
    renderContext.shapeRenderer.rect(x, y, width, height)
    renderContext.shapeRenderer.rect(x, y, TILE_SIZE_FLOAT, TILE_SIZE_FLOAT)
    renderContext.shapeRenderer.end()
  }

  private def renderTileCursor(renderContext: GameRenderContext, color: Color = Color.WHITE): Unit = {
    renderContext.shapeRenderer.begin()
    renderContext.shapeRenderer.setColor(color)
    renderContext.shapeRenderer.rect(renderContext.mouseTileX.pixelFloat, renderContext.mouseTileY.pixelFloat, TILE_SIZE_FLOAT, TILE_SIZE_FLOAT)
    renderContext.shapeRenderer.end()
  }

  private def renderPersonSelection(renderContext: GameRenderContext, person: Person): Unit = {
    val path  = person.getPath
    val steps = person.position :: path.steps

    if (!steps.isEmpty) {
      PathAnimation
        .fromPositions(
          steps,
          renderContext.resources.arrowRenderable,
          renderContext.resources.shape.red,
          TILE_SIZE_FLOAT
        )
        .render(renderContext, null, 0f, 0f, 10f)
    }

    renderContext.resources.drawRectangle(
      renderContext,
      renderContext.resources.shape.whiteTransparent,
      x = person.renderX,
      y = person.renderY,
      z = 0f,
      width = person.width.pixelFloat,
      height = person.height.pixelFloat,
      thickness = 2f * renderContext.camera.zoom
    )
  }

  private def renderBuildingSelection(renderContext: GameRenderContext, building: Building): Unit = {
    val thickness = 10f
    renderContext.resources.drawRectangle(
      renderContext,
      renderContext.resources.shape.whiteTransparent,
      x = building.position.x.pixelFloat,
      y = building.position.y.pixelFloat,
      z = 0f,
      width = building.width.pixelFloat,
      height = building.height.pixelFloat,
      thickness = thickness * renderContext.camera.zoom
    )
  }
}

sealed trait GameHandleInput { game: Game =>

  private[input] var _scrollAmount = 0f
  private[input] var _relative     = new Vector2f()

  protected def handleInput(renderContext: GameRenderContext): Unit = {
    if (_relative.x != 0f || _relative.y != 0f) {
      val horizontal = MathUtils.clamp((_relative.x - Gdx.input.getX) / 100f, -2f, 2f)
      val vertical   = MathUtils.clamp((_relative.y - Gdx.input.getY) / 100f, -2f, 2f)

      renderContext.moveCamera(-horizontal, -vertical)
    }

    if (Gdx.input.isKeyPressed(GdxKeys.UP)) {
      renderContext.moveCamera(0f, -1f)
    } else if (Gdx.input.isKeyPressed(GdxKeys.DOWN)) {
      renderContext.moveCamera(0f, 1f)
    } else if (Gdx.input.isKeyPressed(GdxKeys.LEFT)) {
      renderContext.moveCamera(-1f, 0f)
    } else if (Gdx.input.isKeyPressed(GdxKeys.RIGHT)) {
      renderContext.moveCamera(1f, 0f)
    }

    if (Gdx.input.isKeyPressed(GdxKeys.PAGE_DOWN)) {
      _scrollAmount += 3f
    } else if (Gdx.input.isKeyPressed(GdxKeys.PAGE_UP)) {
      _scrollAmount -= 3f
    } else if (Gdx.input.isKeyPressed(GdxKeys.ESCAPE)) {
      game.localSettings.deselectAll()
      game.localSettings.setAreaInput(SelectionAreaInput)
    } else if (Gdx.input.isKeyJustPressed(GdxKeys.F8)) {
      game.localSettings.showDebug = !game.localSettings.showDebug
    } else if (Gdx.input.isKeyJustPressed(GdxKeys.F9)) {
      game.localSettings.showFogOfWar = !game.localSettings.showFogOfWar
    } else if (Gdx.input.isKeyJustPressed(GdxKeys.F10)) {
      game.localSettings.showBorders = !game.localSettings.showBorders
    } else if (Gdx.input.isKeyJustPressed(GdxKeys.F11)) {
      game.localSettings.showOwnership = !game.localSettings.showOwnership
    } else if (Gdx.input.isKeyJustPressed(GdxKeys.F12)) {
      Gdx.app.exit()
    } else if (Gdx.input.isKeyJustPressed(GdxKeys.F1)) {
      Export.export(game)
      Export.export(game, "last")
    }

    if (Gdx.input.isKeyJustPressed(GdxKeys.HOME)) {
      renderContext.increaseOpacitiy()
    } else if (Gdx.input.isKeyJustPressed(GdxKeys.END)) {
      renderContext.decreaseOpacity()
    }

    if (_scrollAmount != 0f) {
      renderContext.zoomCamera(_scrollAmount)
      _scrollAmount = 0f
    }
  }
}

trait GameInput extends InputAdapter with GameRenderInput with GameHandleInput { game: Game => 
  override def touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int) = {
    lastRenderContext.setupTouchDown(
      downX = PixelFloat(lastRenderContext.unprojectedMousePosition.x),
      downY = PixelFloat(lastRenderContext.unprojectedMousePosition.y),
      downButton = button
    )

    if (button == com.badlogic.gdx.Input.Buttons.RIGHT) {
      _relative.set(Gdx.input.getX, Gdx.input.getY())
    }

    false
  }

  override def touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int) = {
    val upX = PixelFloat(lastRenderContext.unprojectedMousePosition.x)
    val upY = PixelFloat(lastRenderContext.unprojectedMousePosition.y)

    for {
      downArea <- game.area(lastRenderContext.downX.tileInt, lastRenderContext.downY.tileInt)
      upArea   <- game.area(upX.tileInt, upY.tileInt)
      if (downArea == upArea)
    } yield {
      if (upX == lastRenderContext.downX && upY == lastRenderContext.downY) {
        game.localSettings.areaInput.tileClicked(game, upArea, x = upX - upArea.position.x, y = upY - upArea.position.y, button = button)
      } else {
        if (button != com.badlogic.gdx.Input.Buttons.RIGHT) {
          game.localSettings.areaInput.tilesDragged(
            game,
            upArea,
            fromX = lastRenderContext.downX - downArea.position.x,
            fromY = lastRenderContext.downY - downArea.position.y,
            toX = upX - upArea.position.x,
            toY = upY - upArea.position.y,
            button = button
          )
        }
      }
    }

    lastRenderContext.setupTouchDown(
      downX = PixelFloat(0),
      downY = PixelFloat(0),
      downButton = -1
    )

    if (button == com.badlogic.gdx.Input.Buttons.RIGHT) {
      _relative.set(0f, 0f)
    }

    false
  }

  override def scrolled(amount: Int) = {
    _scrollAmount = 4f * amount
    true
  }
}

sealed trait AreaInput {
  def tileClicked(game: Game, area: Area, x: PixelFloat, y: PixelFloat, button: Int): Unit
  def tilesDragged(game: Game, area: Area, fromX: PixelFloat, fromY: PixelFloat, toX: PixelFloat, toY: PixelFloat, button: Int)
}

case object NoopAreaInput extends AreaInput {
  override def tileClicked(game: Game, area: Area, x: PixelFloat, y: PixelFloat, button: Int)                                            = {}
  override def tilesDragged(game: Game, area: Area, fromX: PixelFloat, fromY: PixelFloat, toX: PixelFloat, toY: PixelFloat, button: Int) = {}
}

case object SelectionAreaInput extends AreaInput {
  override def tileClicked(game: Game, area: Area, x: PixelFloat, y: PixelFloat, button: Int) = {
    game.localSettings.deselectAll()

    for (player <- game.localSettings.playerOption) {
      val position = area.position(x.tileInt, y.tileInt)

      val selection = area.persons(position).filter(_.playerId == player.id).headOption match {
        case Some(person) => PersonSelection(person)
        case None =>
          area.building(position).filter(_.playerId == player.id) match {
            case Some(building) => BuildingSelection(building)
            case None           => PositionSelection(position)
          }
      }

      game.localSettings.setSelection(selection)
    }
  }
  override def tilesDragged(game: Game, area: Area, fromX: PixelFloat, fromY: PixelFloat, toX: PixelFloat, toY: PixelFloat, button: Int) = {}
}

final case class BuildWayAreaInput(wayType: WayType) extends AreaInput {
  override def tileClicked(game: Game, area: Area, x: PixelFloat, y: PixelFloat, button: Int) = {
    val currentPlayerId = game.localSettings.player.id
    val tileX           = x.tileInt
    val tileY           = y.tileInt
    if (button == com.badlogic.gdx.Input.Buttons.LEFT) {
      game.submit(BuildWayAction(tileX, tileY, wayType, currentPlayerId.value))
    } else if (button == com.badlogic.gdx.Input.Buttons.RIGHT) {
      game.submit(BuildWaypointAction(tileX, tileY, DefaultWaypointType))
    } else if (button == com.badlogic.gdx.Input.Buttons.MIDDLE) {
      game.submit(DestroyWayAction(tileX, tileY))
    }
  }
  override def tilesDragged(game: Game, area: Area, fromX: PixelFloat, fromY: PixelFloat, toX: PixelFloat, toY: PixelFloat, button: Int) = {
    val currentPlayerId = game.localSettings.player.id
    val fromTileX       = fromX.tileInt
    val fromTileY       = fromY.tileInt
    val toTileX         = toX.tileInt
    val toTileY         = toY.tileInt
    if (fromTileX != toTileX ^ fromTileY != toTileY) {
      for (x <- Math.min(fromTileX, toTileX) to Math.max(fromTileX, toTileX)) {
        for (y <- Math.min(fromTileY, toTileY) to Math.max(fromTileY, toTileY)) {
          if (button == com.badlogic.gdx.Input.Buttons.LEFT) {
            game.submit(BuildWayAction(x, y, wayType, currentPlayerId.value))
          } else if (button == com.badlogic.gdx.Input.Buttons.MIDDLE) {
            game.submit(DestroyWayAction(x, y))
          }
        }
      }
    }
  }
}

final case class BuildBuildingAreaInput(buildingType: BuildingType) extends AreaInput {
  override def tileClicked(game: Game, area: Area, x: PixelFloat, y: PixelFloat, button: Int) = {
    val currentPlayerId         = game.localSettings.player.id
    val newBuildingBuildingType = if (!buildingType.needItemsToBuild.isEmpty) BuildingType.BuildingLot(buildingType) else buildingType
    game.submit(BuildBuildingAction(x.tileInt, y.tileInt, newBuildingBuildingType, currentPlayerId.value))
  }
  override def tilesDragged(game: Game, area: Area, fromX: PixelFloat, fromY: PixelFloat, toX: PixelFloat, toY: PixelFloat, button: Int) = {
    // ignore
  }
}

object BuildStockAreaInput {
  private val dummyStockBuildingType = BuildingType.Stock(DummyItemType)
  def canBuild(game: Game, position: Position): Boolean = {
    val currentPlayerId = game.localSettings.player.id
    game.localSettings.selection.collect { case b: BuildingSelection => b }.map(_.building).map(building => game.area.isAroundBuilding(position, building)).getOrElse(false) && game.area.canBuildBuilding(position, dummyStockBuildingType, currentPlayerId)
  }
}
final case class BuildStockAreaInput(itemType: ItemType) extends AreaInput {
  override def tileClicked(game: Game, area: Area, x: PixelFloat, y: PixelFloat, button: Int) = {
    val position        = area.position(x.tileInt, y.tileInt)
    val currentPlayerId = game.localSettings.player.id

    if (BuildStockAreaInput.canBuild(game, position)) {
      game.submit(BuildBuildingAction(x.tileInt, y.tileInt, BuildingType.Stock(itemType), currentPlayerId.value))
    }
  }
  override def tilesDragged(game: Game, area: Area, fromX: PixelFloat, fromY: PixelFloat, toX: PixelFloat, toY: PixelFloat, button: Int) = {
    // ignore
  }
}

final case object GotoAreaInput extends AreaInput {
  override def tileClicked(game: Game, area: Area, x: PixelFloat, y: PixelFloat, button: Int) = {
    val position = area.position(x.tileInt, y.tileInt)

    for (person <- game.localSettings.selectedPerson) {
      if (person.personType.userControlled) {
        game.submit(GotoAction(person.id, position.x, position.y))
      }
    }
  }
  override def tilesDragged(game: Game, area: Area, fromX: PixelFloat, fromY: PixelFloat, toX: PixelFloat, toY: PixelFloat, button: Int) = {}
}
