package de.hetzge.sgame.item

import de.hetzge.sgame._
import de.hetzge.sgame.base._
import de.hetzge.sgame.render._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.waypoint._
import de.hetzge.sgame.stock._
import de.hetzge.sgame.game._
import de.hetzge.sgame.job._
import de.hetzge.sgame.person._
import de.hetzge.sgame.format
import de.hetzge.sgame.export._
import de.hetzge.sgame.format._

import scala.collection.mutable
import scala.collection.mutable.{Map, Set}
import de.hetzge.sgame.format.ItemEconomy.ItemMapping

final case class ItemProto(
  itemClass: String,
  itemType: String,
  stock: ReferenceProto,
  usage: String,
  prio: Int,
  targetStock: ReferenceProto,
  nextStock: ReferenceProto
)

final case class ItemEconomyProto(
  supplied: List[ItemMapping],
  supplies: List[ReferenceProto],
  demands: List[ReferenceProto]
)

final case class ItemMappingProto(
  from: Reference,
  to: Reference
)


final case class ItemRenderableSet(
    default: Renderable,
    supply: Renderable,
    supplied: Renderable,
    demanded: Renderable,
    demand: Renderable
) {
  def apply(item: Item): Renderable = {
    item match {
      case _: SupplyItem   => supply
      case _: SuppliedItem => supplied
      case _: DemandItem   => demand
      case _: DemandedItem => demanded
      case _: NoItem       => GameApplicationAdapter.resources.emptyRenderable
      case _: BlockedItem  => default
    }
  }
}

sealed trait ItemDirection
object ItemDirection {
  case object In      extends ItemDirection
  case object Out     extends ItemDirection
  case object Neutral extends ItemDirection
}

sealed trait ItemType extends Serializable {
  def renderableSet: ItemRenderableSet
  def exportType: format.ItemType
}
object ItemType {
  def apply(from: format.ItemType): ItemType = {
    import format.ItemType._
    from match {
      case DUMMY       => DummyItemType
      case WOOD        => WoodItemType
      case WOOD_BLANK  => WoodBlankItemType
      case STONE       => StoneItemType
      case STONE_BLOCK => StoneBlockItemType
      case CROP        => CropItemType
      case TOOL        => ToolItemType
      case itemType    => throw new IllegalStateException(s"Can't read '$itemType'")
    }
  }
}

case object DummyItemType extends ItemType {
  @transient override lazy val renderableSet = GameApplicationAdapter.resources.item.dummy
  override def exportType                    = format.ItemType.DUMMY
}
case object WoodItemType extends ItemType {
  @transient override lazy val renderableSet = GameApplicationAdapter.resources.item.wood
  override def exportType                    = format.ItemType.WOOD
}
case object WoodBlankItemType extends ItemType {
  @transient override lazy val renderableSet = GameApplicationAdapter.resources.item.woodBlank
  override def exportType                    = format.ItemType.WOOD_BLANK
}
case object StoneItemType extends ItemType {
  @transient override lazy val renderableSet = GameApplicationAdapter.resources.item.stone
  override def exportType                    = format.ItemType.STONE
}
case object StoneBlockItemType extends ItemType {
  @transient override lazy val renderableSet = GameApplicationAdapter.resources.item.stoneBlock
  override def exportType                    = format.ItemType.STONE_BLOCK
}
case object CropItemType extends ItemType {
  @transient override lazy val renderableSet = GameApplicationAdapter.resources.item.dummy // TODO
  override def exportType                    = format.ItemType.CROP
}
case object ToolItemType extends ItemType {
  @transient override lazy val renderableSet = GameApplicationAdapter.resources.item.dummy // TODO
  override def exportType                    = format.ItemType.TOOL
}

sealed trait ItemUsage {
  def key: String
  def canReachEachOther(item: Item, otherItem: Item): Boolean
  def formatUsage: format.ItemUsage

  override def toString() = key
}
object ItemUsage {
  case object Default extends ItemUsage {
    override def key                                            = "DEFAULT"
    override def canReachEachOther(item: Item, otherItem: Item) = item.isSameWaypointGroup(otherItem)
    override def formatUsage                                    = format.ItemUsage.DEFAULT
  }
  case object Way extends ItemUsage {
    override def key                                            = "WAY"
    override def canReachEachOther(item: Item, otherItem: Item) = item.isSameWayGroup(otherItem)
    override def formatUsage                                    = format.ItemUsage.WAY
  }

  def read(from: format.ItemUsage): ItemUsage = {
    import format.ItemUsage._
    from match {
      case DEFAULT => Default
      case WAY     => Way
      case usage   => throw new IllegalStateException(s"Can't read '$usage'")
    }
  }
}

sealed trait RenderItem { item: Item =>
  def render(renderContext: GameRenderContext, x: PixelFloat, y: PixelFloat, z: PixelFloat): Unit = {
    itemType.renderableSet(item).render(renderContext, userData = null, x, y, z)
  }
}

object PrioItem {
  def ordering[T <: PrioItem] = new Ordering[T] {
    def compare(a: T, b: T) = a.prio.compareTo(b.prio)
  }
}
sealed trait PrioItem { item: Item =>
  def prio: Integer
}

sealed trait Item extends RenderItem with ExportObject[format.Item] {
  val itemType: ItemType
  def usage: ItemUsage
  def direction: ItemDirection
  def stock: Stock

  def isReal: Boolean

  def canReach(otherItem: Item): Boolean = usage.canReachEachOther(this, otherItem)

  @inline def isSameWaypointGroup(otherItem: Item): Boolean = stock.isSameWaypointGroup(otherItem.stock)
  @inline def isSameWayGroup(otherItem: Item): Boolean      = stock.isSameWayGroup(otherItem.stock)
  @inline def isSameMaster(otherItem: Item): Boolean        = stock.master == otherItem.stock.master
  @inline def isSameStock(otherItem: Item): Boolean         = stock == otherItem.stock
  @inline def isSameItemType(otherItem: Item): Boolean      = itemType == otherItem.itemType
  @inline def isSameUsage(otherItem: Item): Boolean         = usage == otherItem.usage

  private[item] def replaceInStockWith(other: Item): Unit = stock.replace(this, other)
  private[item] def removeFromStock(): Unit               = stock.remove(this)

  private[this] def wayGroup      = stock.entity.stockWay.map(_.getGroup).getOrElse(0)
  private[this] def waypointGroup = stock.entity.stockWaypoint.map(_.getGroup).getOrElse(0)

  override def toString: String = s"${getClass.getSimpleName}($itemType, $usage, $wayGroup, $waypointGroup${if (isInstanceOf[PrioItem]) ", " + asInstanceOf[PrioItem].prio.toString else ""})"
}
object ImportItem {

  def load(from: format.Item)(implicit index: ImportedIndex): Item = {
    import format.ItemClass._

    val stock    = index[Stock](from.stock.get)
    val itemType = ItemType(from.`type`)

    from._class match {
      case NO =>
        new NoItem(
          stock = stock,
          itemType = itemType
        )
      case BLOCKED =>
        new BlockedItem(
          stock = stock,
          itemType = itemType
        )
      case DEMAND =>
        new DemandItem(
          stock = stock,
          itemType = itemType,
          usage = ItemUsage.read(from.usage),
          prio = from.prio
        )
      case DEMANDED =>
        new DemandedItem(
          stock = stock,
          itemType = itemType,
          usage = ItemUsage.read(from.usage)
        )
      case SUPPLY =>
        new SupplyItem(
          stock = stock,
          itemType = itemType,
          usage = ItemUsage.read(from.usage),
          prio = from.prio
        )
      case SUPPLIED =>
        new SuppliedItem(
          stock = stock,
          itemType = itemType,
          target = index[Stock](from.targetStock.get),
          _nextTarget = index[Stock](from.nextStock.get),
          usage = ItemUsage.read(from.usage)
        )
      case item => throw new IllegalStateException(s"Can't read '$item'")
    }

  }
}

/* Neutral side */
final class NoItem(override val stock: Stock, override val itemType: ItemType) extends Item {
  override def isReal    = false
  override def usage     = ItemUsage.Default
  override def direction = ItemDirection.Neutral

  def demand(economy: ItemEconomy, itemType: ItemType = itemType, usage: ItemUsage = ItemUsage.Default): Unit = {
    require(itemType != DummyItemType)
    require(itemType == itemType, "Can not change type of non dummy item.")

    val demandItem = new DemandItem(stock, itemType, usage)
    economy.addDemand(demandItem)
    replaceInStockWith(demandItem)
  }

  def supply(economy: ItemEconomy, itemType: ItemType = itemType, usage: ItemUsage = ItemUsage.Default): Unit = {
    require(itemType != DummyItemType)
    require(itemType == itemType, "Can not change type of non dummy item.")

    val supplyItem = new SupplyItem(stock, itemType, usage)
    economy.addSupply(supplyItem)
    replaceInStockWith(supplyItem)
  }

  def block(itemType: ItemType = itemType): Unit = {
    require(itemType != DummyItemType)
    require(itemType == itemType, "Can not change type of non dummy item.")

    val blockedItem = new BlockedItem(stock, itemType)
    replaceInStockWith(blockedItem)
  }

  override def export()(implicit index: ExportedIndex) = format.Item(
    _class = format.ItemClass.NO,
    stock = Some(index(stock)),
    `type` = itemType.exportType,
    usage = usage.formatUsage
  )
}

final class BlockedItem private[item] (override val stock: Stock, override val itemType: ItemType) extends Item {
  override def isReal    = true
  override def usage     = ItemUsage.Default
  override def direction = ItemDirection.In

  def supply(economy: ItemEconomy, usage: ItemUsage = ItemUsage.Default): Unit = {
    val supplyItem = new SupplyItem(stock, itemType, usage)
    economy.addSupply(supplyItem)
    replaceInStockWith(supplyItem)
  }

  def transfer(to: NoItem): Unit = {
    to.replaceInStockWith(new BlockedItem(to.stock, itemType))
    removeFromStock()
  }

  override def export()(implicit index: ExportedIndex) = format.Item(
    _class = format.ItemClass.BLOCKED,
    stock = Some(index(stock)),
    `type` = itemType.exportType
  )
}

/* Demand side */
/** A request for a item which is not fullfiled */
final class DemandItem private[item] (override val stock: Stock, override val itemType: ItemType, override val usage: ItemUsage = ItemUsage.Default, override val prio: Integer = 0) extends Item with PrioItem {
  override def isReal    = false
  override def direction = ItemDirection.In

  def demanded(economy: ItemEconomy, supplyItem: SupplyItem): Unit = {
    economy.order(this, supplyItem)
  }

  override def export()(implicit index: ExportedIndex) = format.Item(
    _class = format.ItemClass.DEMAND,
    stock = Some(index(stock)),
    `type` = itemType.exportType,
    usage = usage.formatUsage,
    prio = prio
  )
}

/** A demand which is fullfiled by a supplied item */
final class DemandedItem private[item] (override val stock: Stock, override val itemType: ItemType, override val usage: ItemUsage = ItemUsage.Default) extends Item {
  override def isReal    = false
  override def direction = ItemDirection.In

  def cancel(economy: ItemEconomy): Unit = {
    economy.cancelOrder(this)
  }

  def finish(economy: ItemEconomy)(implicit game: Game): Unit = {
    economy.finishOrder(this)
  }

  override def export()(implicit index: ExportedIndex) = format.Item(
    _class = format.ItemClass.DEMANDED,
    stock = Some(index(stock)),
    `type` = itemType.exportType,
    usage = usage.formatUsage
  )
}

/* Supply side */
/** A supplied item which is not reserved */
final class SupplyItem private[item] (override val stock: Stock, override val itemType: ItemType, override val usage: ItemUsage = ItemUsage.Default, override val prio: Integer = 0) extends Item with PrioItem {
  override def isReal    = true
  override def direction = ItemDirection.Out

  def supplied(economy: ItemEconomy, demandItem: DemandItem): Unit = {
    economy.order(demandItem, this)
  }

  override def export()(implicit index: ExportedIndex) = format.Item(
    _class = format.ItemClass.SUPPLY,
    stock = Some(index(stock)),
    `type` = itemType.exportType,
    usage = usage.formatUsage,
    prio = prio
  )
}

/** A supplied item which is reserved by a demanded item */
final class SuppliedItem private[item] (override val stock: Stock, override val itemType: ItemType, val target: Stock, var _nextTarget: Stock, override val usage: ItemUsage = ItemUsage.Default) extends Item {
  require(target != null)
  require(_nextTarget != null)

  override def isReal    = true
  override def direction = ItemDirection.Out

  def nextTarget = _nextTarget

  def setNextTarget(nextTarget: Stock): Unit = {
    require(nextTarget != null)
    _nextTarget = nextTarget
  }

  def isTargetWaypoint(waypoint: Waypoint): Boolean = {
    target.entity.stockWaypoint.exists(_ == waypoint)
  }

  def transfer(economy: ItemEconomy, to: NoItem): SuppliedItem = {
    economy.transfer(this, to)
  }

  def cancel(economy: ItemEconomy): Unit = {
    economy.cancelOrder(this)
  }

  def finish(economy: ItemEconomy): Unit = {
    economy.finishOrder(this)
  }

  override def export()(implicit index: ExportedIndex) = format.Item(
    _class = format.ItemClass.SUPPLY,
    stock = Some(index(stock)),
    `type` = itemType.exportType,
    usage = usage.formatUsage,
    targetStock = Some(index(target)),
    nextStock = Some(index(nextTarget))
  )
}

sealed trait ExportItemEconomy extends ExportObject[format.ItemEconomy] with ExportIndexedObject { itemEconomy: ItemEconomy =>

  override def export()(implicit index: ExportedIndex) = format.ItemEconomy(
    demands = itemEconomy._demands.map(index(_)).toSeq,
    supplies = itemEconomy._supplies.map(index(_)).toSeq,
    supplied = itemEconomy._suppliedByDemanded.map {
      case (key, value) =>
        format.ItemEconomy.ItemMapping(
          from = Some(index(key)),
          to = Some(index(key))
        )
    }.toSeq
  )

  override def index() = ExportIndex(
    objects = (
      itemEconomy._demandedBySupplied.keys ++
        itemEconomy._demandedBySupplied.values ++
        itemEconomy._supplies ++
        itemEconomy._demands
    ).toSet
  )
}

object ImportItemEconomy {

  def load(from: format.ItemEconomy)(implicit index: ImportedIndex): ItemEconomy = {
    val suppliedByDemanded = mutable.LinkedHashMap(from.supplied.map(mapping => (index[DemandedItem](mapping.from.get) -> index[SuppliedItem](mapping.to.get))): _*)
    val demandedBySupplied = mutable.LinkedHashMap(from.supplied.map(mapping => (index[SuppliedItem](mapping.to.get) -> index[DemandedItem](mapping.from.get))): _*)
    val demands            = mutable.LinkedHashSet(from.demands.map(it => index[DemandItem](it)): _*)
    val supplies           = mutable.LinkedHashSet(from.supplies.map(it => index[SupplyItem](it)): _*)

    new ItemEconomy(
      _suppliedByDemanded = suppliedByDemanded,
      _demandedBySupplied = demandedBySupplied,
      _demands = demands,
      _supplies = supplies
    )
  }
}

final class ItemEconomy(
    private[item] val _suppliedByDemanded: Map[DemandedItem, SuppliedItem] = mutable.LinkedHashMap(),
    private[item] val _demandedBySupplied: Map[SuppliedItem, DemandedItem] = mutable.LinkedHashMap(),
    private[item] val _supplies: Set[SupplyItem] = mutable.LinkedHashSet(),
    private[item] val _demands: Set[DemandItem] = mutable.LinkedHashSet()
) extends ExportItemEconomy {

  def update(): Unit = {
    // cancel invalid orders
    for ((demandedItem, suppliedItem) <- _suppliedByDemanded) {
      // check if supplied item is at person (in that case canReach check makes no sense)
      if (!suppliedItem.stock.entity.isInstanceOf[Person]) {
        if (!demandedItem.canReach(suppliedItem)) {
          println("cancel by update")
          cancelOrder(demandedItem, suppliedItem)
        }
      }
    }

    // create new orders
    for (demandItem <- _demands.toArray.sortBy(_.prio).reverse) {
      _supplies.find(supplyItem => isCompatible(demandItem, supplyItem)) match {
        case Some(supplyItem) => order(demandItem, supplyItem)
        case None             => // continue
      }
    }
  }

  private def isCompatible(demandItem: DemandItem, supplyItem: SupplyItem): Boolean = {
    !supplyItem.isSameMaster(demandItem) &&
    supplyItem.isSameItemType(demandItem) &&
    supplyItem.isSameUsage(demandItem) &&
    supplyItem.canReach(demandItem)
  }

  private def nextItemTarget(supplyStock: Stock, demandStock: Stock): Option[Stock] = {
    for {
      suppliedItemWaypoint <- supplyStock.master.entity.stockWaypoint
      demandedItemWaypoint <- demandStock.master.entity.stockWaypoint
      stock <- suppliedItemWaypoint.findPath(demandedItemWaypoint) match {
                case Some(current :: next :: tail) => Some(next.stock)
                case _                             => None
              }
    } yield stock
  }

  def remove(item: Item): Unit = {
    item match {
      case demandedItem: DemandedItem => {
        cancelDemandedItem(demandedItem)
        removeDemanded(demandedItem)
      }
      case suppliedItem: SuppliedItem => {
        cancelSuppliedItem(suppliedItem)
        removeSupplied(suppliedItem)
      }
      case demandItem: DemandItem   => removeDemand(demandItem)
      case supplyItem: SupplyItem   => removeSupply(supplyItem)
      case blockedItem: BlockedItem => // ignore
      case noItem: NoItem           => // ignore
      case _                        => throw new IllegalStateException
    }
  }

  private def contains(demandItem: DemandItem): Boolean = {
    _demands.contains(demandItem)
  }

  private def contains(supplyItem: SupplyItem): Boolean = {
    _supplies.contains(supplyItem)
  }

  private[item] def addDemand(demandItem: DemandItem): Unit = {
    _demands.add(demandItem)
  }

  private[item] def addSupply(supplyItem: SupplyItem): Unit = {
    _supplies.add(supplyItem)
  }

  private[item] def removeDemand(demandItem: DemandItem): Unit = assert {
    _demands.remove(demandItem)
  }

  private[item] def removeSupply(supplyItem: SupplyItem): Unit = assert {
    _supplies.remove(supplyItem)
  }

  private def removeDemanded(demandedItem: DemandedItem): Unit = {
    if (_suppliedByDemanded.contains(demandedItem)) {
      _demandedBySupplied.remove(_suppliedByDemanded(demandedItem))
    }
    _suppliedByDemanded.remove(demandedItem)
  }

  private def removeSupplied(suppliedItem: SuppliedItem): Unit = {
    if (_demandedBySupplied.contains(suppliedItem)) {
      _suppliedByDemanded.remove(_demandedBySupplied(suppliedItem))
    }
    _demandedBySupplied.remove(suppliedItem)
  }

  private[item] def transfer(suppliedItem: SuppliedItem, to: NoItem): SuppliedItem = {
    val nextTarget =
      if (suppliedItem.nextTarget == to.stock) nextItemTarget(to.stock, suppliedItem.target).getOrElse(throw new IllegalStateException("Impossible transfer"))
      else suppliedItem.nextTarget
    val newSuppliedItem = new SuppliedItem(to.stock, suppliedItem.itemType, suppliedItem.target, nextTarget, suppliedItem.usage)
    val demandedItem    = _demandedBySupplied(suppliedItem)

    _suppliedByDemanded.put(demandedItem, newSuppliedItem)
    _demandedBySupplied.remove(suppliedItem)
    _demandedBySupplied.put(newSuppliedItem, demandedItem)

    suppliedItem.removeFromStock()
    to.replaceInStockWith(newSuppliedItem)

    newSuppliedItem
  }

  private[item] def order(demandItem: DemandItem, supplyItem: SupplyItem): Unit = {
    require(demandItem != null)
    require(supplyItem != null)
    require(demandItem.itemType == supplyItem.itemType)
    require(demandItem.usage == supplyItem.usage)
    assume(contains(demandItem))
    assume(contains(supplyItem))

    val nextTarget   = nextItemTarget(supplyItem.stock, demandItem.stock).getOrElse(throw new IllegalStateException("Impossible order"))
    val demandedItem = new DemandedItem(demandItem.stock, demandItem.itemType, demandItem.usage)
    val suppliedItem = new SuppliedItem(supplyItem.stock, supplyItem.itemType, demandedItem.stock, nextTarget, supplyItem.usage)
    demandItem.replaceInStockWith(demandedItem)
    supplyItem.replaceInStockWith(suppliedItem)

    removeDemand(demandItem)
    removeSupply(supplyItem)

    _demandedBySupplied.put(suppliedItem, demandedItem)
    _suppliedByDemanded.put(demandedItem, suppliedItem)

    println(s"ordered '${demandedItem.itemType}' from '${suppliedItem.stock.entity}' to '${demandedItem.stock.entity}'")
  }

  private[item] def cancelOrder(demandedItem: DemandedItem): Unit = cancelOrder(demandedItem, _suppliedByDemanded(demandedItem))
  private[item] def cancelOrder(suppliedItem: SuppliedItem): Unit = cancelOrder(_demandedBySupplied(suppliedItem), suppliedItem)
  private def cancelOrder(demandedItem: DemandedItem, suppliedItem: SuppliedItem): Unit = {
    require(demandedItem != null)
    require(suppliedItem != null)
    require(demandedItem.itemType == suppliedItem.itemType)
    assume(_demandedBySupplied(suppliedItem) == demandedItem)
    assume(_suppliedByDemanded(demandedItem) == suppliedItem)

    cancelDemandedItem(demandedItem)
    cancelSuppliedItem(suppliedItem)

    removeDemanded(demandedItem)
    removeSupplied(suppliedItem)

    println(s"canceled '${demandedItem.itemType}' from '${suppliedItem.stock.entity}' to '${demandedItem.stock.entity}'")
  }
  private def cancelDemandedItem(demandedItem: DemandedItem): Unit = {
    val demandItem = new DemandItem(demandedItem.stock, demandedItem.itemType, demandedItem.usage)
    demandedItem.replaceInStockWith(demandItem)
    addDemand(demandItem)
  }
  private def cancelSuppliedItem(suppliedItem: SuppliedItem): Unit = {
    val supplyItem = new SupplyItem(suppliedItem.stock, suppliedItem.itemType, suppliedItem.usage)
    suppliedItem.replaceInStockWith(supplyItem)
    addSupply(supplyItem)
  }

  private[item] def finishOrder(demandedItem: DemandedItem): Unit = finishOrder(demandedItem, _suppliedByDemanded(demandedItem))
  private[item] def finishOrder(suppliedItem: SuppliedItem): Unit = finishOrder(_demandedBySupplied(suppliedItem), suppliedItem)
  private def finishOrder(demandedItem: DemandedItem, suppliedItem: SuppliedItem): Unit = {
    require(demandedItem != null)
    require(suppliedItem != null)
    require(demandedItem.itemType == suppliedItem.itemType)
    assume(_demandedBySupplied(suppliedItem) == demandedItem)
    assume(_suppliedByDemanded(demandedItem) == suppliedItem)

    demandedItem.replaceInStockWith(new BlockedItem(demandedItem.stock, demandedItem.itemType))
    suppliedItem.replaceInStockWith(new NoItem(suppliedItem.stock, suppliedItem.itemType))

    _demandedBySupplied.remove(suppliedItem)
    _suppliedByDemanded.remove(demandedItem)

    executeOrderFinishedTrigger(demandedItem)
  }
  private def executeOrderFinishedTrigger(demandedItem: DemandedItem): Unit = {
    val demandStockEntity = demandedItem.stock.entity
    if (demandStockEntity.isInstanceOf[JobEntity[_]]) {
      demandStockEntity.asInstanceOf[JobEntity[_]].execute(Trigger.FinishedOrder)
    }
    val masterDemandStockEntity = demandedItem.stock.master.entity
    if (masterDemandStockEntity.isInstanceOf[JobEntity[_]]) {
      masterDemandStockEntity.asInstanceOf[JobEntity[_]].execute(Trigger.FinishedOrder)
    }
  }
}
