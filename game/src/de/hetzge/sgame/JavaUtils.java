package de.hetzge.sgame;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.Attribute;
import com.badlogic.gdx.math.MathUtils;

import java.util.Objects;

public final class JavaUtils {

    private JavaUtils() {
        // private utils constructor
    }

    public static TextureRegion[][] splitFlipped(TextureRegion region, int tileWidth, int tileHeight) {
        int width = region.getRegionWidth();
        int height = region.getRegionHeight();
        int x = region.getRegionX();
        int y = region.getRegionY() - height;
        int rows = height / tileHeight;
        int cols = width / tileWidth;

        int startX = x;
        TextureRegion[][] tiles = new TextureRegion[rows][cols];
        for (int row = 0; row < rows; row++, y += tileHeight) {
            x = startX;
            for (int col = 0; col < cols; col++, x += tileWidth) {
                tiles[row][col] = new TextureRegion(region.getTexture(), x, y, tileWidth, tileHeight);
                tiles[row][col].flip(false, true);
            }
        }

        return tiles;
    }

    public static class OrientationSetShaderAttribute extends Attribute {
        public static final long MAX_TYPE = register("max");
        public static final long MIN_TYPE = register("min");

        private final float value;

        public OrientationSetShaderAttribute(long type, float value) {
            super(type);
            this.value = value;
        }

        public float getValue() {
            return value;
        }

        @Override
        public Attribute copy() {
            return new OrientationSetShaderAttribute(type, value);
        }

        @Override
        public int compareTo(Attribute other) {
            if (type != other.type) return (int) (type - other.type);
            final float otherValue = ((OrientationSetShaderAttribute) other).value;
            return MathUtils.isEqual(value, otherValue) ? 0 : value < otherValue ? -1 : 1;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            if (!super.equals(o)) return false;
            OrientationSetShaderAttribute that = (OrientationSetShaderAttribute) o;
            return Float.compare(that.value, value) == 0;
        }

        @Override
        public int hashCode() {
            return Objects.hash(super.hashCode(), value);
        }
    }
}
