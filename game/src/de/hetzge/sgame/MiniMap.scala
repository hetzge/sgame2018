package de.hetzge.sgame.minimap

import de.hetzge.sgame.area._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.render._
import de.hetzge.sgame.tile.TileType._
import de.hetzge.sgame.environment._
import de.hetzge.sgame.base._
import com.badlogic.gdx.graphics.glutils.FrameBuffer
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.MathUtils
import de.hetzge.sgame.settings.LocalSettings

class MiniMap(area: Area, width: PixelInt, height: PixelInt) {
  require(height % 10 == 0, s"height can must be divisable with 10 but is '${height.value}'")

  private[this] val _rowsPerUpdate           = 10
  private[this] val _miniMapCamera           = new OrthographicCamera(width.pixelFloat, height.pixelFloat)
  private[this] val _shapeRenderer           = new ShapeRenderer()
  private[this] val _frameBuffer             = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, false)
  private[this] var _offsetX: TileInt        = 0
  private[this] var _offsetY: TileInt        = 0
  private[this] var _cameraTileBounds: Tiled = SimpleTiled()
  private[this] var _row                     = 0

  def texture: Texture = _frameBuffer.getColorBufferTexture()
  def offsetX: TileInt = _offsetX
  def offsetY: TileInt = _offsetY

  def update(localSettings: LocalSettings, cameraTileBounds: Tiled): Unit = {
    val hasCameraChanged = _cameraTileBounds != cameraTileBounds
    _cameraTileBounds = cameraTileBounds

    _offsetX = MathUtils.clamp(_cameraTileBounds.position.x - (width - _cameraTileBounds.width) / 2, 0, area.width - width)
    _offsetY = MathUtils.clamp(_cameraTileBounds.position.y - (height - _cameraTileBounds.height) / 2, 0, area.height - height)

    updateMiniMapCamera(
      x = width / 2f + _offsetX,
      y = height / 2f + _offsetY
    )

    _frameBuffer.begin()
    drawTiles(localSettings, all = hasCameraChanged)
    _frameBuffer.end()

    _row = (_row + _rowsPerUpdate) % height
  }

  private[this] def drawTiles(localSettings: LocalSettings, all: Boolean): Unit = {
    _shapeRenderer.begin(ShapeRenderer.ShapeType.Point)

    val y    = if (all) _offsetY else TileInt(_offsetY + _row)
    val rows = if (all) TileInt(height) else TileInt(_rowsPerUpdate)

    area.iterate(_offsetX, y, TileInt(width), rows) { position =>
      _shapeRenderer.setColor(tileColor(localSettings, position))
      _shapeRenderer.point(position.x.value, position.y.value, 0f)
    }

    _shapeRenderer.end()
  }

  private[this] def tileColor(localSettings: LocalSettings, position: Position): Color = {
    def isFogOfWar      = localSettings.showFogOfWar && area.fogOfWar.isHidden(position)
    def isStaticEntity  = area.isStaticEntity(position)
    def isDynamicEntity = area.isDynamicEntity(position)
    def ownerColor      = area.owner(position).color
    def isBorder        = localSettings.showOwnership || area.isOtherOwnerAround(position)
    def isViewport      = _cameraTileBounds.isEndOfTiled(position, area)

    if (isViewport) {
      Color.BLACK
    } else if (isFogOfWar) {
      Color.WHITE
    } else if (isStaticEntity) {
      ownerColor
    } else if (isDynamicEntity) {
      ownerColor
    } else if (isBorder) {
      ownerColor
    } else {
      val environmentByte = area.environment.environmentByte(position)
      if (!environmentByte.isEmpty) {
        environmentByte.environmentType match {
          case EnvironmentType.Stone  => Color.LIGHT_GRAY
          case EnvironmentType.Forest => Color.FOREST
          case EnvironmentType.Crop   => Color.LIME
        }
      } else {
        val topTileType = area.tileMap.topTileType(position)
        topTileType match {
          case Grass         => Color.GREEN
          case Cloud         => Color.WHITE
          case Desert        => Color.YELLOW
          case Mountain      => Color.GRAY
          case Water         => Color.BLUE
          case DefaultWay    => Color.BROWN
          case BuildStoneWay => Color.DARK_GRAY
          case StoneWay      => Color.DARK_GRAY
          case None          => Color.PINK
        }
      }
    }
  }

  private[this] def updateMiniMapCamera(x: PixelFloat, y: PixelFloat): Unit = {
    _miniMapCamera.position.x = x
    _miniMapCamera.position.y = y
    _miniMapCamera.update()
    _shapeRenderer.setProjectionMatrix(_miniMapCamera.combined)
  }
}
