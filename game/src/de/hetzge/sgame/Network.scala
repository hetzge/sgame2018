package de.hetzge.sgame.network

import scala.collection.mutable.{HashMap, ListBuffer}
import org.nustaq.net.TCPObjectSocket
import org.nustaq.net.TCPObjectServer
import org.nustaq.net.TCPObjectServer.NewClientListener
import java.util.UUID

import scala.collection.mutable

/** client identifier independent from a active connection */
final case class Client(val name: String)

/** represents a connected client */
final case class Connection(val client: Client, val socket: TCPObjectSocket) {
  private val messages             = new mutable.Queue[Object]()
  private var exception: Throwable = null

  def start = {
    ReadSocketThread.start
  }
  def disconnect = {
    socket.close()
    ReadSocketThread.kill
  }

  def send(message: Message): Unit = {
    // println(s"send to '${client.name}': $message")
    socket.writeObject(message)
    socket.flush()
  }

  def flush: Seq[Object] = {
    if (exception != null) throw exception
    messages.dequeueAll(_ => true)
  }

  object ReadSocketThread extends Thread("Read socket thread") {
    setUncaughtExceptionHandler((thread, exception) => Connection.this.exception = exception)

    private var running = true

    def kill = running = false

    override def run(): Unit = {
      while (running) {
        val message: Object = socket.readObject()
        messages.enqueue(message)
      }
    }
  }
}

/** Marker interface for something that can be send via network */
@SerialVersionUID(1L) trait Message extends Serializable

/** Message which is sent after a connection is established */
final case class ConnectMessage(val client: Client) extends Message

/** Message which confirms a established connection from the other side */
final case class ConnectedMessage(val client: Client) extends Message

object Network {
  val DEFAULT_PORT = 43567
}

final class Network(val client: Client = Client(UUID.randomUUID().toString()), port: Int = Network.DEFAULT_PORT) {
  private val server: TCPObjectServer                  = new TCPObjectServer(port)
  private val connections: HashMap[Client, Connection] = HashMap()

  private def register(client: Client, socket: TCPObjectSocket): Unit = {
    println(s"register $client")

    val connection = Connection(client, socket)
    connection.start
    connections.put(client, connection)

    connection.send(ConnectedMessage(Network.this.client))
  }

  private def unregister(client: Client): Unit = {
    println(s"unregister $client")
    connections.remove(client)
  }

  def start: Unit = server.start(NetworkNewClientListener)
  def connect(host: String = "127.0.0.1", port: Int = port): Unit = {
    println(s"Try to connect to '$host:$port'")

    val socket = new TCPObjectSocket(host, port)
    socket.writeObject(ConnectMessage(client))
    socket.flush()

    socket.readObject() match {
      case ConnectedMessage(client) => register(client, socket)
      case message                  => throw new IllegalStateException(s"unknown connected message: $message")
    }
  }
  def disconnect(client: Client): Unit = {
    connections(client).disconnect
  }

  def send(client: Client, message: Message): Unit = connections(client).send(message)
  def sendAll(message: Message): Unit              = connections.values.foreach(_.send(message))

  def handle(callback: (Client, Object) => Unit) = {
    val toRemove = ListBuffer[Connection]()
    connections.foreach {
      case (client, connection) => {
        try {
          for (messageObject <- connection.flush) {
            callback(client, messageObject)
          }
        } catch {
          case e: Throwable => {
            e.printStackTrace()
            toRemove.append(connection)
          }
        }
      }
    }
    for (connection <- toRemove) {
      unregister(connection.client)
    }
  }

  object NetworkNewClientListener extends NewClientListener {
    def connectionAccepted(socket: TCPObjectSocket): Unit = {
      println(s"${client.name}: connection accepted")

      socket.readObject() match {
        case ConnectMessage(client) => register(client, socket)
        case message                => throw new IllegalStateException(s"unknown connect message: $message")
      }
    }
  }
}

object NetworkExampleApp extends App {
  case object Ping extends Message
  case object Pong extends Message

  val serverPort = 1235

  new Thread {
    override def run() {
      val server = new Network(Client("Server"), serverPort)
      server.start

      while (true) {
        server.handle { (_, message) =>
          println(message)
          server.sendAll(Pong)
        }
        Thread.sleep(1000)
      }
    }
  }.start()

  Thread.sleep(1000)

  new Thread {
    override def run() {
      val client = new Network(Client("Client"), 1234)
      client.start
      client.connect("127.0.0.1", serverPort)
      client.sendAll(Ping)

      while (true) {
        client.handle { (_, message) =>
          println(message)
          client.sendAll(Ping)
        }
        Thread.sleep(1000)
      }
    }
  }.start()

  Thread.sleep(1000)

  println("done")
}
