package de.hetzge.sgame.path

import de.hetzge.sgame.unit._
import de.hetzge.sgame.format

import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks._

final case class PathProto(
    steps: Seq[PositionProto]
)

object Path {
  def apply(): Path               = Path(List.empty)
  def apply(f: format.Path): Path = Path(f.positions.map(Position(_)).toList)
  def apply(from: PathProto): Path = Path(from.steps.map(Position(_)).toList)
}
final case class Path(val steps: List[Position]) extends AnyVal {
  def distance: TileInt = {
    var result: Int                    = 0
    var previousStep: Option[Position] = None
    for (step <- steps) {
      if (previousStep.isDefined) {
        result += Math.hypot(step.x - previousStep.get.x, step.y - previousStep.get.y).intValue()
      }
      previousStep = Some(step)
    }

    result
  }

  def optimize: Path = {
    // TODO quark
    Path(optimizeDistinct.optimizeTarget.steps.distinct)
  }

  private def optimizeDistinct: Path = {
    if (steps.isEmpty) this
    else {
      val filtered = steps.distinct
        .sliding(3)
        .toList
        .filter {
          case List(a, b, c) => a.x != c.x && a.y != c.y
          case _             => false
        }
        .map {
          case List(a, b, c) => b
        }

      Path(List(steps.head) ++ filtered ++ Seq(steps.last))
    }
  }

  private def optimizeTarget(): Path = {
    if (steps.isEmpty) this
    else {
      val target  = steps.last
      val targetX = target.x
      val targetY = target.y

      val result = ListBuffer[Position]()

      var last: Option[Position] = None
      breakable {
        for (step <- steps) {

          if (last.isDefined) {
            val a    = last.get
            val b    = step
            val aX   = a.x
            val aY   = a.y
            val bX   = b.x
            val bY   = b.y
            val minX = Math.min(aX, bX)
            val minY = Math.min(aY, bY)
            val maxX = Math.max(aX, bX)
            val maxY = Math.max(aY, bY)

            if (minX <= targetX && maxX >= targetX && aY == bY && aY == targetY ||
                minY <= targetY && maxY >= targetY && aX == bX && aX == targetX) {
              break
            }
          }

          result.append(step)
          last = Some(step)
        }
      }

      result.append(target)

      Path(result.toList)
    }
  }

  def ++(other: Path): Path  = Path(steps ++ other.steps)
  def isEmpty: Boolean       = steps.isEmpty
  def size: Int              = steps.size
  def head: Option[Position] = steps.headOption
  def last: Option[Position] = steps.lastOption
  def tailPath: Path         = Path(steps.tail)
  def reverse: Path          = Path(steps.reverse)

  override def toString: String = steps.map(step => s"${step.x.value}/${step.y.value}").mkString("->")
  def export: format.Path       = format.Path(positions = steps.map(_.export))
  def exportProto: PathProto    = PathProto(steps.map(_.exportProto))
}
