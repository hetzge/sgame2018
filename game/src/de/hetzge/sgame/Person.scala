package de.hetzge.sgame.person

import de.hetzge.sgame.area._
import de.hetzge.sgame.frames._
import de.hetzge.sgame.render._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.game._
import de.hetzge.sgame.base._
import de.hetzge.sgame.job._
import de.hetzge.sgame.resources._
import de.hetzge.sgame.stock._
import de.hetzge.sgame.path._
import de.hetzge.sgame.building._
import de.hetzge.sgame.way._
import de.hetzge.sgame.player._
import de.hetzge.sgame.environment._
import de.hetzge.sgame.item._
import de.hetzge.sgame.GameApplicationAdapter
import de.hetzge.sgame.tile._
import de.hetzge.sgame.format
import de.hetzge.sgame.export._
import de.hetzge.sgame.format.PersonState._
import de.hetzge.sgame.format.PersonType._

import com.badlogic.gdx.math.MathUtils
import java.util.concurrent.TimeUnit
import scala.concurrent.duration._
import de.hetzge.sgame.format.Reference

final case class PersonProto(
    personType: String,
    id: Long,
    area: Reference,
    position: PositionProto,
    stock: Reference,
    playerId: Long,
    path: PathProto,
    building: Reference,
    reservedBuilding: Reference,
    orientation: String,
    personState: String,
    sourceFrame: Long,
    targetFrame: Long,
    job: Reference
)

sealed trait PersonType extends Serializable {
  def createJob: Job[Person] = DummyPersonJob
  val stockType: StockType   = DummyStockType

  /** tiles per second */
  val speed: TileFloat = 1f
  val useWays: Boolean = true

  val viewRadius: TileInt = 10

  val userControlled: Boolean = false

  val renderableSet: PersonRenderableSet

  def exportType: format.PersonType

  val key: String
}
object PersonType {

  def apply(f: format.PersonType): PersonType = {
    f match {
      case CARRIER_PERSON       => PersonType.Carrier
      case FARMER_PERSON        => PersonType.Farmer
      case LUMBERJACK_PERSON    => PersonType.Lumberjack
      case MASON_PERSON         => PersonType.Mason
      case START_PERSON         => PersonType.Start
      case STREET_WORKER_PERSON => PersonType.StreetWorker
      case TEST_PERSON          => throw new IllegalStateException("TestPerson is not supported.")
    }
  }

  /** For test only */
  final case class Test(
      override val createJob: Job[Person] = DummyPersonJob,
      override val stockType: StockType = DummyStockType,
      override val speed: TileFloat = 1f,
      override val renderableSet: PersonRenderableSet = null,
      override val useWays: Boolean = true
  ) extends PersonType {
    override def exportType = format.PersonType.TEST_PERSON
    override val key        = "Test"
  }

  case object Start extends PersonType {
    override val renderableSet  = GameApplicationAdapter.resources.person.one
    override val userControlled = true
    override val speed          = 3f
    override def exportType     = format.PersonType.START_PERSON
    override val key            = "Start"
  }

  case object Carrier extends PersonType {
    @transient override lazy val renderableSet = GameApplicationAdapter.resources.person.one
    override def createJob: Job[Person]        = CarrierSearchJob
    override val stockType                     = SingleItemStockType
    override def exportType                    = format.PersonType.CARRIER_PERSON
    override val key                           = "Carrier"
  }

  case object StreetWorker extends PersonType {
    @transient override lazy val renderableSet = GameApplicationAdapter.resources.person.two
    override def createJob: Job[Person]        = StreetWorkerBuildingSearchJob
    override val stockType                     = SingleItemStockType
    override def exportType                    = format.PersonType.STREET_WORKER_PERSON
    override val key                           = "StreetWorker"
  }

  case object Lumberjack extends PersonType {
    @transient override lazy val renderableSet = GameApplicationAdapter.resources.person.three
    override def createJob: Job[Person]        = EnvironmentWorkerJob(EnvironmentType.Forest, BuildingType.Lumberjack, WoodItemType)
    override val stockType                     = SingleItemStockType
    override def exportType                    = format.PersonType.LUMBERJACK_PERSON
    override val key                           = "Lumberjack"
  }

  case object Mason extends PersonType {
    @transient override lazy val renderableSet = GameApplicationAdapter.resources.person.three
    override def createJob: Job[Person]        = EnvironmentWorkerJob(EnvironmentType.Stone, BuildingType.Quarry, StoneItemType)
    override val stockType                     = SingleItemStockType
    override def exportType                    = format.PersonType.MASON_PERSON
    override val key                           = "Mason"
  }

  case object Farmer extends PersonType {
    @transient override lazy val renderableSet = GameApplicationAdapter.resources.person.three
    override def createJob: Job[Person]        = FarmerBuildingSearchJob
    override val stockType                     = SingleItemStockType
    override def exportType                    = format.PersonType.FARMER_PERSON
    override val key                           = "Farmer"
  }
}

sealed trait PersonState {
  val key: String
}
object PersonState {
  def apply(key: String): PersonState = {
    key match {
      case "Idle" => Idle
      case "Walk" => Walk
      case "Work" => Work
      case _      => throw new IllegalStateException(s"Unsupported person state key: '$key'")
    }
  }

  case object Idle extends PersonState {
    override val key = "Idle"
  }
  case object Walk extends PersonState {
    override val key = "Walk"
  }
  case object Work extends PersonState {
    override val key = "Work"
  }
}

sealed trait RenderPerson { person: Person =>
  @transient private[this] var _renderX: PixelFloat = 0.0f
  @transient private[this] var _renderY: PixelFloat = 0.0f

  def renderX: PixelFloat = _renderX
  def renderY: PixelFloat = _renderY

  override def render(renderContext: GameRenderContext, x: PixelFloat, y: PixelFloat, z: PixelFloat) = {
    val game = GameContext.game // TODO

    val offsetPercent = MathUtils.clamp(1f - (game.frames.current.id - sourceFrameId).toFloat / (targetFrameId - sourceFrameId).toFloat, 0.0f, 1.0f)

    val isWalk = state == PersonState.Walk
    val offset = TILE_SIZE_FLOAT * offsetPercent
    _renderX = if (isWalk && offset > 0f) x.toFloat - orientation.xInt * offset else x.toFloat
    _renderY = if (isWalk && offset > 0f) y.toFloat - orientation.yInt * offset else y.toFloat

    val renderable = personType.renderableSet(orientation, state)
    if (renderable == null) throw new IllegalStateException(s"renderable is null for '$orientation' and state='$state'")

    renderable.render(renderContext, null, _renderX, _renderY - 10f, z)
    stock.render(renderContext, _renderX, _renderY, z + 1f)
  }
}

sealed trait PathFinderPerson { person: Person =>

  /** Set the person to goto given coordinates. If possible returns true. */
  def goto(targetPosition: Position): Boolean = {
    pathTo(targetPosition) match {
      case Some(path) => {
        setPath(path.optimize)
        true
      }
      case None => {
        false
      }
    }
  }

  def stop(): Unit = {
    setPath(Path())
  }

  def pathTo(targetPosition: Position): Option[Path] = {
    area.pathTo(startPosition, targetPosition, personType.useWays)
  }

  private def startPosition: Position = {
    buildingStay match {
      case EnteredBuildingStay(building) => building.doorPosition
      case _                             => this.position
    }
  }
}

sealed trait StockPerson extends StockEntity { person: Person =>
  stock.assignEntity(person)
  override def stockWay      = area.way(this.position)
  override def stockWaypoint = area.waypoint(this.position)
}

sealed trait BuildingStay {
  def hasBuilding: Boolean
  def building: Option[Building]
}

/** The person is currently not related to a building */
case object NoneBuildingStay extends BuildingStay {
  override val hasBuilding = false
  override val building    = None
}

/** The person have reserved a stay in the building */
final case class ReservedBuildingStay(_building: Building) extends BuildingStay {
  override def hasBuilding = true
  override def building    = Some(_building)
}

/** The person is in the building */
final case class EnteredBuildingStay(_building: Building) extends BuildingStay {
  override def hasBuilding = true
  override def building    = Some(_building)
}

sealed trait BuildingPerson { person: Person =>
  private[person] var _buildingStay: BuildingStay       = NoneBuildingStay
  def buildingStay: BuildingStay                        = _buildingStay
  def setBuildingStay(buildingStay: BuildingStay): Unit = _buildingStay = buildingStay

  def leaveBuilding(keepReservation: Boolean = false): Unit = {
    buildingStay match {
      case EnteredBuildingStay(building) => building.leave(person = this, keepReservation)
      case _                             => throw new IllegalStateException(s"Can't leave building because '$this' is not inside")
    }
  }
  def cancelBuildingReservation(): Unit = {
    buildingStay match {
      case ReservedBuildingStay(building) => building.cancelReservation(person = this)
      case _                              => throw new IllegalStateException(s"Can't leave building because '$this' is not inside")
    }
  }
  def isInBuilding: Boolean                                     = buildingStay.isInstanceOf[EnteredBuildingStay]
  def isInBuilding(building: Building): Boolean                 = isInBuilding && buildingStay.building == Some(building)
  def isInBuilding(buildingType: BuildingType): Boolean         = isInBuilding && buildingStay.building.get.buildingType == buildingType
  def isReservedInBuilding: Boolean                             = buildingStay.isInstanceOf[ReservedBuildingStay]
  def isReservedInBuilding(building: Building): Boolean         = isReservedInBuilding && buildingStay.building == Some(building)
  def isReservedInBuilding(buildingType: BuildingType): Boolean = isReservedInBuilding && buildingStay.building.get.buildingType == buildingType
}

sealed trait ExportPerson extends ExportObject[format.Person] with ExportIndexedObject { person: Person =>

  def export()(implicit index: ExportedIndex) = format.Person(
    id = id,
    area = Some(index(area)),
    `type` = Some(
      format.Person.Type(
        `type` = personType.exportType
      )
    ),
    position = Some(format.Position(position.v)),
    stock = Some(index(stock)),
    path = Some(getPath.export),
    building = buildingStay match {
      case EnteredBuildingStay(_building) => Some(index(_building))
      case _                              => None
    },
    reservedBuilding = buildingStay match {
      case ReservedBuildingStay(_building) => Some(index(_building))
      case _                               => None
    },
    orientation = orientation.export,
    personState = _state match {
      case PersonState.Idle => format.PersonState.IDLE
      case PersonState.Work => format.PersonState.WORK
      case PersonState.Walk => format.PersonState.WALK
    },
    job = Some(index(job))
  )

  override def initLoad(f: format.Person)(implicit index: ImportedIndex) = {
    person.assignArea(index[Area](f.area.get))
    person.setJob(index[Job[Person]](f.job.get))
    person._path = Path(f.path.get)
    f.building match {
      case Some(building) => _buildingStay = EnteredBuildingStay(index[Building](building))
      case None           => // ignore
    }
    f.reservedBuilding match {
      case Some(building) => _buildingStay = ReservedBuildingStay(index[Building](building))
      case None           => // ignore
    }
    person._orientation = Orientation(f.orientation)
    person._state = f.personState match {
      case format.PersonState.IDLE => PersonState.Idle
      case format.PersonState.WORK => PersonState.Work
      case format.PersonState.WALK => PersonState.Walk
    }
    person._sourceFrameId = FrameId(f.sourceFrame)
    person._targetFrameId = FrameId(f.targetFrame)
  }

  def index() = ExportIndex(
    objects = Set(stock) ++ Set(job)
  )
}

object ImportPerson {

  def load(f: format.Person)(implicit index: ImportedIndex): Person = {
    new Person(
      id = f.id,
      _position = Position(f.position.get.value),
      personType = PersonType(f.`type`.get.`type`),
      playerId = PlayerId.get(f.playerId.toByte),
      stock = index[Stock](f.stock.get)
    )
  }

}

object Person {
  def apply(
      position: Position,
      personType: PersonType,
      playerId: PlayerId
  ): Person = {
    new Person(
      id = Entity.nextId,
      _position = position,
      personType = personType,
      playerId = playerId,
      stock = new SingleStock(personType.stockType)
    )
  }
}
final class Person(
    id: Int,
    private[this] var _position: Position,
    val personType: PersonType,
    override val playerId: PlayerId,
    override val stock: Stock
) extends Entity(id)
    with PlayerEntity
    with JobEntity[Person]
    with KiEntity[format.Person]
    with BuildingPerson
    with AreaEntity
    with RenderPerson
    with PathFinderPerson
    with StockPerson
    with ExportPerson {

  // start and end frame id of one state step
  private[person] var _sourceFrameId: FrameId = FrameId(-1L)
  private[person] var _targetFrameId: FrameId = FrameId(-1L)
  def sourceFrameId: FrameId                  = _sourceFrameId
  def targetFrameId: FrameId                  = _targetFrameId

  private[person] var _orientation: Orientation = Orientation.DEFAULT
  def orientation: Orientation                  = _orientation

  private[person] var _state: PersonState = PersonState.Idle
  def state: PersonState                  = _state
  def setupState(state: PersonState, orientation: Orientation, duration: FrameDuration): Unit = {
    _state = state
    _orientation = orientation
    _sourceFrameId = GameContext.frames.current.id
    _targetFrameId = _sourceFrameId + duration

    GameContext.frames.get(_targetFrameId).add(this)
  }

  override def position = _position
  def setPosition(x: TileInt, y: TileInt): Unit = {
    _position = _position.withPosition(x, y)
  }
  def isAt(checkPosition: Position): Boolean = this.position == checkPosition

  override def width  = 1
  override def height = 1

  override def viewRadius      = personType.viewRadius
  override def ownershipRadius = 0

  override def ki() = {
    _state match {
      case PersonState.Walk => doWalk()
      case PersonState.Work => doWork()
      case PersonState.Idle => doIdle()
    }

    if (!GameContext.frames.isFuture(targetFrameId)) {
      startIdle(5.seconds)
    }
  }

  private def doWork(): Unit = {
    require(_state == PersonState.Work)

    if (GameContext.frames.isCurrentOrPast(_targetFrameId)) {
      unsetState()
      execute(Trigger.FinishedWork)
    }
  }

  private def doWalk(): Unit = {
    require(_state == PersonState.Walk)

    if (GameContext.frames.isCurrentOrPast(_targetFrameId)) {
      if (GameContext.frames.isCurrent(_targetFrameId)) {
        if (!hasPath) {
          execute(Trigger.ReachedStep)
          unsetState()
          execute(Trigger.ReachedEndOfPath)
        } else {
          execute(Trigger.ReachedStep)
        }
      }

      // Process next step
      startMoveNextStep()
    }
  }

  private def doIdle(): Unit = {
    require(_state == PersonState.Idle)

    execute(Trigger.Default)
  }

  def startWork(duration: Duration, orientation: Orientation): Unit = {
    require(_state == PersonState.Idle)

    setupState(
      state = PersonState.Work,
      orientation = orientation,
      duration = FrameDuration(duration)
    )
  }

  private def startMoveNextStep(): Unit = {
    getNextStep match {
      case Some(nextStep) => {
        val nextOrientation = position.orientation(nextStep)
        if (canMove(nextOrientation)) {
          startMove(nextOrientation)
          while (getNextStep.contains(position)) dequeueNextStep
        } else {
          stop()
          resetAndExecuteJob(Trigger.Default)
        }
      }
      case None => {}
    }
  }

  private def startMove(orientation: Orientation): Unit = {
    val area = this.area

    area.reregisterPerson(this) {
      setPosition(
        x = position.x + orientation.xInt,
        y = position.y + orientation.yInt
      )
    }

    setupState(
      state = PersonState.Walk,
      orientation = orientation,
      duration = FrameDuration(Duration(1f / calculateSpeedInTilesPerSecond(position), TimeUnit.SECONDS))
    )
  }

  private def unsetState(): Unit = _state = PersonState.Idle
  private def startIdle(duration: Duration): Unit = {
    setupState(
      state = PersonState.Idle,
      orientation = orientation,
      duration = FrameDuration(duration)
    )
  }

  private def canMove(orientation: Orientation): Boolean = {
    position.around(orientation).exists(it => !area.isBlocking(it))
  }

  private def calculateSpeedInTilesPerSecond(position: Position): TileFloat = {
    personType.speed * area.speedFactor(position)
  }

  private[person] var _path: Path = Path()

  def setPath(path: Path): Unit = {
    require(!isInBuilding, "Person have to leave building before path is set")

    _path = path

    // skip current position
    while (getNextStep.contains(position)) dequeueNextStep

    if (!path.isEmpty && !hasPath) {
      execute(Trigger.ReachedEndOfPath)
    } else if (_state != PersonState.Walk) {
      startMoveNextStep()
    }
  }
  def getPath: Path    = _path
  def hasPath: Boolean = !_path.isEmpty

  def hasNextStep: Boolean          = !_path.isEmpty
  def getNextStep: Option[Position] = _path.steps.headOption
  def dequeueNextStep: Position = {
    val head = _path.head
    _path = _path.tailPath
    head.get
  }

  def getWay: Option[Way] = {
    if (isInBuilding) buildingStay.asInstanceOf[EnteredBuildingStay].building.get.doorWay
    else area.way(position)
  }

  override val isStatic = false

  override def createDefaultJob = personType.createJob

  override def toString() = s"Person($position, $personType)"
}

sealed trait Trigger
object Trigger {
  case object Default          extends Trigger
  case object ReachedStep      extends Trigger
  case object ReachedEndOfPath extends Trigger
  case object FinishedWork     extends Trigger
  case object FinishedOrder    extends Trigger
}
