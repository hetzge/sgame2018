package de.hetzge.sgame.player

import com.badlogic.gdx.graphics.Color
import de.hetzge.sgame.GameApplicationAdapter
import de.hetzge.sgame.game._
import de.hetzge.sgame.network._
import de.hetzge.sgame.render._

final class PlayerRenderableSet(templateRenderable: TextureRegionRenderable) {
  private[this] val _renderables = new Array[TextureRegionRenderable](PlayerId.PLAYER_COLORS.size)
  def apply(playerId: PlayerId): TextureRegionRenderable = {
    val index      = playerId.value
    val renderable = _renderables(index)
    if (renderable != null) {
      renderable
    } else {
      val color = playerId.color.cpy()
      color.a = 0.0f // set 0 to trigger dummy color replacement mode
      _renderables(index) = new TextureRegionRenderable(templateRenderable.textureRegion, color, templateRenderable.rotationDegrees)
      apply(playerId)
    }
  }
}

object PlayerId {
  private[player] val PLAYER_COLORS = Array[String](
    "#000000",
    "#FFFF00",
    "#1CE6FF",
    "#FF34FF",
    "#FF4A46",
    "#008941",
    "#006FA6",
    "#A30059",
    "#FFDBE5",
    "#7A4900",
    "#0000A6",
    "#63FFAC",
    "#B79762",
    "#004D43",
    "#8FB0FF",
    "#997D87",
    "#5A0007",
    "#809693",
    "#FEFFE6",
    "#1B4400",
    "#4FC601",
    "#3B5DFF",
    "#4A3B53",
    "#FF2F80",
    "#61615A",
    "#BA0900",
    "#6B7900",
    "#00C2A0",
    "#FFAA92",
    "#FF90C9",
    "#B903AA",
    "#D16100",
    "#DDEFFF",
    "#000035",
    "#7B4F4B",
    "#A1C299",
    "#300018",
    "#0AA6D8",
    "#013349",
    "#00846F",
    "#372101",
    "#FFB500",
    "#C2FFED",
    "#A079BF",
    "#CC0744",
    "#C0B9B2",
    "#C2FF99",
    "#001E09",
    "#00489C",
    "#6F0062",
    "#0CBD66",
    "#EEC3FF",
    "#456D75",
    "#B77B68",
    "#7A87A1",
    "#788D66",
    "#885578",
    "#FAD09F",
    "#FF8A9A",
    "#D157A0",
    "#BEC459",
    "#456648",
    "#0086ED",
    "#886F4C",
    "#34362D",
    "#B4A8BD",
    "#00A6AA",
    "#452C2C",
    "#636375",
    "#A3C8C9",
    "#FF913F",
    "#938A81",
    "#575329",
    "#00FECF",
    "#B05B6F",
    "#8CD0FF",
    "#3B9700",
    "#04F757",
    "#C8A1A1",
    "#1E6E00",
    "#7900D7",
    "#A77500",
    "#6367A9",
    "#A05837",
    "#6B002C",
    "#772600",
    "#D790FF",
    "#9B9700",
    "#549E79",
    "#FFF69F",
    "#201625",
    "#72418F",
    "#BC23FF",
    "#99ADC0",
    "#3A2465",
    "#922329",
    "#5B4534",
    "#FDE8DC",
    "#404E55",
    "#0089A3",
    "#CB7E98",
    "#A4E804",
    "#324E72",
    "#6A3A4C",
    "#83AB58",
    "#001C1E",
    "#D1F7CE",
    "#004B28",
    "#C8D0F6",
    "#A3A489",
    "#806C66",
    "#222800",
    "#BF5650",
    "#E83000",
    "#66796D",
    "#DA007C",
    "#FF1A59",
    "#8ADBB4",
    "#1E0200",
    "#5B4E51",
    "#C895C5",
    "#320033",
    "#FF6832",
    "#66E1D3",
    "#CFCDAC",
    "#D0AC94",
    "#7ED379",
    "#012C58"
  )

  private[this] val _cache = new Array[PlayerId](PLAYER_COLORS.size)
  for (i <- 0 until PLAYER_COLORS.size) {
    _cache(i) = new PlayerId(i.toByte)
  }

  private def apply(value: Byte): PlayerId = new PlayerId(value)
  def get(value: Byte): PlayerId           = _cache(value)
}
final case class PlayerId private (value: Byte) {
  val intValue                              = value.toInt
  val color                                 = Color.valueOf(PlayerId.PLAYER_COLORS(intValue % PlayerId.PLAYER_COLORS.size))
  @transient lazy val renderable            = Renderable(GameApplicationAdapter.resources.region.shape, color)
  @transient lazy val transparentRenderable = Renderable(GameApplicationAdapter.resources.region.shape, new Color(color.r, color.g, color.b, 0.4f))
}

sealed trait Player {
  val id: PlayerId
  val name: String
  val isEmpty: Boolean
  val isHuman: Boolean
}
final case class EmptyPlayer(
    override val id: PlayerId
) extends Player {
  override val name    = "EMPTY"
  override val isEmpty = true
  override val isHuman = false
}
final case class NormalPlayer(
    override val id: PlayerId
) extends Player {
  override val name    = "PLAYER"
  override val isEmpty = false
  override val isHuman = true
}
final case class NetworkPlayer(
    override val id: PlayerId,
    override val name: String,
    client: Client
) extends Player {
  override val isEmpty = false
  override val isHuman = true
}
final case class ComputerPlayer(
    override val id: PlayerId
) extends Player {
  override val name    = "COMPUTER"
  override val isEmpty = false
  override val isHuman = false
}
final case object GaiaPlayer extends Player {
  override val id      = PlayerId.get(0)
  override val name    = "GAIA"
  override val isEmpty = false
  override val isHuman = false
}

final class Players(playerCount: Int) {
  private[this] var _players: Array[Player] = {
    (List(GaiaPlayer) ++ (1 until (playerCount + 1)).map(idValue => EmptyPlayer(PlayerId.get(idValue.toByte)))).toArray[Player]
  }
  def player(id: PlayerId): Player = {
    require(id.intValue <= playerCount)
    _players(id.intValue)
  }
  def setPlayer(player: Player): Unit = {
    val oldPlayer = this.player(player.id)
    require(oldPlayer.isEmpty || player.isEmpty, "Only empty players can be set (or the new player have to be empty")
    _players(player.id.intValue) = player
  }
  def nextEmptyPlayer: Option[EmptyPlayer]                 = _players.collectFirst { case e @ EmptyPlayer(id)                                                 => e }
  def networkPlayer(client: Client): Option[NetworkPlayer] = _players.collectFirst { case n @ NetworkPlayer(id, name, playerClient) if client == playerClient => n }
}

trait PlayerEntity {
  def playerId: PlayerId

  def isOwnedByCurrentPlayer: Boolean = {
    GameContext.isSet && GameContext.game.localSettings.playerOption.exists(playerId == _.id)
  }
}
