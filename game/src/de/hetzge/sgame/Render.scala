package de.hetzge.sgame.render

import java.util.Comparator

import de.hetzge.sgame.unit._
import de.hetzge.sgame.game._
import de.hetzge.sgame.base._
import de.hetzge.sgame.GameApplicationAdapter
import de.hetzge.sgame.JavaUtils.OrientationSetShaderAttribute
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g3d.utils.RenderableSorter
import com.badlogic.gdx.graphics.g3d
import com.badlogic.gdx.utils
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.FPSLogger
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.graphics.profiling.GLProfiler
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g3d.utils.DefaultShaderProvider
import com.badlogic.gdx.graphics.g3d.Shader
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g3d.shaders.BaseShader
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g3d.Attributes
import com.badlogic.gdx.graphics.g3d.ModelCache
import com.badlogic.gdx.math.Vector2

private final class MyRenderableSorter extends RenderableSorter with Comparator[g3d.Renderable] {
  override def sort(camera: Camera, renderables: utils.Array[g3d.Renderable]) = {
    renderables.sort(this)
  }

  private[this] val position1: Vector3 = new Vector3()
  private[this] val position2: Vector3 = new Vector3()
  override def compare(o1: g3d.Renderable, o2: g3d.Renderable) = {
    o1.worldTransform.getTranslation(position1)
    o2.worldTransform.getTranslation(position2)

    val z = position1.z.compareTo(position2.z)

    if (z == 0) {
      o1.userData match {
        case ZIndexOffset(offsetY) => (position1.y - offsetY.value).compareTo(position2.y)
        case _ =>
          o2.userData match {
            case ZIndexOffset(offsetY) => position1.y.compareTo(position2.y - offsetY.value)
            case _                     => position1.y.compareTo(position2.y)
          }
      }
    } else {
      z
    }
  }
}

final case class ZIndexOffset(yOffset: PixelFloat)

final class GameRenderContext {
  @inline def resources = GameApplicationAdapter.resources

  private[this] var _cameraWorldWidth: PixelFloat  = 0f
  private[this] var _cameraWorldHeight: PixelFloat = 0f
  private[this] var _frame: Int                    = 0
  private[this] var _delta: Float                  = 0f

  private[this] var _opacity: Float = 1f
  def increaseOpacitiy(): Unit      = _opacity = Math.min(_opacity + 0.1f, 1f)
  def decreaseOpacity(): Unit       = _opacity = Math.max(_opacity - 0.1f, 0f)

  private[this] var _downX: PixelFloat = PixelFloat(0)
  private[this] var _downY: PixelFloat = PixelFloat(0)
  private[this] var _downButton: Int   = -1
  def setupTouchDown(downX: PixelFloat, downY: PixelFloat, downButton: Int): Unit = {
    _downX = downX
    _downY = downY
    _downButton = downButton
  }

  def cameraWorldWidth  = _cameraWorldWidth
  def cameraWorldHeight = _cameraWorldHeight
  def frame             = _frame
  def delta             = _delta
  def downX             = _downX
  def downY             = _downY
  def downButton        = _downButton

  val camera    = new OrthographicCamera
  val fpsLogger = new FPSLogger
  val viewport  = new ScreenViewport(camera)

  val modelBatch    = new ModelBatch(RenderShaderProvider)
  val shapeRenderer = new ShapeRenderer()
  val profiler      = new GLProfiler(Gdx.graphics)
  profiler.enable()

  private[this] val _defaultModelCache: ModelCache = new ModelCache(new MyRenderableSorter, new ModelCache.SimpleMeshPool)
  private[this] var _modelCache: ModelCache        = _defaultModelCache
  def modelCache: ModelCache                       = _modelCache

  val unprojectedCameraPosition: Vector3 = new Vector3(0f, 0f, 0f)
  val unprojectedMousePosition: Vector3  = new Vector3(0f, 0f, 0f)
  def mouseX: PixelFloat                 = unprojectedMousePosition.x
  def mouseY: PixelFloat                 = unprojectedMousePosition.y
  def mouseTileX: TileInt                = mouseX.tileInt
  def mouseTileY: TileInt                = mouseY.tileInt

  shapeRenderer.setAutoShapeType(true)
  camera.setToOrtho(true)

  val test = PathAnimation.fromPoints(
    List(
      new Vector2(100f, 100f),
      new Vector2(100f, 200f),
      new Vector2(200f, 200f),
      new Vector2(200f, 100f),
      new Vector2(100f, 100f)
    ),
    resources.arrowRenderable,
    resources.shape.red,
    30f
  )

  def render(game: Game): Unit = {
    unprojectedMousePosition.x = Gdx.input.getX
    unprojectedMousePosition.y = Gdx.input.getY
    camera.unproject(unprojectedMousePosition)

    unprojectedCameraPosition.x = 0f
    unprojectedCameraPosition.y = 0f
    camera.unproject(unprojectedCameraPosition)

    _cameraWorldWidth = PixelFloat(Gdx.graphics.getWidth * camera.zoom)
    _cameraWorldHeight = PixelFloat(Gdx.graphics.getHeight * camera.zoom)

    shapeRenderer.setProjectionMatrix(camera.combined)

    modelCache.begin(camera)
    game.renderContent()
    test.render(this, null, 0f, 0f, 10f)
    modelCache.end()

    modelBatch.begin(camera)
    modelBatch.render(modelCache)
    modelBatch.end()

    _frame = (_frame + 1) % 60
    _delta += Gdx.graphics.getDeltaTime()
  }

  def resize(width: Int, height: Int): Unit = {
    viewport.update(width, height)
  }

  def dispose(): Unit = {
    modelBatch.dispose()
    modelCache.dispose()
    shapeRenderer.dispose()
    RenderShaderProvider.dispose()
    resources.dispose()
  }

  def visibleTileBounds(tiled: Tiled, rootX: PixelFloat = 0, rootY: PixelFloat = 0, offset: TileInt = 0): Tiled = {
    val tileX         = PixelFloat(unprojectedCameraPosition.x - rootX).tileInt - offset
    val tileY         = PixelFloat(unprojectedCameraPosition.y - rootY).tileInt - offset
    val widthInTiles  = cameraWorldWidth.tileInt + 2 + offset
    val heightInTiles = cameraWorldHeight.tileInt + 2 + offset

    SimpleTiled(tiled.clampPosition(tileX, tileY), widthInTiles, heightInTiles)
  }

  def moveCamera(xAmount: Float, yAmount: Float): Unit = {
    camera.translate(500f * xAmount * camera.zoom * Gdx.graphics.getDeltaTime, 500f * yAmount * camera.zoom * Gdx.graphics.getDeltaTime)
    camera.update()
  }

  def zoomCamera(amount: Float): Unit = {
    camera.zoom = MathUtils.clamp(camera.zoom + amount * Gdx.graphics.getDeltaTime, 0.5f, 10f)
    camera.update()
  }

  def setCamera(x: TileInt, y: TileInt): Unit = {
    camera.position.set(x.pixelFloat, y.pixelFloat, camera.position.z)
    camera.update()
  }

  ShaderProgram.pedantic = false
  private val textureShaderProgram = {
    val fragmentShader = scala.io.Source.fromInputStream(Gdx.files.internal(s"assets/shader/texture.frag").read).mkString
    val vertexShader   = scala.io.Source.fromInputStream(Gdx.files.internal(s"assets/shader/texture.vert").read).mkString
    new ShaderProgram(vertexShader, fragmentShader)
  }

  private sealed abstract class RenderShader(shaderProgram: ShaderProgram, renderable: g3d.Renderable) extends BaseShader {

    private val U_PROJ_TRANS = register(
      "u_projTrans",
      new BaseShader.GlobalSetter() {
        override def set(shader: BaseShader, inputID: Int, renderable: g3d.Renderable, combinedAttributes: Attributes) = {
          shader.program.setUniformMatrix("u_projTrans", camera.combined)
        }
      }
    )

    private val U_COLOR = register(
      "u_color",
      new BaseShader.GlobalSetter() {
        override def set(shader: BaseShader, inputID: Int, renderable: g3d.Renderable, combinedAttributes: Attributes) = {
          Gdx.gl.glEnable(GL20.GL_BLEND)
          Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)
          textureShaderProgram.setUniformf("u_color", new Color(1f, 1f, 1f, _opacity))
        }
      }
    )

    override def init() = {
      init(shaderProgram, renderable)
    }
    override def compareTo(other: Shader) = {
      if (other == null) -1
      else if (other eq this) 0
      else 0
    }
  }

  private final class TextureShader(renderable: g3d.Renderable) extends RenderShader(textureShaderProgram, renderable) {
    override def canRender(instance: g3d.Renderable): Boolean = !instance.material.has(OrientationSetShaderAttribute.MIN_TYPE)
  }

  object RenderShaderProvider extends DefaultShaderProvider {
    override def createShader(renderable: g3d.Renderable): Shader = {
      new TextureShader(renderable)
    }
  }
}
