package de.hetzge.sgame.render

import de.hetzge.sgame.unit._
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.VertexAttribute
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import _root_.com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.math.Vector2
import de.hetzge.sgame.frames.Frames
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.math.Quaternion

object Renderable {
  def apply(texture: Texture): TextureRegionRenderable                           = new TextureRegionRenderable(new TextureRegion(texture))
  def apply(textureRegion: TextureRegion): TextureRegionRenderable               = new TextureRegionRenderable(textureRegion)
  def apply(textureRegion: TextureRegion, color: Color): TextureRegionRenderable = new TextureRegionRenderable(textureRegion, color)
  def apply(animation: Animation[TextureRegion]): AnimationRenderable            = new AnimationRenderable(animation)
}
sealed trait Renderable {
  def render(
      renderContext: GameRenderContext,
      userData: AnyRef,
      x: PixelFloat,
      y: PixelFloat,
      z: PixelFloat = 0f,
      originX: PixelFloat = 0f,
      originY: PixelFloat = 0f,
      width: PixelFloat = this.width,
      height: PixelFloat = this.height,
      rotation: Float = 0f
  ): Unit

  def width: Float
  def height: Float
}

sealed trait TextureRenderable extends Renderable {
  def textureRegion: TextureRegion
  final def texture: Texture = textureRegion.getTexture

  def textureWidth: Float  = width
  def textureHeight: Float = height

  def textureX: Float
  def textureY: Float

  /** x, y, width, height */
  def textureCoordinates = (textureX / textureWidth, textureY / textureHeight, width / textureWidth, height / textureHeight)
}

object TextureRegionRenderable {
  val material = new Material()

  val positionAttribute          = new VertexAttribute(VertexAttributes.Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE)
  val colorAttribute             = new VertexAttribute(VertexAttributes.Usage.ColorPacked, 4, ShaderProgram.COLOR_ATTRIBUTE)
  val textureCoordinateAttribute = new VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, ShaderProgram.TEXCOORD_ATTRIBUTE + "0")

  val verticesCount     = 4
  val verticeValueCount = positionAttribute.numComponents + 1 + textureCoordinateAttribute.numComponents
  val valueCount        = verticesCount * verticeValueCount

  val indices = Array[Short](0, 1, 2, 0, 2, 3)

  val vertexAttributes = new VertexAttributes(positionAttribute, colorAttribute, textureCoordinateAttribute)
}

final class TextureRegionRenderable(override val textureRegion: TextureRegion, color: Color = Color.WHITE, val rotationDegrees: Float = 0f) extends TextureRenderable {
  @transient private[this] lazy val _mesh = createMesh(color)
  @transient private[this] val _q         = new Quaternion

  override def render(renderContext: GameRenderContext, userData: AnyRef, x: PixelFloat, y: PixelFloat, z: PixelFloat, originX: PixelFloat, originY: PixelFloat, width: PixelFloat, height: PixelFloat, rotation: Float) = {
    val renderable = new com.badlogic.gdx.graphics.g3d.Renderable()
    renderable.meshPart.set(null, _mesh, 0, _mesh.getNumIndices, GL20.GL_TRIANGLES)

    renderable.worldTransform.set(_q.setEulerAngles(0f, 0f, rotationDegrees + rotation))
    renderable.worldTransform.setTranslation(x, y, z)

    renderable.worldTransform.scale(width, height, 1f)

    renderable.material = TextureRegionRenderable.material
    renderable.userData = userData

    renderContext.modelCache.add(renderable)
  }

  override val width  = textureRegion.getRegionWidth
  override val height = textureRegion.getRegionHeight

  override val textureWidth  = textureRegion.getTexture.getWidth
  override val textureHeight = textureRegion.getTexture.getHeight

  override val textureX = textureRegion.getRegionX
  override val textureY = textureRegion.getRegionY

  lazy val flipedX: TextureRegionRenderable = {
    val newTextureRegion = new TextureRegion(textureRegion)
    newTextureRegion.flip(true, false)
    new TextureRegionRenderable(newTextureRegion, color)
  }

  lazy val flipedY: TextureRegionRenderable = {
    val newTextureRegion = new TextureRegion(textureRegion)
    newTextureRegion.flip(false, true)
    new TextureRegionRenderable(newTextureRegion, color)
  }

  lazy val rotated: TextureRegionRenderable = {
    new TextureRegionRenderable(new TextureRegion(textureRegion), color, rotationDegrees + 90f)
  }

  def createVertices(color: Color): Array[Float] = {
    val (x, y, width, height) = textureCoordinates

    val vertices = new Array[Float](TextureRegionRenderable.valueCount)
    var i        = 0
    vertices(i + 0) = 0.0f
    vertices(i + 1) = 0.0f
    vertices(i + 2) = 0.0f
    vertices(i + 3) = color.toFloatBits
    vertices(i + 4) = x
    vertices(i + 5) = y

    i += TextureRegionRenderable.verticeValueCount
    vertices(i + 0) = 1.0f
    vertices(i + 1) = 0.0f
    vertices(i + 2) = 0.0f
    vertices(i + 3) = color.toFloatBits
    vertices(i + 4) = x + width
    vertices(i + 5) = y

    i += TextureRegionRenderable.verticeValueCount
    vertices(i + 0) = 1.0f
    vertices(i + 1) = 1.0f
    vertices(i + 2) = 0.0f
    vertices(i + 3) = color.toFloatBits
    vertices(i + 4) = x + width
    vertices(i + 5) = y + height

    i += TextureRegionRenderable.verticeValueCount
    vertices(i + 0) = 0.0f
    vertices(i + 1) = 1.0f
    vertices(i + 2) = 0.0f
    vertices(i + 3) = color.toFloatBits
    vertices(i + 4) = x
    vertices(i + 5) = y + height

    vertices
  }

  def createMesh(color: Color): Mesh = {
    val mesh = new Mesh(true, TextureRegionRenderable.verticesCount, TextureRegionRenderable.indices.length, TextureRegionRenderable.vertexAttributes)
    mesh.setVertices(createVertices(color))
    mesh.setIndices(TextureRegionRenderable.indices)
    mesh
  }
}

final class AnimationRenderable(val animation: Animation[TextureRegion]) extends TextureRenderable {
  animation.setPlayMode(Animation.PlayMode.LOOP)

  private[this] val _frameRenderables: Array[TextureRenderable] = animation.getKeyFrames.map(Renderable(_))
  private[this] var _currentFrame: TextureRenderable            = null
  private def currentFrame: TextureRenderable                   = if (_currentFrame != null) _currentFrame else _frameRenderables(0)

  override def render(renderContext: GameRenderContext, userData: AnyRef, x: PixelFloat, y: PixelFloat, z: PixelFloat, originX: PixelFloat, originY: PixelFloat, width: PixelFloat, height: PixelFloat, rotation: Float) = {
    _currentFrame = _frameRenderables(animation.getKeyFrameIndex(renderContext.delta))
    _currentFrame.render(renderContext, null, x, y, z, originX, originY, width, height, rotation)
  }

  override def width  = currentFrame.width
  override def height = currentFrame.height

  override def textureRegion = currentFrame.textureRegion

  override def textureWidth  = currentFrame.textureWidth
  override def textureHeight = currentFrame.textureHeight

  override def textureX = currentFrame.textureX
  override def textureY = currentFrame.textureY
}

final case class TiledRenderable(val renderable: TextureRenderable) extends TextureRenderable {
  override def render(renderContext: GameRenderContext, userData: AnyRef, x: PixelFloat, y: PixelFloat, z: PixelFloat, originX: PixelFloat, originY: PixelFloat, width: PixelFloat, height: PixelFloat, rotation: Float) = {
    for (wx <- 0 until width.tileInt; hy <- 0 until height.tileInt) {
      renderable.render(
        renderContext,
        null,
        x = x + wx * TILE_SIZE_FLOAT,
        y = y + hy * TILE_SIZE_FLOAT,
        z = z,
        originX,
        originY,
        width = TILE_SIZE_FLOAT,
        height = TILE_SIZE_FLOAT,
        rotation
      )
    }
  }

  override def textureRegion = renderable.textureRegion
  override def textureX      = renderable.textureX
  override def textureY      = renderable.textureY
  override def width         = renderable.width
  override def height        = renderable.height
}

object OrientationSetRenderable {
  private val positionAttribute          = new VertexAttribute(VertexAttributes.Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE)
  private val textureCoordinateAttribute = new VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, ShaderProgram.TEXCOORD_ATTRIBUTE + "0")
  private val colorAttribute             = new VertexAttribute(VertexAttributes.Usage.ColorPacked, 4, ShaderProgram.COLOR_ATTRIBUTE)

  private val perVerticeCount = positionAttribute.numComponents + textureCoordinateAttribute.numComponents + 1
  private val verticesCount   = 9
  private val valueCount      = perVerticeCount * verticesCount

  val indices: Array[Short] = Array[Short](0, 1, 2, 2, 3, 0, 3, 4, 0, 4, 5, 0, 5, 6, 0, 6, 7, 0, 7, 8, 0, 8, 1, 0)

  private val vertexAttributes = new VertexAttributes(positionAttribute, textureCoordinateAttribute, colorAttribute)
}

object PathAnimation {
  def fromPositions(positions: Seq[Position], renderable: Renderable, shapeRenderable: Renderable, space: Float): PathAnimation = {
    new PathAnimation(
      positions
        .sliding(2)
        .filter(pair => pair.head.x != pair.last.x || pair.head.y != pair.last.y)
        .map(
          pair =>
            new PathAnimationSegment(
              ax = pair.head.x.pixelFloat + HALF_TILE_SIZE_FLOAT,
              ay = pair.head.y.pixelFloat + HALF_TILE_SIZE_FLOAT,
              bx = pair.last.x.pixelFloat + HALF_TILE_SIZE_FLOAT,
              by = pair.last.y.pixelFloat + HALF_TILE_SIZE_FLOAT
            )
        )
        .toList,
      renderable,
      shapeRenderable,
      space
    )
  }
  def fromPoints(points: List[Vector2], renderable: Renderable, shapeRenderable: Renderable, space: Float): PathAnimation = {
    new PathAnimation(
      points
        .sliding(2)
        .filter(pair => pair.head.x != pair.last.x || pair.head.y != pair.last.y)
        .map(
          pair =>
            new PathAnimationSegment(
              ax = pair.head.x,
              ay = pair.head.y,
              bx = pair.last.x,
              by = pair.last.y
            )
        )
        .toList,
      renderable,
      shapeRenderable,
      space
    )
  }
}
final class PathAnimation(segments: List[PathAnimationSegment], renderable: Renderable, shapeRenderable: Renderable, space: Float) extends Renderable {
  override def render(
      renderContext: GameRenderContext,
      userData: AnyRef,
      x: PixelFloat,
      y: PixelFloat,
      z: PixelFloat = 0f,
      originX: PixelFloat = 0f,
      originY: PixelFloat = 0f,
      width: PixelFloat = this.width,
      height: PixelFloat = this.height,
      rotation: Float = 0f
  ): Unit = {
    val target          = new Vector2()
    val zoomedSpace     = space * renderContext.camera.zoom
    val progress: Float = renderContext.delta % 1f
    for (segment <- segments) {
      renderContext.resources.drawLine(
        renderContext,
        shapeRenderable,
        x1 = segment.ax,
        y1 = segment.ay,
        x2 = segment.bx,
        y2 = segment.by,
        z = z,
        thickness = 3f * renderContext.camera.zoom
      )
      val count = Math.floor(segment.c / zoomedSpace).toInt
      for (i <- 0 until count) {
        segment.pos(progress, target, zoomedSpace, i)
        renderable.render(
          renderContext,
          null,
          x = x + target.x - renderable.width / 2f,
          y = y + target.y - renderable.height / 2f,
          z = z + 1,
          width = renderable.width * renderContext.camera.zoom,
          height = renderable.height * renderContext.camera.zoom,
          rotation = segment.alpha
        )
      }
    }
  }

  override def width: Float  = 0f
  override def height: Float = 0f
}

private final class PathAnimationSegment(val ax: PixelFloat, val ay: PixelFloat, val bx: PixelFloat, val by: PixelFloat) {
  import Math._
  require(ax != bx || ay != by)
  // https://www.arndt-bruenner.de/mathe/scripts/dreiecksberechnungrw.htm
  val a: Float = by - ay
  val b: Float = bx - ax
  val c: Float = sqrt(a * a + b * b).toFloat
  val q: Float = a * a / c
  val p: Float = c - q
  val h: Float = sqrt(p * q).toFloat
  val alpha: Float =
    if (ay == by)
      if (ax < bx) 0
      else 180
    else if (ax == bx)
      if (ay < by) 90
      else 270
    else Math.toDegrees(atan(h / p)).toFloat

  /*   println(s"""
    a: $a
    b: $b
    c: $c
    q: $q
    p: $p
    h: $h
    alpha: $alpha
  """) */

  @inline def pos(progress: Float, target: Vector2, space: Float, i: Int): Unit = {
    val xSpace: Float = b / c * space
    val ySpace: Float = a / c * space
    target.x = ax + (xSpace * i) + (xSpace * progress)
    target.y = ay + (ySpace * i) + (ySpace * progress)
  }
}
