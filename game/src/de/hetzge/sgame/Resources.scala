package de.hetzge.sgame.resources

import java.{util => ju}
import java.util.Base64

import de.hetzge.sgame.base._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.render._
import de.hetzge.sgame.tile._
import de.hetzge.sgame.environment._
import de.hetzge.sgame.item._
import de.hetzge.sgame.area._
import de.hetzge.sgame.person._
import de.hetzge.sgame.player._
import de.hetzge.sgame.tilemaploader._
import de.hetzge.sgame.base.helper._
import play.api.libs.json._
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d._
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle
import com.badlogic.gdx.scenes.scene2d.utils.{NinePatchDrawable, TextureRegionDrawable}
import scala.annotation.tailrec
import scala.collection.mutable
import scala.collection.mutable.HashMap

final class ApplicationResources {
  private lazy val atlas    = new TextureAtlas(Gdx.files.internal("assets/generated/pack.atlas"), true)

  object region {
    private[this] def load(regionName: String): AtlasRegion = {
      val region = atlas.findRegion(regionName)
      region.flip(false, true)
      region
    }

    val test5            = load("test5")
    val shape            = load("shape")
    val empty            = load("empty")
    val item_wood        = load("item_wood")
    val item_wood_blank  = load("item_wood_blank")
    val item_stone       = load("item_stone")
    val item_stone_block = load("item_stone_block")
    val item_dummy       = load("item_dummy")
    val sprite           = load("sprite.generated")
    val stock            = load("stock")
    val building1        = load("building1.generated")
    val building2        = load("building2.generated")
    val building3        = load("building3.generated")
    val buildingx1       = load("buildingx1.generated")
    val buildingx2       = load("buildingx2.generated")
    val buildingx3       = load("buildingx3.generated")
    val buildingx4       = load("buildingx4.generated")
    val landmark         = load("landmark")
    val cloud            = load("cloud")
    val cursor           = load("cursor.generated")
    val border           = load("border")
    val close            = load("close.generated")
    val mountain         = load("mountain.generated")
    val mountain_water   = load("mountain_water.generated")
    val mountain_open    = load("mountain_open.generated")
    val arrow            = load("arrow")

    val tree1 = load("tree1.generated")
    val tree2 = load("tree2.generated")
    val tree3 = load("tree3.generated")
    val tree4 = load("tree4.generated")
    val tree5 = load("tree5.generated")
    val tree6 = load("tree6.generated")

    val stone1 = load("stone1.generated")
    val stone2 = load("stone2.generated")
    val stone3 = load("stone3.generated")
    val stone4 = load("stone4.generated")

    val crop1 = load("crop1.generated")
    val crop2 = load("crop2.generated")

    val button_square_beige         = load("buttonSquare_beige.generated")
    val button_square_beige_pressed = load("buttonSquare_beige_pressed.generated")

    val button_square_blue         = load("buttonSquare_blue.generated")
    val button_square_blue_pressed = load("buttonSquare_blue_pressed.generated")

    val button_square_brown         = load("buttonSquare_brown.generated")
    val button_square_brown_pressed = load("buttonSquare_brown_pressed.generated")

    val button_square_grey         = load("buttonSquare_grey.generated")
    val button_square_grey_pressed = load("buttonSquare_grey_pressed.generated")

    val buttonRound_grey = load("buttonRound_grey.generated")

    val panel_blue = load("panel_blue.generated")

    val barBack_verticalMid = load("barBack_verticalMid.generated")

    val arial15 = load("arial15")

    val grass_packed           = load("grass_packed.generated")
    val water_packed           = load("water_packed.generated")
    val desert_packed          = load("desert_packed.generated")
    val cloud_packed           = load("cloud_packed.generated")
    val default_way_packed     = load("default_way_packed.generated")
    val stone_way_packed       = load("stone_way_packed.generated")
    val stone_way_build_packed = load("stone_way_build_packed.generated")

    object icon {
      val delete = load("icon/delete")
      val goto   = load("icon/goto")
      val start  = load("icon/start")
    }

    object ui {
      val item          = load("ui/item")
      val item_demand   = load("ui/item_demand")
      val item_demanded = load("ui/item_demanded")
      val item_blocked  = load("ui/item_blocked")
    }
  }

  object nine {
    private val PADDING = 5

    val button_square_beige         = new NinePatch(region.button_square_beige, PADDING, PADDING, PADDING, PADDING)
    val button_square_beige_pressed = new NinePatch(region.button_square_beige_pressed, PADDING, PADDING, PADDING, PADDING)
    val button_square_blue          = new NinePatch(region.button_square_blue, PADDING, PADDING, PADDING, PADDING)
    val button_square_blue_pressed  = new NinePatch(region.button_square_blue_pressed, PADDING, PADDING, PADDING, PADDING)
    val button_square_brown         = new NinePatch(region.button_square_brown, PADDING, PADDING, PADDING, PADDING)
    val button_square_brown_pressed = new NinePatch(region.button_square_brown_pressed, PADDING, PADDING, PADDING, PADDING)
    val button_square_grey          = new NinePatch(region.button_square_grey, PADDING, PADDING, PADDING, PADDING)
    val button_square_grey_pressed  = new NinePatch(region.button_square_grey_pressed, PADDING, PADDING, PADDING, PADDING)

    val buttonRound_grey = new NinePatch(region.buttonRound_grey, 6, 6, 6, 6)

    val panel_blue = new NinePatch(region.panel_blue, PADDING, PADDING, PADDING, PADDING)

    val border = new NinePatch(region.border, 20, 20, 20, 20)
  }

  private def loadMountainTiles(region: TextureRegion, collision: Boolean): Array[Tile] = {
    for (region <- region.split(TILE_SIZE, TILE_SIZE)(0)) yield {
      region.flip(false, true)
      new Tile(TileType.Mountain, Renderable(region), collision)
    }
  }

  val mountainTiles      = loadMountainTiles(region.mountain, true)
  val mountainWaterTiles = loadMountainTiles(region.mountain_water, true)
  val mountainOpenTiles  = loadMountainTiles(region.mountain_open, false)

  val mountainTileSetMapping      = new MountainTileSetMapping(mountainTiles)
  val mountainWaterTileSetMapping = new MountainTileSetMapping(mountainWaterTiles)
  val mountainOpenTileSetMapping  = new MountainTileSetMapping(mountainOpenTiles)

  val orientationTileSetMapping: OrientationTileSetMapping = {
    def group(tileType: TileType, atlasRegion: AtlasRegion, collision: Boolean = false): OrientationTileGroup = {
      val splited = atlasRegion.split(TILE_SIZE, TILE_SIZE)(0)
      def pair(orientationSet: OrientationSet, i: Int): OrientationTilePair = {
        splited(i).flip(false, true)
        OrientationTilePair(orientationSet, new Tile(tileType, Renderable(splited(i)), collision))
      }

      OrientationTileGroup(
        tileType,
        List(
          pair(OrientationSet(Center), 0),
          pair(OrientationSet(Center, North), 1),
          pair(OrientationSet(Center, North, South), 2),
          pair(OrientationSet(Center, South, East), 3),
          pair(OrientationSet(Center, South, SouthEast, East), 4),
          pair(OrientationSet(Center, North, South, East), 5),
          pair(OrientationSet(Center, North, West, East, South), 6),
          pair(OrientationSet(Center, North, South, East, SouthEast), 7),
          pair(OrientationSet(Center, North, South, East, SouthEast, NorthEast), 8),
          pair(OrientationSet(Center, North, West, East, SouthWest, South), 9),
          pair(OrientationSet(Center, North, NorthWest, West, East, South, SouthEast), 10),
          pair(OrientationSet(Center, North, NorthWest, West, East, South, SouthWest), 11),
          pair(OrientationSet(Center, North, NorthWest, West, East, South, SouthWest, SouthEast), 12),
          pair(OrientationSet(Orientation.ALL_VALUES), 13)
        )
      )
    }

    OrientationTileSetMapping(
      List(
        group(TileType.Grass, new AtlasRegion(region.grass_packed)),
        group(TileType.Desert, new AtlasRegion(region.desert_packed)),
        group(TileType.Water, new AtlasRegion(region.water_packed), true),
        group(TileType.Cloud, new AtlasRegion(region.cloud_packed)),
        group(TileType.DefaultWay, new AtlasRegion(region.default_way_packed)),
        group(TileType.StoneWay, new AtlasRegion(region.stone_way_packed)),
        group(TileType.BuildStoneWay, new AtlasRegion(region.stone_way_build_packed))
      )
    )
  }

  val tileSet: TileSet = new SimpleTileSet(orientationTileSetMapping.tiles ++ mountainTiles ++ mountainWaterTiles ++ mountainOpenTiles)

  object building {
    val stock        = Renderable(region.stock)
    val main         = Renderable(region.building1)
    val streetWorker = Renderable(region.building2)
    val lumberjack   = Renderable(region.building3)
    val building1    = Renderable(region.buildingx1)
    val building2    = Renderable(region.buildingx2)
    val building3    = Renderable(region.buildingx3)
    val building4    = Renderable(region.buildingx4)
    val landmark     = Renderable(region.landmark)
  }

  object item {
    private[this] def load(textureRegion: TextureRegion): ItemRenderableSet = {
      ItemRenderableSet(
        default = Renderable(textureRegion),
        supply = Renderable(textureRegion, new Color(1f, 1f, 1f, 1.0f)),
        supplied = Renderable(textureRegion, new Color(1f, 1f, 1f, 0.9f)),
        demanded = Renderable(textureRegion, new Color(1f, 1f, 1f, 0.7f)),
        demand = Renderable(textureRegion, new Color(1f, 1f, 1f, 0.6f))
      )
    }

    val wood       = load(region.item_wood)
    val woodBlank  = load(region.item_wood_blank)
    val stone      = load(region.item_stone)
    val stoneBlock = load(region.item_stone_block)
    val dummy      = load(region.item_dummy)
  }

  object way {
    val default = new WayRenderableSet(orientationTileSetMapping, TileType.DefaultWay, TileType.DefaultWay)
    val stone   = new WayRenderableSet(orientationTileSetMapping, TileType.StoneWay, TileType.BuildStoneWay)
  }

  object environment {
    val forest = EnvironmentRenderableSet(Array(Array(region.tree1, region.tree2, region.tree3, region.tree4, region.tree5, region.tree6).map(Renderable(_)): _*))
    val stone  = EnvironmentRenderableSet(Array(Array(region.stone1, region.stone2, region.stone3, region.stone4).map(Renderable(_)): _*))
    val crop   = EnvironmentRenderableSet(Array(Array(region.crop1, region.crop2).map(Renderable(_)): _*))
  }

  object skin extends Skin {
    object key {
      val DEFAULT      = "default"
      val DEFAULT_FONT = "DEFAULT_FONT"
    }
    addRegions(atlas)
    add(key.DEFAULT, new TextButtonStyle(textButtonStyle))
    add(key.DEFAULT, new ImageButtonStyle(imageButtonStyle))
    add(key.DEFAULT, new LabelStyle(labelStyle))
    add(key.DEFAULT, new WindowStyle(windowStyle))
    add(key.DEFAULT, new ScrollPaneStyle(scrollPaneStyle))

    object buttonStyle extends ButtonStyle {
      up = new NinePatchDrawable(nine.button_square_beige_pressed)
      down = new NinePatchDrawable(nine.button_square_brown_pressed)
      checked = down
      over = new NinePatchDrawable(nine.button_square_grey_pressed)
    }

    object imageButtonStyle extends ImageButtonStyle(buttonStyle)

    object textButtonStyle extends TextButtonStyle {
      font = ApplicationResources.this.font
      fontColor = Color.WHITE
      downFontColor = Color.GRAY
      overFontColor = Color.RED
      checkedFontColor = Color.BLUE
      checkedOverFontColor = Color.CYAN
      disabledFontColor = Color.LIGHT_GRAY

      up = buttonStyle.up
      down = buttonStyle.down
      checked = buttonStyle.checked
      over = buttonStyle.over
    }

    object labelStyle extends LabelStyle {
      font = ApplicationResources.this.font
      fontColor = Color.WHITE
    }

    object windowStyle extends WindowStyle {
      background = new NinePatchDrawable(nine.panel_blue)
      titleFont = font
      titleFontColor = Color.WHITE
    }

    object scrollPaneStyle extends ScrollPaneStyle {
      hScroll = new TextureRegionDrawable(region.barBack_verticalMid)
      hScrollKnob = new NinePatchDrawable(nine.buttonRound_grey)
      vScroll = new TextureRegionDrawable(region.barBack_verticalMid)
      vScrollKnob = new NinePatchDrawable(nine.buttonRound_grey)
    }
  }

  val font = new BitmapFont(Gdx.files.absolute("assets/arial15.fnt"), region.arial15, false)

  object shape {
    val white            = Renderable(region.shape, Color.WHITE)
    val whiteTransparent = Renderable(region.shape, new Color(Color.rgba8888(1f, 1f, 1f, 0.5f)))
    val red              = Renderable(region.shape, Color.RED)
  }

  val shapeTiledRenderable  = TiledRenderable(shape.white)
  val waypointRenderable    = Renderable(region.test5)
  val emptyRenderable       = Renderable(region.empty)
  val buildingLotRenderable = TiledRenderable(building.stock)
  val arrowRenderable       = Renderable(region.arrow)

  def dispose(): Unit = {
    atlas.dispose()
  }

  def drawRectangle(renderContext: GameRenderContext, renderable: Renderable, x: PixelFloat, y: PixelFloat, z: PixelFloat, width: PixelFloat, height: PixelFloat, thickness: PixelFloat = 1f): Unit = {
    val halfThickness = thickness / 2

    drawLine(renderContext, renderable, x, y, x + width, y, z, thickness)
    drawLine(renderContext, renderable, x, y + height, x + width, y + height, z, thickness)

    drawLine(renderContext, renderable, x + halfThickness, y + halfThickness, x + halfThickness, y + halfThickness + height, z, thickness)
    drawLine(renderContext, renderable, x + halfThickness + width, y + halfThickness, x + halfThickness + width, y + halfThickness + height, z, thickness)
  }

  def drawLine(renderContext: GameRenderContext, renderable: Renderable, x1: PixelFloat, y1: PixelFloat, x2: PixelFloat, y2: PixelFloat, z: PixelFloat = 0f, thickness: PixelFloat = 1f): Unit = {
    val dx: Float        = x2 - x1
    val dy: Float        = y2 - y1
    val dist: PixelFloat = Math.sqrt(dx * dx + dy * dy).toFloat
    val deg: Float       = Math.toDegrees(Math.atan2(dy, dx)).toFloat

    renderable.render(
      renderContext,
      userData = null,
      x = x1,
      y = y1 - thickness / 2f,
      z = z,
      width = dist,
      height = thickness,
      rotation = deg
    )
  }

  object person {
    val one: PersonRenderableSet   = new SimplePersonRenderableSet(0, 0)
    val two: PersonRenderableSet   = new SimplePersonRenderableSet(3, 0)
    val three: PersonRenderableSet = new SimplePersonRenderableSet(6, 0)
  }

  private class SimplePersonRenderableSet(offsetX: TileInt, offsetY: TileInt) extends PersonRenderableSet {
    private val textureRegions = region.sprite.split(32, 32)

    override val person_idle = {
      Renderable(textureRegions(0 + offsetY)(1 + offsetX))
    }

    override val person_walk_south = {
      val frames = for (x <- 0 to 2 if x != 1) yield textureRegions(0 + offsetY)(x + offsetX)
      Renderable(new Animation(0.25f, frames: _*))
    }

    override val person_walk_east = {
      val frames = for (x <- 0 to 2 if x != 1) yield textureRegions(2 + offsetY)(x + offsetX)
      Renderable(new Animation(0.25f, frames: _*))
    }

    override val person_walk_west = {
      val frames = for (x <- 0 to 2 if x != 1) yield textureRegions(1 + offsetY)(x + offsetX)
      Renderable(new Animation(0.25f, frames: _*))
    }

    override val person_walk_north = {
      val frames = for (x <- 0 to 2 if x != 1) yield textureRegions(3 + offsetY)(x + offsetX)
      Renderable(new Animation(0.25f, frames: _*))
    }
  }

}

trait PersonRenderableSet {
  val person_idle: Renderable
  val person_walk_south: Renderable
  val person_walk_east: Renderable
  val person_walk_north: Renderable
  val person_walk_west: Renderable
  def person_default = person_walk_south

  def apply(orientation: Orientation, state: PersonState): Renderable = {
    state match {
      case PersonState.Idle => person_idle
      case PersonState.Walk =>
        orientation match {
          case South => person_walk_south
          case North => person_walk_north
          case West  => person_walk_west
          case East  => person_walk_east
          case _     => person_default
        }
      case PersonState.Work =>
        orientation match { // TODO
          case South => person_walk_south
          case North => person_walk_north
          case West  => person_walk_west
          case East  => person_walk_east
          case _     => person_default
        }
    }
  }
}

final class WayRenderableSet(
    tileMapping: OrientationTileSetMapping,
    defaultType: TileType,
    disabledType: TileType
) {
  def defaultTile(aroundOrientationTileSet: OrientationSet): Tile  = tileMapping.get(defaultType, aroundOrientationTileSet)
  def disabledTile(aroundOrientationTileSet: OrientationSet): Tile = tileMapping.get(disabledType, aroundOrientationTileSet)
}
