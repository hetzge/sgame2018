package de.hetzge.sgame.server

import de.hetzge.sgame.action._
import de.hetzge.sgame.base._
import de.hetzge.sgame.frames._
import de.hetzge.sgame.game._
import de.hetzge.sgame.network._
import de.hetzge.sgame.player._

final case class ActionMessage(action: Action) extends Message
object TriggerFrameMessage {
  def apply(frame: Frame): TriggerFrameMessage = new TriggerFrameMessage(frame.id, frame.getActions.toList)
}
final case class TriggerFrameMessage(frameId: FrameId, actions: List[Action]) extends Message
final case class StartGameMessage(gameConfiguration: GameConfiguration, frameMessages: List[TriggerFrameMessage]) extends Message {
  def fromFrameId: FrameId = frameMessages.headOption.map(_.frameId).getOrElse(FrameId(0))
  def toFrameId: FrameId   = frameMessages.lastOption.map(_.frameId).getOrElse(FrameId(0))
}
final case class PlayerConnectedMessage(client: Client, playerIdValue: Byte) extends Message {
  def playerId: PlayerId           = PlayerId.get(playerIdValue)
  def networkPlayer: NetworkPlayer = new NetworkPlayer(playerId, client.name, client)
}

object ServerApp extends App {
  new Server().start
}

object Server {
  val defaultPort = 1235
}
final class Server {
  private[this] val _gameConfiguration = GameConfiguration(playerCount = 5)
  private[this] val _players           = new Players(_gameConfiguration.playerCount)
  private[this] var _gameRunning       = false
  private[this] val _network           = new Network(Client("Server"), Server.defaultPort)
  private[this] val _frames            = new Frames(ByTime)
  _frames.setCallback(onFrame(_))
  private[this] val _firstFrame = _frames.current

  def start: Unit = {
    println("start server")
    _network.start
    println("network started")

    // wait for players
    while (true) {
      _network.handle { (client, message) =>
        println(s"server received: '$message' from '$client'")
        message match {
          case ActionMessage(action) => {
            _frames.next.add(action)
          }
          case ConnectedMessage(_) => {
            _network.send(client, StartGameMessage(_gameConfiguration, pastFrames()))

            _players.networkPlayer(client) match {
              case Some(networkPlayer) => {
                // ignore ??!
              }
              case None => {
                _players.nextEmptyPlayer match {
                  case Some(emptyPlayer) => {
                    val playerConnectedMessage = PlayerConnectedMessage(client, emptyPlayer.id.value)
                    val networkPlayer          = playerConnectedMessage.networkPlayer
                    _players.setPlayer(networkPlayer)
                    _network.sendAll(playerConnectedMessage)

                    println(s"$client is now player id ${networkPlayer.id.value}")
                  }
                  case None => {
                    println(s"Disconnect '$client' because all slots are in use.")
                    _network.disconnect(client)
                  }
                }
              }
            }

            _gameRunning = true
            println("game running")
          }
          case _ =>
        }
      }

      if (_gameRunning) {
        _frames.processFrame()
      }

      Utils.sleep(10)
    }
  }

  private def onFrame(frame: Frame): Unit = {
    _network.sendAll(TriggerFrameMessage(frame))
  }

  private def pastFrames(): List[TriggerFrameMessage] = {
    val fromFrame = _firstFrame.next
    val toFrame   = _frames.current

    println(s"past frames from '${fromFrame.id}' to '${toFrame.id}'")
    fromFrame.iterator(toFrame.id).map(TriggerFrameMessage(_)).toList
  }
}
