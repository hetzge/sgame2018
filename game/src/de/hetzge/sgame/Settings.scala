package de.hetzge.sgame.settings

import de.hetzge.sgame.building._
import de.hetzge.sgame.game._
import de.hetzge.sgame.input._
import de.hetzge.sgame.person._
import de.hetzge.sgame.player._
import de.hetzge.sgame.way._
import de.hetzge.sgame.waypoint._
import de.hetzge.sgame.item._
import de.hetzge.sgame.unit._

final class LocalSettings(game: Game) extends SelectionLocalSettings {
  private[this] var _areaInput: AreaInput      = SelectionAreaInput
  def areaInput                                = _areaInput
  def setAreaInput(areaInput: AreaInput): Unit = _areaInput = areaInput

  private[this] var _player: Option[Player] = None
  def setPlayer(player: Player): Unit = {
    println(s"Local player is set to $player")
    _player = Some(player)
    game.area.resetFogOfWar()
  }
  def player: Player = {
    require(_player.isDefined, "No player is set")
    _player.get
  }
  def playerOption: Option[Player] = _player

  var showDebug: Boolean     = false
  var showFogOfWar: Boolean  = true
  var showBorders: Boolean   = true
  var showOwnership: Boolean = false
}

sealed trait SelectionLocalSettings { localSettings: LocalSettings =>
  private[this] var _selection: Option[Selection] = None

  def selection: Option[Selection]       = _selection
  def selectedBuilding: Option[Building] = _selection.collect { case BuildingSelection(building) => building }
  def selectedPerson: Option[Person]     = _selection.collect { case PersonSelection(person) => person }

  def deselectAll(): Unit = {
    _selection = None
  }

  def setSelection(selection: Selection): Unit = {
    _selection = Some(selection)
  }
}

sealed trait Selection
final case class ItemSelection(item: Item)             extends Selection
final case class BuildingSelection(building: Building) extends Selection
final case class PersonSelection(person: Person)       extends Selection
final case class PositionSelection(position: Position) extends Selection
