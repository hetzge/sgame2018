package de.hetzge.sgame.stock

import de.hetzge.sgame.item._
import de.hetzge.sgame.render._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.way._
import de.hetzge.sgame.waypoint._
import de.hetzge.sgame.export._
import de.hetzge.sgame.format

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import de.hetzge.sgame.format.StockClass
import de.hetzge.sgame.format.StockClass.MASTER
import de.hetzge.sgame.format.StockClass.MULTI
import de.hetzge.sgame.format.StockClass.NONE
import de.hetzge.sgame.format.StockClass.SINGLE
import de.hetzge.sgame.format.StockClass.SUB
import de.hetzge.sgame.format.StockType.DUMMY_ST
import de.hetzge.sgame.format.StockType.ITEM_TYPE
import de.hetzge.sgame.format.StockType.MIXED
import de.hetzge.sgame.format.StockType.SINGLE_ITEM

final case class StockProto(
  stockClass: String,
  stockType: String,
  maxItemCount: Int,
  itemType: String,
  master: ReferenceProto,
  stocks: List[ReferenceProto],
  items: List[ReferenceProto]

  // Entity is linked from other side
)

sealed trait StockType extends Serializable {
  val maxItemCount: Int  = 0
  val itemType: ItemType = DummyItemType
  def canContain(itemType: ItemType): Boolean
  def exportType: format.StockType
}

/** Not a real stock. Can be used as Null-Object pattern. */
case object DummyStockType extends StockType {
  override val maxItemCount                   = 0
  override def canContain(itemType: ItemType) = false
  override def exportType                     = format.StockType.DUMMY_ST
}

/** Can contain a stack of a specific ItemType */
final case class ItemTypeStockType(override val itemType: ItemType, override val maxItemCount: Int = 8) extends StockType {
  override def canContain(itemType: ItemType) = this.itemType == itemType
  override def exportType                     = format.StockType.ITEM_TYPE
}

/** Can contain a stack of mixed ItemTypes */
sealed case class MixedStockType(override val maxItemCount: Int = 8) extends StockType {
  override def canContain(itemType: ItemType) = true
  override def exportType                     = format.StockType.MIXED
}

/** Can contain one instance of every ItemType */
case object SingleItemStockType extends StockType {
  override val maxItemCount                   = 1
  override def canContain(itemType: ItemType) = true
  override def exportType                     = format.StockType.SINGLE_ITEM
}

object RenderStock {
  val OFFSET: PixelInt = 5
}
sealed trait RenderStock { stock: Stock =>
  def render(renderContext: GameRenderContext, x: PixelFloat, y: PixelFloat, z: PixelFloat): Unit = {
    var i: Int = 0
    for (item <- items) {
      if (item.isReal) {
        item.render(
          renderContext,
          x = x + i % 4 * RenderStock.OFFSET,
          y = y + i / 4 * RenderStock.OFFSET,
          z
        )
        i += 1
      }
    }
  }
}

sealed trait EntityStock { stock: Stock =>
  private[this] var _entity: StockEntity                 = StockEntity.None
  def entity                                             = _entity
  private[this] def setEntity(entity: StockEntity): Unit = _entity = entity
  def hasEntity                                          = _entity != StockEntity.None

  /* Set the entity of the stock, but throws a exception if the stock is already assigned to a entity. */
  def assignEntity(entity: StockEntity): Unit = {
    require(!hasEntity)
    setEntity(entity)
  }
}

sealed trait ExportStock extends ExportObject[format.Stock] with ExportIndexedObject { stock: Stock =>

  override def export()(implicit index: ExportedIndex) = format.Stock(
    `type` = stockType.exportType,
    maxItemCount = stockType.maxItemCount,
    itemType = stockType.itemType.exportType,
    _class = exportStockClass,
    master = Some(index(master)),
    items = items.map(index(_)),
    stocks = stocks.map(index(_))
  )

  override def initLoad(f: format.Stock)(implicit index: ImportedIndex) = {
    this match {
      case baseStock: BaseStock => {
        baseStock._items.clear()
        baseStock._items ++= f.items.map(index[Item](_))
      }
      case _ => // ignore
    }
  }

  override def index() = ExportIndex(
    objects = (items ++ stocks).toSet
  )

}

object ImportStock {

  def load(f: format.Stock)(implicit index: ImportedIndex): Stock = {
    def stockType: StockType = f.`type` match {
      case DUMMY_ST => DummyStockType
      case ITEM_TYPE =>
        ItemTypeStockType(
          itemType = ItemType(f.itemType),
          maxItemCount = f.maxItemCount
        )
      case MIXED =>
        MixedStockType(
          maxItemCount = f.maxItemCount
        )
      case SINGLE_ITEM => SingleItemStockType
    }
    f._class match {
      case MASTER => new MasterStock()
      case MULTI =>
        new MultiStock(
          stocks = f.stocks.map(index[Stock](_))
        )
      case NONE => Stock.None
      case SINGLE =>
        new SingleStock(
          stockType = stockType
        )
      case SUB =>
        new SubStock(
          stockType = stockType,
          _master = f.master.map(index[MasterStock](_))
        )
    }
  }

}

sealed trait Stock extends RenderStock with EntityStock with ExportStock {
  def master: Stock

  def items: Seq[Item]
  private[stock] def stocks: Seq[Stock]
  def stockType: StockType
  def exportStockClass: format.StockClass

  def onDestroy(economy: ItemEconomy): Unit

  def add(item: Item): Unit
  def remove(item: Item): Unit
  def replace(item: Item, newItem: Item): Unit

  def contains(item: Item): Boolean = items.contains(item)

  def supplyItems: Seq[SupplyItem]                       = items.collect { case s: SupplyItem                       => s }
  def suppliedItems: Seq[SuppliedItem]                   = items.collect { case s: SuppliedItem                     => s }
  def suppliedItems(usage: ItemUsage): Seq[SuppliedItem] = items.collect { case s: SuppliedItem if s.usage == usage => s }
  def demandItems: Seq[DemandItem]                       = items.collect { case d: DemandItem                       => d }
  def demandedItems: Seq[DemandedItem]                   = items.collect { case d: DemandedItem                     => d }
  def demandedItems(usage: ItemUsage): Seq[DemandedItem] = items.collect { case d: DemandedItem if d.usage == usage => d }
  def blockedItems: Seq[BlockedItem]                     = items.collect { case b: BlockedItem                      => b }
  def noItems: Seq[NoItem]                               = items.collect { case n: NoItem                           => n }

  def supplyItem: Option[SupplyItem]     = items.find(_.isInstanceOf[SupplyItem]).map(_.asInstanceOf[SupplyItem])
  def suppliedItem: Option[SuppliedItem] = items.find(_.isInstanceOf[SuppliedItem]).map(_.asInstanceOf[SuppliedItem])
  def demandItem: Option[DemandItem]     = items.find(_.isInstanceOf[DemandItem]).map(_.asInstanceOf[DemandItem])
  def demandedItem: Option[DemandedItem] = items.find(_.isInstanceOf[DemandedItem]).map(_.asInstanceOf[DemandedItem])
  def blockedItem: Option[BlockedItem]   = items.find(_.isInstanceOf[BlockedItem]).map(_.asInstanceOf[BlockedItem])

  def blockedItem(itemType: ItemType): Option[BlockedItem] =
    items.toStream.collect {
      case item: BlockedItem if item.itemType == itemType => item
    }.headOption
  def demandingItems: Seq[Item] = demandedItems ++ demandItems

  def hasSuppliedItems: Boolean = !suppliedItems.isEmpty
  def hasDemandItems: Boolean   = !demandItems.isEmpty
  def hasDemandedItems: Boolean = !demandedItems.isEmpty
  def isDemanding: Boolean      = hasDemandItems || hasDemandedItems

  def noItems(itemType: ItemType): Seq[NoItem] = items.collect {
    case noItem: NoItem if noItem.itemType == itemType || noItem.itemType == DummyItemType => noItem
  }
  def noItem(itemType: ItemType): Option[NoItem] = {
    items
      .find {
        case noItem: NoItem if noItem.itemType == itemType || noItem.itemType == DummyItemType => true
        case _                                                                                 => false
      }
      .map(_.asInstanceOf[NoItem])
  }
  def hasNoItem(itemType: ItemType): Boolean = items.exists {
    case noItem: NoItem if noItem.itemType == itemType || noItem.itemType == DummyItemType => true
    case _                                                                                 => false
  }

  def demand(economy: ItemEconomy, itemTypes: Seq[ItemType], usage: ItemUsage = ItemUsage.Default): Unit = {
    for (itemType <- itemTypes) {
      noItem(itemType) match {
        case Some(noItem) => noItem.demand(economy, itemType, usage)
        case None         => throw new IllegalStateException(s"Can't demand item type '$itemType' (no noitem available)")
      }
    }
  }
  def supply(economy: ItemEconomy, itemTypes: Seq[ItemType], usage: ItemUsage = ItemUsage.Default): Unit = {
    for (itemType <- itemTypes) {
      noItem(itemType) match {
        case Some(noItem) => noItem.supply(economy, itemType, usage)
        case None         => throw new IllegalStateException(s"Can't supply item type '$itemType' (no noitem available)")
      }
    }
  }
  def block(itemTypes: Seq[ItemType]): Unit = {
    for (itemType <- itemTypes) {
      noItem(itemType) match {
        case Some(noItem) => noItem.block(itemType)
        case None         => throw new IllegalStateException(s"Can't block item type '$itemType' (no noitem available)")
      }
    }
  }

  def isSameWayGroup(otherStock: Stock): Boolean = {
    (for {
      way      <- master.entity.stockWay
      otherWay <- otherStock.master.entity.stockWay
    } yield way.getGroup == otherWay.getGroup).getOrElse(false)
  }
  def isSameWaypointGroup(otherStock: Stock): Boolean = {
    (for {
      waypoint      <- master.entity.stockWaypoint
      otherWaypoint <- otherStock.master.entity.stockWaypoint
    } yield waypoint.getGroup == otherWaypoint.getGroup).getOrElse(false)
  }

  def fill: Unit
}
object Stock {
  case object None extends Stock {
    override def entity                             = StockEntity.None
    override def stockType                          = DummyStockType
    override def exportStockClass                   = format.StockClass.NONE
    override def master                             = this
    override def stocks                             = Seq.empty
    override def items                              = List.empty
    override def add(item: Item)                    = throw new UnsupportedOperationException
    override def remove(item: Item)                 = throw new UnsupportedOperationException
    override def replace(item: Item, newItem: Item) = throw new UnsupportedOperationException
    override def fill                               = throw new UnsupportedOperationException
    override def assignEntity(entity: StockEntity)  = throw new UnsupportedOperationException
    override def onDestroy(economy: ItemEconomy)    = {}
  }
}

sealed abstract class BaseStock(
    private[stock] val _items: mutable.Buffer[Item] = ArrayBuffer()
) extends Stock {
  def stockType: StockType

  override def items = _items

  override def add(item: Item) = {
    _items.append(item)
    assert(_items.contains(item))
  }
  override def remove(item: Item) = {
    remove0(item)
    fill
  }
  override def replace(item: Item, newItem: Item) = {
    remove0(item)
    newItem match {
      case _: NoItem => fill
      case _         => add(newItem)
    }
  }
  private def remove0(item: Item): Unit = {
    require(contains(item), "Can't remove item because it is not available.")
    _items -= item
    assert(!contains(item))
  }

  override def toString: String = s"${getClass.getSimpleName}($stockType)"

  private def canContain(item: Item): Boolean         = canContain(item.itemType)
  private def canContain(itemType: ItemType): Boolean = stockType.canContain(itemType)

  override def fill = {
    while (stockType.maxItemCount > _items.size) {
      _items += new NoItem(this, stockType.itemType)
    }
  }

  fill
}

final class SingleStock(
    override val stockType: StockType,
    items: mutable.Buffer[Item] = ArrayBuffer()
) extends BaseStock(items) {
  override def master           = this
  override def stocks           = Seq.empty
  override def exportStockClass = format.StockClass.SINGLE
  override def onDestroy(economy: ItemEconomy) = {
    for (item <- items.clone()) {
      economy.remove(item)
    }
  }
}

final class SubStock(
    override val stockType: StockType,
    private[this] var _master: Option[MasterStock] = None,
    items: mutable.Buffer[Item] = ArrayBuffer()
) extends BaseStock(items) {

  override def exportStockClass = format.StockClass.SUB

  override def master: Stock = _master match {
    case Some(master) => master
    case None         => this
  }

  override def stocks = Seq.empty

  override def entity = _master match {
    case Some(master) => master.entity
    case None         => entity
  }

  override def onDestroy(economy: ItemEconomy) = {
    for (item <- items.clone()) {
      economy.remove(item)
    }
    unsetMaster()
  }

  def setMaster(master: MasterStock): Unit = {
    _master = Some(master)
    master.addStock(this)
  }

  def unsetMaster(): Unit = {
    for (master <- _master) {
      master.removeStock(this)
    }
    _master = None
  }

  def hasMaster: Boolean = _master.isDefined
}

sealed trait BaseMultiStock extends Stock {
  def stockType: StockType = DummyStockType

  override def master = this
  override def items  = stocks.flatMap(_.items)

  override def add(item: Item) = stocks.find(_.hasNoItem(item.itemType)) match {
    case Some(stock) => stock.add(item)
    case None        => throw new IllegalStateException("Can't add item to stock.")
  }
  override def remove(item: Item) = stocks.find(_.contains(item)) match {
    case Some(stock) => stock.remove(item)
    case None        => throw new IllegalStateException("Can't remove item from stock.")
  }
  override def replace(item: Item, newItem: Item) = {
    remove(item)
    add(newItem)
  }

  override def fill = {
    for (stock <- stocks) {
      stock.fill
    }
  }
}

final class MultiStock(override val stocks: Seq[Stock]) extends BaseMultiStock {
  override def exportStockClass                = format.StockClass.MULTI
  override def onDestroy(economy: ItemEconomy) = {} // do nothing
}

final class MasterStock extends BaseMultiStock {
  override def exportStockClass                   = format.StockClass.MASTER
  private[this] val _stocks: ListBuffer[SubStock] = new ListBuffer()
  override def stocks: Seq[Stock]                 = _stocks
  private[stock] def addStock(stock: SubStock): Unit = {
    require(stock.master == this)
    require(!_stocks.contains(stock))

    _stocks.append(stock)
  }
  private[stock] def removeStock(stock: SubStock): Unit = {
    require(stock.master == this)
    require(_stocks.contains(stock))

    _stocks.remove(_stocks.indexOf(stock))
  }
  override def onDestroy(economy: ItemEconomy): Unit = {
    for (stock <- _stocks) {
      stock.unsetMaster()
    }
  }
}

trait StockEntity {
  def stock: Stock
  def stockWay: Option[Way]
  def stockWaypoint: Option[Waypoint]
}
object StockEntity {
  case object None extends StockEntity {
    override def stock         = Stock.None
    override def stockWay      = Option.empty
    override def stockWaypoint = Option.empty
  }
}

final case class CanNotAddException(
    item: Item,
    stock: Stock,
    isFull: Boolean = false,
    canNotContain: Boolean = false
) extends RuntimeException(s"Can't add item '$item' to '$stock' (isFull: $isFull, canNotContain: $canNotContain)")
