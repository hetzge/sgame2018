package de.hetzge.sgame.tile

import java.util

import de.hetzge.sgame.render._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.base._
import de.hetzge.sgame.export._
import de.hetzge.sgame.format

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.SpriteCache

import scala.collection.mutable
import scala.collection.mutable.Buffer
import scala.collection.mutable.ArrayBuffer

final case class TileMapProto(
  width: Int,
  height: Int, 
  layers: Seq[TileMapLayerProto]
)

final case class TileMapLayerProto(
  name: String,
  tiles: Seq[Int]
)

sealed trait RenderTileMap { tileMap: TileMap =>
  private[tile] val _offset: TileInt              = 8
  private[tile] var _lastVisibleTileBounds: Tiled = SimpleTiled()
  private[tile] var _spriteCache: SpriteCache     = null
  private[tile] var _cacheId: Int                 = 0

  def render(renderContext: GameRenderContext, x: PixelFloat, y: PixelFloat): Unit = {
    val bounds = renderContext.visibleTileBounds(tileMap, x, y, 6)
    if (_lastVisibleTileBounds != bounds) {
      renderToCache(renderContext, x, y)
      _lastVisibleTileBounds = bounds
    }

    _spriteCache.setProjectionMatrix(renderContext.camera.combined)
    _spriteCache.begin()
    Gdx.gl.glEnable(GL20.GL_BLEND)
    Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)
    _spriteCache.draw(_cacheId)
    _spriteCache.end()
  }
  def renderToCache(renderContext: GameRenderContext, x: PixelFloat, y: PixelFloat): Unit = {
    if (_spriteCache == null) {
      _spriteCache = new SpriteCache(layers.size * size, false)
    }

    _spriteCache.clear()
    _spriteCache.beginCache()
    for (layer <- layers) {
      layer.renderToCache(renderContext, _spriteCache, x, y)
    }
    _cacheId = _spriteCache.endCache()
  }

  def forceRender(): Unit = {
    _lastVisibleTileBounds = SimpleTiled()
  }

  // TODO call
  def dispose(): Unit = {
    _spriteCache.dispose()
  }
}

sealed trait SpeedFactorTileMap { tileMap: TileMap =>
  private[this] val _speedFactors                                           = new Array[Float](size)
  private[tile] def setSpeedFactor(position: Position, factor: Float): Unit = _speedFactors(position) = factor
  def speedFactor(position: Position): Float                                = _speedFactors(position)
}

sealed trait CachedTopLayerTileMap { tileMap: TileMap =>

  private[this] val _topLayer: TileMapLayer                          = new TileMapLayer("FIRST_TOP", width, height, tileSet, _ => ())
  private[this] def topTile(position: Position): Tile                = _topLayer.get(position)
  private[tile] def setTopTile(position: Position, tile: Tile): Unit = _topLayer.set(position, tile)
  def topTileType(position: Position): TileType                      = topTile(position).tileType
  def isTopType(position: Position, tileType: TileType): Boolean     = topTileType(position) == tileType

  private[this] val _secondTopLayer: TileMapLayer                          = new TileMapLayer("SECOND_TOP", width, height, tileSet, _ => ())
  private[this] def secondTopTile(position: Position): Tile                = _secondTopLayer.get(position)
  private[tile] def setSecondTopTile(position: Position, tile: Tile): Unit = _secondTopLayer.set(position, tile)
  def secondTopTileType(position: Position): TileType                      = secondTopTile(position).tileType
  def isSecondTopType(position: Position, tileType: TileType): Boolean     = secondTopTileType(position) == tileType
}

sealed trait ExportTileMap extends ExportObject[TileMapProto] { tileMap: TileMap =>

  def export()(implicit index: ExportedIndex) = TileMapProto(
    width = width,
    height = height,
    layers = layers.map(layer => TileMapLayerProto(
      name = layer.name,
      tiles = layer.tiles.map(_.toInt)
    ))
  )
}
object ImportTileMap {

  def load(from: TileMapProto, tileSet: TileSet)(implicit index: ImportedIndex): TileMap = {
    val tileMap = new TileMap(from.width, from.height, tileSet)
    for (fromLayer <- from.layers) {
      val layer = tileMap.getOrCreateLayer(fromLayer.name)
      for ((position, tile) <- layer.areaPositions().zip(fromLayer.tiles)) {
        layer.set(position, tile.toShort)
      }
    }
    tileMap
  }
}

final class TileMap(
    override val width: TileInt,
    override val height: TileInt,
    val tileSet: TileSet
) extends RenderTileMap
    with Tiled
    with SpeedFactorTileMap
    with CachedTopLayerTileMap
    with ExportTileMap {

  private[this] val _layers: Buffer[TileMapLayer] = ArrayBuffer()
  private def addLayer(layer: TileMapLayer): Unit = _layers += layer
  def layers: Seq[TileMapLayer]                   = _layers

  private[this] val _collision: util.BitSet = new util.BitSet(size)
  private[tile] def updateCollision(position: Position): Unit = {
    val collision = layers.map(_.get(position)).exists(_.collision)
    _collision.set(position, collision)
  }
  def isCollision(position: Position): Boolean = _collision.get(position)

  private[tile] def updateTopTile(position: Position): Unit = {
    val tiles = _layers.reverse.map(_.get(position)).filter(_.tileType != TileType.None)
    setTopTile(position, tiles.headOption.getOrElse(Tile.EMPTY))
    setSecondTopTile(position, if (tiles.size > 1) tiles.tail.headOption.getOrElse(Tile.EMPTY) else Tile.EMPTY)
    setSpeedFactor(position, tiles.map(_.tileType.speedFactor).fold(1f)(_ * _))
  }

  def getOrCreateLayer(name: String): TileMapLayer = {
    layers.find(_.name == name).getOrElse(createLayer(name))
  }
  private[this] def createLayer(name: String): TileMapLayer = {
    val layer = new TileMapLayer(name, width, height, tileSet, position => {
      updateCollision(position)
      updateTopTile(position)
    })
    addLayer(layer)
    layer
  }
}

sealed trait RenderTileMapLayer { tileMapLayer: TileMapLayer =>
  def renderToCache(renderContext: GameRenderContext, spriteCache: SpriteCache, x: PixelFloat, y: PixelFloat): Unit = {
    val Tiled(tileX, tileY, tileWidth, tileHeight) = renderContext.visibleTileBounds(tileMapLayer, x, y, 6)
    iterate(tileX, tileY, tileWidth, tileHeight) { position =>
      val tile = get(position)
      val rx   = x + position.x.pixelFloat
      val ry   = y + position.y.pixelFloat

      if (tile.renderable != null) {
        val renderable      = tile.renderable
        val width           = renderable.width
        val height          = renderable.height
        val offsetX         = width / 2f
        val offsetY         = height / 2f
        val rotationDegrees = renderable.rotationDegrees
        spriteCache.add(tile.renderable.textureRegion, rx, ry, offsetX, offsetY, width, height, 1f, 1f, rotationDegrees)
      }
    }
  }
}

final class TileMapLayer private[tile] (val name: String, override val width: TileInt, override val height: TileInt, tileSet: TileSet, onSetCallback: Position => Unit) extends RenderTileMapLayer with Tiled {
  private[this] val _tiles: Array[Short] = new Array(size)
  private[tile] def tiles: Seq[Short]    = _tiles

  def fill(tile: Tile): Unit = {
    iterate()(position => set(position, tile))
  }

  def isSet(position: Position): Boolean = _tiles(position) != 0

  def set(position: Position, tile: Tile): Unit = set(position, tileSet.index(tile))
  def set(position: Position, tileIndex: Short): Unit = {
    _tiles(position) = tileIndex
    onSetCallback(position)
  }

  def unset(position: Position): Unit = set(position, 0.toShort)

  def get(position: Position): Tile = tileSet.tile(_tiles(position))
}

object TileSet {
  val FAKE: TileSet = new TileSet() {
    private[this] val tiles         = TileType.all.map(Tile.empty(_))
    override def tile(index: Short) = tiles(index)
    override def index(tile: Tile)  = tiles.indexWhere(_.tileType == tile.tileType).toShort
  }
  val EMPTY: TileSet = new TileSet() {
    override def tile(index: Short) = Tile.EMPTY
    override def index(tile: Tile)  = 0
  }
}
sealed trait TileSet {
  def tile(index: Short): Tile
  def index(tile: Tile): Short
}

final class SimpleTileSet(tiles: Seq[Tile]) extends TileSet {
  private[this] val indexByTile: Map[Tile, Short] = tiles.zipWithIndex.map(e => e._1 -> e._2.toShort).toMap
  private[this] val tilesByIndex: Array[Tile]     = tiles.toArray

  override def tile(index: Short) = tilesByIndex(index)
  override def index(tile: Tile)  = indexByTile(tile)
}

final case class OrientationTilePair(orientationSet: OrientationSet, tile: Tile)
final case class OrientationTileGroup(tileType: TileType, pairs: List[OrientationTilePair])

object OrientationTileSetMapping {

  def apply(groups: List[OrientationTileGroup]): OrientationTileSetMapping = {
    val allTiles = ArrayBuffer[Tile]()
    val mapping  = new mutable.HashMap[TileType, mutable.HashMap[OrientationSet, Int]]()

    var i = 0

    // empty default value
    allTiles.append(Tile.EMPTY)
    i = i + 1

    for (group <- groups) {
      val pairs = combineTiles(group.tileType, group.pairs)
      for (pair <- pairs) {
        allTiles.append(pair.tile)
        mapping.getOrElseUpdate(group.tileType, new mutable.HashMap[OrientationSet, Int]()).put(pair.orientationSet, i)
        i = i + 1
      }
    }

    new OrientationTileSetMapping(allTiles.toArray, mapping.map(entry => (entry._1, entry._2.toMap)).toMap)
  }

  private def combineTiles(tileType: TileType, pairs: List[OrientationTilePair]): List[OrientationTilePair] = {
    val rotate: Function[OrientationTilePair, OrientationTilePair] = pair => OrientationTilePair(pair.orientationSet.rotate, new Tile(tileType, pair.tile.renderable.rotated, pair.tile.collision))
    val flipX: Function[OrientationTilePair, OrientationTilePair]  = pair => OrientationTilePair(pair.orientationSet.flipVertical, new Tile(tileType, pair.tile.renderable.flipedX, pair.tile.collision))
    val flipY: Function[OrientationTilePair, OrientationTilePair]  = pair => OrientationTilePair(pair.orientationSet.flipHorizontal, new Tile(tileType, pair.tile.renderable.flipedY, pair.tile.collision))

    val combinations: Array[Array[Function[OrientationTilePair, OrientationTilePair]]] = Array(
      Array(rotate),
      Array(rotate, rotate),
      Array(rotate, rotate, rotate),
      Array(flipX, rotate),
      Array(flipX, rotate, rotate),
      Array(flipX, rotate, rotate, rotate),
      Array(flipY, rotate),
      Array(flipY, rotate, rotate),
      Array(flipY, rotate, rotate, rotate),
      Array(flipX, flipY, rotate),
      Array(flipX, flipY, rotate, rotate),
      Array(flipX, flipY, rotate, rotate, rotate)
    )

    pairs.flatMap(pair => combinations.map(Function.chain(_)(pair)))
  }

  val FAKE: OrientationTileSetMapping = new OrientationTileSetMapping(
    tiles = TileType.all.map(Tile.empty(_)).toArray,
    mapping = Map[TileType, Map[OrientationSet, Int]]().withDefault(tileType => Map[OrientationSet, Int]().withDefault(_ => TileType.all.indexOf(tileType)))
  )
}

final class OrientationTileSetMapping private (val tiles: Array[Tile], mapping: Map[TileType, Map[OrientationSet, Int]]) {

  def get(tileType: TileType, aroundOrientationSet: OrientationSet): Tile = {
    tiles(mapping(tileType)(mask(aroundOrientationSet)))
  }

  private def mask(aroundOrientationSet: OrientationSet): OrientationSet = {
    var result = OrientationSet(Center)
    for (orientation <- aroundOrientationSet.values) {
      result = result + (orientation match {
        case NorthWest if (aroundOrientationSet.containsAll(North, West)) => NorthWest
        case NorthEast if (aroundOrientationSet.containsAll(North, East)) => NorthEast
        case SouthWest if (aroundOrientationSet.containsAll(South, West)) => SouthWest
        case SouthEast if (aroundOrientationSet.containsAll(South, East)) => SouthEast
        case North                                                        => North
        case West                                                         => West
        case East                                                         => East
        case South                                                        => South
        case _                                                            => Center
      })
    }
    result
  }
}

final class MountainTileSetMapping(val tiles: Array[Tile]) {

  def get(lastEast: Boolean, lastSouth: Boolean, aroundOrientationSet: OrientationSet): Option[Tile] = {
    val filteredAroundOrientationSet = OrientationSet(aroundOrientationSet.values.filter(!_.isDiagonal))
    val horizontal: Orientation      = if (lastEast) East else West
    val vertical: Orientation        = if (lastSouth) South else North
    _mapping(horizontal)(vertical).get(filteredAroundOrientationSet).map(tiles(_))
  }

  private[this] val _mapping: Orientation => Orientation => Map[OrientationSet, Int] = Map(
    West -> Map(
      South -> Map(
        OrientationSet(Center, North, South, East) -> 8, // ?
        OrientationSet(Center, North, East)        -> 5,
        OrientationSet(Center, North, West)        -> 7,
        OrientationSet(Center, North, South)       -> 4,
        OrientationSet(Center, South, West, East)  -> 8,
        OrientationSet(Center, South, East)        -> 8,
        OrientationSet(Center, West, East)         -> 6,
        OrientationSet(Center, North, West, East)  -> 6
      ),
      North -> Map(
        OrientationSet(Center, South, East)        -> 8, // 0
        OrientationSet(Center, South, West)        -> 9,
        OrientationSet(Center, North, South)       -> 3,
        OrientationSet(Center, North, South, East) -> 3,
        OrientationSet(Center, North, West)        -> 11,
        OrientationSet(Center, North, South, West) -> 9,
        OrientationSet(Center, North, East)        -> 5,
        OrientationSet(Center, North, West, East)  -> 5,
        OrientationSet(Center, West, East)         -> 6,
        OrientationSet(Center, South, West, East)  -> 6
      )
    ),
    East -> Map(
      South -> Map(
        OrientationSet(Center, North, West)        -> 7,
        OrientationSet(Center, North, East)        -> 10,
        OrientationSet(Center, North, East, South) -> 10,
        OrientationSet(Center, North, South)       -> 4,
        OrientationSet(Center, South, West)        -> 2,
        OrientationSet(Center, South, West, East)  -> 2,
        OrientationSet(Center, West, East)         -> 1,
        OrientationSet(Center, North, West, East)  -> 1
      ),
      North -> Map(
        OrientationSet(Center, North, West)        -> 11,
        OrientationSet(Center, North, West, South) -> 11,
        OrientationSet(Center, South, West)        -> 2,
        OrientationSet(Center, South, East)        -> 0,
        OrientationSet(Center, North, South)       -> 3,
        OrientationSet(Center, West, East)         -> 1,
        OrientationSet(Center, South, West, East)  -> 1
      )
    )
  )

}

sealed abstract class TileType(val key: String, val speedFactor: Float = 1f)
object TileType {
  case object None          extends TileType("NONE")
  case object Desert        extends TileType("DESERT")
  case object Grass         extends TileType("GRASS")
  case object Water         extends TileType("WATER")
  case object DefaultWay    extends TileType("DEFAULT_WAY", 0.75f)
  case object StoneWay      extends TileType("STONE_WAY", 1f)
  case object BuildStoneWay extends TileType("BUILD_STONE_WAY", 0.5f)
  case object Cloud         extends TileType("CLOUD")
  case object Mountain      extends TileType("MOUNTAIN", 0.5f)
  val all: List[TileType] = List(None, Desert, Grass, Water, DefaultWay, StoneWay, BuildStoneWay, Cloud, Mountain)
}

object Tile {
  val EMPTY                     = new Tile(TileType.None, null, false)
  def empty(tileType: TileType) = new Tile(tileType, renderable = null, collision = false)
}

/** !!! renderable can be null */
final case class Tile(val tileType: TileType, val renderable: TextureRegionRenderable, val collision: Boolean = false)
