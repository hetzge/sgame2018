package de.hetzge.sgame.tilemaploader

import com.badlogic.gdx.files.FileHandle
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import play.api.libs.json.JsArray
import de.hetzge.sgame.tile._
import de.hetzge.sgame.base._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.resources._
import scala.collection.mutable
import scala.annotation.tailrec
import java.{util => ju}

object TileMapLoader {

  def load(resources: ApplicationResources, handle: FileHandle): TileMap = {
    val random = new ju.Random(12345L)
    val in     = handle.read()
    try {
      val jsValue: JsValue = Json.parse(in)
      val width            = (jsValue \ "width").get.as[Int]
      val height           = (jsValue \ "height").get.as[Int]

      val tileMap    = new TileMap(width, height, resources.tileSet)
      var keyByLayer = new mutable.HashMap[TileMapLayer, TileType]()

      (jsValue \ "layers").get match {
        case JsArray(layerJsValues) => {
          for (layerJsValue <- layerJsValues) {
            val layer = tileMap.getOrCreateLayer(ju.UUID.randomUUID().toString())
            
            val name = (layerJsValue \ "name").get.as[String]

            val data   = (layerJsValue \ "data").get.as[String]
            val bitSet = ju.BitSet.valueOf(ju.Base64.getDecoder().decode(data))

            for (key <- List(TileType.Grass, TileType.Desert, TileType.Water).find(_.key == name)) {
              keyByLayer.put(layer, key)

              val tile = resources.orientationTileSetMapping.get(key, OrientationSet.ALL)
              tileMap.iterate() { position =>
                if (bitSet.get(position)) {
                  layer.set(position, tile)
                }
              }
            }
            if (name startsWith TileType.Mountain.key) {
              println("Mountain layer: " + name)

              val blacklist = new mutable.HashSet[Position]()
              tileMap.iterate() { position =>
                if (bitSet.get(position) && !blacklist.contains(position)) {

                  def isValid(position: Position): Boolean = {
                    bitSet.get(position) && !blacklist.contains(position) && position.simpleAround.count(bitSet.get(_)) > 1
                  }
                  def isValidAround(position: Position): Boolean = {
                    position.simpleAround().exists(isValid(_))
                  }
                  def nextOrientation(orientation: Orientation): Orientation = {
                    orientation match {
                      case North => East
                      case East  => South
                      case South => West
                      case West  => North
                      case _     => throw new IllegalStateException()
                    }
                  }

                  def visit(lastEast: Boolean, lastSouth: Boolean, position: Position): Unit = {
                    val aroundOrientationSet = OrientationSet(Orientation.ALL_VALUES.filter(position.around(_).exists(bitSet.get(_))))
                    // println(s"$position, $aroundOrientationSet, lastEast: $lastEast, lastSouth: $lastSouth")

                    def isSimple = !aroundOrientationSet.values.exists(_.isDiagonal)
                    def isSingleDirection = aroundOrientationSet.values.exists(_.isHorizontal) ^ aroundOrientationSet.values.exists(_.isVertical)
                    def isNotBorder = !tileMap.isEndOfTiled(position)

                    val mapping =
                      if (tileMap.isTopType(position, TileType.Water)) resources.mountainWaterTileSetMapping
                      else if (random.nextDouble() < 0.10d && isSimple && isNotBorder && isSingleDirection) resources.mountainOpenTileSetMapping
                      else resources.mountainTileSetMapping

                    mapping.get(lastEast, lastSouth, aroundOrientationSet) match {
                      case Some(tile) => layer.set(position, tile)
                      case None       => // ignore
                    }

                  }

                  def mountainOutline(position: Position): List[Position] = {
                    List(position).filter(isValid(_)) ++ mountainOutline0(position, West, List())
                  }

                  @tailrec
                  def mountainOutline0(position: Position, orientation: Orientation, result: List[Position]): List[Position] = {

                    blacklist.add(position) // TODO replace blacklist with result ?!

                    position.around(orientation) match {
                      case Some(aroundPosition) if isValid(aroundPosition) => mountainOutline0(aroundPosition, orientation.opposit, result ++ List(aroundPosition))
                      case _ if isValidAround(position)                    => mountainOutline0(position, nextOrientation(orientation), result)
                      case _                                               => result
                    }
                  }

                  var lastEast                = true
                  var lastSouth               = false
                  val outline: List[Position] = mountainOutline(position)
                  for (i <- 0 until outline.size) {
                    val before  = if (i - 1 >= 0) outline(i - 1) else outline.last
                    val current = outline(i)
                    val next    = if (i + 1 < outline.size) outline(i + 1) else outline.head

                    val beforeOrientation = before.orientation(current)
                    val nextOrientation   = current.orientation(next)

                    if (beforeOrientation == West || beforeOrientation == East || nextOrientation == West || nextOrientation == East) {
                      lastEast = beforeOrientation == East || nextOrientation == East
                    }

                    if (beforeOrientation == South || beforeOrientation == North || nextOrientation == South || nextOrientation == North) {
                      lastSouth = beforeOrientation == South || nextOrientation == South
                    }

                    // println(beforeOrientation, nextOrientation)
                    visit(lastEast, lastSouth, current)
                  }
                }
              }
            }
          }
        }
        case _ => throw new IllegalStateException
      }

      for (layer <- tileMap.layers if keyByLayer.contains(layer)) {
        val key = keyByLayer(layer)
        tileMap.iterate() { position =>
          resources.tileSet.index(layer.get(position))

          if (layer.isSet(position)) {
            val aroundOrientations = OrientationSet(Orientation.ALL_VALUES.filter(orientation => position.around(orientation).exists(layer.isSet(_))))
            val tile               = resources.orientationTileSetMapping.get(key, aroundOrientations)
            layer.set(position, tile)
          }
        }
      }

      tileMap

    } finally {
      in.close()
    }
  }

}
