package de.hetzge.sgame

import de.hetzge.sgame.base._
import de.hetzge.sgame.format

import scala.language.implicitConversions
import java.util.UUID

import com.badlogic.gdx.math.MathUtils

package object unit {
  val TILE_SIZE: PixelInt              = 32
  val TILE_SIZE_FLOAT: PixelFloat      = TILE_SIZE.pixelFloat
  val HALF_TILE_SIZE_FLOAT: PixelFloat = TILE_SIZE_FLOAT / 2

  sealed trait Value[T] extends Any {
    def value: T
  }

  object EntityKey {
    private var next = 0
    def apply(): EntityKey = synchronized {
      val result = EntityKey(next.toString)
      next += 1
      result
    }
  }
  final case class EntityKey(key: String) extends AnyVal
  @inline implicit def entityKeyToString(key: EntityKey): String   = key.key
  @inline implicit def stringToEntityKey(value: String): EntityKey = EntityKey(value)

  implicit object EntityKeyOrdering extends Ordering[EntityKey] {
    val stringOrdering = implicitly[Ordering[String]]
    override def compare(x: EntityKey, y: EntityKey) = {
      stringOrdering.compare(x.key, y.key)
    }
  }

  object BytePercent {
    def apply(percent: Float): BytePercent = {
      require(percent <= 1.0 && percent >= 0.0)
      BytePercent((Byte.MaxValue * percent).toByte)
    }
  }
  final case class BytePercent(val value: Byte) extends AnyVal with Value[Byte] {
    def percentFloat: Float = value.toFloat / Byte.MaxValue.toFloat
    def isEmpty: Boolean    = value == 0
    def isFull: Boolean     = value == Byte.MaxValue
  }

  final case class TileInt(val value: Int) extends AnyVal with Value[Int] {
    @inline def tileFloat: TileFloat   = TileFloat(value)
    @inline def pixelInt: PixelInt     = PixelInt(value * TILE_SIZE)
    @inline def pixelFloat: PixelFloat = PixelFloat(value * TILE_SIZE)
    @inline def between(other: TileInt): TileInt = {
      val min = Math.min(value, other)
      val max = Math.max(value, other)

      min + (max - min) / 2
    }
    @inline def isEven: Boolean = value % 2 == 0
    @inline def isOdd: Boolean  = value % 2 == 1
  }
  @inline implicit def tileIntToInt(tileInt: TileInt): Int = tileInt.value
  @inline implicit def intToTileInt(int: Int): TileInt     = TileInt(int)

  implicit object TileIntOrdering extends Ordering[TileInt] {
    val intOrdering = implicitly[Ordering[Int]]
    override def compare(x: TileInt, y: TileInt) = {
      intOrdering.compare(x.value, y.value)
    }
  }

  final case class TileFloat(val value: Float) extends AnyVal with Value[Float] {
    @inline def pixelInt: PixelInt     = PixelInt((value * TILE_SIZE).intValue)
    @inline def pixelFloat: PixelFloat = PixelFloat(value * TILE_SIZE)
  }
  @inline implicit def tileFloatToFloat(tileFloat: TileFloat): Float = tileFloat.value
  @inline implicit def floatToTileFloat(float: Float): TileFloat     = TileFloat(float)

  final case class PixelInt(val value: Int) extends AnyVal with Value[Int] {
    @inline def tileFloat: TileFloat   = TileFloat(value)
    @inline def tileInt: TileInt       = TileInt(Math.floor(value / TILE_SIZE).toInt)
    @inline def pixelFloat: PixelFloat = PixelFloat(value)
  }
  @inline implicit def pixelIntToInt(pixelInt: PixelInt): Int = pixelInt.value
  @inline implicit def intToPixelInt(int: Int): PixelInt      = PixelInt(int)

  final case class PixelFloat(val value: Float) extends AnyVal with Value[Float] {
    @inline def tileInt: TileInt = TileInt(Math.floor(value / TILE_SIZE_FLOAT).toInt)
  }
  @inline implicit def pixelFloatToFloat(pixelFloat: PixelFloat): Float = pixelFloat.value
  @inline implicit def floatToPixelFloat(float: Float): PixelFloat      = PixelFloat(float)

  final case class PositionProto(
      x: Int,
      y: Int,
      width: Int,
      height: Int
  )

  object Position {
    private val STEP   = 15
    private val X      = 0L * STEP
    private val Y      = 1L * STEP
    private val WIDTH  = 2L * STEP
    private val HEIGHT = 3L * STEP
    private val MAX    = 1 << STEP
    val EMPTY          = apply(0, 0, 1, 1)
    def apply(x: Int, y: Int)(implicit tiled: Tiled): Position = {
      apply(x, y, tiled.width, tiled.height)
    }
    def apply(x: Int, y: Int, width: Int, height: Int): Position = {
      require(x < width, s"x $x is greater then width $width")
      require(y < height, s"y $y is greater then width $height")
      require(x >= 0, s"x $x is smaller then 0")
      require(y >= 0, s"y $y is smaller then 0")
      require(x < MAX, s"x $x is greater then max value")
      require(y < MAX, s"y $y is greater then max value")
      require(width < MAX, s"width $width is greater then max value")
      require(height < MAX, s"height $height is greater then max value")

      Position(x.toLong << X | y.toLong << Y | width.toLong << WIDTH | height.toLong << HEIGHT)
    }
    def apply(f: format.Position): Position = {
      Position(f.value)
    }
    def apply(from: PositionProto): Position = {
      Position(from.x, from.y, from.width, from.height)
    }
    @inline def apply(): Position                                          = EMPTY
    @inline def x(v: Position): TileInt                                    = ((1 << STEP) - 1 & v.v >> X).toInt
    @inline def y(v: Position): TileInt                                    = ((1 << STEP) - 1 & v.v >> Y).toInt
    @inline def width(v: Position): TileInt                                = ((1 << STEP) - 1 & v.v >> WIDTH).toInt
    @inline def height(v: Position): TileInt                               = ((1 << STEP) - 1 & v.v >> HEIGHT).toInt
    @inline def unapply(v: Position): (TileInt, TileInt, TileInt, TileInt) = (x(v), y(v), width(v), height(v))
  }
  final case class Position(val v: Long) extends AnyVal {
    @inline def index: Int                                               = width * y + x
    @inline def x: TileInt                                               = Position.x(this)
    @inline def y: TileInt                                               = Position.y(this)
    @inline def width: TileInt                                           = Position.width(this)
    @inline def height: TileInt                                          = Position.height(this)
    @inline def valid(): Boolean                                         = index >= 0 && index < width * height
    @inline def isSameCoordinateSystem(otherPosition: Position): Boolean = width == otherPosition.width && height == otherPosition.height
    @inline def simpleAround(): Seq[Position]                            = Orientation.SIMPLE_AROUND_VALUES.flatMap(around(_))
    @inline def advancedAround(): Seq[Position]                          = Orientation.ADVANCED_AROUND_VALUES.flatMap(around(_))
    @inline def around(orientation: Orientation): Option[Position]       = relative(orientation.xInt, orientation.yInt)
    @inline def around(writeTo: Array[Int]): Unit = {
      require(writeTo.length == 4)
      // north
      writeTo(0) = if (y > 0) withPosition(x, y - 1) else -1
      // east
      writeTo(1) = if (x < width - 1) withPosition(x + 1, y) else -1
      // south
      writeTo(2) = if (y < height - 1) withPosition(x, y + 1) else -1
      // west
      writeTo(3) = if (x > 0) withPosition(x - 1, y) else -1
    }
    @inline def aroundValid(orientation: Orientation): Boolean = relativeValid(orientation.xInt, orientation.yInt)
    @inline def orientation(to: Position): Orientation         = Orientation.byOffset(to.x - x, to.y - y)

    @inline def withPosition(x: TileInt, y: TileInt): Position = Position(x, y, width, height)

    @inline def relative(x: TileInt, y: TileInt): Option[Position] =
      if (relativeValid(x, y)) Some(Position(this.x + x, this.y + y, width, height)) else None

    @inline def relativeValid(x: TileInt, y: TileInt): Boolean =
      this.x + x < width && this.x + x >= 0 && this.y + y < height && this.y + y >= 0

    @inline def isOnSameLine(to: Position): Boolean = x == to.x ^ y == to.y

    @inline def between(otherPosition: Position): Position = {
      require(isSameCoordinateSystem(otherPosition))

      val otherX = otherPosition.x
      val otherY = otherPosition.y
      val minX   = Math.min(x, otherX)
      val maxX   = Math.max(x, otherX)
      val minY   = Math.min(y, otherY)
      val maxY   = Math.max(y, otherY)

      Position(minX.between(maxX), minY.between(maxY), width, height)
    }

    @inline def distance(otherPosition: Position): TileInt = {
      require(width == otherPosition.width)
      require(height == otherPosition.height)
      Math.ceil(Math.sqrt(Math.abs(otherPosition.x - x) * Math.abs(otherPosition.x - x) + Math.abs(otherPosition.y - y) * Math.abs(otherPosition.y - y))).toInt
    }

    override def toString()     = s"(${x.value}, ${y.value})"
    def toDebugString(): String = s"$index = (${x.value}, ${y.value})"
    def export                  = format.Position(value = v)
    def exportProto             = PositionProto(x, y, width, height)
  }

  @inline implicit def positionToIndexInt(position: Position): Int = position.index

  implicit object IndexIntOrdering extends Ordering[Position] {
    val intOrdering = implicitly[Ordering[Int]]
    override def compare(x: Position, y: Position) = {
      intOrdering.compare(x.index, y.index)
    }
  }
}
