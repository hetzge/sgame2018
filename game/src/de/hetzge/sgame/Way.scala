package de.hetzge.sgame.way

import de.hetzge.sgame.GameApplicationAdapter
import de.hetzge.sgame.unit._
import de.hetzge.sgame.base._
import de.hetzge.sgame.render._
import de.hetzge.sgame.resources._
import de.hetzge.sgame.waypoint._
import de.hetzge.sgame.item._
import de.hetzge.sgame.stock._
import de.hetzge.sgame.area._
import de.hetzge.sgame.game._
import de.hetzge.sgame.tile._
import de.hetzge.sgame.export._
import de.hetzge.sgame.format
import de.hetzge.sgame.format.WayType._

final case class WayProto(
  wayType: String,
  area: ReferenceProto,
  position: PositionProto,
  waypoint: ReferenceProto,
  group: Int
)

sealed trait ExportWayType extends ExportObject[format.Way.Type] { wayType: WayType =>
  def export()(implicit index: ExportedIndex) = format.Way.Type(
    `type` = exportType
  )
}

object ImportWayType {
  def load(f: format.Way.Type): WayType = {
    f.`type` match {
      case DEFAULT_WAY => DefaultWayType
      case STONE_WAY   => StoneWayType
    }
  }
}

sealed trait WayType extends ExportWayType with Serializable {
  val renderableSet: WayRenderableSet
  val requiresItemType: Option[ItemType]
  val speedFactor: Float
  def exportType: format.WayType
}

case object DefaultWayType extends WayType {
  @transient override lazy val renderableSet = {
    // TODO find more elegant solution
    if (GameContext.isTestMode) {
      new WayRenderableSet(OrientationTileSetMapping.FAKE, TileType.DefaultWay, TileType.DefaultWay)
    } else {
      GameApplicationAdapter.resources.way.default
    }
  }
  override val requiresItemType = None
  override val speedFactor      = 1f
  override def exportType       = format.WayType.DEFAULT_WAY
}
case object StoneWayType extends WayType {
  @transient override lazy val renderableSet = GameApplicationAdapter.resources.way.stone
  override val requiresItemType              = Some(StoneItemType)
  override val speedFactor                   = 3f
  override def exportType                    = format.WayType.STONE_WAY
}

sealed trait RenderWay { way: Way =>
  private var orientationSet: OrientationSet = OrientationSet()

  def setupRenderable(orientationSet: OrientationSet) = this.orientationSet = orientationSet

  def render(renderContext: GameRenderContext, x: PixelFloat, y: PixelFloat, z: PixelFloat): Unit = {
    if (way._waypoint != null) {
      _waypoint.render(renderContext, x, y, z + 1)
    }

    for (demandingItems <- stock.demandingItems) {
      demandingItems.render(renderContext, x, y, z - 1f)
    }
  }
}

sealed trait WaypointWay {
  private[way] var _waypoint: Waypoint = null

  def hasWaypoint: Boolean          = _waypoint != null
  def getWaypoint: Option[Waypoint] = Option(_waypoint)
  def setWaypoint(waypoint: Waypoint): Unit = {
    require(waypoint != null, "to remove a waypoint use removeWaypoint instead")
    _waypoint = waypoint
  }

  def removeWaypoint(): Unit = {
    require(hasWaypoint, "Try to remove non existing waypoint.")
    _waypoint.onDestroy()
    _waypoint = null
  }
}

sealed trait StockWay extends StockEntity { way: Way =>
  private[this] var _stock: Stock = wayType.requiresItemType match {
    case Some(itemType) => {
      val stock = new SingleStock(ItemTypeStockType(itemType, 1))
      stock.assignEntity(this)
      stock
    }
    case None => {
      Stock.None
    }
  }

  private[way] def setStock(stock: Stock): Unit = _stock = stock
  override def stock                            = _stock
  override def stockWay                         = Some(this)
  override def stockWaypoint: Option[Waypoint]  = None

  // inital demand
  stock.demand(area.economy, wayType.requiresItemType.toSeq, ItemUsage.Way)

  def build: Unit      = _stock = Stock.None
  def isBuild: Boolean = !stock.isDemanding
}

sealed trait GroupWay { way: Way =>
  private[way] var _group: Int = 0

  def setGroup(group: Int): Unit       = _group = group
  def getGroup: Int                    = _group
  def isSameGroup(other: Way): Boolean = other.getGroup == _group
}

sealed trait ExportWay extends ExportObject[format.Way] with ExportIndexedObject { way: Way =>

  override def export()(implicit index: ExportedIndex) = format.Way(
    `type` = Some(wayType.export()),
    area = Some(index(area)),
    position = Some(position.export),
    waypoint = if (hasWaypoint) Some(index(getWaypoint.get)) else None,
    group = _group
  )

  def index() = ExportIndex(
    objects = Set() ++ getWaypoint.toList
  )

}

object ImportWay {

  def load(f: format.Way)(implicit index: ImportedIndex): Way = {
    val way = new Way(
      area = index[Area](f.area.get),
      position = Position(f.position.get),
      wayType = ImportWayType.load(f.`type`.get)
    )
    way.setGroup(f.group)
    f.waypoint match {
      case Some(waypoint) => way.setWaypoint(index[Waypoint](waypoint))
      case None           => // ignore
    }
    way
  }

}

final class Way(
    val area: Area,
    val position: Position,
    val wayType: WayType
) extends RenderWay
    with WaypointWay
    with StockWay
    with GroupWay
    with ExportWay {

  def speedFactor: Float = if (isBuild) wayType.speedFactor else 0.8f

  override def toString() = s"Way($position, $wayType, $getGroup)"
}
