package de.hetzge.sgame.waypoint

import de.hetzge.sgame._
import de.hetzge.sgame.render._
import de.hetzge.sgame.stock._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.base._
import de.hetzge.sgame.area._
import de.hetzge.sgame.base.helper._
import de.hetzge.sgame.building._
import de.hetzge.sgame.player._
import de.hetzge.sgame.item._
import de.hetzge.sgame.format
import de.hetzge.sgame.export._
import de.hetzge.sgame.format._

import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import scala.annotation.tailrec

final case class WaypointProto(
  waypointType: String,
  position: PositionProto,
  stock: Reference,
  group: Int,
  connections: List[Reference],
  area: Reference,
  building: Reference
)

sealed trait ExportWaypointType extends ExportObject[format.Waypoint.Type] { waypointType: WaypointType =>

  def export()(implicit index: ExportedIndex) = format.Waypoint.Type(
    `type` = waypointType.exportType
  )
}

object ImportWaypointType {

  def load(f: format.Waypoint.Type): WaypointType = {
    f.`type` match {
      case format.WaypointType.DEFAULT_WAYPOINT => DefaultWaypointType
      case format.WaypointType.DUMMY_WAYPOINT   => DummyWaypointType
    }
  }
}

sealed trait WaypointType extends ExportWaypointType with Serializable {
  @transient lazy val renderable: PlayerRenderableSet = new PlayerRenderableSet(GameApplicationAdapter.resources.waypointRenderable)
  val stockType: StockType
  def exportType: format.WaypointType
}
case object DummyWaypointType extends WaypointType {
  override val stockType  = DummyStockType
  override def exportType = format.WaypointType.DUMMY_WAYPOINT
}
case object DefaultWaypointType extends WaypointType {
  override val stockType  = MixedStockType()
  override def exportType = format.WaypointType.DEFAULT_WAYPOINT
}

sealed trait RenderWaypoint { waypoint: Waypoint =>
  def render(renderContext: GameRenderContext, x: PixelFloat, y: PixelFloat, z: PixelFloat): Unit = {

    waypointType.renderable(playerId).render(renderContext, userData = null, x, y - 6, z, width = TILE_SIZE_FLOAT, height = TILE_SIZE_FLOAT)
    waypoint.stock.render(renderContext, x, y + 6, z + 1f)
  }
}

sealed trait ConnectedWaypoint { waypoint: Waypoint =>
  private[waypoint] var _connections: Array[Waypoint] = Waypoint.EMPTY_ARRAY

  def connections: Seq[Waypoint]                             = _connections
  def setConnections(waypoints: Traversable[Waypoint]): Unit = _connections = waypoints.toArray
  def addConnections(waypoints: Traversable[Waypoint]): Unit = waypoints.foreach(addConnection(_))
  def addConnection(waypoint: Waypoint): Unit = {
    if (!_connections.contains(waypoint)) {
      _connections = _connections :+ waypoint
    }
  }

  def removeAllConnections(): Unit                        = _connections = Waypoint.EMPTY_ARRAY
  def removeConnections(_connections: Iterable[Waypoint]) = _connections.foreach(removeConnection(_))
  def removeConnection(waypoint: Waypoint): Unit = {
    _connections = _connections.filter(_ != waypoint)
  }

  def unconnect(): Unit = {
    for (connection <- _connections) {
      connection.removeConnection(waypoint)
    }
    removeAllConnections()
  }

  def findPath(targetWaypoint: Waypoint): Option[Seq[Waypoint]] = {
    PathFinder.searchWithWeight(
      start = waypoint,
      next = (w: Waypoint) => {
        w._connections.map { w2 =>
          val weight = w.position.distance(w2.position).value
          (weight, w2)
        }
      },
      predicate = (w: Waypoint) => w == targetWaypoint
    )
  }

  @tailrec
  private def findPath(targetWaypoint: Waypoint, ratings: HashMap[Waypoint, Int], result: ListBuffer[Waypoint]): Unit = {
    if (targetWaypoint == waypoint) return
    findPath(targetWaypoint._connections.maxBy(ratings.get(_).getOrElse(0)), ratings, result += targetWaypoint)
  }

  def traverse(visit: Waypoint => Traverse): Unit = helper.traverse(this, Integer.MAX_VALUE)(_._connections)(visit)
}

/** Waypoints that can reach each other has the same group */
sealed trait GroupedWaypoint { waypoint: Waypoint =>
  private[waypoint] var _group: Int = 0

  def setGroup(group: Int): Unit = _group = group
  def getGroup: Int              = _group

  def groupAround: Option[Int] = _connections.map(_.getGroup).headOption

  // TODO flood on remove
  def floodGroup(floodGroup: Int): Unit = {
    traverse { waypoint =>
      if (waypoint == this) {
        _group = floodGroup
        Continue
      } else {
        if (waypoint._group < floodGroup) {
          waypoint._group = floodGroup
          Continue
        } else if (waypoint._group > floodGroup) {
          waypoint.floodGroup(waypoint._group)
          Stop
        } else {
          DontGoDeeper
        }
      }
    }
  }
}

sealed trait StockWaypoint extends StockEntity { waypoint: Waypoint =>
  stock.assignEntity(waypoint)
  override def stockWay      = area.way(position)
  override def stockWaypoint = Some(waypoint)
}

sealed trait ExportWaypoint extends ExportObject[format.Waypoint] with ExportIndexedObject { waypoint: Waypoint =>

  override def export()(implicit index: ExportedIndex) = format.Waypoint(
    `type` = Some(waypointType.export()),
    position = Some(position.export),
    stock = Some(index(stock)),
    group = _group,
    connections = _connections.map(index(_)),
    area = Some(index(area)),
    building = _building.map(index(_))
  )

  override def initLoad(f: format.Waypoint)(implicit index: ImportedIndex) = {
    assignArea(index(f.area.get))
    _connections = f.connections.map(index[Waypoint](_)).toArray
    _building = f.building.map(index[Building](_))
  }

  override def index() = ExportIndex(
    objects = Set(stock)
  )

}

object ImportWaypoint {

  def load(f: format.Waypoint)(implicit index: ImportedIndex): Waypoint = {
    new Waypoint(
      waypointType = ImportWaypointType.load(f.`type`.get),
      position = Position(f.position.get),
      stock = index[Stock](f.stock.get)
    )
  }
}

object Waypoint {
  private[waypoint] val EMPTY_ARRAY: Array[Waypoint] = new Array(0)

  def apply(
      waypointType: WaypointType,
      position: Position
  ): Waypoint = {
    new Waypoint(
      waypointType = waypointType,
      position = position,
      stock = new SingleStock(waypointType.stockType)
    )
  }
}
final class Waypoint(
    val waypointType: WaypointType,
    override val position: Position,
    override val stock: Stock
) extends ConnectedWaypoint
    with StockWaypoint
    with GroupedWaypoint
    with RenderWaypoint
    with ExportWaypoint
    with AreaEntity {
  override def width  = 1
  override def height = 1

  private[waypoint] var _building: Option[Building] = None
  def building: Option[Building]                    = _building
  def setBuilding(building: Building): Unit         = _building = Some(building)

  /** Use with care !!! Not always it make sense combine the stocks as multi stock. */
  def multiStock: Stock = {
    val buildingStock: Stock = building.map(_.stock).getOrElse(Stock.None)
    val stock                = new MultiStock(Seq(this.stock, buildingStock))
    stock.assignEntity(this)
    stock
  }

  def playerId: PlayerId = area.owner(position)

  override def viewRadius      = 3
  override def ownershipRadius = 0

  override def isStatic = true
  override def toString = s"Waypoint${position.toString}"

  def onDestroy(): Unit = {
    val _connections = this._connections
    unconnect()
    for (connection <- _connections) {
      connection.floodGroup(area.nextWayGroup)
    }
    stock.onDestroy(area.economy)
  }

  /** Checks if the [[SuppliedItem]] next target is a valid next target relative to the current stock */
  private def isNextItemTargetValid(suppliedItem: SuppliedItem): Boolean = {

    /** Converts the stock entity to a [[Waypoint]] if possible */
    def stockWaypoint(stock: Stock): Option[Waypoint] = stock.entity match {
      case waypoint: Waypoint => Some(waypoint)
      case _                  => None
    }

    (for {
      currentWaypoint <- stockWaypoint(suppliedItem.stock)
      nextWaypoint    <- stockWaypoint(suppliedItem.nextTarget)
    } yield {
      nextWaypoint.connections.contains(currentWaypoint)
    }).getOrElse(false)
  }

  def updateItemTargets(): Unit = {
    val stocksAround: Array[Stock]                     = _connections.map(_.stock)
    def hasValidNextTarget(suppliedItem: SuppliedItem) = !stocksAround.contains(suppliedItem.nextTarget)
    for (suppliedItem <- multiStock.suppliedItems(ItemUsage.Default)) {
      if (!hasValidNextTarget(suppliedItem)) {
        updateNextItemTarget(area.economy, suppliedItem)
      }
    }
  }

  private def updateNextItemTarget(economy: ItemEconomy, suppliedItem: SuppliedItem): Unit = {
    nextItemTarget(suppliedItem) match {
      case Some(nextTarget) => suppliedItem.setNextTarget(nextTarget)
      case None             => suppliedItem.cancel(economy)
    }
  }

  private def nextItemTarget(suppliedItem: SuppliedItem): Option[Stock] = {
    for {
      suppliedItemWaypoint <- suppliedItem.stock.master.entity.stockWaypoint
      demandedItemWaypoint <- suppliedItem.target.master.entity.stockWaypoint
      stock <- suppliedItemWaypoint.findPath(demandedItemWaypoint) match {
                case Some(current :: next :: tail) => Some(next.stock)
                case _                             => None
              }
    } yield stock
  }
}
