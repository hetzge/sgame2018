package de.hetzge.sgame.job

import de.hetzge.sgame.building._
import de.hetzge.sgame.item._
import de.hetzge.sgame.export._
import de.hetzge.sgame.format
import de.hetzge.sgame.person._

object BuildingLotJob extends Job[Building] {
  override def trigger(building: Building, trigger: Trigger) = {
    trigger match {
      case Trigger.FinishedOrder => {
        if (!building.stock.isDemanding) {
          building.upgrade(building.buildingType.asInstanceOf[BuildingType.BuildingLot].targetBuildingType)
        }
      }
      case _ => {}
    }
  }
  override def export()(implicit index: ExportedIndex) = exportType(format.JobType.BUILDING_LOT_JOB)
}
