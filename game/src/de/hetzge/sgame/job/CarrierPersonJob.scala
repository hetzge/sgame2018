package de.hetzge.sgame.job

import de.hetzge.sgame.person._
import de.hetzge.sgame.path._
import de.hetzge.sgame.building._
import de.hetzge.sgame.waypoint._
import de.hetzge.sgame.area._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.item._
import de.hetzge.sgame.stock._
import de.hetzge.sgame.item._
import de.hetzge.sgame.export._
import de.hetzge.sgame.format

case object CarrierSearchJob extends Job[Person] {
  override def trigger(person: Person, trigger: Trigger) = {
    val area = person.area
    if (!person.hasPath) {
      findJob(person) match {
        case Some(job) => {
          person.setAndExecuteJob(job, trigger)
        }
        case None => {
          if (!person.isInBuilding) {
            // go home
            val buildings = area.searchBuilding { building =>
              building.canReserve(person) && building.stock.isSameWayGroup(person.stock)
            }

            if (!buildings.isEmpty) {
              var nearest: (Path, Building) = null
              for (building <- buildings) {
                val pathOption = person.pathTo(building.doorPosition)
                if (pathOption.isDefined) {
                  val path = pathOption.get.optimize
                  if (nearest == null || path.distance < nearest._1.distance) {
                    nearest = (path, building)
                  }
                }
              }

              nearest match {
                case (nearestPath, nearestBuilding) => {
                  nearestBuilding.reserve(person)
                  person.setPath(nearestPath)
                  person.setAndExecuteJob(GoHomePersonJob, trigger)
                }
                case null => {}
              }
            } else {
              person.setJob(PanicJob)
            }
          }
        }
      }
    }
  }

  private def findJob(person: Person): Option[CarrierJob] = {
    val area = person.area
    person.getWay match {
      case Some(way) => {
        area
          .findJobs(job => job.isOpen && job.isInstanceOf[CarrierJob] && area.way(job.position).exists(_.getGroup == way.getGroup))
          .headOption
          .map(_.asInstanceOf[CarrierJob])
      }
      case None => {
        None
      }
    }
  }

  override def export()(implicit index: ExportedIndex) = exportType(format.JobType.CARRIER_SEARCH_JOB)
}

final class CarrierJob(val waypointA: Waypoint, val waypointB: Waypoint) extends AreaJob[Person] {
  require(
    waypointA.position.isOnSameLine(waypointB.position),
    s"Invalid carrier job waypoints. a: ${waypointA.position}, b: ${waypointB.position}"
  )
  // require(waypointA.area == waypointB.area, "Area of waypoints is not the same")

  override def area = waypointA.area

  val idlePosition                = waypointA.position.between(waypointB.position)
  override def position: Position = idlePosition

  override def trigger(person: Person, trigger: Trigger) = {
    if (!area.isJob(idlePosition, this)) {
      person.resetAndExecuteJob(trigger)
    }

    def area    = person.area
    def economy = area.economy
    
    if (person.state == PersonState.Idle) {
      if (isAtWaypoint(person)) {
        // Is at waypoint

        val currentWaypoint = if (isAtWaypoint(person, waypointA)) waypointA else waypointB
        val otherWaypoint   = if (currentWaypoint == waypointA) waypointB else waypointA

        // Handle items in persons stock
        for (suppliedItem <- person.stock.suppliedItems) {
          if (suppliedItem.isTargetWaypoint(currentWaypoint)) {
            // Finish the order
            suppliedItem.finish(economy)
          } else {
            currentWaypoint.stock.noItem(suppliedItem.itemType) match {
              case Some(noItem) => suppliedItem.transfer(economy, to = noItem) // dropped
              case _            => throw new IllegalStateException(s"Can't drop '$suppliedItem' at '$currentWaypoint'")
            }
          }
        }

        // Handle items at current waypoint
        for (suppliedItem <- currentWaypoint.multiStock.suppliedItems(ItemUsage.Default)) {
          if (canBeTransfered(suppliedItem, person, otherWaypoint)) {
            // Take the item
            suppliedItem.transfer(economy, person.stock.noItem(suppliedItem.itemType).get)
          }
        }

        if (person.stock.hasSuppliedItems) {
          // Bring to other waypoint
          person.goto(otherWaypoint.position)
        } else {
          // Go back to idle position
          person.goto(idlePosition)
        }
      } else {
        // Is not at waypoint
        
        if (person.stock.hasSuppliedItems) {
          // Go to waypoint
          person.goto(waypointA.position)
        } else {
          findWaypointWithItem(person) match {
            case Some(waypoint) => {
              // Go to waypoint
              if (person.isInBuilding) {
                person.leaveBuilding()
              }
              person.goto(waypoint.position)
            }
            case None => {
              // Go back to idle position
              if (!person.isAt(idlePosition)) {
                if (person.isInBuilding) {
                  person.leaveBuilding()
                }
                person.goto(idlePosition)
              }
            }
          }
        }
      }
    }
  }

  /** Find a [[Waypoint]] around the `idlePosition` that `canBeTransfered` by the [[Person]] */
  private def findWaypointWithItem(person: Person): Option[Waypoint] = {
    if (hasItem(person, waypointA, waypointB)) Some(waypointA)
    else if (hasItem(person, waypointB, waypointA)) Some(waypointB)
    else None
  }

  /** Check if `fromWaypoint` has a [[SuppliedItem]] that can be transfered `toWaypoint` */
  private def hasItem(person: Person, fromWaypoint: Waypoint, toWaypoint: Waypoint): Boolean = {
    fromWaypoint.multiStock.suppliedItems(ItemUsage.Default).exists(suppliedItem => canBeTransfered(suppliedItem, person, toWaypoint))
  }

  private def canBeTransfered(suppliedItem: SuppliedItem, person: Person, otherWaypoint: Waypoint): Boolean = {
    def isToOtherWaypoint = suppliedItem.nextTarget == otherWaypoint.stock
    def canCarry          = person.stock.hasNoItem(suppliedItem.itemType)
    def canAdd            = otherWaypoint.stock.hasNoItem(suppliedItem.itemType)
    def isTarget          = suppliedItem.target == otherWaypoint.stock

    isToOtherWaypoint && canCarry && (canAdd || isTarget)
  }

  private def isAtWaypoint(person: Person): Boolean = {
    isAtWaypoint(person, waypointA) || isAtWaypoint(person, waypointB)
  }

  private def isAtWaypoint(person: Person, waypoint: Waypoint): Boolean = {
    person.position == waypoint.position
  }

  override def export()(implicit index: ExportedIndex) = format.Job(
    `type` = Some(
      format.Job.Type(
        `type` = format.JobType.CARRIER_PERSON_JOB
      )
    ),
    waypointA = Some(index(waypointA)),
    waypointB = Some(index(waypointB))
  )
}
