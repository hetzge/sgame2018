package de.hetzge.sgame.job

import de.hetzge.sgame.environment._
import de.hetzge.sgame.building._
import de.hetzge.sgame.item._
import de.hetzge.sgame.person._
import de.hetzge.sgame.export._
import de.hetzge.sgame.format

import scala.concurrent.duration._

case object EnvironmentWorkerJob {
  val workDistance: Int = 1000
}
final case class EnvironmentWorkerJob(environmentType: EnvironmentType, buildingType: BuildingType, itemType: ItemType) extends Job[Person] {

  override def trigger(person: Person, trigger: Trigger) = {
    val area    = person.area
    val economy = area.economy

    if (person.state == PersonState.Idle) {
      if (!person.isInBuilding) {
        if (hasCapacity(person, itemType)) {
          val environmentPositionAroundOption = person.position.simpleAround.find(position => area.environment.has(position, environmentType))
          if (trigger == Trigger.FinishedWork) {
            for (environmentPositionAround <- environmentPositionAroundOption) {
              area.environment.takeItem(environmentPositionAround, person.stock)
            }

            person.setAndExecuteJob(GoHomePersonJob, Trigger.Default)
          } else {
            for (environmentPositionAround <- environmentPositionAroundOption) {
              person.startWork(2.seconds, person.position.orientation(environmentPositionAround))
            }
          }
        }
      } else if (person.isInBuilding(buildingType)) {
        val building = person.buildingStay.building.get

        if (hasCapacity(person, itemType)) {
          for (aroundEnvironmentPosition <- JobUtils.findAroundEnvironment(building, environmentType, EnvironmentWorkerJob.workDistance)) {
            person.leaveBuilding(keepReservation = true)
            if (aroundEnvironmentPosition == building.doorPosition) {
              this.trigger(person, Trigger.Default)
            } else {
              person.goto(aroundEnvironmentPosition)
            }
          }
        } else {
          JobUtils.dropAndSupply(economy, person.stock, building.stock)
        }
      } else if (!person.isReservedInBuilding(buildingType)) {
        JobUtils.findAndGotoBuilding(person, buildingType, trigger)
      }
    }
  }

  private def hasCapacity(person: Person, itemType: ItemType): Boolean = {
    if (person.buildingStay.building.isDefined) {
      person.buildingStay.building.get.stock.hasNoItem(itemType) && person.stock.hasNoItem(itemType)
    } else {
      false
    }
  }

  override def export()(implicit index: ExportedIndex) = format.Job(
    `type` = Some(
      format.Job.Type(
        `type` = format.JobType.ENVIRONMENT_WORKER_PERSON_JOB
      )
    ),
    environmentType = environmentType.exportType,
    itemType = itemType.exportType,
    buildingType = Some(buildingType.exportType)
  )
}
