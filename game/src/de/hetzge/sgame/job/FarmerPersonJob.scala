package de.hetzge.sgame.job

import de.hetzge.sgame.building._
import de.hetzge.sgame.person._
import de.hetzge.sgame.base._
import de.hetzge.sgame.base.helper._
import de.hetzge.sgame.environment._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.area._
import de.hetzge.sgame.item._
import de.hetzge.sgame.tile._
import de.hetzge.sgame.export._
import de.hetzge.sgame.format

import scala.concurrent.duration._

case object FarmerBuildingSearchJob extends BuildingSearchJob {
  override def buildingType = BuildingType.CropFarm
  override def job          = FarmerJob

  override def export()(implicit index: ExportedIndex) = exportType(format.JobType.FARMER_BUILDING_SEARCH_JOB)
}

case object FarmerJob extends Job[Person] {
  val workDistance = 100

  override def trigger(person: Person, trigger: Trigger) = {
    val area     = person.area
    val economy  = area.economy
    val position = person.position

    if (person.state == PersonState.Idle && !person.hasPath) {
      trigger match {
        case Trigger.ReachedStep => // ignore
        case Trigger.ReachedEndOfPath => {
          val isInBuilding = person.isInBuilding

          if (!isInBuilding && (canPlant(area, position) || canHarvest(person, position))) {
            person.startWork(2.second, South)
          } else if (!isInBuilding) {
            person.setAndExecuteJob(GoHomePersonJob, trigger)
          } else if (isInBuilding) {
            JobUtils.dropAndSupply(economy, person.stock, person.buildingStay.building.get.stock)
          }
        }
        case Trigger.FinishedWork => {
          if (canHarvest(person, position)) {
            harvest(person)
          } else if (canPlant(area, position)) {
            plant(person)
          }
          person.setAndExecuteJob(GoHomePersonJob, trigger)
        }
        case Trigger.Default => {
          if (person.isInBuilding(BuildingType.CropFarm)) {
            findWorkPosition(person) match {
              case Some(workPosition) => {
                person.leaveBuilding(keepReservation = true)
                person.goto(workPosition)
              }
              case None => {
                // ignore
              }
            }
          } else if (!person.isReservedInBuilding(BuildingType.CropFarm)) {
            person.resetAndExecuteJob(trigger)
          } else {
            person.setAndExecuteJob(GoHomePersonJob, trigger)
          }
        }
        case _ => // ignore
      }
    }
  }

  private def plant(person: Person): Unit = {
    val area        = person.area
    val environment = area.environment
    val position    = person.position

    if (isGrass(area, position)) {
      environment.set(position, EnvironmentType.Crop, count = 1, level = 0.1f)
    }
  }

  private def harvest(person: Person): Unit = {
    val area        = person.area
    val environment = area.environment
    val position    = person.position

    if (canHarvest(person, position)) {
      environment.takeItem(position, person.stock)
    }
  }

  private def findWorkPosition(person: Person): Option[Position] = {
    require(person.isInBuilding, s"$person is not in building")

    val area = person.area

    area.traverseFind(person.buildingStay.building.get.doorPosition, FarmerJob.workDistance) { position =>
      if (canPlant(area, position)) {
        Found
      } else if (canHarvest(person, position)) {
        Found
      } else if (!area.isBlocking(position)) {
        Continue
      } else {
        DontGoDeeper
      }
    }
  }

  private def canHarvest(person: Person, position: Position): Boolean = {
    def hasEnvironment      = person.area.environment.has(position, EnvironmentType.Crop, minLevel = 0.7f)
    def hasPersonCapacity   = person.stock.hasNoItem(CropItemType)
    def hasBuildingCapacity = person.buildingStay.building.get.stock.hasNoItem(CropItemType)

    hasEnvironment && hasPersonCapacity && hasBuildingCapacity
  }
  private def canPlant(area: Area, position: Position): Boolean = isGrass(area, position) && !area.isBlocking(position) && !area.environment.isEnvironment(position)
  private def isGrass(area: Area, position: Position): Boolean  = area.tileMap.isTopType(position, TileType.Grass)

  override def export()(implicit index: ExportedIndex) = format.Job(
    `type` = Some(
      format.Job.Type(
        `type` = format.JobType.FARMER_PERSON_JOB
      )
    )
  )
}
