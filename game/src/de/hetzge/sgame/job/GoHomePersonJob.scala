package de.hetzge.sgame.job

import de.hetzge.sgame.building._
import de.hetzge.sgame.person._
import de.hetzge.sgame.export._
import de.hetzge.sgame.format

case object GoHomePersonJob extends Job[Person] {
  override def trigger(person: Person, trigger: Trigger) = {
    person.buildingStay match {
      case ReservedBuildingStay(building) => trigger0(person, building, trigger)
      case EnteredBuildingStay(building)  => person.resetAndExecuteJob(trigger)
      case NoneBuildingStay               => throw new IllegalStateException(s"$person has no home")
    }
  }

  private def trigger0(person: Person, building: Building, trigger: Trigger): Unit = {
    if (person.state == PersonState.Idle) {
      if (!building.canEnter(person)) {
        person.resetAndExecuteJob(trigger)
      } else if (person.position == building.doorPosition) {
        enterBuilding(person, building, trigger)
      } else {
        gotoBuilding(person, building, trigger)
      }
    }
  }

  private def enterBuilding(person: Person, building: Building, trigger: Trigger): Unit = {
    if (person.position == building.doorPosition) {
      if (building.canEnter(person)) {
        building.enter(person)
      }
      person.resetAndExecuteJob(trigger)
    }
  }

  private def gotoBuilding(person: Person, building: Building, trigger: Trigger): Unit = {
    person.goto(building.doorPosition) match {
      case true => // everything is fine
      case false => {
        person.cancelBuildingReservation()
        person.resetAndExecuteJob(trigger)
      }
    }
  }

  override def export()(implicit index: ExportedIndex) = exportType(format.JobType.GO_HOME_PERSON_JOB)
}
