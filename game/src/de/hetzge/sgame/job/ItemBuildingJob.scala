package de.hetzge.sgame.job

import de.hetzge.sgame.building._
import de.hetzge.sgame.item._
import de.hetzge.sgame.export._
import de.hetzge.sgame.format
import de.hetzge.sgame.person._

object ItemBuildingJob extends Job[Building] {
  override def trigger(building: Building, trigger: Trigger) = {

    // temp
    for (noItem <- building.stock.noItem(StoneItemType)) {
      noItem.supply(building.area.economy, StoneItemType)
    }

    // temp
    for (needItemType <- building.buildingType.needItemTypes) {
      for (noItem <- building.stock.noItem(needItemType)) {
        noItem.demand(building.area.economy, needItemType)
      }
    }
  }

  override def export()(implicit index: ExportedIndex) = exportType(format.JobType.ITEM_BUILDING_JOB)
}
