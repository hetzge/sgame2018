package de.hetzge.sgame.job

import de.hetzge.sgame.area._
import de.hetzge.sgame.base._
import de.hetzge.sgame.base.helper._
import de.hetzge.sgame.person._
import de.hetzge.sgame.waypoint._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.item._
import de.hetzge.sgame.path._
import de.hetzge.sgame.building._
import de.hetzge.sgame.environment._
import de.hetzge.sgame.frames._
import de.hetzge.sgame.game._
import de.hetzge.sgame.stock._
import de.hetzge.sgame.tile._
import de.hetzge.sgame.export._
import de.hetzge.sgame.format

import scala.concurrent.duration._

final case class JobProto(
  jobType: String,
  waypointA: ReferenceProto,
  waypointB: ReferenceProto,
  environmentType: String,
  buildingType: String,
  itemType: String
)

private[job] object JobUtils {

  def findAndGotoBuilding(person: Person, buildingType: BuildingType, trigger: Trigger): Option[Building] = {
    JobUtils.findBuilding(person, buildingType) match {
      case Some(building: Building) => {
        if (person.isInBuilding) {
          person.leaveBuilding()
        }
        require(building.canReserve(person), s"can't reserve $person in $building")
        building.reserve(person)
        person.setAndExecuteJob(GoHomePersonJob, trigger)
        Some(building)
      }
      case None => {
        None
      }
    }
  }

  def findBuilding(person: Person, buildingType: BuildingType): Option[Building] = {
    val area = person.area
    area.searchBuilding { building =>
      def isSameWayGroup: Boolean = {
        (for {
          doorWay   <- building.doorWay
          personWay <- person.getWay
        } yield (doorWay.getGroup == personWay.getGroup)).getOrElse(false)
      }
      def isBuildingType: Boolean = building.buildingType == BuildingType.StreetWorker
      def canReserve: Boolean     = building.canReserve(person)

      isBuildingType && canReserve && isSameWayGroup
    }.headOption
  }

  def findAroundEnvironment(building: Building, environmentType: EnvironmentType, searchStepCountLimit: Int): Option[Position] = {
    val area         = building.area
    val doorPosition = building.doorPosition

    def isEnvironmentAround(position: Position): Boolean = Orientation.SIMPLE_AROUND_VALUES.exists(position.around(_).exists(area.environment.has(_, environmentType)))

    area
      .traverseFind(doorPosition, searchStepCountLimit) { traversePosition =>
        if (isEnvironmentAround(traversePosition) && !area.isBlocking(traversePosition)) {
          Found
        } else if (area.isBlocking(traversePosition)) {
          DontGoDeeper
        } else {
          Continue
        }
      }
  }

  def dropAndSupply(economy: ItemEconomy, fromStock: Stock, toStock: Stock): Unit = {
    for (blockedItem <- fromStock.blockedItems) {
      for (noItem <- toStock.noItem(blockedItem.itemType)) {
        blockedItem.transfer(noItem)
      }
    }
    for (blockedItem <- toStock.blockedItems) {
      blockedItem.supply(economy)
    }

    assert(fromStock.blockedItems.isEmpty, "from stock items should be empty now")
  }
}

object ImportJob {

  def load(f: format.Job)(implicit index: ImportedIndex): Job[_] = {
    f.`type`.get.`type` match {
      case format.JobType.BUILDING_LOT_JOB => BuildingLotJob
      case format.JobType.CARRIER_PERSON_JOB =>
        new CarrierJob(
          waypointA = index[Waypoint](f.waypointA.get),
          waypointB = index[Waypoint](f.waypointB.get)
        )
      case format.JobType.CARRIER_SEARCH_JOB => CarrierSearchJob
      case format.JobType.DUMMY_JOB          => DummyJob
      case format.JobType.DUMMY_PERSON_JOB   => DummyPersonJob
      case format.JobType.ENVIRONMENT_WORKER_PERSON_JOB =>
        EnvironmentWorkerJob(
          buildingType = BuildingType(f.buildingType.get),
          environmentType = EnvironmentType(f.environmentType),
          itemType = ItemType(f.itemType)
        )
      case format.JobType.FARMER_BUILDING_SEARCH_JOB        => FarmerBuildingSearchJob
      case format.JobType.FARMER_PERSON_JOB                 => FarmerJob
      case format.JobType.GO_HOME_PERSON_JOB                => GoHomePersonJob
      case format.JobType.ITEM_BUILDING_JOB                 => ItemBuildingJob
      case format.JobType.PANIC_JOB                         => PanicJob
      case format.JobType.STREET_WORKER_BUILDING_SEARCH_JOB => StreetWorkerJob
    }
  }

}

trait Job[T] extends ExportObject[format.Job] {
  def onSet(t: T): Unit   = {}
  def onUnset(t: T): Unit = {}
  def trigger(t: T, trigger: Trigger): Unit

  /* Util method to create singleto export type */
  protected def exportType(jobType: format.JobType) = format.Job(
    `type` = Some(
      format.Job.Type(
        `type` = jobType
      )
    )
  )
}

object JobEntity {
  val defaultJobFrameDelay = FrameDuration(5.seconds)
}
trait JobEntity[T] extends AreaEntity { entity: T =>
  private[this] var _job: Job[T] = createDefaultJob()
  def job: Job[T]                = _job

  def resetAndExecuteJob(trigger: Trigger): Unit = {
    resetJob()
    execute(trigger)
  }
  def resetJob(): Unit = {
    setJob(createDefaultJob())
  }

  def setAndExecuteJob(job: Job[T], trigger: Trigger): Unit = {
    setJob(job)
    execute(trigger)
  }
  def setJob(job: Job[T]): Unit = {
    _job.onUnset(entity)
    _job = job
    _job.onSet(entity)
  }

  def execute(trigger: Trigger): Unit = {
    _job.trigger(this, trigger)
  }

  def createDefaultJob(): Job[T]
}

trait KiEntity[F] extends FrameCallback[F] {
  override def apply(frameId: FrameId) = {
    ki()
  }

  def ki(): Unit

  final def startKi(): Unit = {
    GameContext.frames.executeAsync(_ => ki())
  }
}

case object DummyJob extends Job[AreaEntity] {
  override def trigger(t: AreaEntity, trigger: Trigger) = {}
  override def export()(implicit index: ExportedIndex) = format.Job(
    `type` = Some(
      format.Job.Type(
        `type` = format.JobType.DUMMY_JOB
      )
    )
  )
}

case object PanicJob extends Job[Person] {
  override def trigger(person: Person, trigger: Trigger) = {
    trigger match {
      case Trigger.ReachedStep => {
        if (person.area.isWay(person.position)) {
          person.stop
          person.resetAndExecuteJob(trigger)
        } else {
          PanicJob.this.trigger(person, Trigger.Default)
        }
      }
      case _ => {
        if (!person.hasPath) {
          val random           = GameContext.game.random
          val distance         = random.nextInt(5)
          val orientationIndex = random.nextInt(Orientation.SIMPLE_AROUND_VALUES.size)
          val orientation      = Orientation.SIMPLE_AROUND_VALUES(orientationIndex)
          val targetX          = person.position.x + distance * orientation.xInt
          val targetY          = person.position.y + distance * orientation.yInt

          if (person.area.isValid(targetX, targetY)) {
            person.setPath(Path(List(person.area.position(targetX, targetY))))
          }
        }
      }
    }
  }
  override def export()(implicit index: ExportedIndex) = exportType(format.JobType.PANIC_JOB)
}

trait BuildingSearchJob extends Job[Person] {
  def buildingType: BuildingType
  def job: Job[Person]

  override def trigger(person: Person, trigger: Trigger): Unit = {
    val area = person.area

    person.buildingStay match {
      case ReservedBuildingStay(building: Building) if building.buildingType == buildingType => {
        person.setAndExecuteJob(GoHomePersonJob, trigger)
      }
      case EnteredBuildingStay(building: Building) if building.buildingType == buildingType => {
        person.setAndExecuteJob(job, trigger)
      }
      case _ => {
        JobUtils.findAndGotoBuilding(person, buildingType, trigger)
      }
    }
  }
}

case object DummyPersonJob extends Job[Person] {
  override def trigger(t: Person, trigger: Trigger)    = ()
  override def export()(implicit index: ExportedIndex) = exportType(format.JobType.DUMMY_PERSON_JOB)
}
