package de.hetzge.sgame.job

import de.hetzge.sgame.building._
import de.hetzge.sgame.person._
import de.hetzge.sgame.item._
import de.hetzge.sgame.export._
import de.hetzge.sgame.format

case object StreetWorkerBuildingSearchJob extends BuildingSearchJob {
  override def buildingType = BuildingType.StreetWorker
  override def job          = StreetWorkerJob

  override def export()(implicit index: ExportedIndex) = exportType(format.JobType.STREET_WORKER_BUILDING_SEARCH_JOB)
}

case object StreetWorkerJob extends Job[Person] {
  override def trigger(person: Person, trigger: Trigger) = {
    val area    = person.area
    val economy = area.economy

    if (!person.hasPath) {
      person.stock.suppliedItem match {
        case Some(suppliedItem) => {
          // person has supplied item

          if (isAtWayToBuild(person, suppliedItem)) {
            suppliedItem.finish(economy)
            area.setupWay(person.position)
          } else {
            gotoWayToBuild(person, suppliedItem, trigger)
          }
        }
        case None => {
          // person has no supplied item

          person.buildingStay match {
            case EnteredBuildingStay(building) => {
              demandItems(building)
              supplyItemsToBuildWays(building)

              building.stock.suppliedItems(ItemUsage.Way).headOption match {
                case Some(suppliedItem) => {
                  takeItem(person, suppliedItem)
                  gotoWayToBuild(person, suppliedItem, trigger)
                }
                case None => {
                  // nothing to do
                }
              }
            }
            case ReservedBuildingStay(_building) => {
              person.setAndExecuteJob(GoHomePersonJob, trigger)
            }
            case _ => {
              person.resetAndExecuteJob(trigger)
            }
          }
        }
      }
    }
  }

  private def demandItems(building: Building): Unit = {
    val area    = building.area
    val economy = area.economy

    for (needItemType <- building.buildingType.needItemTypes) {
      for (noItem <- building.stock.noItems(needItemType)) {
        noItem.demand(economy, needItemType)
      }
    }
  }

  private def supplyItemsToBuildWays(building: Building): Unit = {
    val area    = building.area
    val economy = area.economy

    for (blockedItem <- building.stock.blockedItems) {
      blockedItem.supply(economy, ItemUsage.Way)
    }
  }

  private def takeItem(person: Person, suppliedItem: SuppliedItem): Unit = {
    val area    = person.area
    val economy = area.economy

    suppliedItem.transfer(economy, person.stock.noItem(suppliedItem.itemType).get)
  }

  private def gotoWayToBuild(person: Person, suppliedItem: SuppliedItem, trigger: Trigger): Unit = {
    assume(suppliedItem.target.entity.stockWay.isDefined)
    val way = suppliedItem.target.entity.stockWay.get

    if (person.isInBuilding) {
      person.leaveBuilding(true)
    }
    person.goto(way.position) match {
      case true  => // everything is fine (ignore)
      case false => cancel(person, suppliedItem, trigger)
    }
  }

  private def cancel(person: Person, suppliedItem: SuppliedItem, trigger: Trigger): Unit = {
    suppliedItem.cancel(person.area.economy)
    person.resetAndExecuteJob(trigger)
  }

  private def isAtWayToBuild(person: Person, suppliedItem: SuppliedItem): Boolean = {
    suppliedItem.target.entity.stockWay.exists(_.position == person.position)
  }

  override def export()(implicit index: ExportedIndex) = exportType(format.JobType.STREET_WORKER_PERSON_JOB)
}