package de.hetzge.sgame.ui

import de.hetzge.sgame.{Application, GameApplicationAdapter}
import de.hetzge.sgame.game._
import de.hetzge.sgame.render._
import de.hetzge.sgame.way._
import de.hetzge.sgame.building._
import de.hetzge.sgame.input._
import de.hetzge.sgame.person._
import de.hetzge.sgame.stock._
import de.hetzge.sgame.item._
import de.hetzge.sgame.waypoint._
import de.hetzge.sgame.base._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.action._
import de.hetzge.sgame.minimap._
import de.hetzge.sgame.area._
import de.hetzge.sgame.settings._
import com.badlogic.gdx.graphics.{Color, OrthographicCamera}
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle
import com.badlogic.gdx.scenes.scene2d.ui.{Button, ButtonGroup, Cell, HorizontalGroup, ImageButton, Label, ScrollPane, Skin, Table, VerticalGroup}
import com.badlogic.gdx.{Gdx, InputMultiplexer}
import com.badlogic.gdx.scenes.scene2d.{Actor, InputEvent, InputListener, Stage}
import com.badlogic.gdx.scenes.scene2d.utils.{ClickListener, Drawable, NinePatchDrawable, TextureRegionDrawable}
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.badlogic.gdx.graphics.g3d.utils.RenderContext
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.utils.DragListener
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer

class GameUi(game: Game) {
  private[this] val area: Area = game.area
  private[this] val _miniMap   = new MiniMap(area, 230, 230)

  private[this] val FONT_SCALE: Float = 1f
  private[this] def resources         = GameApplicationAdapter.resources
  private[this] def skin              = resources.skin
  private[this] val viewport          = new ScreenViewport(new OrthographicCamera())
  private[this] val stockItemsUi      = new StockItemsUi(resources, 230f)
  private[this] val personsUi         = new PersonsUi(resources, 230f)

  object stage extends Stage(viewport) {
    addActor(border)
    addActor(layout)
    addActor(sideTable)
    setDebugAll(false)
  }

  object layout extends Table {
    setFillParent(true)
    add(horizontalTop).expand().top().right().pad(10f)
  }

  object fpsLabel extends Label("", skin) {
    setColor(Color.RED)
    setFontScale(FONT_SCALE)

    override def draw(batch: Batch, delta: Float) = {
      setText(Gdx.graphics.getFramesPerSecond().toString)
      super.draw(batch, delta)
    }
  }

  object cpuFpsLabel extends Label("", skin) {
    setColor(Color.PINK)
    setFontScale(FONT_SCALE)

    override def draw(batch: Batch, delta: Float) = {
      setText(Application.cpuFpsCounter.fps.toString)
      super.draw(batch, delta)
    }
  }

  object frameLabel extends Label("", skin) {
    setColor(Color.BLUE)
    setFontScale(FONT_SCALE)

    override def draw(batch: Batch, delta: Float) = {
      setText(game.frames.current.id.value.toString)
      super.draw(batch, delta)
    }
  }

  object glProfilerLabel extends Label("", skin) {
    setColor(Color.PINK)
    setFontScale(FONT_SCALE)

    override def draw(batch: Batch, delta: Float) = {
      val profiler = game.renderContext.profiler

      val calls           = profiler.getCalls
      val drawCalls       = profiler.getDrawCalls
      val shaderSwitches  = profiler.getShaderSwitches
      val textureBindings = profiler.getTextureBindings
      val vertexCount     = profiler.getVertexCount.total

      setText(s"(calls: $calls draw: $drawCalls shader: $shaderSwitches texture: $textureBindings vertex: $vertexCount)")

      super.draw(batch, delta)
    }
  }

  object coordinateLabel extends Label("", skin) {
    setColor(Color.WHITE)
    setFontScale(FONT_SCALE)

    override def draw(batch: Batch, delta: Float) = {
      setText(game.renderContext.mouseTileX.value + "/" + game.renderContext.mouseTileY.value)
      super.draw(batch, delta)
    }
  }

  object horizontalTop extends HorizontalGroup {
    space(20f)
    addActor(fpsLabel)
    addActor(cpuFpsLabel)
    addActor(frameLabel)
    addActor(coordinateLabel)
    addActor(glProfilerLabel)
  }

  object mainButtonGroup extends ButtonGroup[Button] {
    val buttonSize: PixelFloat = 50f
  }
  object actionsTable extends Table {
    val size = mainButtonGroup.buttonSize
    mainButtonGroup.add(selectionModeButton)
    mainButtonGroup.add(gotoModeButton)
    add(selectionModeButton).minSize(size).maxSize(size).prefSize(size)
    add(gotoModeButton).minSize(size).maxSize(size).prefSize(size)
    add(startBuildingButton).minSize(size).maxSize(size).prefSize(size)
    add(deleteButton).minSize(size).maxSize(size).prefSize(size)
  }
  object buildStockTable extends Table {
    private var _stockButtons: Iterable[StockButton] = Iterable()

    init(None)
    def init(buildingOption: Option[Building]): Unit = {
      clear()
      mainButtonGroup.remove(_stockButtons.toArray: _*)

      val stockItemTypes = buildingOption match {
        case Some(building) => building.buildingType.needItemTypes ++ building.buildingType.producesItemTypes
        case None           => List()
      }
      _stockButtons = stockItemTypes.map(new StockButton(_))

      for (button <- _stockButtons) {
        val size = mainButtonGroup.buttonSize
        mainButtonGroup.add(button)
        add(button).minSize(size).maxSize(size).prefSize(size)
      }
    }
  }
  object buildBuildingTable extends FluentTable(skin, 250) {
    private var _buildingButtons: Iterable[BuildingButton] = Iterable()

    init()
    def init(): Unit = {
      clear()
      mainButtonGroup.remove(_buildingButtons.toArray: _*)

      _buildingButtons = List(
        BuildingType.Main,
        BuildingType.Lumberjack,
        BuildingType.Quarry,
        BuildingType.StreetWorker,
        BuildingType.CropFarm,
        BuildingType.SmallResidence,
        BuildingType.NormalResidence,
        BuildingType.BigResidence,
        BuildingType.LandMark
      ).map(new BuildingButton(_))

      for (button <- _buildingButtons) {
        val size = mainButtonGroup.buttonSize
        button.setSize(size, size)
        mainButtonGroup.add(button)
        fluentAdd(button).size(size, size)
      }
    }
  }
  object buildWayTable extends Table {
    private var _wayButtons: Iterable[WayButton] = Iterable()

    init()
    def init(): Unit = {
      clear()
      mainButtonGroup.remove(_wayButtons.toArray: _*)

      _wayButtons = List(StoneWayType, DefaultWayType).map(new WayButton(_))

      for (button <- _wayButtons) {
        val size = mainButtonGroup.buttonSize
        mainButtonGroup.add(button)
        add(button).minSize(size).maxSize(size).prefSize(size)
      }
    }
  }

  object border extends Table {
    setBackground(new NinePatchDrawable(resources.nine.border))
  }

  object selectionModeButton
      extends ImageButton(new ImageButtonStyle(skin.buttonStyle) {
        imageUp = new TextureRegionDrawable(resources.region.cursor)
      }) {
    addListener(new ClickListener() {
      override def clicked(event: InputEvent, x: Float, y: Float) = {
        GameContext.localSettings.setAreaInput(SelectionAreaInput)
      }
    })
  }

  object gotoModeButton
      extends ImageButton(new ImageButtonStyle(skin.buttonStyle) {
        imageUp = new TextureRegionDrawable(resources.region.icon.goto)
      }) {
    setVisible(false)
    addListener(new ClickListener() {
      override def clicked(event: InputEvent, x: Float, y: Float) = {
        GameContext.localSettings.setAreaInput(GotoAreaInput)
      }
    })
  }

  object deleteButton
      extends ImageButton(new ImageButtonStyle(skin.buttonStyle) {
        imageUp = new TextureRegionDrawable(resources.region.icon.delete)
        imageDown = new TextureRegionDrawable(resources.region.icon.delete)
      }) {
    setVisible(false)
    addCaptureListener(new ClickListener() {
      override def clicked(event: InputEvent, x: Float, y: Float) = {
        // TODO
      }
    })
  }

  object startBuildingButton
      extends ImageButton(new ImageButtonStyle(skin.buttonStyle) {
        imageUp = new TextureRegionDrawable(resources.region.icon.start)
      }) {
    setVisible(false)
    addListener(new ClickListener() {
      override def clicked(event: InputEvent, x: Float, y: Float) = {
        GameContext.localSettings.selectedPerson.filter { case p: Person if p.personType == PersonType.Start => true } match {
          case Some(startPerson) => GameContext.game.submit(BuildStartBuilding(startPerson.id))
          case None              => println("No start person is selected. Can't build start building.")
        }
      }
    })
  }

  object infoTable extends Table {
    val personInfoTable   = new PersonInfoTable(skin)
    val buildingInfoTable = new BuildingInfoTable(skin)
    val wayInfoTable      = new WayInfoTable(skin)
  }

  object miniMapImage extends Image {
    private[this] val _shapeRenderer: ShapeRenderer = new ShapeRenderer()
    setDrawable(new TextureRegionDrawable(new TextureRegion(_miniMap.texture)))

    addListener(new DragListener() {
      override def touchDragged(event: InputEvent, x: Float, y: Float, pointer: Int): Unit = {
        setCamera(x, y)
      }
    })
    addListener(new ClickListener() {
      override def clicked(event: InputEvent, x: Float, y: Float): Unit = {
        setCamera(x, y)
      }
    })

    private def setCamera(x: Float, y: Float): Unit = {
      val tileX = x.toInt + _miniMap.offsetX
      val tileY = 230 - y.toInt + _miniMap.offsetY

      game.renderContext.setCamera(tileX, tileY)
    }
  }

  object sideTable extends Table {
    setupSize()
    setBackground(new NinePatchDrawable(resources.nine.panel_blue))

    def nullDrawable: Drawable = null
    val innerTable             = new Table()
    innerTable.setBackground(nullDrawable)
    innerTable.add(miniMapImage).center().expandX().height(230f).pad(10f)
    innerTable.row()
    innerTable.add(actionsTable).left().expandX().pad(10f)
    innerTable.row()
    innerTable.add(buildWayTable).left().expandX().pad(10f)
    innerTable.row()
    innerTable.add(buildBuildingTable).left().expandX().pad(10f)
    innerTable.row()
    innerTable.add(buildStockTable).left().expandX().pad(10f)
    innerTable.row()

    add(innerTable).top().expandX().fillX()
    row()

    val stockItemsScrollPane = new FocusScrollPane(stockItemsUi, skin)
    add(stockItemsScrollPane).minHeight(150f).expandY().fillX()
    row()

    val personsScrollPane = new FocusScrollPane(personsUi, skin)
    add(personsScrollPane).minHeight(150f).expandY().fillX()
    row()

    val infoScrollPane = new FocusScrollPane(infoTable, skin)
    add(infoScrollPane).expandY().fillX()
    row()

    def setupSize(): Unit = {
      setPosition(10f, 10f)
      setWidth(250f)
      setHeight(Gdx.graphics.getHeight - 20f)
    }
  }

  object areaInputWatcher extends Watcher[AreaInput] {
    override def get(game: Game) = Some(game.localSettings.areaInput)
    override def onSet(entity: AreaInput) = {
      entity match {
        case SelectionAreaInput => selectionModeButton.setChecked(true)
        case GotoAreaInput      => gotoModeButton.setChecked(true)
        case _                  => // ignore
      }
    }
    override def onUnset(entity: AreaInput)  = {}
    override def onUpdate(entity: AreaInput) = {}
  }
  object selectionWatcher extends Watcher[Selection] {
    override def get(game: Game) = game.localSettings.selection
    override def onSet(selection: Selection) = {
      selection match {
        case BuildingSelection(building) => {
          buildStockTable.init(Some(building))
          infoTable.add(infoTable.buildingInfoTable)
          deleteButton.setVisible(true)
        }
        case PersonSelection(person) => {
          infoTable.add(infoTable.personInfoTable)
          if (person.personType.userControlled) {
            GameContext.localSettings.setAreaInput(GotoAreaInput)
            gotoModeButton.setVisible(true)
          }

          startBuildingButton.setVisible(person.personType == PersonType.Start)
        }
        case _ => {}
      }
    }
    override def onUnset(selection: Selection) = {
      selection match {
        case BuildingSelection(building) => {
          infoTable.clear()
          buildStockTable.init(None)
          deleteButton.setVisible(false)
        }
        case PersonSelection(person) => {
          infoTable.clear()
          startBuildingButton.setVisible(false)
          gotoModeButton.setVisible(false)
        }
        case _ => {}
      }
    }
    override def onUpdate(selection: Selection) = {
      selection match {
        case BuildingSelection(building) => {
          stockItemsUi.setup(building.stock)
          personsUi.setup(building.persons)
          infoTable.buildingInfoTable.setup(building)
        }
        case PersonSelection(person) => {
          stockItemsUi.setup(person.stock)
          infoTable.personInfoTable.setup(person)
        }
        case _ => {}
      }
    }
  }

  def render(): Unit = {
    stage.act(Gdx.graphics.getDeltaTime)
    stage.draw()

    selectionWatcher.update(game)

    game.renderContext.profiler.reset()

    _miniMap.update(game.localSettings, game.renderContext.visibleTileBounds(area))
  }

  def resize(width: Int, height: Int): Unit = {
    stage.getViewport.update(width, height, true)
    border.setSize(stage.getWidth, stage.getHeight)
    sideTable.setupSize()
  }

  def dispose(): Unit = {
    stage.dispose()
  }

  object input extends InputMultiplexer {
    addProcessor(stage)
  }

  private final class WayButton(wayType: WayType)
      extends ImageButton(new ImageButtonStyle(skin.buttonStyle) {
        imageUp = new TextureRegionDrawable(wayType.renderableSet.defaultTile(OrientationSet.ALL).renderable.textureRegion)
      }) {
    addListener(new ClickListener() {
      override def clicked(event: InputEvent, x: Float, y: Float): Unit = {
        GameContext.localSettings.setAreaInput(BuildWayAreaInput(wayType))
      }
    })
  }

  private final class BuildingButton(buildingType: BuildingType)
      extends ImageButton(new ImageButtonStyle(skin.buttonStyle) {
        imageUp = {
          buildingType.renderable match {
            case textureRenderable: TextureRenderable => new TextureRegionDrawable(textureRenderable.textureRegion)
            case _                                    => throw new IllegalStateException(s"${buildingType.renderHeight}")
          }
        }
      }) {
    addListener(new ClickListener() {
      override def clicked(event: InputEvent, x: Float, y: Float): Unit = {
        GameContext.localSettings.setAreaInput(BuildBuildingAreaInput(buildingType))
      }
    })
  }

  private final class StockButton(itemType: ItemType)
      extends ImageButton(new ImageButtonStyle(skin.buttonStyle) {
        imageUp = {
          itemType.renderableSet.default match {
            case textureRenderable: TextureRenderable => new TextureRegionDrawable(textureRenderable.textureRegion)
            case _                                    => throw new IllegalStateException(s"${itemType.renderableSet.default}")
          }
        }
      }) {
    addListener(new ClickListener() {
      override def clicked(event: InputEvent, x: Float, y: Float): Unit = {
        GameContext.localSettings.setAreaInput(BuildStockAreaInput(itemType))
      }
    })
  }
}

private final case class KeyValueRow(key: String)(implicit table: Table) {
  object value extends Label("", table.getSkin)
  table.add(key).pad(3f).top().fillX().expandX()
  table.add(value).pad(3f).top().fillX().expandX()
  table.row()
}

final class BuildingInfoTable(skin: Skin) extends Table(skin) {
  implicit def table: Table = this

  private object rows {
    val buildingType = KeyValueRow("type")
    val x            = KeyValueRow("x")
    val y            = KeyValueRow("y")
    val width        = KeyValueRow("width")
    val height       = KeyValueRow("height")
    val job          = KeyValueRow("job")
    val playerId     = KeyValueRow("player")
    val persons      = KeyValueRow("persons")
  }

  def setup(building: Building): Unit = {
    rows.buildingType.value.setText(building.buildingType.toString)
    rows.x.value.setText(building.position.x.value.toString)
    rows.y.value.setText(building.position.y.value.toString)
    rows.width.value.setText(building.width.value.toString)
    rows.height.value.setText(building.height.value.toString)
    rows.job.value.setText(building.job.getClass.getSimpleName)
    rows.playerId.value.setText(building.playerId.value.toString)
    rows.persons.value.setText(building.persons.mkString("\n"))

    setVisible(true)
  }
}

final class PersonInfoTable(skin: Skin) extends Table(skin) {
  implicit def table: Table = this

  private object rows {
    val personType = KeyValueRow("type")
    val x          = KeyValueRow("x")
    val y          = KeyValueRow("y")
    val state      = KeyValueRow("state")
    val path       = KeyValueRow("path")
    val job        = KeyValueRow("job")
    val playerId   = KeyValueRow("player")
    val building   = KeyValueRow("building")
  }

  def setup(person: Person): Unit = {
    rows.personType.value.setText(person.personType.toString)
    rows.x.value.setText(person.position.x.value.toString)
    rows.y.value.setText(person.position.y.value.toString)
    rows.state.value.setText(person.state.toString)
    rows.path.value.setText(person.getPath.steps.map(step => step.x.value + "/" + step.y.value).mkString("\n"))
    rows.job.value.setText(person.job.getClass.getSimpleName)
    rows.playerId.value.setText(person.playerId.value.toString)
    rows.building.value.setText(person.buildingStay.toString)

    setVisible(true)
  }
}

class FluentTable(skin: Skin, width: PixelFloat) extends Table(skin) {
  setWidth(width)
  private[this] var _x = 0f
  def fluentAdd[T <: Actor](actor: T): Cell[T] = {
    if (_x + actor.getWidth > width) {
      row()
      _x = 0f
    }
    _x = _x + actor.getWidth
    add(actor)
  }
}

class FocusScrollPane(actor: Actor, skin: Skin) extends ScrollPane(actor, skin) {
  addListener(inputListener)
  object inputListener extends InputListener() {
    override def exit(event: InputEvent, x: Float, y: Float, pointer: Int, toActor: Actor) = {
      getStage.setScrollFocus(null)
    }
    override def enter(event: InputEvent, x: Float, y: Float, pointer: Int, fromActor: Actor) = {
      getStage.setScrollFocus(FocusScrollPane.this)
    }
  }
}

final class WayInfoTable(skin: Skin) extends Table(skin) {
  implicit def table: Table = this

  private object rows {
    val wayType = KeyValueRow("type")
    val x       = KeyValueRow("x")
    val y       = KeyValueRow("y")
    val group   = KeyValueRow("group")
  }

  object other {
    val waypointInfoTable = new WaypointInfoTable(skin)
    add(waypointInfoTable).fillX().expandX().colspan(2)
    row()
  }

  def setup(way: Way): Unit = {
    rows.wayType.value.setText(way.wayType.toString)
    rows.x.value.setText(way.position.x.value.toString)
    rows.y.value.setText(way.position.y.value.toString)
    rows.group.value.setText(way.getGroup.toString)

    way.getWaypoint match {
      case Some(waypoint) => other.waypointInfoTable.setup(waypoint)
      case None           => other.waypointInfoTable.setVisible(false)
    }

    setVisible(true)
  }
}

final class WaypointInfoTable(skin: Skin) extends Table(skin) {
  implicit def table: Table = this

  private object rows {
    val waypointType = KeyValueRow("type")
    val x            = KeyValueRow("x")
    val y            = KeyValueRow("y")
    val group        = KeyValueRow("group")
  }

  def setup(waypoint: Waypoint): Unit = {
    rows.waypointType.value.setText(waypoint.waypointType.toString)
    rows.x.value.setText(waypoint.position.x.value.toString)
    rows.y.value.setText(waypoint.position.y.value.toString)
    rows.group.value.setText(waypoint.getGroup.toString)

    setVisible(true)
  }
}
