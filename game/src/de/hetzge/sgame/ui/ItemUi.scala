package de.hetzge.sgame.ui

import de.hetzge.sgame.unit._
import de.hetzge.sgame.item._
import de.hetzge.sgame.resources._
import de.hetzge.sgame.stock._
import de.hetzge.sgame.render._
import de.hetzge.sgame.tile._

import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Stack
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup
import com.badlogic.gdx.scenes.scene2d.ui.Label

final class StockItemsUi(resources: ApplicationResources, width: PixelFloat) extends VerticalGroup {
  setWidth(width)
  left().columnLeft()

  private[this] val out     = new SortedItemsUi(resources, width)
  private[this] val in      = new SortedItemsUi(resources, width)
  private[this] val neutral = new SortedItemsUi(resources, width)

  addActor(new Label("Out", resources.skin.labelStyle))
  addActor(out)
  addActor(new Label("In", resources.skin.labelStyle))
  addActor(in)
  addActor(new Label("Neutral", resources.skin.labelStyle))
  addActor(neutral)

  def setup(stock: Stock): Unit = {
    val itemsByDirection = stock.items.groupBy(item => item.direction)

    out.setup(itemsByDirection.get(ItemDirection.Out).getOrElse(List.empty))
    in.setup(itemsByDirection.get(ItemDirection.In).getOrElse(List.empty))
    neutral.setup(itemsByDirection.get(ItemDirection.Neutral).getOrElse(List.empty))
  }
}

final class SortedItemsUi(resources: ApplicationResources, width: PixelFloat) extends VerticalGroup {
  setWidth(width)
  left().columnLeft()

  def setup(items: Seq[Item]): Unit = {
    clear()

    val itemsByType = items.groupBy(_.itemType)
    val itemUis = itemsByType.map {
      case (itemType, items) =>
        val itemsUi = new ItemsUi(resources, width)
        itemsUi.setup(sort(items))
        itemsUi
    }

    for (itemUi <- itemUis) {
      addActor(itemUi)
    }
  }

  private def sort(items: Seq[Item]): Seq[Item] = {
    items.sortWith((a, b) => {

      def index =
        (item: Item) =>
          item match {
            case _: BlockedItem  => 0
            case _: DemandItem   => 1
            case _: DemandedItem => 2
            case _: NoItem       => 3
            case _: SuppliedItem => 4
            case _: SupplyItem   => 5
          }

      index(a) < index(b)
    });
  }
}

final class ItemsUi(resources: ApplicationResources, width: PixelFloat) extends FluentTable(resources.skin, width) {
  def setup(items: Seq[Item]): Unit = {
    clear()
    for (item <- items) {
      fluentAdd(new ItemUi(resources, item))
    }
  }
}

final class ItemUi(resources: ApplicationResources, item: Item) extends Stack {
  val overlayDrawable = item match {
    case _: BlockedItem  => new TextureRegionDrawable(resources.region.ui.item_blocked)
    case _: DemandItem   => new TextureRegionDrawable(resources.region.ui.item_demand)
    case _: DemandedItem => new TextureRegionDrawable(resources.region.ui.item_demanded)
    case _: SupplyItem   => new TextureRegionDrawable(resources.region.ui.item_demand)
    case _: SuppliedItem => new TextureRegionDrawable(resources.region.ui.item_demanded)
    case _: NoItem       => new TextureRegionDrawable(resources.region.ui.item)
  }

  val itemDrawable = item.itemType.renderableSet.default match {
    case t: TextureRegionRenderable => new TextureRegionDrawable(t.textureRegion)
    case _                          => throw new IllegalStateException
  }

  setSize(overlayDrawable.getMinWidth(), overlayDrawable.getMinHeight())

  addActor(new Image(itemDrawable))
  addActor(new Image(overlayDrawable))
}
