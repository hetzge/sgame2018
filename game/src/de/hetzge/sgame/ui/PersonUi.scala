package de.hetzge.sgame.ui

import de.hetzge.sgame.resources._
import de.hetzge.sgame.person._
import de.hetzge.sgame.unit._

import com.badlogic.gdx.scenes.scene2d.ui.Stack
import de.hetzge.sgame.render.TextureRegionRenderable
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label

final class PersonsUi(resources: ApplicationResources, width: PixelFloat) extends FluentTable(resources.skin, width) {
  def setup(persons: Seq[Person]): Unit = {
    clear()

    val countByPersonType = persons.groupBy(person => (person.personType, person.buildingStay)).map(it => (it._1, it._2.size))
    for (((personType, buildingStay), count) <- countByPersonType) {
      val personTypeUi = new PersonTypeUi(resources, personType, buildingStay)
      personTypeUi.setup(count)
      fluentAdd(personTypeUi)
    }
  }
}

final class PersonTypeUi(resources: ApplicationResources, personType: PersonType, buildingStay: BuildingStay) extends Stack {
  private[this] val personDrawable = new TextureRegionDrawable(personType.renderableSet.person_idle match {
    case t: TextureRegionRenderable => t.textureRegion
    case _                          => throw new IllegalStateException
  })
  private[this] val overlayDrawable = buildingStay match {
    case EnteredBuildingStay(_building)  => new TextureRegionDrawable(resources.region.ui.item_blocked)
    case ReservedBuildingStay(_building) => new TextureRegionDrawable(resources.region.ui.item_demanded)
    case NoneBuildingStay                => new TextureRegionDrawable(resources.region.ui.item)
  }
  private[this] val label = new Label("", resources.skin.labelStyle)
  label.setWidth(overlayDrawable.getMinWidth())
  label.setEllipsis(true)

  setSize(overlayDrawable.getMinWidth(), overlayDrawable.getMinHeight())
  addActor(new Image(personDrawable))
  addActor(new Image(overlayDrawable))
  addActor(label)

  def setup(count: Int): Unit = {
    label.setText(count.toString())
  }
}
