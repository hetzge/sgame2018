package de.hetzge.sgame.area

import de.hetzge.sgame.TestUtils
import de.hetzge.sgame.building._
import de.hetzge.sgame.item._
import de.hetzge.sgame.job._
import de.hetzge.sgame.player._
import de.hetzge.sgame.stock._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.way._
import de.hetzge.sgame.waypoint._
import de.hetzge.sgame.settings._
import de.hetzge.sgame.game.GameContext
import org.scalatest._

class AreaTest extends FunSuite with GivenWhenThen {

  test("update carrier job") {
    Given("a area with two waypoints with a gap (no way) between.")

    val area = TestUtils.createArea(3, 3)
    area.buildWay(area.position(0, 0), DefaultWayType, GaiaPlayer.id)
    area.buildWay(area.position(0, 1), DefaultWayType, GaiaPlayer.id)

    area.buildWay(area.position(2, 0), DefaultWayType, GaiaPlayer.id)
    area.buildWay(area.position(2, 1), DefaultWayType, GaiaPlayer.id)

    /*
     * Now the area should look like:
     * x-x
     * x-x
     * ---
     */

    When("a way is build in the gap.")
    area.buildWay(area.position(1, 1), DefaultWayType, GaiaPlayer.id)

    Then("a carrier job should be created between the waypoints.")
    assert(area.isJob(area.position(1, 1), classOf[CarrierJob]))
  }

  test("get waypoints around") {
    Given("a area with two connected waypoints")
    val area = TestUtils.createArea(3, 3)
    area.buildWay(area.position(0, 0), DefaultWayType, GaiaPlayer.id)
    area.buildWay(area.position(0, 1), DefaultWayType, GaiaPlayer.id)
    area.buildWay(area.position(1, 1), DefaultWayType, GaiaPlayer.id)
    area.buildWay(area.position(2, 1), DefaultWayType, GaiaPlayer.id)
    area.buildWay(area.position(2, 0), DefaultWayType, GaiaPlayer.id)

    /*
     * Now the area should look like:
     * x-x
     * XxX
     * ---
     */
    assert(area.isWaypoint(area.position(0, 1)))
    assert(area.isWaypoint(area.position(2, 1)))

    When("waypoints around are requested")
    val waypointsAround = area.getWaypointsAround(area.position(1, 1))

    Then("there should be 2 around")
    assert(waypointsAround.size == 2)
  }

  test("is way at invalid x y") {
    val area = TestUtils.createArea(3, 3)
    assertThrows[Exception](!area.isWay(3, 0))
  }

  // TODO rework after pathToWaypoint no longer exists
  test("find path to waypoint") {
    Given("a area with a way with waypoints")
    val area = TestUtils.createArea(10, 10)
    area.buildWay(area.position(4, 0), area.position(4, 9), DefaultWayType, GaiaPlayer.id)
    area.buildWaypoint(area.position(4, 0))
    area.buildWaypoint(area.position(4, 9))

    When("path to waypoint is requested")
    val path = area.pathTo(area.position(0, 0), area.position(4, 0))

    println(path.map(_.toString()))

    Then("there should be a path")
    assert(path.isDefined)
    Then("the path should start at 0 0")
    assert(path.flatMap(_.head) == Some(area.position(0, 0)))
    Then("the path should end at 4 0 (where the nearest waypoint is)")
    assert(path.flatMap(_.last) == Some(area.position(4, 0)))
  }

  // TODO rework after pathToWay no longer exists
  test("find path to way") {
    Given("a area with different ways")
    val area = TestUtils.createArea(10, 10)
    area.buildWay(area.position(2, 2), area.position(7, 2), DefaultWayType, GaiaPlayer.id)
    area.buildWay(area.position(7, 3), area.position(7, 7), DefaultWayType, GaiaPlayer.id)
    area.buildWay(area.position(6, 7), area.position(2, 7), DefaultWayType, GaiaPlayer.id)
    area.buildWay(area.position(2, 6), area.position(2, 3), DefaultWayType, GaiaPlayer.id)

    area.buildWay(area.position(4, 4), area.position(5, 4), DefaultWayType, GaiaPlayer.id)

    When("path to way is requested")
    val path = area.pathTo(area.position(1, 1), area.position(5, 4), useWay = true)

    Then("there should be a path")
    assert(path.isDefined)

    Then("the path should end at 5 4")
    assert(path.flatMap(_.last) == Some(area.position(5, 4)))
  }

  test("find path between waypoints") {
    Given("a area with a way with waypoints")
    val area = TestUtils.createArea(10, 10)
    area.buildWay(area.position(1, 1), area.position(8, 1), DefaultWayType, GaiaPlayer.id)
    area.buildWay(area.position(8, 2), area.position(8, 8), DefaultWayType, GaiaPlayer.id)
    area.buildWay(area.position(7, 8), area.position(4, 8), DefaultWayType, GaiaPlayer.id)
    area.buildWay(area.position(4, 7), area.position(4, 6), DefaultWayType, GaiaPlayer.id)
    area.buildWaypoint(area.position(1, 1))
    area.buildWaypoint(area.position(4, 6))

    When("path from waypoint to waypoint is requested")
    println(area.pathOnWay(1, 1, 4, 6).map(_.toString()))
    val path = area.pathOnWay(1, 1, 4, 6).map(_.optimize)
    println(path.map(_.toString()))

    Then("there should be a path")
    assert(path.isDefined)
    Then("the path should start at 1 1")
    assert(path.flatMap(_.head) == Some(area.position(1, 1)))
    Then("the path should end at 4 6")
    assert(path.flatMap(_.last) == Some(area.position(4, 6)))
    Then("the path should have 5 steps")
    assert(path.map(_.steps.size).getOrElse(-1) == 5)
  }

  test("automatically add/remove carrier job between two waypoints") {
    Given("a area with a way")
    val area = TestUtils.createArea(3, 3)
    area.buildWay(area.position(0, 1), area.position(2, 1), DefaultWayType, GaiaPlayer.id)

    When("two waypoints in a line are build")
    area.buildWaypoint(area.position(0, 1))
    area.buildWaypoint(area.position(2, 1))

    Then("there should be a carrier job between them")
    assert(area.isJob(area.position(1, 1), classOf[CarrierJob]))

    When("a waypoint of a pair is removed")
    area.destroyWaypoint(area.position(0, 1))

    Then("the carrier job should also be removed")
    assert(!area.isJob(area.position(1, 1), classOf[CarrierJob]))
  }

  test("automatically remove carrier job if some way is removed from long way") {
    Given("a area with a long way")
    val area         = TestUtils.createArea(50, 50)
    val fromPosition = area.position(0, 25)
    val toPosition   = area.position(49, 25)
    area.buildWay(fromPosition, toPosition, DefaultWayType, GaiaPlayer.id)
    area.buildWaypoint(fromPosition, DefaultWaypointType)
    area.buildWaypoint(toPosition, DefaultWaypointType)

    Then("there should be a long way with two waypoints")
    assert(area.areaPositions(fromPosition.x, fromPosition.y, 50, 1).forall(area.isWay(_)))
    assert(area.isWaypoint(fromPosition))
    assert(area.isWaypoint(toPosition))
    val fromWaypoint = area.waypoint(fromPosition).get
    val toWaypoint   = area.waypoint(toPosition).get
    assert(fromWaypoint.connections.contains(toWaypoint))
    assert(toWaypoint.connections.contains(fromWaypoint))
    assert(area.isJob(fromPosition.between(toPosition), classOf[CarrierJob]))

    When("there is a single way is removed")
    area.destroyWay(area.position(10, 25))

    Then("there should be no more connection between the waypoints")
    assert(!fromWaypoint.connections.contains(toWaypoint))
    assert(!toWaypoint.connections.contains(fromWaypoint))

    Then("there shouldn't be a carrier job between the waypoints")
    assert(!area.isJob(fromPosition.between(toPosition), classOf[CarrierJob]))
  }

  test("automatically add/remove carrier job if new waypoints are build around") {
    Given("a area with a way")
    val area = TestUtils.createArea(10, 10)
    area.buildWay(area.position(0, 5), area.position(9, 5), DefaultWayType, GaiaPlayer.id)

    When("there are two waypoints at the way")
    area.buildWaypoint(area.position(0, 5))
    area.buildWaypoint(area.position(9, 5))

    Then("there should be a carrier job between the waypoints")
    val centerPosition = area.position(0, 5).between(area.position(9, 5))
    assert(area.isJob(centerPosition, classOf[CarrierJob]))

    When("additional way/waypoints are build around the existing carrier job")
    area.buildWay(area.position(centerPosition.x, 0), area.position(5, 9), DefaultWayType, GaiaPlayer.id)
    area.buildWaypoint(area.position(centerPosition.x, 0))
    area.buildWaypoint(area.position(centerPosition.x, 9))

    Then("the original carrier job should no longer exist")
    assert(!area.isJob(centerPosition, classOf[CarrierJob]))

    Then("additional carrier jobs should be available")
    assert(area.isJob(centerPosition.between(area.position(centerPosition.x, 0)), classOf[CarrierJob]))
    assert(area.isJob(centerPosition.between(area.position(centerPosition.x, 9)), classOf[CarrierJob]))
    assert(area.isJob(centerPosition.between(area.position(0, 5)), classOf[CarrierJob]))
    assert(area.isJob(centerPosition.between(area.position(9, 5)), classOf[CarrierJob]))
  }

  test("building door waypoint") {
    Given("a area")
    val area = TestUtils.createGame(10, 10).area

    When("a building is build")
    val buildingType = BuildingType.StreetWorker
    val building     = area.buildBuilding(area.position(2, 2), buildingType, GaiaPlayer.id).get

    Then("there should be a door way")
    assert(building.doorWay.isDefined)
    assert(area.isWay(building.doorPosition))

    Then("there should be a door waypoint")
    assert(building.doorWaypoint.isDefined)
    assert(area.isWaypoint(building.doorPosition))

    Then("the door waypoint should be connected with the building")
    assert(area.waypoint(building.doorPosition).get.building == Some(building))

    When("the building is destroyed")
    area.destroyBuilding(building)

    Then("the door waypoint should be no longer connected to the building")
    assert(!building.stock.asInstanceOf[MasterStock].stocks.contains(building.doorWaypoint.get.stock))
  }

  test("can't build building if door waypoint is not possible") {
    Given("a area")
    val area = TestUtils.createArea(10, 10)

    val buildingType = BuildingType.StreetWorker
    val x            = 2
    val y            = 2
    val doorX        = x + buildingType.doorX
    val doorY        = y + buildingType.doorY

    When("a waypoint would be directly beside the door")
    area.buildWay(area.position(doorX, doorY + 1), DefaultWayType, GaiaPlayer.id)
    area.buildWaypoint(area.position(doorX, doorY + 1))

    Then("building can't be build")
    area.canBuildBuilding(area.position(x, y), buildingType, GaiaPlayer.id)
  }

  test("build building without door") {
    Given("a area")
    val area     = TestUtils.createArea(10, 10)

    Given("a way")
    area.buildWay(area.position(3, 7), DefaultWayType, GaiaPlayer.id)
    area.buildWaypoint(area.position(3, 7), DefaultWaypointType)
    assert(area.isWay(area.position(3, 7)))
    assert(area.isWaypoint(area.position(3, 7)))

    When("a doorless building is build near the waypoint")
    area.buildBuilding(area.position(3, 6), BuildingType.Test(), GaiaPlayer.id)

    Then("it should be builded (without problems with the missing door)")
    assert(area.building(area.position(3, 6)).exists(_.buildingType.isInstanceOf[BuildingType.Test]))
  }

  test("connect stock to correct building") {
    Given("a game with a area")
    val game = TestUtils.createGame(20, 20)
    val area = game.area

    Given("two buildings")
    val topBuilding    = area.buildBuilding(area.position(10, 10 - BuildingType.Lumberjack.height - 1), BuildingType.Lumberjack, GaiaPlayer.id).get
    val bottomBuilding = area.buildBuilding(area.position(10, 10), BuildingType.Lumberjack, GaiaPlayer.id).get

    Given("one of the two buildings is selected")
    game.localSettings.setSelection(BuildingSelection(bottomBuilding))

    When("a stock is build between the buildings")
    val stockBuilding = area.buildBuilding(area.position(10, 9), BuildingType.Stock(WoodItemType), GaiaPlayer.id).get

    Then("the stock should be connected with the bottom (selected) building")
    assert(stockBuilding.stock.master == bottomBuilding.stock)
  }
}
