package de.hetzge.sgame.base

import de.hetzge.sgame.base.helper.Continue
import org.scalatest._

class AreaTest extends FunSuite with GivenWhenThen {

  test("pathfinder with weight") {
    val steps = PathFinder.searchWithWeight(
      start = 6.toInt,
      next = (it: Int) => {
        if (it == 0) {
          Seq(10).map(i => (1, i))
        } else {
          Seq(it - 1, it + 1).map(i => (i, i))
        }
      },
      predicate = (it: Int) => it == 10
    )

    assert(steps.isDefined)
    assert(steps.get == Seq(6, 5, 4, 3, 2, 1, 0, 10))
  }

  test("traverse max steps") {
    val count = 123
    var counter = 0
    helper.traverse(start = 1, maxStepCount = count)(i => Seq(i + 1, i + 2)) { i =>
      counter += 1
      Continue
    }
    assert(counter == count)
  }

}
