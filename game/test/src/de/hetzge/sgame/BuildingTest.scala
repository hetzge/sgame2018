package de.hetzge.sgame.building

import de.hetzge.sgame.TestUtils
import de.hetzge.sgame.area._
import de.hetzge.sgame.person._
import de.hetzge.sgame.player.GaiaPlayer
import org.scalatest._

class BuildingTest extends FunSuite with GivenWhenThen {

  test("enter building when not reserved") {
    val (area, building, person) = createAreaAndBuildingAndPerson
    assertThrows[Exception](building.enter(person))
  }

  test("enter building when reserved") {
    Given("a area, building and person")
    val (area, building, person) = createAreaAndBuildingAndPerson

    When("the person is reserved in the building")
    building.reserve(person)

    Then("it should be contained in the buildings persons")
    assert(building.persons.contains(person))

    Then("it should know that it is reserved for the building")
    assert(person.isReservedInBuilding)
    assert(person.isReservedInBuilding(building))

    When("the person enters the building")
    building.enter(person)

    Then("it should be contained in the buildings persons")
    assert(building.persons.contains(person))

    Then("it should know that it is in the building")
    assert(person.isInBuilding)
    assert(person.isInBuilding(building))
  }

  test("leave building completely") {
    Given("a area and a building with a person")
    val (area, building, person) = createAreaAndBuildingWithPerson

    When("the person leaves the building")
    person.leaveBuilding(keepReservation = false)

    Then("the building should no longer contains the person")
    assert(!building.persons.contains(person))

    Then("the person should no longer be in the building")
    assert(!person.isInBuilding)
    assert(!person.isInBuilding(building))

    Then("the person should no longer be reserved in the building")
    assert(!person.isReservedInBuilding)
    assert(!person.isReservedInBuilding(building))

    Then("the person should be at the buildings door position")
    assert(person.position == building.doorPosition)
    assert(area.persons(building.doorPosition).contains(person))
  }

  test("leave building but keep reservation") {
    Given("a area and a building with a person")
    val (area, building, person) = createAreaAndBuildingWithPerson

    When("the person leaves the building")
    person.leaveBuilding(keepReservation = true)

    Then("the building should contains the person (because it still is reservated)")
    assert(building.persons.contains(person))

    Then("the person should no longer be in the building")
    assert(!person.isInBuilding)
    assert(!person.isInBuilding(building))

    Then("the person should be reserved in the building")
    assert(person.isReservedInBuilding)
    assert(person.isReservedInBuilding(building))

    Then("the person should be at the buildings door position")
    assert(person.position == building.doorPosition)
    assert(area.persons(building.doorPosition).contains(person))
  }

  test("leave building when not inside it") {
    val (area, building, person) = createAreaAndBuildingAndPerson
    assertThrows[Exception](building.leave(person))
  }

  private def createAreaAndBuildingAndPerson: (Area, Building, Person) = {
    val area     = TestUtils.createArea(10, 10)
    val building = area.buildBuilding(area.position(1, 1), BuildingType.Main, GaiaPlayer.id).get
    val person   = area.buildPerson(building.doorPosition, PersonType.Carrier, GaiaPlayer.id).get
    (area, building, person)
  }

  private def createAreaAndBuildingWithPerson: (Area, Building, Person) = {
    val area     = TestUtils.createArea(10, 10)
    val building = area.buildBuilding(area.position(1, 1), BuildingType.Main, GaiaPlayer.id).get
    val person   = building.buildPerson(PersonType.Carrier).get
    (area, building, person)
  }

}
