package de.hetzge.sgame.fogofwar

import de.hetzge.sgame.base._
import de.hetzge.sgame.unit._
import org.scalatest._

class FogOfWarTest extends FunSuite with GivenWhenThen {

  test("show") {
    val fogOfWar = createFogOfWar()
    fogOfWar.show(fogOfWar.position(5, 5), 3)
    fogOfWar.process()

    val visiblePositions = fogOfWar.areaPositions(fogOfWar.position(5, 5), 3)
    assert(visiblePositions.forall(fogOfWar.isVisible(_)))
    val hiddenPositions = fogOfWar.areaPositions().filter(!visiblePositions.contains(_))
    assert(hiddenPositions.forall(fogOfWar.isHidden(_)))
  }

  test("hide") {
    val fogOfWar = createFogOfWar()
    fogOfWar.show(fogOfWar.position(5, 5), 3)
    fogOfWar.process()

    fogOfWar.hide(fogOfWar.position(5, 5), 3)
    fogOfWar.process()

    Thread.sleep(200)

    assert(fogOfWar.areaPositions().forall(fogOfWar.isHidden(_)))
  }

  private def createFogOfWar(): FogOfWar = {
    new FogOfWar(new Tiled {
      override def width  = 10
      override def height = 10
    })
  }
}
