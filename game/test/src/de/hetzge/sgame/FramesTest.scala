package de.hetzge.sgame.frames

import org.scalatest._

class FramesTest extends FunSuite with GivenWhenThen {

  test("FramesIterator (range)") {
    val fromFrameId = FrameId(0)
    val toFrameId   = FrameId(20)
    val iterator    = new FramesIterator(new Frame(fromFrameId), toFrameId)
    val frameIds    = iterator.map(_.id).toList

    assert(frameIds.head == fromFrameId)
    assert(frameIds.size == toFrameId.value + 1)
    assert(frameIds.last == toFrameId)
  }

  test("FramesIterator backward (empty)") {
    val fromFrameId = FrameId(1)
    val toFrameId   = FrameId(0)
    val iterator    = new FramesIterator(new Frame(fromFrameId), toFrameId)
    val frameIds    = iterator.map(_.id).toList

    assert(frameIds.isEmpty)
  }

  test("FramesIterator single (self)") {
    val fromFrameId = FrameId(1)
    val toFrameId   = FrameId(1)
    val iterator    = new FramesIterator(new Frame(fromFrameId), toFrameId)
    val frameIds    = iterator.map(_.id).toList

    assert(frameIds.size == 1)
    assert(frameIds(0) == fromFrameId)
    assert(frameIds(0) == toFrameId)
  }
}
