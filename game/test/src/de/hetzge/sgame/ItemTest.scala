package de.hetzge.sgame.item

import de.hetzge.sgame.TestUtils
import de.hetzge.sgame.unit._
import de.hetzge.sgame.stock._
import de.hetzge.sgame.building._
import de.hetzge.sgame.player._
import de.hetzge.sgame.way._
import de.hetzge.sgame.settings._
import org.scalatest._

class ItemTest extends FunSuite with GivenWhenThen {

  test("economy") {

    Given("a area")
    val area = TestUtils.createArea(10, 10)
    area.buildWay(area.position(3, 3), area.position(8, 3), DefaultWayType, GaiaPlayer.id)
    area.buildWaypoint(area.position(3, 3))
    area.buildWaypoint(area.position(5, 3))
    area.buildWaypoint(area.position(8, 3))

    Given("with three stocks")
    val stockA = new SingleStock(MixedStockType())
    stockA.assignEntity(area.waypoint(area.position(3, 3)).get)

    val stockB = new SingleStock(ItemTypeStockType(WoodItemType))
    stockB.assignEntity(area.waypoint(area.position(5, 3)).get)

    val stockC = new SingleStock(MixedStockType())
    stockC.assignEntity(area.waypoint(area.position(8, 3)).get)

    Then("the stocks should initial behave as expected")
    assert(stockA.suppliedItems.isEmpty)
    assert(stockA.noItem(WoodItemType).isDefined)
    assert(stockA.noItem(StoneItemType).isDefined)
    assert(stockB.noItem(StoneItemType).isEmpty)
    assert(stockB.noItem(WoodItemType).isDefined)

    When("the first stock is filled")
    for (i <- 0 until stockA.stockType.maxItemCount) {
      for (noItem <- stockA.noItem(WoodItemType)) {
        noItem.supply(area.economy, WoodItemType)
      }
    }

    Then("the stock should provide no 'NoItem'")
    assert(stockA.noItem(WoodItemType).isEmpty)

    Then("supplies should be registered in economy")
    assert(area.economy._supplies.size == stockA.stockType.maxItemCount)

    When("the third stock is demand all items")
    for (i <- 0 until stockC.stockType.maxItemCount) {
      for (noItem <- stockC.noItem(WoodItemType)) {
        noItem.demand(area.economy, WoodItemType)
      }
    }

    Then("the stock should provide no 'NoItem'")
    assert(stockC.noItem(WoodItemType).isEmpty)

    Then("demands should be registered in economy")
    assert(area.economy._demands.size == stockC.stockType.maxItemCount)

    When("economy is updated")
    area.economy.update()

    Then("all items in first stock should be supplied")
    assert(stockA.suppliedItems.size == stockA.stockType.maxItemCount)

    Then("the target of the supplied items should be the other stock")
    assert(stockA.suppliedItems.forall(_.target == stockC))

    Then("economy should contain no more supply / demand items")
    assert(area.economy._demands.isEmpty)
    assert(area.economy._supplies.isEmpty)

    Then("transfer should be mapped in economy")
    assert(area.economy._suppliedByDemanded.size == stockA.stockType.maxItemCount)
    assert(area.economy._demandedBySupplied.size == stockC.stockType.maxItemCount)

    When("the items are transfered from first to second stock")
    for (suppliedItem <- stockA.suppliedItems) {
      suppliedItem.transfer(area.economy, stockB.noItem(WoodItemType).get)
    }

    Then("the second stock should contain the items")
    assert(stockB.suppliedItems.size == stockA.stockType.maxItemCount)

    When("transfers are finished")
    for (suppliedItem <- stockB.suppliedItems) {
      suppliedItem.finish(area.economy)
    }

    Then("transfers should be empty in economy")
    assert(area.economy._suppliedByDemanded.isEmpty)
    assert(area.economy._demandedBySupplied.isEmpty)

    Then("first stock should provide 'NoItem'")
    assert(stockA.noItem(WoodItemType).isDefined)
  }

  test("economy stock building") {
    Given("a area with stock building and a way with waypoints starting at stock building")
    val game = TestUtils.createGame(10, 10)
    val area = game.area

    val testBuilding = area.buildBuilding(area.position(2, 0), BuildingType.Test(doorY = 1), GaiaPlayer.id).get
    game.localSettings.setSelection(BuildingSelection(testBuilding))

    area.buildWay(area.position(2, 1), area.position(2, 5), DefaultWayType, GaiaPlayer.id)
    area.buildWaypoint(area.position(2, 1))
    area.buildWaypoint(area.position(2, 5))

    val building = area.buildBuilding(area.position(1, 0), BuildingType.Stock(WoodItemType), GaiaPlayer.id).get

    assert(area.building(area.position(1, 0)).isDefined)
    assert(area.waypoint(area.position(2, 1)).isDefined)
    assert(area.waypoint(area.position(2, 5)).isDefined)

    When("wood is demanded by stock buildings stock")
    for (noItem <- building.stock.noItem(WoodItemType)) {
      noItem.demand(area.economy)
    }

    When("wood is supplied by waypoint at the other end of the way")
    val waypoint = area.waypoint(area.position(2, 5)).get
    for (noItem <- waypoint.stock.noItem(WoodItemType)) {
      noItem.supply(area.economy, WoodItemType)
    }

    When("the economy is updated and orders are finished")
    area.economy.update()
    assert(!area.economy._suppliedByDemanded.values.isEmpty)
    for (suppliedItem <- area.economy._suppliedByDemanded.values) {
      suppliedItem.finish(area.economy)
    }

    Then("the stock building should contain the blocked wood item")
    assert(building.stock.blockedItem(WoodItemType).isDefined)

    Then("the stock buildings waypoint shouldn't contain the wood item")
    assert(building.stockWaypoint.get.stock.blockedItem(WoodItemType).isEmpty)
    assert(building.stockWaypoint.get.stock.suppliedItems.isEmpty)
  }

  test("update economy if stock is destroyed") {
    Given("a area with a stock building")
    val area    = TestUtils.createArea(10, 10)
    val economy = area.economy

    area.buildWay(area.position(1, 1), area.position(5, 1), DefaultWayType, GaiaPlayer.id)
    val waypoint      = area.buildWaypoint(area.position(1, 1)).get
    val otherWaypoint = area.buildWaypoint(area.position(5, 1)).get

    assert(waypoint.getGroup == otherWaypoint.getGroup)
    assert(economy._demands.size == 0)
    assert(economy._supplies.size == 0)

    Given("different items with different states are added to the stock")
    waypoint.stock.demand(economy, Seq(WoodItemType, WoodItemType))
    waypoint.stock.block(Seq(WoodItemType))
    waypoint.stock.supply(economy, Seq(WoodItemType, WoodItemType))

    otherWaypoint.stock.demand(economy, Seq(WoodItemType))
    otherWaypoint.stock.supply(economy, Seq(WoodItemType))

    assert(economy._demands.size == 3)
    assert(economy._supplies.size == 3)

    When("the economy is updated")
    economy.update()

    Then("the economy and stocks should be in a expected state")
    assert(waypoint.stock.demandItems.size == 1)
    assert(waypoint.stock.demandedItems.size == 1)
    assert(waypoint.stock.supplyItems.size == 1)
    assert(waypoint.stock.suppliedItems.size == 1)
    assert(waypoint.stock.blockedItems.size == 1)

    assert(otherWaypoint.stock.demandItems.size == 0)
    assert(otherWaypoint.stock.demandedItems.size == 1)
    assert(otherWaypoint.stock.supplyItems.size == 0)
    assert(otherWaypoint.stock.suppliedItems.size == 1)
    assert(otherWaypoint.stock.blockedItems.size == 0)

    assert(economy._demandedBySupplied.size == 2)
    assert(economy._suppliedByDemanded.size == 2)
    assert(economy._demands.size == 1)
    assert(economy._supplies.size == 1)

    When("the waypoint is destroyed")
    area.destroyWaypoint(area.position(1, 1))

    Then("the waypoint should not be longer at the area")
    assert(!area.isWaypoint(waypoint.position))

    Then("the active orders in the stock should be canceled")
    assert(waypoint.stock.demandItems.size == 2)
    assert(waypoint.stock.demandedItems.size == 0)
    assert(waypoint.stock.supplyItems.size == 2)
    assert(waypoint.stock.suppliedItems.size == 0)
    assert(waypoint.stock.blockedItems.size == 1)

    assert(otherWaypoint.stock.demandItems.size == 0)
    assert(otherWaypoint.stock.demandedItems.size == 1)
    assert(otherWaypoint.stock.supplyItems.size == 0)
    assert(otherWaypoint.stock.suppliedItems.size == 1)
    assert(otherWaypoint.stock.blockedItems.size == 0)

    Then("the demands/supplies should be removed from economy")
    assert(economy._demandedBySupplied.size == 0)
    assert(economy._suppliedByDemanded.size == 0)
    // the demand/supply of the other waypoint
    assert(economy._demands.size == 1)
    assert(economy._supplies.size == 1)
  }
}
