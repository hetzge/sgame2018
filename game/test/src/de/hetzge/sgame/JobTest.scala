package de.hetzge.sgame.job

import de.hetzge.sgame.TestUtils
import de.hetzge.sgame.base.helper._
import de.hetzge.sgame.building._
import de.hetzge.sgame.environment._
import de.hetzge.sgame.game._
import de.hetzge.sgame.item._
import de.hetzge.sgame.person._
import de.hetzge.sgame.player._
import de.hetzge.sgame.stock._
import de.hetzge.sgame.tile._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.way._
import de.hetzge.sgame.settings._
import org.scalatest._

class JobTest extends FunSuite with GivenWhenThen {

  test("drop and supply") {
    val stockEntityA = new StockEntity {
      override val stock = new SingleStock(ItemTypeStockType(WoodItemType))
      stock.assignEntity(this)

      override val stockWay      = None
      override val stockWaypoint = None
    }
    val stockEntityB = new StockEntity {
      override val stock = new SingleStock(ItemTypeStockType(WoodItemType))
      stock.assignEntity(this)

      override val stockWay      = None
      override val stockWaypoint = None
    }

    stockEntityA.stock.noItem(WoodItemType).get.block(WoodItemType)
    stockEntityA.stock.noItem(WoodItemType).get.block(WoodItemType)
    stockEntityA.stock.noItem(WoodItemType).get.block(WoodItemType)

    JobUtils.dropAndSupply(new ItemEconomy, stockEntityA.stock, stockEntityB.stock)

    assert(stockEntityA.stock.blockedItems.isEmpty)
    assert(stockEntityB.stock.supplyItems.size == 3)
  }

  test("CarrierJob") {
    println("start ---------------------")

    Given("a game")
    val game = Game(EntityKey("TEST_GAME"), TestGameType, None, GameConfiguration(playerCount = 1), TestUtils.createArea(10, 10))

    Given("a area with a way with waypoints at the ends")
    val area            = game.area
    val startPosition   = area.position(0, 5)
    val endPosition     = area.position(9, 5)
    val betweenPosition = startPosition.between(endPosition)
    area.buildWay(startPosition, endPosition, DefaultWayType, GaiaPlayer.id)

    val startWaypoint = area.buildWaypoint(startPosition).get
    val endWaypoint   = area.buildWaypoint(endPosition).get

    Given("a item supplied by one and demanded by ")
    startWaypoint.stock.noItem(WoodItemType).get.supply(area.economy, WoodItemType, ItemUsage.Default)
    endWaypoint.stock.noItem(WoodItemType).get.demand(area.economy, WoodItemType, ItemUsage.Default)

    Then("the waypoints should contain supply and demand")
    assert(startWaypoint.stock.supplyItem.isDefined)
    assert(endWaypoint.stock.demandItem.isDefined)

    Then("a carrier job should exist between the waypoints")
    assert(area.isJob(betweenPosition, classOf[CarrierJob]))

    Given("a carrier person")
    val person = area.buildPerson(betweenPosition, PersonType.Carrier, GaiaPlayer.id).get

    When("frames passing by")
    for (i <- 0 until 1000) {
      game.update()
    }

    println("done ----------")

    Then("the item should be moved from one to other waypoint")
    assert(endWaypoint.stock.demandItems.size == 0)
    assert(endWaypoint.stock.demandedItems.size == 0)
    assert(endWaypoint.stock.blockedItem.exists(item => item.itemType == WoodItemType))
  }

  test("FarmerJob") {
    Given("a game")
    val game = TestUtils.createGame(100, 100)

    Given("a area with a farm building")
    val area     = game.area
    val building = area.buildBuilding(area.position(0, 0), BuildingType.CropFarm, GaiaPlayer.id).get
    game.localSettings.setSelection(BuildingSelection(building))

    assert(area.isWay(building.doorPosition))
    assert(area.tileMap.layers.last.get(building.doorPosition).tileType == TileType.DefaultWay)
    assert(area.tileMap.topTileType(building.doorPosition) == TileType.DefaultWay)

    When("frames passing by")
    for (i <- 0 until 70000) {
      game.update()
    }

    Then("crop should be planted")
    val cropEnvironmentCount = area.areaPositions().count(area.environment.has(_, EnvironmentType.Crop))
    assert(cropEnvironmentCount == FarmerJob.workDistance - building.width - building.height)

    When("stocks are added to the building")
    val stockPosition     = building.position.relative(building.width, 0).get
    val stockBuildingType = BuildingType.Stock(CropItemType)
    assert(area.canBuildBuilding(stockPosition, stockBuildingType, GaiaPlayer.id))
    val stockBuilding = area.buildBuilding(stockPosition, stockBuildingType, GaiaPlayer.id).get

    When("frames passing by")
    for (i <- 0 until 10000) {
      game.update()
    }

    Then("the stock should be full of crop")
    stockBuilding.stock.suppliedItems.filter(_.itemType == CropItemType).size == 8
  }

  test("LumberjackJob") {
    Given("a game")
    val game = TestUtils.createGame(100, 100)

    Given("a area with a lumberjack building")
    val area     = game.area
    val building = area.buildBuilding(area.position(0, 0), BuildingType.Lumberjack, GaiaPlayer.id).get
    game.localSettings.setSelection(BuildingSelection(building))

    Given("stock buildings around the lumberjack")
    val stockBuildingType = BuildingType.Stock(WoodItemType)
    val stockBuildings =
      for (aroundPosition <- building.around(area) if area.canBuildBuilding(aroundPosition, stockBuildingType, GaiaPlayer.id))
        yield area.buildBuilding(aroundPosition, stockBuildingType, GaiaPlayer.id).get

    Then("there should be stock buildings")
    assert(building.stock.noItems(WoodItemType).size == 48)
    assert(stockBuildings.size == building.width + building.height)
    assert(!stockBuildings.isEmpty)

    Given("a forest")
    area.traverse(building.doorPosition, EnvironmentWorkerJob.workDistance) { position =>
      if (position == building.doorPosition) {
        Continue
      } else if (area.isEmpty(position)) {
        area.environment.set(position, EnvironmentType.Forest, count = 2, level = 1.0f)
        Continue
      } else {
        DontGoDeeper
      }
    }

    Then("there should be a forest")
    val woodItemCount = area.areaPositions().map(area.environment.itemCount(_, WoodItemType)).sum
    assert(woodItemCount > 0)

    When("frames passing by")
    for (i <- 0 until 100000) {
      game.update()
    }

    Then("the stocks should be full of wood")
    assert(stockBuildings.flatMap(_.stock.supplyItems).size == stockBuildings.size * 8)

    Then("there should be less forest")
    assert(area.areaPositions().map(area.environment.itemCount(_, WoodItemType)).sum == woodItemCount - stockBuildings.size * 8)
  }
}
