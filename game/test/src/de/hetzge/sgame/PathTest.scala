package de.hetzge.sgame.path

import de.hetzge.sgame.base._
import de.hetzge.sgame.unit._

import org.scalatest._
import org.scalatest.Matchers._

class PathTest extends FunSuite with GivenWhenThen {

  test("optimize path 1/1 -> 1/3 -> 1/3") {
    Given("a path 1/1 -> 1/3 -> 1/3 at a 10x10 tiled")
    implicit val tiled = new Tiled {
      override def width  = 10
      override def height = 10
    }
    val path = Path(List(tiled.position(1, 1), tiled.position(1, 3), tiled.position(1, 3)))

    When("the path is optimized")
    val optimizedPath = path.optimize

    Then("the path should be 1/1 -> 1/3")
    assert(optimizedPath.size == 2)
    assert(optimizedPath.head.get == tiled.position(1, 1) && optimizedPath.last.get == tiled.position(1, 3))
  }
}
