package de.hetzge.sgame.person

import java.util.concurrent.TimeUnit

import de.hetzge.sgame.TestUtils
import org.scalatest._
import org.scalatest.Matchers._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.building._
import de.hetzge.sgame.frames._
import de.hetzge.sgame.game._
import de.hetzge.sgame.job._
import de.hetzge.sgame.path._
import de.hetzge.sgame.player.GaiaPlayer
import de.hetzge.sgame.way.DefaultWayType

import scala.collection.mutable
import scala.concurrent.duration.Duration
import de.hetzge.sgame.export.ExportedIndex

class PersonTest extends FunSuite with GivenWhenThen {

  test("goto door from inside building") {
    Given("a area with a person in a building")
    val area     = TestUtils.createGame(10, 10).area
    val building = area.buildBuilding(area.position(0, 0), BuildingType.Main, GaiaPlayer.id).get
    val person   = building.buildPerson(PersonType.Carrier).get
    assert(person.isInBuilding)

    When("the person should goto door")
    person.leaveBuilding()
    val isGoto = person.goto(building.doorPosition)

    Then("goto should be true")
    assert(isGoto)
  }

  test("goto with way") {
    val area = TestUtils.createGame(10, 10).area
    val game = Game(EntityKey("TestGame"), TestGameType, None, GameConfiguration(playerCount = 1))

    area.buildWay(area.position(3, 0), area.position(3, 9), DefaultWayType, GaiaPlayer.id)
    area.buildWay(area.position(5, 0), area.position(5, 9), DefaultWayType, GaiaPlayer.id)
    area.buildWaypoint(area.position(3, 0))
    area.buildWaypoint(area.position(3, 8))
    area.buildWaypoint(area.position(5, 0))
    area.buildWaypoint(area.position(5, 8))

    val person = area.buildPerson(area.position(1, 1), PersonType.Carrier, GaiaPlayer.id).get

    person.goto(area.position(5, 9))
    assert(!person.getPath.steps.isEmpty)
    assert(person.getPath.last == Some(area.position(5, 9)))
  }

  test("find path to neighbor waypoints") {
    Given("a cross of ways with waypoints at the ends")
    val waypointCoordinates = List((1, 0), (1, 2), (0, 1), (2, 1))
    val game                = TestUtils.createGame(10, 10)
    val area                = game.area
    area.buildWay(area.position(1, 1), DefaultWayType, GaiaPlayer.id)
    waypointCoordinates.foreach {
      case (x, y) => {
        area.buildWay(area.position(x, y), DefaultWayType, GaiaPlayer.id)
        area.buildWaypoint(area.position(x, y))
      }
    }

    val person = area.buildPerson(area.position(1, 1), PersonType.Carrier, GaiaPlayer.id).get

    waypointCoordinates.foreach {
      case (x, y) => {
        When(s"goto end ($x, $y) of the cross")
        val path = person.pathTo(area.position(x, y)).map(_.optimize)
        Then(s"person should have path with start (1, 1) and end ($x, $y)")
        val start = area.position(1, 1)
        val end   = area.position(x, y)
        println(s"path: $path")
        path should matchPattern {
          case Some(Path(Seq(start, end))) =>
        }
      }
    }
  }

  test("go away from building") {
    Given("a person that directly stands at a building")
    val game = TestUtils.createGame(10, 10)
    val area = game.area
    area.buildBuilding(area.position(1, 1), BuildingType.Lumberjack, GaiaPlayer.id)

    val personX        = 1
    val personY        = 1 + BuildingType.Lumberjack.height
    val personPosition = area.position(personX, personY)
    val person         = area.buildPerson(personPosition, PersonType.Carrier, GaiaPlayer.id).get

    When("the person is set to go away")
    val targetX        = personX
    val targetY        = personY + 3
    val targetPosition = area.position(targetX, targetY)
    person.goto(targetPosition)

    Then("it should has a path go away")
    assert(person.getPath.head == Some(targetPosition))
    assert(person.getPath.last == Some(targetPosition))
    assert(person.getPath.size == 1)
  }

  test(s"trigger '${Trigger.ReachedEndOfPath}' triggers when move") {
    Given("a area with a person")
    val area = TestUtils.createGame(10, 10).area
    val job  = new TestJob()
    val personType = PersonType.Test(
      createJob = job
    )
    val person = area.buildPerson(area.position(3, 3), personType, GaiaPlayer.id).get

    When("a path is set with only the current position")
    person.setPath(Path(List(area.position(3, 3))))

    Then(s"job should be triggered with ${Trigger.ReachedEndOfPath}")
    assert(job.triggers.collect { case Trigger.ReachedEndOfPath => }.size == 1)
  }

  test(s"move triggers '${Trigger.ReachedStep}'") {
    Given("a area with a person")
    val game = TestUtils.createGame(10, 10)
    val area = game.area
    val job  = new TestJob()
    val personType = PersonType.Test(
      createJob = job
    )
    val startX        = 3
    val startY        = 3
    val startPosition = area.position(startX, startY)
    val person        = area.buildPerson(startPosition, personType, GaiaPlayer.id).get

    When("the person should move")
    val distance       = 5
    val targetX        = startX + distance
    val targetY        = startY
    val targetPosition = area.position(targetX, targetY)
    person.setPath(Path(List(targetPosition)))

    When("frames pass by")
    // the movement is delayed because setPath is called after ki is executed
    val frameDelay = JobEntity.defaultJobFrameDelay.frames.toInt
    val frameCount = frameDelay + (distance * FrameDuration(Duration(1f / person.personType.speed, TimeUnit.SECONDS)).frames.toInt)
    for (i <- 0 until frameCount) {
      game.update()
    }

    Then(s"the person should trigger '${Trigger.ReachedStep}' triggers")
    assert(job.triggers.collect { case Trigger.ReachedEndOfPath => }.size == 1)
    assert(job.triggers.collect { case Trigger.ReachedStep      => }.size == distance)
  }

  test(s"set empty path should not trigger '${Trigger.ReachedEndOfPath}'") {
    Given("a area with a person")
    val area   = TestUtils.createGame(10, 10).area
    val job    = new TestJob()
    val person = area.buildPerson(area.position(3, 3), PersonType.Carrier, GaiaPlayer.id).get
    person.setJob(job)

    When("setPath is called with a empty path")
    person.setPath(Path())

    Then(s"'${Trigger.ReachedEndOfPath}' should have been triggered")
    assert(job.triggers.isEmpty)
  }

  private final class TestJob extends Job[Person] {
    val triggers = mutable.Queue[Trigger]()
    override def trigger(person: Person, trigger: Trigger) = {
      triggers.enqueue(trigger)
    }
    def export()(implicit index: ExportedIndex) = ???
  }
}
