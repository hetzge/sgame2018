package de.hetzge.sgame.stock

import de.hetzge.sgame.item._
import de.hetzge.sgame.way._
import de.hetzge.sgame.waypoint._

import org.scalatest._

class StockTest extends FunSuite with GivenWhenThen {

  test("remove item from stock") {
    Given("a stock")
    val economy   = new ItemEconomy()
    val stockType = ItemTypeStockType(WoodItemType)
    val stock     = createStock(stockType)

    Then("it should be filled with no items")
    assert(stock.noItems.size == stockType.maxItemCount)

    When("all items are demand")
    for (noItem <- stock.noItems) {
      noItem.demand(economy, WoodItemType)
    }

    Then("it should be filled with demand items")
    assert(stock.demandItems.size == stockType.maxItemCount)

    When("a demand item is removed")
    stock.remove(stock.demandItem.get)

    Then("the stock should be refilled with no items")
    assert(stock.demandItems.size == stockType.maxItemCount - 1)
    assert(stock.noItems.size == 1)
  }

  private def createStock(stockType: StockType): Stock =
    new StockEntity {
      override val stock: Stock = new SingleStock(stockType)
      stock.assignEntity(this)

      override def stockWay: Option[Way]           = None
      override def stockWaypoint: Option[Waypoint] = None
    }.stock
}
