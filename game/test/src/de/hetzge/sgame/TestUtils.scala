package de.hetzge.sgame

import de.hetzge.sgame.area._
import de.hetzge.sgame.game._
import de.hetzge.sgame.tile._
import de.hetzge.sgame.unit._
import de.hetzge.sgame.player.GaiaPlayer

object TestUtils {

  def createGame(width: TileInt, height: TileInt): Game = {
    val game = Game(EntityKey("TEST_GAME"), TestGameType, None, GameConfiguration(playerCount = 1), createArea(width, height))
    game.localSettings.setPlayer(GaiaPlayer)
    game
  }

  def createArea(width: TileInt, height: TileInt): Area = {
    val tileMap = new TileMap(width, height, TileSet.FAKE)
    tileMap.getOrCreateLayer("test123").fill(new Tile(TileType.Grass, renderable = null, collision = false))
    Area(Position(), tileMap)
  }
}
