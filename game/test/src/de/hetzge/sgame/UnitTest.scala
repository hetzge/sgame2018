package de.hetzge.sgame.unit

import org.scalatest._

class UnitTest extends FunSuite with GivenWhenThen {
  test("position x y width height") {
    val position = Position(1, 2, 3, 4)
    assert(position.x == TileInt(1))
    assert(position.y == TileInt(2))
    assert(position.width == TileInt(3))
    assert(position.height == TileInt(4))

    val position2 = Position(0, 0, 1, 1)
    assert(position2.x == TileInt(0))
    assert(position2.y == TileInt(0))
    assert(position2.width == TileInt(1))
    assert(position2.height == TileInt(1))

    val position3 = Position(1234, 5432, 9999, 9900)
    assert(position3.x == TileInt(1234))
    assert(position3.y == TileInt(5432))
    assert(position3.width == TileInt(9999))
    assert(position3.height == TileInt(9900))
  }

  test("position between both ways") {
    val positionA = Position(2, 3, 10, 10)
    val positionB = Position(5, 6, 10, 10)

    assert(positionA.between(positionB) == positionB.between(positionA))
  }
}
