package de.hetzge.sgame.waypoint

import de.hetzge.sgame.unit._
import de.hetzge.sgame.tile._
import de.hetzge.sgame.area._
import org.scalatest._
import org.scalatest.Matchers._

class WaypointTest extends FunSuite with GivenWhenThen {

  test("find path to neighbor waypoint") {
    Given("A path of two connected waypoints")
    val area      = Area(Position(), new TileMap(10, 10, new SimpleTileSet(Seq.empty)))
    val waypointA = Waypoint(DefaultWaypointType, area.position(1, 1))
    waypointA.assignArea(area)
    val waypointB = Waypoint(DefaultWaypointType, area.position(3, 1))
    waypointB.assignArea(area)
    waypointA.setConnections(List(waypointB))
    waypointB.setConnections(List(waypointA))

    When("path to other waypoint is requested")
    val path = waypointA.findPath(waypointB)

    Then("should find a path")
    path should matchPattern {
      case Some(Seq(waypointA, waypointB)) =>
    }
  }

}
